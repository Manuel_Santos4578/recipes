using CronService.Jobs;
using Quartz;
using Quartz.AspNetCore;
using Serilog;
using Log = Serilog.Log;

const string log_template = "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{@Variables}{NewLine} {Exception}";

string log_directory_path = Path.Combine(AppContext.BaseDirectory, "Logs");
if (!Directory.Exists(log_directory_path))
{
    Directory.CreateDirectory(log_directory_path);
}

var builder = WebApplication.CreateBuilder(args);

Log.Logger = new LoggerConfiguration()
       .ReadFrom.Configuration(new ConfigurationBuilder().AddJsonFile("appsettings.json").Build())
       .WriteTo.Console()
       .WriteTo.File(log_directory_path, outputTemplate: log_template, rollingInterval: RollingInterval.Day)
       .CreateLogger();
Log.Information(messageTemplate: "Application starting");
try
{
    builder.Services
        .AddQuartz(options =>
        {
            options.SetupCron<UnblockUserJob>(cronString: "* */30 * ? * *");
        })
        .AddQuartzServer(quartz =>
        {
            quartz.WaitForJobsToComplete = true;
            quartz.AwaitApplicationStarted = true;
            quartz.StartDelay = TimeSpan.FromSeconds(5);
        });

    var app = builder.Build();

    app.UseHttpsRedirection();

    app.Run();
}
catch (Exception ex)
{
    Log.Fatal(ex, "Application failed to start");
}
finally
{
    Log.CloseAndFlush();

}