﻿
document.addEventListener('DOMContentLoaded', () => {
    let username_input: HTMLElement = document.getElementById("Username");
    username_input.addEventListener('input', () => {
        document.getElementById('username_validation').innerHTML = null;
    })

    let password_input: HTMLElement = document.getElementById("Password");
    password_input.addEventListener('input', () => {
        document.getElementById('password_validation').innerHTML = null;
    })
})