﻿namespace DataContracts.Entities;

public interface IRecipeUser
{
    public int RecipeId { get; set; }
    public bool IsCreator { get; set; }
    public IUser User { get; set; }
}