﻿let options = {
    backdrop: "static",
    keyboard: false,
}
let controller = location.href.split('/')[3].replace(/\?.*$/, '');

async function ShowModal(url, modal) {
    modal.innerHTML = await (await fetch(url)).text();
    new bootstrap.Modal(modal, options).show()
}

function AddCreateEvent() {
    let add_button = document.getElementsByClassName('add')[0];
    if (add_button) {
        add_button.addEventListener('click', async function () {
            let url = location.origin + `/${controller}/create`;
            let base_slug = this.getAttribute('data-base-slug');
            if (base_slug) url += `?base_slug=${base_slug}`;
            let modal = document.getElementById('modal');
            ShowModal(url, modal)
        })
    }
}


function AddEditEvent() {
    let edit_buttons = document.getElementsByClassName('edit');
    for (const edit_button of edit_buttons) {
        edit_button.addEventListener('click', async function () {
            let slug = this.getAttribute('data-slug');
            let url = `${location.origin}/${controller}/edit/${slug}`;
            let modal = document.getElementById('modal');
            ShowModal(url, modal)
        })
    }
}
function AddDeleteEvent() {
    let delete_buttons = document.getElementsByClassName('delete');
    for (const delete_button of delete_buttons) {
        delete_button.addEventListener('click', async function () {
            let slug = this.getAttribute('data-slug');
            let url = `${location.origin}/${controller}/delete/${slug}`;
            let modal = document.getElementById('modal')
            ShowModal(url, modal)
        });
    }
}

AddCreateEvent();
AddEditEvent();
AddDeleteEvent();