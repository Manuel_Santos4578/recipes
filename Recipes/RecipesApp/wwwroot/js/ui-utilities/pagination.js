function AddPageSizeEvent() {
    document.getElementById('size').addEventListener('change', async () => {
        document.getElementById('page').value = 1;
        await LoadData();
    })
}

function AddPageEvent() {
    let index_page = document.getElementsByClassName('index_page');
    if (index_page.length !== 0) {
        for (let page_icon of index_page) {
            page_icon.addEventListener('click', async function (e) {
                e.preventDefault();
                let page = this.getAttribute('data-page');
                document.getElementById('page').value = page
                await LoadData();
            });
        }
    }
}