﻿using DataContracts.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ModelsLibrary.Models;
using Newtonsoft.Json.Linq;
using RecipesApp.Models;
using RecipesApp.Models.FormModels;
using RecipesApp.Validators.FormValidation;
using RecipesLibrary.Util;
using System.Linq;
using System.Threading.Tasks;
using Util;
using I18N = ModelsLibrary.Constants.AppConstants.I18N;

namespace RecipesApp.Controllers;

[Route("ingredients")]
[Authorize]
public sealed class IngredientController : MyBaseController
{
    public IngredientController(IConfiguration config, IWebHostEnvironment env) : base(config, env) { }

    [HttpGet]
    public async Task<IActionResult> IndexAsync()
    {
        await ApiAuthAsync();
        IngredientViewModel model = new IngredientViewModel
        {
            Countries = await dal.SelectAllCountriesAsync(),
        };
        HandleViewData();
        return View(model);
    }

    [HttpPost]
    public async Task<IActionResult> IndexAjaxAsync()
    {
        JToken body = await Request.Body.ReadObject<JToken>();
        //int region_id = body.region_id;
        int country_id = body.Value<int>("country_id");
        string name = body.Value<string>("name");
        int page = body.Value<int>("page");
        int size = body.Value<int>("size");
        await ApiAuthAsync();
        IngredientViewModel model = new IngredientViewModel
        {
            PageSize = size,
            CurrentPage = page,
            Ingredients = await dal.SelectAllIngredientsAsync(name, 0, country_id, page, size),
            Count = await dal.SelectIngredientCountAsync(name, 0, country_id)
        };
        return Ok(model);
    }

    [HttpGet("show/{friendly_url}")]
    public async Task<IActionResult> ShowAsync(string friendly_url)
    {
        await ApiAuthAsync();
        IngredientViewModel model = new IngredientViewModel
        {
            Ingredient = await dal.SelectIngredientBySlugAsync(friendly_url)
        };
        HandleViewData();
        return View(model);
    }

    [HttpGet("create")]
    public async Task<IActionResult> CreateAsync()
    {
        await ApiAuthAsync();
        IngredientFormModel model = new IngredientFormModel
        {
            Countries = await dal.SelectAllCountriesAsync()
        };
        return View(model);
    }

    [HttpPost("store")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> StoreAsync(IngredientFormModel model)
    {
        FluentValidation.Results.ValidationResult validation_result = await new IngredientFormModelValidator().ValidateAsync(model);
        if (!validation_result.IsValid)
        {
            TempData["Error"] = string.Join("; ", validation_result.Errors.Select(err => err.ErrorMessage));
            return Redirect(Request.Headers["Referer"]);
        }

        await ApiAuthAsync();
        model.Countries = await dal.SelectAllCountriesAsync();

        model.Name = model.Name.Sanitize();

        IIngredient ingredient = new Ingredient
        {
            Name = model.Name,
            FriendlyUrl = model.FriendlyUrl,
            Country = new Country
            {
                ID = model.CountryId
            }
        };
        ingredient = await dal.InsertIngredientAsync(ingredient);
        if (ingredient is not null)
        {
            TempData["Success"] = string.Format(translation[I18N.Successfully_created], ingredient.Name);
            return RedirectToAction("Index", "Ingredient"/*, new { friendly_url = ingredient.FriendlyUrl }*/);
        }
        TempData["Error"] = string.Format(translation[I18N.Failed_creation_of], ingredient.Name);
        return RedirectToAction("Index", "Ingredient");
    }

    [HttpGet("edit/{friendly_url}")]
    public async Task<IActionResult> EditAsync(string friendly_url)
    {
        if (string.IsNullOrWhiteSpace(friendly_url)) return BadRequest();
        await ApiAuthAsync();
        IIngredient ingredient = await dal.SelectIngredientBySlugAsync(friendly_url);
        if (ingredient is null) return NotFound();
        IngredientFormModel form_model = new IngredientFormModel(ingredient) { Countries = await dal.SelectAllCountriesAsync() };

        return View(form_model);
    }

    [HttpPost("update/{friendly_url}")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> UpdateAsync(IngredientFormModel model, string friendly_url)
    {
        FluentValidation.Results.ValidationResult validation_result = await new IngredientFormModelValidator().ValidateAsync(model);
        if (!validation_result.IsValid)
        {
            TempData["Error"] = string.Join("; ", validation_result.Errors.Select(err => err.ErrorMessage));
            return Redirect(Request.Headers["Referer"]);
        }

        await ApiAuthAsync();
        model.Name = model.Name.Sanitize();

        IIngredient ingredient = await dal.UpdateIngredientAsync(new Ingredient
        {
            Name = model.Name,
            FriendlyUrl = friendly_url,
            Country = new Country { ID = model.CountryId },
            ID = model.ID
        });
        if (ingredient is not null)
        {
            TempData["Success"] = string.Format(translation[I18N.Successfully_updated], ingredient.Name);
            return RedirectToAction("Show", "Ingredient", new { friendly_url = ingredient.FriendlyUrl });
        }
        TempData["Error"] = string.Format(translation[I18N.Failed_to_update], ingredient.Name);
        return RedirectToAction("Show", "Ingredient", new { friendly_url });
    }

    [HttpGet("delete/{friendly_url}")]
    public async Task<IActionResult> DeleteAsync(string friendly_url)
    {
        if (string.IsNullOrWhiteSpace(friendly_url)) return BadRequest();
        await ApiAuthAsync();
        IIngredient ingredient = await dal.SelectIngredientBySlugAsync(friendly_url);
        if (ingredient is null) return NotFound();
        IngredientFormModel form_model = new IngredientFormModel(ingredient);
        return View(form_model);
    }

    [HttpPost("destroy")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DestroyAsync(IngredientFormModel model)
    {
        Ingredient ingredient = new Ingredient
        {
            Name = model.Name,
            FriendlyUrl = model.FriendlyUrl,
            Country = new Country { ID = model.CountryId },
            ID = model.ID
        };
        await ApiAuthAsync();
        if (await dal.DeleteIngredientAsync(ingredient))
        {
            TempData["Success"] = string.Format(translation[I18N.Successfully_deleted], ingredient.Name);
            return RedirectToAction("Index", "Ingredient");
        }
        TempData["Error"] = string.Format(translation[I18N.Failed_to_delete], ingredient.Name);
        return RedirectToAction("Show", "Ingredient", new { friendly_url = model.FriendlyUrl });
    }
}
