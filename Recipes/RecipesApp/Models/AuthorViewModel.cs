﻿using DataContracts.Entities;
using ModelsLibrary.Models;
using System.Collections.Generic;
using System.Linq;

namespace RecipesApp.Models;

public sealed class AuthorViewModel : ViewModel
{
    public IEnumerable<IAuthor> Authors { get; set; }
    public IAuthor Author { get; set; }
    public IEnumerable<IRecipe> AuthorRecipes { get; set; }
    public AuthorViewModel()
    {
        Author = new Author();
        Authors = new List<Author>();
        AuthorRecipes = new List<Recipe>();
    }
}
