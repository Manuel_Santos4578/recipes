﻿namespace DataContracts.Entities;

public interface IIngredient : ISimple
{
    int ID { get; set; }
    string Name { get; set; }
    string FriendlyUrl { get; set; }
    ICountry Country { get; set; }
}
