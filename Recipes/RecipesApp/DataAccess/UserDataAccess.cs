﻿using DataContracts.Entities;
using ModelsLibrary.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Util;

namespace RecipesApp.DataAccess;

public sealed partial class DataAccess
{
    public async Task<IUser> LoginAsync(string username, string password)
    {
        Dictionary<string, string> values = new Dictionary<string, string>
        {
            {nameof(username), username },
            {nameof(password), password },
        };
        StringContent content = new StringContent(JsonConvert.SerializeObject(values));
        return await new ApiClientV3(ApiDomain, ApiToken).PostAsync<User>($"{usersEndpoint}/login",content);
    }
    public async Task ChangePreferedCultureAsync(string username, string culture)
    {
        Dictionary<string, string> values = new Dictionary<string, string>
        {
            {nameof(username), username },
            {nameof(culture), culture },
        };
        StringContent content = new StringContent(JsonConvert.SerializeObject(values));
        await new ApiClientV3(ApiDomain, ApiToken).PostAsync<Dictionary<string,string>>($"{usersEndpoint}/setculture", content);
    }
}
