﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace ModelsLibrary.JsonConverters;

internal class JsonConverter<T>: JsonConverter where T : class, new()
{
    public override bool CanWrite => false;
    public override bool CanRead => true;
    public override bool CanConvert(Type objectType)
    {
        return objectType == typeof(T);
    }
    public override void WriteJson(JsonWriter writer,
        object value, JsonSerializer serializer)
    {
        throw new InvalidOperationException("Use default serialization.");
    }

    public override object ReadJson(JsonReader reader,
        Type objectType, object existingValue,
        JsonSerializer serializer)
    {
        var jsonObject = JObject.Load(reader);
        T instance = new T();

        serializer.Populate(jsonObject.CreateReader(), instance);
        return instance;
    }
}
