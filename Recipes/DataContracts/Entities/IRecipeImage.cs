﻿namespace DataContracts.Entities;

public interface IRecipeImage : ISimple
{
    string ContentType { get; set; }
    int ID { get; set; }
    string Image { get; set; }
    string ImageBase64 { get; set; }
    bool IsDisplay { get; set; }
    int RecipeId { get; set; }
}