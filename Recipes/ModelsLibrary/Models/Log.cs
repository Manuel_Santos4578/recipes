﻿using System;

namespace ModelsLibrary.Models;

public class Log
{
    public int ID { get; set; }
    public string Name { get; set; }
    public string JsonObject { get; set; }
    public DateTime CreatedAt { get; set; }
}
