﻿using Newtonsoft.Json;

namespace RecipesApi.ApiAuth;

public class ApiUser
{
    [JsonProperty("username")]
    public string Username { get; set; }
    [JsonProperty("role_name")]
    public string Role { get; set; }
    [JsonProperty("password")]
    public string Password { get; set; }
}
