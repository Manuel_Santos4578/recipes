﻿using Microsoft.Data.SqlClient;
using ModelsLibrary.Models;
using Newtonsoft.Json.Linq;
using Recipes.Database.Config;
using System.Data;
using Util;

namespace Recipes.Database.Repositories;

public class RecipeCourseRepository
{
    public static JToken Store(Recipe recipe, Course course)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query =
        @"
            if not exists (Select * from RecipeCourse where recipe_id = @recipe_id AND course_id = @course_id)
                Insert into RecipeCourse (recipe_id,course_id,created_at,updated_at)
                output inserted.recipe_id, inserted.course_id
                VALUES (@recipe_id,@course_id,@created_at,@updated_at)
            else
                Update RecipeCourse SET
                updated_at = @updated_at,
                deleted_at = null
                output inserted.recipe_id, inserted.course_id
                Where recipe_id = @recipe_id AND course_id = @course_id
        ";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@recipe_id",SqlDbType.Int, recipe.ID),
            executor.GenerateParameter("@course_id",SqlDbType.Int, course.ID),
            executor.GenerateParameter("@created_at",SqlDbType.DateTime, DateTime.Now),
            executor.GenerateParameter("@updated_at",SqlDbType.DateTime, DateTime.Now),
        };
        return executor.LoadData<List<JToken>>(query, parameters).FirstOrDefault();
    }

    public static JToken Delete(Recipe recipe, Course course)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query =
        @"
            Update RecipeCourse SET
            deleted_at = @deleted_at
            output inserted.recipe_id, inserted.course_id
            Where recipe_id = @recipe_id AND course_id = @course_id
        ";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@recipe_id",SqlDbType.Int, recipe.ID),
            executor.GenerateParameter("@course_id",SqlDbType.Int, course.ID),
            executor.GenerateParameter("@deleted_at",SqlDbType.DateTime, DateTime.Now),
        };
        return executor.LoadData<List<JToken>>(query, parameters).FirstOrDefault();
    }
}
