﻿namespace DataContracts.Entities;

public interface ICountry : ISimple
{
    int ID { get; set; }
    string Name { get; set; }
    string FriendlyUrl { get; set; }
    IRegion Region { get; set; }
}
