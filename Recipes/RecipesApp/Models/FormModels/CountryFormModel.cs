﻿using DataContracts.Entities;
using ModelsLibrary.Models;
using System;
using System.Collections.Generic;

namespace RecipesApp.Models.FormModels;

public sealed class CountryFormModel
{
    public int ID { get; set; }
    public string Name { get; set; }
    public string FriendlyUrl { get; set; }
    public int RegionID { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
    public DateTime? DeletedAt { get; set; }
    public IEnumerable<IRegion> Regions { get; set; }

    public CountryFormModel()
    {
        Regions = new List<Region>();
    }
    public CountryFormModel(ICountry country) : this()
    {
        ID = country.ID;
        Name = country.Name;
        FriendlyUrl = country.FriendlyUrl;
        RegionID = country.Region.ID;
        CreatedAt = country.CreatedAt;
        UpdatedAt = country.UpdatedAt;
    }
}
