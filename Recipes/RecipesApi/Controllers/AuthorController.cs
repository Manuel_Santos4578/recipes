﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RecipesLibrary.Util;
using ModelsLibrary.Models;
using Microsoft.AspNetCore.Authorization;
using Util;
using RecipesApi.Configuration;
using Recipes.Database.Repositories;

namespace RecipesApi.Controllers;

/// <summary>
/// Author
/// </summary>
[Route("api/authors")]
[Produces("application/json")]
[ApiController]
[Authorize(Roles = $"{ConfigurationState.ApiRoles.Write}, {ConfigurationState.ApiRoles.Root}" )]
public sealed class AuthorController : BaseApiController
{
    public AuthorController() : base()
    {

    }

    /// <summary>
    /// Select all method
    /// </summary>
    /// <param name="name"></param>
    /// <param name="page"></param>
    /// <param name="size"></param>
    /// <returns>author array</returns>
    [HttpGet]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public IActionResult Index(string name, int page = 1, int size = 0, bool count = false)
    {
        try
        {
            if (count)
            {
                return Ok(new { count = AuthorRepository.ReadCount(name) });
            }
            IEnumerable<Author> authors = AuthorRepository.ReadAll(name, page, size);
            return Ok(authors);
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Select By slug
    /// </summary>
    /// <param name="friendly_url">friendly url</param>
    /// <returns>Author collection</returns>
    [HttpGet("{friendly_url}")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public ActionResult<Author> Show(string friendly_url, bool with_trashed = false)
    {
        try
        {
            return Ok(AuthorRepository.ReadBySlug(friendly_url, with_trashed));
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Insert method
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    [ProducesResponseType(500)]
    [ProducesResponseType(201)]
    [ProducesResponseType(401)]
    public async Task<IActionResult> StoreAsync()
    {
        try
        {
            Author author = await StreamParser.ReadObject<Author>(Request.Body);
            author.FriendlyUrl = Slug.GenerateSlug(author.Name);
            author.FriendlyUrl = Validator.ValidateSlug(author.FriendlyUrl, AuthorRepository.ReadBySlug);
            author = AuthorRepository.Store(author);
            int status = author is not null ? 201 : 200;
            return StatusCode(status, author);
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Bulk Store method
    /// </summary>
    /// <returns></returns>
    [HttpPost("bulk")]
    [ProducesResponseType(500)]
    [ProducesResponseType(201)]
    [ProducesResponseType(401)]
    public async Task<IActionResult> BulkStoreAsync()
    {
        IEnumerable<Author> authors = await StreamParser.ReadObject<IEnumerable<Author>>(Request.Body);
        authors = AuthorRepository.BulkStore(authors);
        return Ok(authors);
    }

    /// <summary>
    /// update author
    /// </summary>
    /// <param name="id">PK</param>
    /// <remarks>
    /// Request Body:
    /// 
    ///     {
    ///         "name": string,
    ///         "friendly_url": string
    ///     }
    ///     
    /// </remarks>
    /// <returns>id</returns>
    [HttpPut("{id:int}")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public async Task<IActionResult> UpdateAsync(int id)
    {
        try
        {
            Author author = await StreamParser.ReadObject<Author>(Request.Body);
            string slug = Slug.GenerateSlug(author.Name);
            if (slug != author.FriendlyUrl)
            {
                author.FriendlyUrl = Validator.ValidateSlug(slug, AuthorRepository.ReadBySlug);
            }
            author.ID = id;
            AuthorRepository.UpdateAuthor(author);
            return Ok(author);
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// delete
    /// </summary>
    /// <param name="id">PK</param>
    /// <returns>id</returns>
    [HttpDelete("{id:int}")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public IActionResult Delete(int id)
    {
        try
        {
            Author author = new Author { ID = id };
            author = AuthorRepository.SoftDeleteAuthor(author);
            return Ok(author);
        }
        catch (Exception)
        {
            throw;
        }
    }
}
