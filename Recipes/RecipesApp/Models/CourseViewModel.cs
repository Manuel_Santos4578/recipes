﻿using DataContracts.Entities;
using ModelsLibrary.Models;
using System.Collections.Generic;
using System.Linq;

namespace RecipesApp.Models;

public sealed class CourseViewModel : ViewModel
{
    public ICourse Course { get; set; }
    public IEnumerable<ICourse> Courses { get; set; }
    public List<Recipe> CourseRecipes { get; set; }

    public CourseViewModel()
    {
        Courses = new List<Course>();
        Course = new Course();
        CourseRecipes = new List<Recipe>();
    }
}
