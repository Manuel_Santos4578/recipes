﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ModelsLibrary.Models;
using Recipes.Database.Repositories;
using RecipesApi.Configuration;
using RecipesLibrary.Util;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Util;

namespace RecipesApi.Controllers;

/// <summary>
/// Ingredient
/// </summary>
[Produces("application/json")]
[Route("api/ingredients")]
[Authorize(Roles = $"{ConfigurationState.ApiRoles.Write}, {ConfigurationState.ApiRoles.Root}")]
public class IngredientController : BaseApiController
{
    public IngredientController(IQueryExecutor _executor) : base(_executor)
    {
    }

    /// <summary>
    /// Select all method
    /// </summary>
    /// <returns>Ingredients collection</returns>
    [HttpGet]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public IActionResult Index(string name, int region, int country, bool count = false, int page = 1, int size = 0)
    {
        try
        {
            if (count)
            {
                return Ok(new { count = IngredientRepository.ReadCount(name, region, country) });
            }
            return Ok(IngredientRepository.ReadAll(name, region, country, page, size));
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Select By slug
    /// </summary>
    /// <param name="friendly_url">friendly url</param>
    /// <returns>Ingredient collection</returns>
    [HttpGet("{friendly_url}")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public ActionResult<Ingredient> Show(string friendly_url, bool with_trashed = false)
    {
        try
        {
            return Ok(IngredientRepository.ReadBySlug(friendly_url, with_trashed));
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Insert method
    /// </summary>
    /// <returns>inserted id</returns>
    [HttpPost]
    [ProducesResponseType(500)]
    [ProducesResponseType(201)]
    [ProducesResponseType(401)]
    public async Task<IActionResult> StoreAsync()
    {
        try
        {
            Ingredient ingredient = await Request.Body.ReadObject<Ingredient>();
            ingredient.FriendlyUrl = Slug.GenerateSlug(ingredient.Name);
            ingredient.FriendlyUrl = Validator.ValidateSlug(ingredient.FriendlyUrl, IngredientRepository.ReadBySlug);
            ingredient = IngredientRepository.Store(ingredient);
            int status = ingredient is not null ? 201 : 200;
            return StatusCode(status, ingredient);
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Bulk Store method
    /// </summary>
    /// <returns></returns>
    [HttpPost("bulk")]
    [ProducesResponseType(500)]
    [ProducesResponseType(201)]
    [ProducesResponseType(401)]
    public async Task<IActionResult> BulkStoreAsync()
    {
        try
        {
            IEnumerable<Ingredient> ingredients = await Request.Body.ReadObject<IEnumerable<Ingredient>>();
            ingredients = IngredientRepository.BulkStore(ingredients);
            int status = ingredients is not null ? 201 : 200;
            return StatusCode(status, ingredients);
        }
        catch (Exception)
        {
            throw;
        }
    }
    /// <summary>
    /// Update method
    /// </summary>
    /// <param name="id">PK</param>
    /// <returns>updated id</returns>
    [HttpPut("{id}")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public async Task<IActionResult> UpdateAsync(int id)
    {
        try
        {
            Ingredient ingredient = await Request.Body.ReadObject<Ingredient>();
            string slug = Slug.GenerateSlug(ingredient.Name);
            if (slug != ingredient.FriendlyUrl)
            {
                ingredient.FriendlyUrl = Validator.ValidateSlug(slug, IngredientRepository.ReadBySlug);
            }
            ingredient.ID = id;
            IngredientRepository.Update(ingredient);
            return Ok(ingredient);
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Delete method
    /// </summary>
    /// <param name="id">PK</param>
    /// <returns>deleted id</returns>
    [HttpDelete("{id}")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public IActionResult Delete(int id)
    {
        try
        {
            Ingredient ingredient = new Ingredient { ID = id };
            return Ok(IngredientRepository.SoftDelete(ingredient));
        }
        catch (Exception)
        {
            throw;
        }
    }
}
