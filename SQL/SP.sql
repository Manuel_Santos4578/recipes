
use Recipes
GO
-- Author Select Procedure
create or alter proc [dbo].[AuthorsSearchProc] @name varchar(255)= '', @page int = 1, @size int = 0, @count bit = 0 as 
BEGIN
	DECLARE @statements varchar(8000)
	BEGIN
		IF @count = 0
			set @statements = (Select dbo.query_columns('Author'))
		ELSE
			set @statements = ' count(*) as count '
	END
	declare @search_name varchar(255) = (Select concat('%' , @name, '%'))
	declare @query nvarchar(max) = 
			'Select '+ @statements + ' FROM Author 
			Where deleted_at is null '
		BEGIN
			IF @name != ''
				set @query = concat(@query, ' AND (Author.name like @name OR Author.friendly_url like @name)')
		END	
		BEGIN
			IF @count = 0
				set @query = CONCAT(@query, ' order by id ' )
					IF @page > 0 AND  @size > 0
					set @query = @query + (Select dbo.paginate_results(@page, @size))
		END
	exec sp_executesql @query, N'@name varchar(255), @page int, @size int, @count bit', @search_name, @page, @size, @count
END
GO

create or alter proc [dbo].[AuthorsFindProc] @slug varchar(255), @with_trashed bit = 0 as 
BEGIN
	DECLARE @statements varchar(8000)
	set @statements = (Select dbo.query_columns('Author'))	
	declare @query nvarchar(max) = 'Select '+ @statements + ' FROM Author Where Author.friendly_url = @slug '
	BEGIN
		IF @with_trashed = 0
			set @query = @query + ' AND deleted_at is null'
	END
	exec sp_executesql @query, N'@slug varchar(255)', @slug
END
GO

-- Category Select Procedure
create or alter proc CategoriesSearchProc @name varchar(255)= '', @base_category int=null, @page int = 1, @size int = 0, @count bit = 0 as 
BEGIN
	DECLARE @statements varchar(8000)
	BEGIN
		IF @count = 0
			set @statements = (Select dbo.query_columns('Category'))
		ELSE
			set @statements = ' count(*) as count '
	END
	declare @search_name varchar(255) = (Select concat('%', @name,'%'))
	declare @query nvarchar(max) = 
			'Select '+ @statements + ' FROM Category 
			Where deleted_at is null '
		BEGIN
			IF @name != ''
				set @query = concat(@query, ' AND (Category.name like @name  OR Category.friendly_url like  @name) ')
		END	
		BEGIN
			IF @base_category is not null
				IF @base_category = 0
					set @query = concat(@query, ' AND Category.base_category_id is null ')
				ELSE
					set @query = CONCAT(@query,' AND Category.base_category_id = @base_category')
		END
		BEGIN
			IF @count = 0
			BEGIN
				set @query = CONCAT(@query, ' order by id ')
					IF @page > 0 AND  @size > 0
						set @query = @query + (Select dbo.paginate_results(@page, @size))
			END
		END
		exec sp_executesql @query, N'@name varchar(255), @base_category int, @page int, @size int, @count bit', @search_name, @base_category, @page, @size, @count

END
GO

-- Country Select Proc
create or alter proc CountriesSearchProc @name varchar(255)= '', @region int=null, @page int = 1, @size int = 0, @count bit = 0 as 
BEGIN
	DECLARE @statements varchar(8000)
	BEGIN
		IF @count = 0
			set @statements = (Select dbo.query_columns_two_tables('Country','Region'))
		ELSE
			set @statements = ' count(*) as count '
	END
	declare @search_name varchar(255) = (Select concat('%', @name,'%'))
	declare @query nvarchar(max) = 
			'Select '+ @statements + ' FROM Country 
			inner join Region on Country.region_id = Region.id
			Where Country.deleted_at is null '
		BEGIN
			IF @name != ''
				set @query = concat(@query, ' AND (Country.name like @name OR Country.friendly_url like @name) ')
		END	
		BEGIN
			IF @region is not null
				IF @region != 0				
					set @query = CONCAT(@query,' AND Country.region_id = @region ')
		END
		BEGIN
			IF @count = 0
				set @query = CONCAT(@query, ' order by Country.id ')
					IF @page > 0 AND  @size > 0
					set @query = @query + (Select dbo.paginate_results(@page, @size))
		END
	exec sp_executesql @query, N'@name varchar(255), @region int, @page int, @size int, @count bit', @search_name, @region, @page, @size, @count
END
GO

create or alter proc CountryFindProc @slug varchar(255), @with_trashed bit = 0 as 
BEGIN
	DECLARE @statements varchar(8000)
	set @statements = (Select dbo.query_columns_two_tables('Country','Region'))	
	declare @query nvarchar(max) = 'Select '+ @statements + ' FROM Country inner join Region on Country.region_id = Region.id Where Country.friendly_url = @slug '
	BEGIN
		IF @with_trashed = 0
			set @query = @query + ' AND Country.deleted_at is null'
	END
	exec sp_executesql @query, N'@slug varchar(255)', @slug
END
GO

-- Course Select procedure
create or alter proc CoursesSearchProc @name varchar(255)= '', @page int = 1, @size int = 0, @count bit = 0 as 
BEGIN
	DECLARE @statements varchar(8000)
	declare @search_name varchar(255) = (SElect concat('%', @name ,'%'))
	BEGIN
		IF @count = 0
			set @statements = (Select dbo.query_columns('Course'))
		ELSE
			set @statements = ' count(*) as count '
	END
	declare @query nvarchar(max) = 
			'Select '+ @statements + ' FROM Course 
			Where deleted_at is null '
		BEGIN
			IF @name != ''
				set @query = concat(@query, ' AND (Course.name like @name OR Course.friendly_url like @name)')
		END	
		BEGIN
			IF @count = 0
				set @query = CONCAT(@query, ' order by id ' )
					IF @page > 0 AND  @size > 0
					set @query = @query + (Select dbo.paginate_results(@page, @size))
		END
	exec sp_executesql @query, N'@name varchar(255), @page int, @size int, @count bit', @search_name, @page, @size, @count
END
GO

create or alter proc CourseFindProc @slug varchar(255)= '', @with_trashed bit = 0 as 
BEGIN
	DECLARE @statements varchar(8000)
	BEGIN
		set @statements = (Select dbo.query_columns('Course'))	
	END
	declare @query nvarchar(max) = 
			'Select '+ @statements + ' FROM Course 
			Where Course.friendly_url = @slug '	
	BEGIN
		IF @with_trashed = 0
			set @query = @query + ' AND deleted_at is null'
	END
	exec sp_executesql @query, N'@slug varchar(255)', @slug
END
GO

-- Region Select procedure
create or alter proc RegionsSearchProc @name varchar(255)= '', @page int = 1, @size int = 0, @count bit = 0 as 
BEGIN
	DECLARE @statements varchar(8000)
	BEGIN
		IF @count = 0
			set @statements = (Select dbo.query_columns('Region'))
		ELSE
			set @statements = ' count(*) as count '
	END
	declare @search_name varchar(255) = (SElect concat('%', @name ,'%'))
	declare @query nvarchar(max) = 
			'Select '+ @statements + ' FROM Region 
			Where deleted_at is null '
		BEGIN
			IF @name != ''
				set @query = concat(@query, ' AND (Region.name like @name OR Region.friendly_url like @name) ')
		END	
		BEGIN
			IF @count = 0
				set @query = CONCAT(@query, ' order by id ' )
					IF @page > 0 AND  @size > 0
					set @query = @query + (Select dbo.paginate_results(@page, @size))
		END
	exec sp_executesql @query, N'@name varchar(255), @page int, @size int, @count bit', @search_name, @page, @size, @count 
END
GO

create or alter proc RegionFindProc @slug varchar(255)= '', @with_trashed bit = 0 as 
BEGIN
	DECLARE @statements nvarchar(4000)
	BEGIN
		set @statements = (Select dbo.query_columns('Region'))	
	END
	declare @query nvarchar(max) = 
			'Select '+ @statements + ' FROM Region 
			Where Region.friendly_url = @slug '	
	BEGIN
		IF @with_trashed = 0
			set @query = @query + ' AND deleted_at is null'
	END
	exec sp_executesql @query, N'@slug varchar(255)', @slug 
END
GO

-- Ingredient Store Procedures
create or alter proc IngredientSearchProc @name varchar(255)='', @region int=0, @country int=0, @page int=1, @size int=0, @count bit = 0 as 
BEGIN
	declare @statements varchar(800)
	BEGIN
		IF @count = 0
			set @statements =' Ingredient.*, Country.id as country_id, Country.name as country_name, Country.friendly_url as country_friendly_url, Country.region_id, Region.name as region_name, Region.friendly_url as region_friendly_url '
		ELSE
			set @statements = ' count(*) as count '
	END
	declare @query nvarchar(max) = 'Select ' + @statements + ' from Ingredient inner join Country on Country.id = Ingredient.country_id inner join Region on Region.id = Country.region_id Where Ingredient.deleted_at is null '
	declare @search_name varchar(255) = (Select dbo.varchar_pattern_matching(@name))
	BEGIN
		IF @name != ''
			set @query = @query + ' AND (Ingredient.name like @name OR Ingredient.friendly_url = @name) '
	END
	BEGIN
		IF @region != 0
			set @query = @query + ' AND Region.id = @region '
	END
	BEGIN
		IF @country != 0
			set @query = @query + ' AND Country.id = @country '
	END
	BEGIN
		IF @count = 0
			set @query = CONCAT(@query, ' order by Ingredient.id ')
				IF @page > 0 AND  @size > 0
				set @query = @query + (Select dbo.paginate_results(@page, @size))
	END
	exec sp_executesql @query, N'@name varchar(255), @region int, @country int, @page int, @size int, @count bit', @search_name, @region, @country, @page, @size, @count
END
GO

create or alter proc IngredientFindProc @slug varchar(255)= '', @with_trashed bit=0 as
BEGIN
	declare @query nvarchar(max) = 'select Ingredient.*, Country.id as country_id, Country.name as country_name, Country.friendly_url as country_friendly_url, Country.region_id, Region.name as region_name, Region.friendly_url as region_friendly_url from Ingredient inner join Country on Country.id = Ingredient.country_id inner join Region on Region.id = Country.region_id Where Ingredient.friendly_url = @slug '
	BEGIN
		IF @with_trashed = 0
			set @query = @query + ' AND Ingredient.deleted_at is null '
	END
	exec sp_executesql @query, N'@slug varchar(255)', @slug
END
GO

-- Recipe Store Procedures
create or alter proc RecipeSearchProc
	@name varchar(255)='', @author varchar(255)='', 
	@category varchar(255)='', @country varchar(255)='', 
	@region varchar(255)='', @source varchar(255)='', @ingredient varchar(255) = '',
	@user int=0, @page int=0, @size int=0,  @count bit=0 , @favorites_only bit=0 as 
BEGIN
	declare @statement varchar(8000)
	BEGIN
		if @count = 0
			BEGIN
			set @statement = 
			' Distinct Recipe.id recipe_id,
			Recipe.name recipe_name,
			Recipe.friendly_url recipe_friendly_url,
			Recipe.recipe_text,
			Category.id as category_id,
			Category.name as category_name,
			Category.friendly_url as category_url,
			RecipeSource.id as recipe_source_id,
			RecipeSource.name as recipe_source_name,
			RecipeSource.friendly_url as recipe_source_friendly_url,
			Author.id as author_id,
			Author.name as author_name,
			Author.friendly_url as author_url,
			Recipe.country_id,
			Country.name as country_name,
			Country.friendly_url as country_url,
			Country.region_id,
			Region.name as region_name,
			Region.friendly_url as region_friendly_url,
			Recipe.created_at,
			Recipe.updated_at,
			Recipe.deleted_at,
			RecipeImages.id as image_id,
			RecipeImages.image,
			RecipeImages.image_base_64,
			RecipeImages.content_type,
			RecipeImages.is_display,
			ISNULL(Recipefavorite.recipe_id,0) as recipe_favorite_id '
			END
		ELSE
			set @statement = ' count(distinct Recipe.id) as count '
	END
	declare @query nvarchar(max) = 'Select ' + @statement + ' From Recipe
		inner join Author on Author.id = Recipe.author_id
		inner join RecipeSource on RecipeSource.id = Recipe.recipe_source_id
		inner join Category on Category.id = Recipe.category_id
		inner join Country on Country.id = Recipe.country_id
		inner join Region on Country.region_id = Region.id
		inner join RecipeImages on Recipe.id = RecipeImages.recipe_id '
	BEGIN
		IF @ingredient != ''
			set @query = @query + 
			' inner join RecipeIngredient on RecipeIngredient.recipe_id = Recipe.id 
			  inner join Ingredient on Ingredient.id = RecipeIngredient.ingredient_id '
	END
	set @query = @query + ' left join Recipefavorite on Recipe.id = Recipefavorite.recipe_id AND Recipefavorite.user_id = @user
		Where Recipe.deleted_at is null AND RecipeImages.is_display = 1 '
	BEGIN
		IF @favorites_only = 1
			set @query = @query + ' AND user_id = @user '
	END
	BEGIN
		IF @ingredient != ''
		set @query = @query + ' AND (Ingredient.name like @ingredient OR Ingredient.friendly_url like @ingredient) '
	END
	BEGIN
		IF @name != ''
			set @query = @query + ' AND (Recipe.name like @name OR Recipe.friendly_url like @name) '
			set @name = dbo.varchar_pattern_matching(@name)
	END
	BEGIN
		IF @author != ''
			set @query = @query + ' AND (Author.name like @author OR Author.friendly_url like @author) '
			set @author = dbo.varchar_pattern_matching(@author)
	END
	BEGIN
		IF @category != ''
			set @query = @query + ' AND (Category.name like @category OR Category.friendly_url like @category) '
			set @category = dbo.varchar_pattern_matching(@category)
	END
	BEGIN
		IF @country != ''
			set @query = @query + ' AND (Country.name like @country OR Country.friendly_url like @country) '
			set @country = dbo.varchar_pattern_matching(@country)
	END
	BEGIN
		IF @region != ''
			set @query = @query + ' AND (Region.name like @region OR Region.friendly_url like @region) '
			set @region = dbo.varchar_pattern_matching(@region)
	END
	BEGIN
		IF @source != ''
			set @query = @query + ' AND (RecipeSource.name like @source OR RecipeSource.friendly_url like @source) '
			set @source = dbo.varchar_pattern_matching(@source)
	END
		BEGIN
			IF @count = 0
				set @query = CONCAT(@query, ' order by Recipe.id ' )
					IF @page > 0 AND  @size > 0
					set @query = @query + (Select dbo.paginate_results(@page, @size))
		END
	exec sp_executesql @query, N'@name varchar(255), @author varchar(255), @category varchar(255), @country varchar(255), @region varchar(255), @source varchar(255), @ingredient varchar(255), @page int, @size int, @user int, @count bit', @name, @author,@category, @country, @region, @source, @ingredient, @page, @size,@user, @count
END
GO
create or alter proc RecipeFindProc @slug varchar(255) = '', @with_trashed bit= 0 as
BEGIN
	declare @query nvarchar(max) = 
		'select Recipe.id recipe_id,
		Recipe.name recipe_name,
		Recipe.friendly_url recipe_friendly_url,
		Recipe.recipe_text,
		Category.id as category_id,
		Category.name as category_name,
		Category.friendly_url as category_url,
		RecipeSource.id as recipe_source_id,
		RecipeSource.name as recipe_source_name,
		RecipeSource.friendly_url as recipe_source_friendly_url,
		Author.id as author_id,
		Author.name as author_name,
		Author.friendly_url as author_url,
		Recipe.country_id,
		Country.name as country_name,
		Country.friendly_url as country_url,
		Country.region_id,
		Region.name as region_name,
		Region.friendly_url as region_friendly_url,
		Recipe.created_at,
		Recipe.updated_at,
		Recipe.deleted_at,
		RecipeImages.id as image_id,
		RecipeImages.image,
		RecipeImages.image_base_64,
		RecipeImages.content_type,
		RecipeImages.is_display,
		ISNULL(Recipefavorite.recipe_id,0) as recipe_favorite_id
		From Recipe
		inner join Author on Author.id = Recipe.author_id
		inner join RecipeSource on RecipeSource.id = Recipe.recipe_source_id
		inner join Category on Category.id = Recipe.category_id
		inner join Country on Country.id = Recipe.country_id
		inner join Region on Country.region_id = Region.id
		inner join RecipeImages on Recipe.id = RecipeImages.recipe_id
		left join Recipefavorite on Recipe.id = Recipefavorite.recipe_id
		Where Recipe.friendly_url = @slug AND RecipeImages.is_display = 1 '
	BEGIN
		IF @with_trashed = 0
			set @query = @query + ' AND Recipe.deleted_at is null'
	END
	exec sp_executesql @query, N'@slug varchar(255)', @slug
END
GO
-- RecipeCourse Store Procedures
create or alter proc RecipeCourseProc @recipe_slug varchar(255) as 
BEGIN
    DECLARE @statements varchar(8000) = (Select dbo.query_columns('Course'))
    DECLARE @query nvarchar(max) = 
	'Select '+ @statements + ' from Course
	inner join RecipeCourse on course.id = RecipeCourse.course_id
	inner join Recipe on Recipe.id = RecipeCourse.recipe_id
	where Recipe.deleted_at is null and RecipeCourse.deleted_at is null and Course.deleted_at is null and Recipe.friendly_url = @recipe_slug'
    execute sp_executesql @query, N'@recipe_slug varchar(255)', @recipe_slug
END
GO

-- Recipe Images Procedures
create or alter proc RecipeImageSearchProc @recipe int, @is_display bit = null, @count bit = 0 as
BEGIN
	declare @statement nvarchar(2000) = ''
	IF @count = 1
		BEGIN
			set @statement = N'count(*) as count'
		END
	ELSE
		BEGIN
			set @statement = (Select dbo.query_columns('RecipeImages'))
		END
	declare @query nvarchar(4000) = 
		(SELECT CONCAT('SELECT ',@statement, ' from RecipeImages Where RecipeImages.deleted_at is null and recipe_id = @recipe_id '))
	IF @is_display is not null
		set @query = @query + ' and is_display = @is_display'
	exec sp_executesql @query, N'@recipe_id int, @is_display bit', @recipe, @is_display
END
GO
-- Source Store Procedures
create or ALTER proc RecipeSourcesSearchProc @name varchar(255)= '', @page int = 1, @size int = 0, @count bit = 0 as 
BEGIN
	DECLARE @statements varchar(8000)
	BEGIN
		IF @count = 0
			set @statements = (Select dbo.query_columns('RecipeSource'))
		ELSE
			set @statements = ' count(*) as count '
	END
	declare @search_name varchar(255) = (Select dbo.varchar_pattern_matching(@name))
	declare @query nvarchar(max) = 
			'Select '+ @statements + ' FROM RecipeSource 
			Where deleted_at is null '
		BEGIN
			IF @name != ''
				set @query = concat(@query, ' AND (RecipeSource.name like @name OR RecipeSource.friendly_url like @name) ')
		END	
		BEGIN
			IF @count = 0
				set @query = CONCAT(@query, ' order by id ' )
					IF @page > 0 AND  @size > 0
					set @query = @query + (Select dbo.paginate_results(@page, @size))
		END
	exec sp_executesql @query, N'@name varchar(255), @page int, @size int, @count bit', @search_name, @page, @size, @count
END
GO

create or alter proc RecipeSourcesFindProc @slug varchar(255)= '', @with_trashed bit = 0 as 
BEGIN
	DECLARE @statements varchar(8000)
	BEGIN
		set @statements = (Select dbo.query_columns('RecipeSource'))	
	END
	declare @query nvarchar(max) = 
			'Select '+ @statements + ' FROM RecipeSource 
			Where RecipeSource.friendly_url = @slug'	
	BEGIN
		IF @with_trashed = 0
			set @query = @query + ' AND deleted_at is null'
	END
	exec sp_executesql @query, N'@slug varchar(255)', @slug
END
GO