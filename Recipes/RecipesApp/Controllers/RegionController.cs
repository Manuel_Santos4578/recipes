﻿using DataContracts.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using RecipesApp.Models;
using RecipesApp.Models.FormModels;
using RecipesApp.Validators.FormValidation;
using RecipesLibrary.Util;
using System.Linq;
using System.Threading.Tasks;
using Util;
using I18N = ModelsLibrary.Constants.AppConstants.I18N;

namespace RecipesApp.Controllers;

[Route("regions")]
[Authorize]
public sealed class RegionController : MyBaseController
{
    public RegionController(IConfiguration config, IWebHostEnvironment env) : base(config, env)
    {
    }

    [HttpGet]
    public async Task<IActionResult> IndexAsync()
    {
        await ApiAuthAsync();
        RegionViewModel model = new RegionViewModel();
        HandleViewData();
        return View(model);
    }

    [HttpPost]
    public async Task<IActionResult> IndexAjaxAsync()
    {
        JToken body = await Request.Body.ReadObject<JToken>();
        int page = body.Value<int>("page");
        int page_size = body.Value<int>("size");
        string name = body.Value<string>("name");
        await ApiAuthAsync();
        RegionViewModel model = new RegionViewModel
        {
            PageSize = page_size,
            CurrentPage = page,
            Count = await dal.SelectRegionCountAsync(name),
            Regions = await dal.SelectAllRegionsAsync(name, page, page_size)
        };
        return Ok(model);
    }
    [HttpGet("show/{friendly_url}")]
    public async Task<IActionResult> ShowAsync(string friendly_url)
    {
        await ApiAuthAsync();
        RegionViewModel model = new RegionViewModel
        {
            Region = await dal.SelectRegionBySlugAsync(friendly_url)
        };
        if (model.Region is null) return View("NotFound");

        model.Countries = await dal.SelectAllCountriesAsync(region: model.Region);

        HandleViewData();
        return View(model);
    }

    [HttpGet("create")]
    public IActionResult Create()
    {
        return View(new RegionFormModel());
    }

    [HttpPost("store")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> StoreAsync(RegionFormModel model)
    {
        FluentValidation.Results.ValidationResult validation_result = await new RegionFormModelValidator().ValidateAsync(model);
        if (!validation_result.IsValid)
        {
            TempData["Error"] = string.Join("; ", validation_result.Errors.Select(err => err.ErrorMessage));
            return RedirectToAction("Index", "Region");
        }
        await ApiAuthAsync();
        model.Name = model.Name.Sanitize();
        IRegion region = await dal.InsertRegionAsync(model);
        if (region is not null)
        {
            TempData["Success"] = string.Format(translation[I18N.Successfully_created], model.Name);
            return RedirectToAction("Show", "Region", new { friendly_url = region.FriendlyUrl });
        }
        TempData["Error"] = string.Format(translation[I18N.Failed_creation_of], model.Name);
        return RedirectToAction("Index", "Region");
    }

    [HttpGet("edit/{friendly_url}")]
    public async Task<IActionResult> EditAsync(string friendly_url)
    {
        await ApiAuthAsync();
        IRegion region = await dal.SelectRegionBySlugAsync(friendly_url);
        if (region is null) return NotFound();
        RegionFormModel model = new RegionFormModel(region);
        return View(model);
    }

    [HttpPost("update/{friendly_url}")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> UpdateAsync(RegionFormModel model, string friendly_url)
    {
        FluentValidation.Results.ValidationResult validation_result = await new RegionFormModelValidator().ValidateAsync(model);
        if (!validation_result.IsValid)
        {
            TempData["Error"] = string.Join("; ", validation_result.Errors.Select(err => err.ErrorMessage));
            return Redirect(Request.Headers["Referer"]);
        }
        await ApiAuthAsync();
        model.Name = model.Name.Sanitize();
        model.FriendlyUrl = friendly_url;
        IRegion region = await dal.UpdateRegionAsync(model);
        if (region is not null)
        {
            TempData["Success"] = string.Format(translation[I18N.Successfully_updated], model.Name);
            return RedirectToAction("Show", "Region", new { friendly_url = region.FriendlyUrl });
        }
        TempData["Error"] = string.Format(translation[I18N.Failed_to_update], model.Name);
        return RedirectToAction("Show", "Region", new { friendly_url });
    }

    [HttpGet("delete/{friendly_url}")]
    public async Task<IActionResult> DeleteAsync(string friendly_url)
    {
        await ApiAuthAsync();
        IRegion region = await dal.SelectRegionBySlugAsync(friendly_url);
        if (region is null) return NotFound();
        return View(new RegionFormModel(region));
    }

    [HttpPost("destroy")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DestroyAsync(RegionFormModel model)
    {
        await ApiAuthAsync();
        bool deleted = await dal.DeleteRegionAsync(model);
        if (deleted)
        {
            TempData["Success"] = string.Format(translation[I18N.Successfully_deleted], model.Name);
            return RedirectToAction("Index", "Region");
        }
        TempData["Error"] = string.Format(translation[I18N.Failed_to_delete], model.Name);
        return RedirectToAction("Show", "Region", new { friendly_url = model.FriendlyUrl });
    }
}
