﻿using DataContracts.Entities;

namespace DataContracts.Access;

public partial interface ICourseDataAccess
{
    Task<bool> DeleteCourseAsync(ICourse course);
    Task<ICourse> InsertCourseAsync(ICourse course);
    Task<ICourse> SelectCourseBySlugAsync(string slug);
    Task<IEnumerable<ICourse>> SelectAllCoursesAsync(string name = "", int page = 1, int size = 0);
    Task<int> SelectCoursesCountAsync(string name);
    Task<ICourse> UpdateCourseAsync(ICourse course);
}