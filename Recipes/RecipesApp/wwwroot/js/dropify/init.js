document.addEventListener('DOMContentLoaded',()=>{
    $('.dropify').dropify({
        messages: {
            'default': '',
            'replace': 'Arrastar ou clicar para substituir',
            'remove':  'Remover',
            'error':   'Ooops, ocorreu um erro.'
        },
        error: {
            'fileSize': 'Tamanho do ficheiro excede o máximo de ({{ value }}B).',
            // 'minWidth': 'The image width is too small ({{ value }}}px min).',
            // 'maxWidth': 'The image width is too big ({{ value }}}px max).',
            // 'minHeight': 'The image height is too small ({{ value }}}px min).',
            // 'maxHeight': 'The image height is too big ({{ value }}px max).',
            'imageFormat': 'Imagem com formato incorreto. Formatos permitidos são: ({{ value }}).'
        }
    })
})