﻿function setThemeMode(themeMode: string): void {
    localStorage.setItem('themeMode', themeMode);
    document.documentElement.setAttribute('data-bs-theme', themeMode);
    let storage_theme_mode: string = localStorage.getItem('themeMode');

    if (storage_theme_mode == null || storage_theme_mode == 'light') {
        document.getElementById('theme-changer-light').classList.add('d-none')
        document.getElementById('theme-changer-dark').classList.remove('d-none')
    } else {
        document.getElementById('theme-changer-dark').classList.add('d-none')
        document.getElementById('theme-changer-light').classList.remove('d-none')
    }
}

function setTheme(theme: string): void {
    localStorage.setItem('theme', theme);
    let css_file: HTMLAnchorElement = document.getElementById('theme') as HTMLAnchorElement;
    css_file.href = `/css/${theme}.css`;
}

function checkThemeMode(): void {
    let storage_Theme: string = localStorage.getItem('themeMode');
    setThemeMode(storage_Theme ?? 'light');
}
function checkTheme(): void {
    let storage_Theme: string = localStorage.getItem('theme');
    let themes_select: HTMLSelectElement = document.getElementById('theme_list') as HTMLSelectElement;
    let options: HTMLOptionElement[] = Array.from(themes_select.options)
    options.forEach(option => {
        if (option.value == storage_Theme) {
            option.selected = true;
        }
    });
    setTheme(storage_Theme ?? 'default');
}
try {
    let theme_modes_array: HTMLElement[] = Array.from(document.getElementsByClassName('theme_changer')) as HTMLElement[];
    theme_modes_array.forEach((toggler) => {
        toggler.addEventListener('click', (e) => {
            e.preventDefault();
            setThemeMode(toggler.getAttribute('data-theme'));
        });
    });
} catch (e) {

}
try {
    let themes_select: HTMLSelectElement = document.getElementById('theme_list') as HTMLSelectElement;
    themes_select.addEventListener('change', function (e) {
        let theme: string = this.options[this.selectedIndex].value;
        setTheme(theme);
    });
} catch (e) {

}
checkTheme();
checkThemeMode();