﻿namespace RecipesApi.Configuration;

internal class ConfigurationState
{
	internal class ApiRoles
	{
		internal const string Root = "root";
		internal const string Write = "write";
	}
}
