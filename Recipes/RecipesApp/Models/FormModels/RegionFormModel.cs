﻿using DataContracts.Entities;
using ModelsLibrary.Models;

namespace RecipesApp.Models.FormModels;

public sealed class RegionFormModel : Region
{
    public RegionFormModel()
    {

    }
    public RegionFormModel(IRegion region)
    {
        ID = region.ID;
        Name = region.Name;
        FriendlyUrl = region.FriendlyUrl;
        CreatedAt = region.CreatedAt;
        UpdatedAt = region.UpdatedAt;
    }
}
