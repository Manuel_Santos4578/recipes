namespace DataContracts.Entities;

public interface IRole 
{
    int ID { get; set; }
    string Name { get; set; }
}