﻿using System;

namespace RecipesApp.Models;

public abstract class ViewModel
{
    public int CurrentPage { get; set; } = 1;
    public int Count { get; set; }
    public int PageSize { get; set; } = 25;
    public int TotalPages => (int)Math.Ceiling(Count / (double)PageSize);

    public int[] PageSizeOptions = new int[] { 5, 10, 25 };
}
