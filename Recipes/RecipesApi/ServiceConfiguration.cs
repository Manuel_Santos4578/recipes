﻿using Microsoft.Extensions.DependencyInjection;
using System;
using Util;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using RecipesApi.ApiAuth;
using Microsoft.OpenApi.Models;
using ModelsLibrary.Constants;
using Recipes.Database.Config;

namespace RecipesApi;

public static class ServiceConfiguration
{
    public static void AddDatabase(this IServiceCollection services)
    {
        string database_domain = Environment.GetEnvironmentVariable(AppConstants.RecipesApiEnv.DatabaseIpAddress).ToString();

        DB.CreateInstance(database_domain,
            Environment.GetEnvironmentVariable(AppConstants.RecipesApiEnv.DatabaseName).ToString(),
            Environment.GetEnvironmentVariable(AppConstants.RecipesApiEnv.DatabaseUser).ToString(),
            Environment.GetEnvironmentVariable(AppConstants.RecipesApiEnv.DatabasePassword).ToString(),
            Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT").ToString() == "Development");
        services.AddScoped<IQueryExecutor, QueryExecutor>((context) =>
        {
            return new QueryExecutor(DB.Instance);
        });
    }

    public static void AddSwagger(this IServiceCollection services)
    {
        services.AddSwaggerGen(gen =>
        {
            gen.SwaggerDoc("v1", new OpenApiInfo
            {
                Title = "Recipes API",
                Version = "1.0.0",
                Description = "Recipes App API"
            });
        });
    }
    public static void AddJwtAuthentication(this IServiceCollection services)
    {
        services.AddAuthentication(options =>
        {
            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

        }).AddJwtBearer(options =>
        {
            options.RequireHttpsMetadata = false;
            options.SaveToken = true;
            options.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Settings.Secret)),
                ValidateIssuer = false,
                ValidateAudience = false
            };
        });
    }
}
