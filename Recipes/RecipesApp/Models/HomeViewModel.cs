﻿using DataContracts.Entities;
using ModelsLibrary.Models;
using System.Collections.Generic;
using System.Linq;

namespace RecipesApp.Models;

public sealed class HomeViewModel: ViewModel
{
    public IEnumerable<IRecipe> FavoriteRecipes { get; set; }
    public HomeViewModel()
    {
        FavoriteRecipes = new List<Recipe>();
    }
}
