﻿namespace DataContracts.Entities;

public interface IRecipe : ISimple
{
    public int ID { get; set; }
    public string Name { get; set; }
    public string FriendlyUrl { get; set; }
    public string Text { get; set; }
    public bool IsFavorite { get; set; }
    public IList<IRecipeIngredient> Ingredients { get; set; }
    public ICategory Category { get; set; }
    public IAuthor Author { get; set; }
    public ISource Source { get; set; }
    public ICountry Country { get; set; }
    public IRecipeImage MainImage { get; set; }
    public IList<IRecipeImage> MiscImages { get; set; }
    public IList<ICourse> Courses { get; set; }
    public IRecipeUser RecipeUser { get; set; }
}
