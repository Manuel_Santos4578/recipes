using System;
using System.Globalization;
using System.Threading.Tasks;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using RecipesApp.Services;

namespace RecipesApp;

public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddLocalization(options => options.ResourcesPath = "Resources");
        services.AddControllersWithViews()
        .AddNewtonsoftJson(action =>
        {
            action.SerializerSettings.DateFormatString = "dd/MM/yyyy HH:mm:ss";
        })
#if DEBUG
            .AddRazorRuntimeCompilation()
#endif
        ;
        services.AddFluentValidationAutoValidation();
        services.AddFluentValidationClientsideAdapters();
        services.AddMemoryCache();
        services.RegisterLocalization();
        services.AddSession(options => { options.IdleTimeout = new TimeSpan(1, 0, 0, 0); });
        services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, options =>
        {
            options.LoginPath = new PathString("/auth/login");
            options.AccessDeniedPath = new PathString("/auth/forbidden");
            options.LogoutPath = new PathString("/auth/logout");
            options.ReturnUrlParameter = "r";
        });
        services.AddOptions();
        services.AddResponseCompression();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IMemoryCache cache,
        IOptions<RequestLocalizationOptions> requestLocalizationOptions,
        IOptionsMonitor<RequestLocalizationOptions> requestLocalizationOptionsMonitor,
        IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }
        else
        {
            app.UseExceptionHandler("/Home/Error");
            // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
        }
        app.UseHttpsRedirection();
        app.UseStaticFiles(/*new StaticFileOptions
        {
            OnPrepareResponse = (context) =>
            {
                // Customize the cache settings for manifest.json and service-worker.js
                if (context.File.Name.Equals("manifest.json") || context.File.Name.Equals("service-worker.js"))
                {
                    context.Context.Response.Headers.Add("Cache-Control", "public, max-age=0");
                }
            }
        }*/);
        app.UseRouting();
        app.UseSession();
        app.UseAuthentication();
        app.UseAuthorization();
        app.UseRequestLocalization();
        app.UseResponseCompression();
        CacheManagementService.UseCache(cache);
        try
        {
            RefreshCulturesAsync(requestLocalizationOptions, requestLocalizationOptionsMonitor).Wait();
        }
        catch (Exception) { } 
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllerRoute(
                name: "default",
                pattern: "{controller=Home}/{action=Index}/{id?}");
        });
    }

    public static async Task RefreshCulturesAsync(
        IOptions<RequestLocalizationOptions> requestLocalizationOptions,
        IOptionsMonitor<RequestLocalizationOptions> requestLocalizationOptionsMonitor)
    {
        await CacheManagementService.RefreshCulturesAsync();
        await CacheManagementService.RefreshTranslationsAsync();
        CultureInfo[] cultures = await CacheManagementService.CulturesAsync();
        RequestLocalizationOptions options = requestLocalizationOptions.Value;
        RequestLocalizationOptions monitor = requestLocalizationOptionsMonitor.CurrentValue;
        options.SupportedCultures = cultures;
        options.SupportedUICultures = cultures;
        monitor.SupportedCultures = cultures;
        monitor.SupportedUICultures = cultures;
    }
}

internal static class LocalizationExtension
{
    public static void RegisterLocalization(this IServiceCollection services)
    {
        services.AddLocalization(opt =>
        {
            opt.ResourcesPath = "Localization";
        });
        services.Configure<RequestLocalizationOptions>(opt =>
        {
            var langs = new CultureInfo[] { new CultureInfo("en-US") };
            opt.DefaultRequestCulture = new RequestCulture("en-US");

            // required for controllers
            opt.SupportedCultures = langs;

            // required for views
            opt.SupportedUICultures = langs;
        });

    }
}