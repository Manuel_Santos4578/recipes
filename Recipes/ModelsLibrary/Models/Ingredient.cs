﻿using DataContracts.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System;

namespace ModelsLibrary.Models;

public class Ingredient : Simple, IIngredient
{
    [JsonProperty("ingredient_id")]
    public int ID { get; set; }
    [JsonProperty("ingredient_name")]
    public string Name { get; set; }
    [JsonProperty("ingredient_friendly_url")]
    public string FriendlyUrl { get; set; }
    [JsonProperty("country", NullValueHandling = NullValueHandling.Ignore)]
    [JsonConverter(typeof(JsonConverters.JsonConverter<Country>))]
    public ICountry Country { get; set; }

    public static DataTable ExportToDataTable(IEnumerable<IIngredient> ingredients)
    {
        DataTable table = new DataTable();
        table.Columns.AddRange(new DataColumn[]
        {
            new DataColumn(nameof(IIngredient.Name), typeof(string)),
            new DataColumn($"{nameof(IIngredient.Country)}.{nameof(IIngredient.Country.Name)}", typeof(string)),
            new DataColumn(nameof(IIngredient.CreatedAt), typeof(DateTime)),
            new DataColumn(nameof(IIngredient.UpdatedAt), typeof(DateTime)),
        });

        foreach (IIngredient country in ingredients)
        {
            table.Rows.Add(
                country.Name,
                country.Country?.Name ?? "---",
                country.CreatedAt,
                country.UpdatedAt);
        }
        return table;
    }
}
