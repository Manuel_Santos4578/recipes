function AddEvents() {
    let inputs = document.querySelectorAll('.form-box input');
    for (const input of inputs) {
        input.addEventListener('focus', Focus);
        input.addEventListener('blur', Blur);
    }
}
function Focus() {
    this.classList.add('focus');
}

function Blur() {
    if (this.value == "") {
        this.classList.remove('focus');
    }
}

function FocusInputs() {
    document.querySelectorAll('.form-box input').forEach((input) => {
        if (input.value) {
            input.classList.add('focus')
        }
    })

}
document.addEventListener('DOMContentLoaded', () => {
    AddEvents();
    FocusInputs();
})