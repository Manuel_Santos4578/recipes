﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ModelsLibrary.Models;
using Recipes.Database.Repositories;
using RecipesApi.Configuration;
using RecipesLibrary.Util;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Util;

namespace RecipesApi.Controllers;

/// <summary>
/// Source
/// </summary>
[Route("api/sources")]
[Produces("application/json")]
[Authorize(Roles = $"{ConfigurationState.ApiRoles.Write}, {ConfigurationState.ApiRoles.Root}")]
public class SourceController : BaseApiController
{
    public SourceController(IQueryExecutor _executor) : base(_executor)
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="name"></param>
    /// <param name="count"></param>
    /// <param name="page"></param>
    /// <param name="size"></param>
    /// <returns></returns>
    [HttpGet]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public IActionResult Index(string name, bool count = false, int page = 1, int size = 0)
    {
        try
        {
            if (count)
            {
                return Ok(new { count = SourceRepository.ReadCount(name) });
            }
            return Ok(SourceRepository.ReadlAll(name, page, size));
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Select By slug
    /// </summary>
    /// <param name="friendly_url">friendly url</param>
    /// <returns>Source collection</returns>
    [HttpGet("{friendly_url}")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public ActionResult<Source> Show(string friendly_url, bool with_trashed = false)
    {
        try
        {
            return Ok(SourceRepository.ReadBySlug(friendly_url, with_trashed));
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Insert method
    /// </summary>
    /// <returns>inserted id</returns>
    [HttpPost]
    [ProducesResponseType(500)]
    [ProducesResponseType(201)]
    [ProducesResponseType(401)]
    public async Task<IActionResult> StoreAsync()
    {
        try
        {
            Source source = await Request.Body.ReadObject<Source>();
            source = SourceRepository.Store(source);
            int status = source is not null ? 201 : 200;
            return StatusCode(status, source);
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Bulk Insert method
    /// </summary>
    /// <returns></returns>
    [HttpPost("bulk")]
    [ProducesResponseType(500)]
    [ProducesResponseType(201)]
    [ProducesResponseType(401)]
    public async Task<IActionResult> BulkInsertAsync()
    {
        IEnumerable<Source> sources = await StreamParser.ReadObject<IEnumerable<Source>>(Request.Body);
        sources = SourceRepository.BulkStore(sources);
        return Ok(sources);
    }

    /// <summary>
    /// Update method
    /// </summary>
    /// <param name="id">PK</param>
    /// <returns>updated id</returns>
    [HttpPut("{id:int}")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public async Task<IActionResult> UpdateAsync(int id)
    {
        try
        {
            Source source = await Request.Body.ReadObject<Source>();
            string slug = Slug.GenerateSlug(source.Name);
            if (slug != source.FriendlyUrl)
            {
                source.FriendlyUrl = Validator.ValidateSlug(slug, SourceRepository.ReadBySlug);
            }
            source.ID = id;
            return Ok(SourceRepository.Update(source));
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Delete method
    /// </summary>
    /// <param name="id">PK</param>
    /// <returns>deleted id</returns>
    [HttpDelete("{id:int}")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public IActionResult Delete(int id)
    {
        try
        {
            Source source = new Source { ID = id };
            return Ok(SourceRepository.SoftDelete(source));
        }
        catch (Exception)
        {
            throw;
        }
    }
}
