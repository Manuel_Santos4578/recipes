﻿using DataContracts.Entities;
using ModelsLibrary.Models;
using System.Collections.Generic;
using System.Linq;

namespace RecipesApp.Models;

public sealed class CountryViewModel: ViewModel
{
    public ICountry Country { get; set; }
    public IEnumerable<ICountry> Countries { get; set; }
    public IEnumerable<IRegion> Regions { get; set; }
    public IEnumerable<IRecipe> CountryRecipes { get; set; }
    public int CountryRecipesCount { get; set; }

    public CountryViewModel()
    {
        Countries = new List<Country>();
        Regions = new List<Region>();
        Country = new Country();
        CountryRecipes = new List<Recipe>();
    }
}
