﻿using System;
using System.IO;

namespace RecipesApp;

public class EnvironmentSetup
{
    private const string EXPORT = "Export";

    private static string[] _export_entities { get { _export_entities_instance ??= new string[] { AuthorExportPath, CategoryExportPath, CourseExportPath, CountryExportPath, IngredientExportPath, RecipeExportPath, RegionExportPath, SourceExportPath }; return _export_entities_instance; } }
    private const string Author = nameof(Author);
    private const string Category = nameof(Category);
    private const string Course = nameof(Course);
    private const string Country = nameof(Country);
    private const string Ingredient = nameof(Ingredient);
    private const string Recipe = nameof(Recipe);
    private const string Region = nameof(Region);
    private const string Source = nameof(Source);

    private static string[] _export_entities_instance;
    private static string _exportPath;
    private static string _authorExportPath;
    private static string _categoryExportPath;
    private static string _countryExportPath;
    private static string _courseExportPath;
    private static string _ingredientExportPath;
    private static string _recipeExportPath;
    private static string _regionExportPath;
    private static string _sourceExportPath;

    public static string ExportPath { get { _exportPath ??= Path.Combine(AppContext.BaseDirectory, EXPORT); return _exportPath; } }
    public static string AuthorExportPath { get { _authorExportPath ??= Path.Combine(ExportPath, Author); return _authorExportPath; } }
    public static string CategoryExportPath { get { _categoryExportPath ??= Path.Combine(ExportPath, Category); return _categoryExportPath; } }
    public static string CountryExportPath { get { _countryExportPath ??= Path.Combine(ExportPath, Country); return _countryExportPath; } }
    public static string CourseExportPath { get { _courseExportPath ??= Path.Combine(ExportPath, Course); return _courseExportPath; } }
    public static string IngredientExportPath { get { _ingredientExportPath ??= Path.Combine(ExportPath, Ingredient); return _ingredientExportPath; } }
    public static string RecipeExportPath { get { _recipeExportPath ??= Path.Combine(ExportPath, Recipe); return _recipeExportPath; } }
    public static string RegionExportPath { get { _regionExportPath ??= Path.Combine(ExportPath, Region); return _regionExportPath; } }
    public static string SourceExportPath { get { _sourceExportPath ??= Path.Combine(ExportPath, Source); return _sourceExportPath; } }

    public static void Setup()
    {
        SetupExportFolders();
    }

    private static void SetupExportFolders()
    {
        DirectorySetup(ExportPath);

        for (int i = 0; i < _export_entities.Length; i++)
        {
            DirectorySetup($"{_export_entities[i]}");
        }
    }

    private static void DirectorySetup(string path)
    {
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }
    }
}
