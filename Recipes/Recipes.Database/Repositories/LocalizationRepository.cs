﻿using Microsoft.Data.SqlClient;
using ModelsLibrary.Localization;
using Recipes.Database.Config;
using System.Globalization;
using Util;

namespace Recipes.Database.Repositories;

public static class LocalizationRepository
{
    public static List<Culture> SelectCultures(string culture = "")
    {
        QueryExecutor executor = new QueryExecutor(DB.Instance);
        string cultures_store_procedure = "exec CulturesSearchProc @culture";

        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@culture", System.Data.SqlDbType.VarChar, culture)
        };

        return executor.LoadData<List<Culture>>(cultures_store_procedure, parameters);
    }

    public static List<Translation> SelectTranslations(string culture)
    {
        QueryExecutor executor = new QueryExecutor(DB.Instance);
        string translation_query = "exec TranslationsSearchProc @culture";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@culture", System.Data.SqlDbType.VarChar, culture),
        };
        return executor.LoadData<List<Translation>>(translation_query, parameters);
    }

    public static CultureInfo CreateCultureAsync(CultureInfo culture)
    {
        QueryExecutor executor = new QueryExecutor(DB.Instance);
        string create_culture_query = "exec CreateCulture @culture, @display_name";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@culture", System.Data.SqlDbType.VarChar, culture.Name),
            executor.GenerateParameter("@display_name", System.Data.SqlDbType.VarChar, culture.EnglishName),
        };
        Dictionary<string, string> inserted = executor.LoadData(create_culture_query, parameters).FirstOrDefault();
        return inserted is not null ? culture : null;
    }

    public static Culture DeleteCulture(Culture culture)
    {
        QueryExecutor executor = new QueryExecutor(DB.Instance);
        string delete_culture_query = "DELETE From i18NCulture output deleted.name Where name = @name";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@name", System.Data.SqlDbType.VarChar, culture.Name),
        };
        List<Dictionary<string, string>> deleted = executor.LoadData(delete_culture_query, parameters);
        return deleted is not null ? culture : null;
    }

    public static Translation CreateTranslation(Translation translation)
    {
        QueryExecutor executor = new QueryExecutor(DB.Instance);
        string create_culture_entry_query = "exec CreateCultureEntry @key";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@key", System.Data.SqlDbType.VarChar, translation.Key)
        };
        List<Dictionary<string, string>> inserted = executor.LoadData(create_culture_entry_query, parameters);
        return inserted is not null ? translation : null;
    }

    public static Translation UpdateTranslation(Translation translation)
    {
        QueryExecutor executor = new QueryExecutor(DB.Instance);
        string update_culture_entry_query = "exec UpdateCultureEntry @key, @culture, @value";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@key", System.Data.SqlDbType.VarChar, translation.Key),
            executor.GenerateParameter("@culture", System.Data.SqlDbType.VarChar, translation.Culture.Name),
            executor.GenerateParameter("@value", System.Data.SqlDbType.VarChar, translation.Value),
        };
        List<Dictionary<string, string>> updated = executor.LoadData(update_culture_entry_query, parameters);
        return updated is not null ? translation : null;
    }

    public static async Task<List<Translation>> UpdateTranslationsAsync(List<Translation> translations)
    {
        QueryExecutor executor = new QueryExecutor(DB.Instance);
        Dictionary<string, List<SqlParameter>> actions = new Dictionary<string, List<SqlParameter>>();
        for (int i = 0; i < translations.Count; i++)
        {
            var translation = translations[i];
            actions.Add($"exec UpdateCultureEntry @key{i}, @culture{i}, @value{i}", new List<SqlParameter>
            {
            executor.GenerateParameter($"@key{i}", System.Data.SqlDbType.VarChar, translation.Key),
            executor.GenerateParameter($"@culture{i}", System.Data.SqlDbType.VarChar, translation.Culture.Name),
            executor.GenerateParameter($"@value{i}", System.Data.SqlDbType.VarChar, translation.Value),
            });
        }
        await executor.TransactionAsync(actions);
        return translations;
    }
}
