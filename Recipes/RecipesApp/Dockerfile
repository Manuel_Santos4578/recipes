#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["DataContracts/DataContracts.csproj", "DataContracts/"]
COPY ["RecipesApp/RecipesApp.csproj", "RecipesApp/"]
COPY ["RecipesLibrary/RecipesLibrary.csproj", "RecipesLibrary/"]
COPY ["ModelsLibrary/ModelsLibrary.csproj", "ModelsLibrary/"]
RUN dotnet restore "RecipesApp/RecipesApp.csproj"
COPY . .
WORKDIR "/src/RecipesApp"
RUN dotnet build "RecipesApp.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "RecipesApp.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .

RUN rm -Rf /src

ENTRYPOINT ["dotnet", "RecipesApp.dll"]