﻿using DataContracts.Entities;
using ModelsLibrary.JsonConverters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace ModelsLibrary.Models;

public class User : IUser
{
    [JsonProperty("user_id")]
    public int ID { get; set; }
    [JsonProperty("user_name")]
    public string Name { get; set; }
    [JsonProperty("user_username")]
    public string Username { get; set; }
    [JsonProperty("user_blocked_at")]
    public DateTime? BlockedAt { get; set; }
    [JsonProperty("prefered_culture")]
    public string PreferedCulture { get; set; }
    [JsonProperty("roles", NullValueHandling = NullValueHandling.Ignore)]
    [JsonConverter(typeof(CollectionJsonConverter<IRole, List<Role>>))]
    public IList<IRole> Roles { get; set; }
}
