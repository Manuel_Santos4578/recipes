USE [Recipes]
GO
CREATE view Recipes_View as
select Recipe.id,
Recipe.name,
Recipe.friendly_url,
Recipe.recipe_text,
Category.id as 'category_id',
Category.name as 'category_name',
Category.friendly_url as 'category_url',
RecipeSource.id as 'recipe_source_id',
RecipeSource.name as 'recipe_source_name',
RecipeSource.friendly_url as 'recipe_source_friendly_url',
Author.id as 'author_id',
Author.name as 'author_name',
Author.friendly_url as 'author_url',
Recipe.country_id,
Country.name as 'country_name',
Country.friendly_url as 'country_url',
Country.region_id,
Region.name as 'region_name',
Region.friendly_url as 'region_friendly_url',
Recipe.created_at,
Recipe.updated_at,
Recipe.deleted_at,
RecipeImages.id as 'image_id',
RecipeImages.image,
RecipeImages.image_base_64,
RecipeImages.content_type,
RecipeImages.is_display
From Recipe
inner join Author on Author.id = Recipe.author_id
inner join RecipeSource on RecipeSource.id = Recipe.recipe_source_id
inner join Category on Category.id = Recipe.category_id
inner join Country on Country.id = Recipe.country_id
inner join Region on Country.region_id = Region.id
inner join RecipeImages on Recipe.id = RecipeImages.recipe_id
GO
CREATE view [dbo].[ApiUserView] as 
Select ApiUser.*, ApiRoles.name as 'role_name'
From ApiUser inner join ApiRoles on ApiUser.role_id = ApiRoles.id

