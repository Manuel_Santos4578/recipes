﻿using Util;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using RecipesLibrary.Util;
using ModelsLibrary.Models;
using RecipesApi.Configuration;
using Recipes.Database.Repositories;

namespace RecipesApi.Controllers;

/// <summary>
/// Category
/// </summary>
[Produces("application/json")]
[Route("api/categories")]
[Authorize(Roles = $"{ConfigurationState.ApiRoles.Write}, {ConfigurationState.ApiRoles.Root}")]
public class CategoryController : BaseApiController
{
    public CategoryController() : base()
    {

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="name"></param>
    /// <param name="base_category"></param>
    /// <param name="count"></param>
    /// <param name="page"></param>
    /// <param name="size"></param>
    /// <returns></returns>
    [HttpGet]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public IActionResult Index(string name, int? base_category, bool count = false, int page = 1, int size = 0)
    {
        try
        {
            if (count)
            {
                return Ok(new { count = CategoryRepository.ReadCount(name, base_category) });
            }
            return Ok(CategoryRepository.ReadAll(name, base_category, page, size));
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Select By slug
    /// </summary>
    /// <param name="friendly_url">friendly url</param>
    /// <returns>Category collection</returns>
    [HttpGet("{friendly_url}")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public ActionResult<Category> Show(string friendly_url, bool with_trashed = false)
    {
        try
        {
            return Ok(CategoryRepository.ReadBySlug(friendly_url, with_trashed));
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Insert
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <returns>id</returns>
    [HttpPost]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public async Task<IActionResult> StoreAsync()
    {
        try
        {
            Category category = await Request.Body.ReadObject<Category>();
            category.FriendlyUrl = Slug.GenerateSlug(category.Name);
            category.FriendlyUrl = Validator.ValidateSlug(category.FriendlyUrl, CategoryRepository.ReadBySlug);
            category = CategoryRepository.Store(category);
            int status = category is not null ? 201 : 200;
            return StatusCode(status, category);
        }
        catch (Exception)
        {
            throw;
        }
    }
    /// <summary>
    /// Bulk Store method
    /// </summary>
    /// <returns></returns>
    [HttpPost("bulk")]
    [ProducesResponseType(500)]
    [ProducesResponseType(201)]
    [ProducesResponseType(401)]
    public async Task<IActionResult> BulkStoreAsync()
    {
        IEnumerable<Category> categories = await StreamParser.ReadObject<IEnumerable<Category>>(Request.Body);
        categories = CategoryRepository.BulkStore(categories);
        return Ok(categories);
    }

    /// <summary>
    /// Update
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <param name="id">PK</param>
    /// <returns>id</returns>
    [HttpPut("{id:int}")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public async Task<IActionResult> UpdateAsync(int id)
    {
        try
        {
            Category category = await Request.Body.ReadObject<Category>();
            string slug = Slug.GenerateSlug(category.Name);
            if (slug != category.FriendlyUrl)
            {
                category.FriendlyUrl = Validator.ValidateSlug(slug, CategoryRepository.ReadBySlug);
            }
            category.ID = id;
            category = CategoryRepository.Update(category);
            return Ok(category);
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Delete
    /// </summary>
    /// <param name="id">PK</param>
    /// <returns>id</returns>
    [HttpDelete("{id:int}")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public IActionResult Delete(int id)
    {
        try
        {
            Category category = new Category { ID = id };
            category = CategoryRepository.SoftDelete(category);
            return Ok(category);
        }
        catch (Exception)
        {
            throw;
        }
    }
}
