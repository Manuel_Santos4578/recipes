﻿using System.ComponentModel.DataAnnotations;

namespace RecipesApp.Models.FormModels;

public sealed class AuthFormModel
{
    [Required(ErrorMessage = "Campo obrigatório")]
    public string Username { get; set; }
    [Required(ErrorMessage = "Campo obrigatório"), DataType(DataType.Password)]
    public string Password { get; set; }
}
