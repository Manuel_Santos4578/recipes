﻿using DataContracts.Entities;

namespace DataContracts.Access;

public partial interface IIngredientDataAccess
{
    Task<bool> DeleteIngredientAsync(IIngredient ingredient);
    Task<IIngredient> InsertIngredientAsync(IIngredient ingredient);
    Task<IEnumerable<IIngredient>> SelectAllIngredientsAsync(string name = "", int region = 0, int country = 0, int page = 0, int size = 0);
    Task<IIngredient> SelectIngredientBySlugAsync(string slug);
    Task<int> SelectIngredientCountAsync(string name = "", int region = 0, int country = 0);
    Task<IIngredient> UpdateIngredientAsync(IIngredient ingredient);
}
