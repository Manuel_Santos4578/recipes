async function LoadData() {
    let form = document.getElementById('search');
    let page = form['page'].value;
    let name = form['n'].value;
    let size = form['size'].options[form['size'].selectedIndex].value;
    let http_response = await fetch(`${location.origin}/categories`, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json;charset=UTF-8',
        },
        body: JSON.stringify({
            page: page,
            name: name,
            size: size
        }),
    });
    let response = await http_response.json();
    await FillTable(response);
}

async function FillTable(response) {
    if (response.categories.length === 0) {
        let keys = [
            "No records were found"
        ]
        let translations = await GetTranslations(keys);
        document.getElementById('table_div').innerHTML = `
        <div class="d-flex justify-content-center align-content-center h-100">
            <p class="text">${translations["No records were found"]}</p>
        </div>`;
        return;
    }
    let keys = [
        "Name", "Edit"
    ]
    let translations = await GetTranslations(keys);
    let table_html = `
        <table class="table table-hover w-100">
            <thead id="thead">
                <tr>
                    <th>${translations["Name"]}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody id="tbody">{body}</tbody>
            <tfoot id="tfoot">{footer}</tfoot>
        </table>`;
    let table_body = []
    let table_footer = []
    for (let category of response.categories) {
        table_body.push(`
            <tr>
                <td><a class="text-color" href="/categories/show/${category.friendlyUrl}">${category.name}</a></td>
                <td class="d-flex justify-content-end">
                    <div class="ml-auto">
                        <button class="btn btn-sm btn-primary edit" data-slug="${category.friendlyUrl}">${translations["Edit"]}</button>
                        <button class="btn btn-sm btn-danger delete" data-slug="${category.friendlyUrl}"><i class="fa fa-trash"></i></button>
                    </div>                
                </td>
            </tr>`);
    }
    if (response.totalPages > 1) {
        let start = response.currentPage >= 3 ? response.currentPage - 2 : 1;
        let end = response.totalPages > response.currentPage + 2 ? response.currentPage + 2 : response.totalPages;
        table_footer.push(`
            <tr>
                <td colspan="2">
                    <nav>
                        <ul class="pagination justify-content-end">`);
        if (response.currentPage > 1 && start > 1) {
            table_footer.push(`
                            <li class="page-item">
                                <a href="#" class="page-link index_page" data-page="1">1</a>
                            </li >`);
        }
        if (start > 2) {
            table_footer.push(`
                            <li class="page-item disabled">
                                <a href="#" class="page-link index_page" data-page="">...</a>
                            </li >`);
        }
        for (let i = start; i <= end; i++) {
            table_footer.push(`
                            <li class="page-item ${i == response.currentPage ? "active" : ""}">
                                <a href="#" class="page-link index_page" data-page="${i}">${i}</a>
                            </li>`);
        }
        if (end < response.totalPages - 1) {
            table_footer.push(`
                            <li class="page-item disabled">
                                <a href="#" class="page-link index_page" data-page="">...</a>
                            </li >`);
        }
        if (end < response.totalPages) {
            table_footer.push(`
                            <li class="page-item" >
                                <a href="#" class="page-link index_page" data-page="${response.totalPages}">${response.totalPages}</a>
                            </li >`);
        }
        table_footer.push(`
                    </ul>
                </td>
            </tr>`);
    }
    table_html = table_html.replace('{body}', table_body.join('')).replace('{footer}', table_footer.join(''))
    document.getElementById('table_div').innerHTML = table_html;
    AddEditEvent();
    AddDeleteEvent();
    AddPageEvent();
}

function AddNameTextBoxEvent() {
    document.getElementById('name').addEventListener('input', debounce(async () => {
        document.getElementById('page').value = 1;
        await LoadData();
    }))
}
document.addEventListener('DOMContentLoaded', async function () {
    await LoadData();
    AddTextBoxEvent('name');
    AddPageSizeEvent();
})