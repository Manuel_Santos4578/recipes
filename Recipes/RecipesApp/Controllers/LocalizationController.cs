﻿using DataContracts.Localization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ModelsLibrary.Localization;
using RecipesApp.Models.FormModels;
using RecipesApp.Services;
using RecipesLibrary.Util;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using I18N = ModelsLibrary.Constants.AppConstants.I18N;

namespace RecipesApp.Controllers;

[Authorize]
[Route("localization")]
public class LocalizationController : MyBaseController
{
    public LocalizationController(IConfiguration config, IWebHostEnvironment env) : base(config, env) { }

    #region Translations

    [HttpPost("translations")]
    public async Task<IActionResult> Translations()
    {
        string[] keys = await Request.Body.ReadObject<string[]>();
        LocalizationCache tran = await CacheManagementService.TranslationsAsync();
        string culture = Thread.CurrentThread.CurrentCulture.IetfLanguageTag;

        Dictionary<string, string> result = new Dictionary<string, string>();
        foreach (var key in keys)
        {
            if (!string.IsNullOrWhiteSpace(key) && tran[culture].ContainsKey(key))
            {
                result.TryAdd(key, tran[culture][key]);
            }
        }
        return Ok(result);
    }

    [HttpGet("translations/create")]
    public IActionResult CreateTranslationKey()
    {
        return View(new TranslationKeyFormModel());
    }

    [HttpPost("translations/store")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> StoreTranslationKeyAsync(TranslationKeyFormModel model)
    {
        CultureInfo info = new CultureInfo(Thread.CurrentThread.CurrentCulture.IetfLanguageTag);
        ITranslation translation = new Translation { Key = model.Key, Culture = new Culture { Name = info.Name, DisplayName = info.EnglishName }, Value = "" };
        translation = await dal.InsertTranslationKeyAsync(translation);
        if (translation is null)
        {
            TempData["Error"] = string.Format(base.translation[I18N.Failed_creation_of], model.Key);
        }
        else
        {
            await CacheManagementService.RefreshCulturesAsync();
            await CacheManagementService.RefreshTranslationsAsync();
            TempData["Success"] = string.Format(base.translation[I18N.Successfully_created], model.Key);
        }
        return Redirect(Request.Headers["Referer"].ToString());
    }

    [HttpGet("translations/{culture}/manage")]
    public async Task<IActionResult> ManageTranslationsAsync(string culture)
    {
        LocalizationCache translations = await CacheManagementService.TranslationsAsync();
        if (!translations.ContainsKey(culture))
        {
            TempData["Error"] = string.Format(translation[I18N.Language_does_not_exist], culture);
            return RedirectToAction("Index", "Localization");
        }

        TranslationManagementFormModel model = new TranslationManagementFormModel
        {
            CultureInfo = new CultureInfo(culture),
            Translations = translations[culture],
        };
        return View(model);
    }

    [HttpPost("translations/{culture}/manage")]
    public async Task<IActionResult> UpdateTranslationsAsync(TranslationManagementFormModel model, string culture)
    {
        LocalizationCache translations = await CacheManagementService.TranslationsAsync();
        Dictionary<string, string> current_translations = translations[culture];
        List<Translation> _translations = new List<Translation>();
        Culture _culture = new Culture { Name = culture, DisplayName = "" };
        foreach (var form_item in Request.Form)
        {
            if (current_translations.ContainsKey(form_item.Key))
            {
                current_translations[form_item.Key] = form_item.Value;
                _translations.Add(new Translation { Key = form_item.Key, Value = form_item.Value, Culture = _culture });
            }
        }
        IEnumerable<ITranslation> result = await dal.UpdateTranslationsAsync(_translations);
        if (result is null)
        {
            TempData["Error"] = string.Format(translation[I18N.Failed_to_update_translations_for], culture);
        }
        else
        {
            TempData["Success"] = string.Format(translation[I18N.Successfully_updated_translations_for], culture);
        }
        return RedirectToAction("Index", "Culture");
    }
    #endregion
}
