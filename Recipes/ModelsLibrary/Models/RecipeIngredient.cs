﻿using DataContracts.Entities;
using Newtonsoft.Json;

namespace ModelsLibrary.Models;

public class RecipeIngredient : Simple, IRecipeIngredient
{
    [JsonProperty("ingredient")]
    [JsonConverter(typeof(JsonConverters.JsonConverter<Ingredient>))]
    public IIngredient Ingredient { get; set; }
    [JsonProperty("quantity")]
    public decimal Quantity { get; set; }
    [JsonProperty("unit")]
    public string Unit { get; set; }
    public RecipeIngredient()
    {
        Ingredient = new Ingredient();
    }
}
