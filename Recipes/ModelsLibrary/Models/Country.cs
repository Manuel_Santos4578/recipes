﻿using DataContracts.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System;

namespace ModelsLibrary.Models;

public class Country : Simple, ICountry
{
    [JsonProperty("country_id")]
    public int ID { get; set; }
    [JsonProperty("country_name")]
    public string Name { get; set; }
    [JsonProperty("country_friendly_url")]
    public string FriendlyUrl { get; set; }
    [JsonProperty("region", NullValueHandling = NullValueHandling.Ignore)]
    [JsonConverter(typeof(JsonConverters.JsonConverter<Region>))]
    public IRegion Region { get; set; }

    public static DataTable ExportToDataTable(IEnumerable<ICountry> countries)
    {
        DataTable table = new DataTable();
        table.Columns.AddRange(new DataColumn[]
        {
            new DataColumn(nameof(ICountry.Name), typeof(string)),
            new DataColumn($"{nameof(ICountry.Region)}.{nameof(ICountry.Region.Name)}", typeof(string)),
            new DataColumn(nameof(ICountry.CreatedAt), typeof(DateTime)),
            new DataColumn(nameof(ICountry.UpdatedAt), typeof(DateTime)),
        });

        foreach (ICountry country in countries)
        {
            table.Rows.Add(
                country.Name,
                country.Region?.Name ?? "---",
                country.CreatedAt,
                country.UpdatedAt);
        }
        return table;
    }
}
