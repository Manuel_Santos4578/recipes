﻿using DataContracts.Entities;
using Newtonsoft.Json;

namespace ModelsLibrary.Models;

public class RecipeUser : IRecipeUser
{
    [JsonProperty("recipe_id")]
    public int RecipeId { get; set; }
    [JsonProperty("is_creator")]
    public bool IsCreator { get; set; }
    [JsonProperty("user")]
    [JsonConverter(typeof(JsonConverters.JsonConverter<User>))]
    public IUser User { get; set; }
    public RecipeUser()
    {
        User = new User();
    }
}