
use Recipes
GO

insert into Roles (name) 
Values 
('Admin'),
('User')
GO

insert into UserRoles (user_id, role_id) 
VALUES ((Select id from Users Where username like 'admin'), (Select id from Roles where name like 'admin'))
GO