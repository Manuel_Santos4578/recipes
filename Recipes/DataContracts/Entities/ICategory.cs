﻿namespace DataContracts.Entities;

public interface ICategory : ISimple
{
    int ID { get; set; }
    string Name { get; set; }
    string FriendlyUrl { get; set; }
    ICategory BaseCategory { get; set; }
}
