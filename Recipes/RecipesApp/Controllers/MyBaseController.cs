﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DataContracts.Access;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ModelsLibrary.Constants;
using ModelsLibrary.Localization;
using RecipesApp.Services;

namespace RecipesApp.Controllers;

public abstract class MyBaseController : Controller
{
    readonly protected IConfiguration _config;
    readonly protected IWebHostEnvironment _env;
    readonly protected IDataAccess dal;
    readonly protected LocalizationCache __t;
    readonly protected Dictionary<string,string> translation;
    public MyBaseController(IConfiguration configuration, IWebHostEnvironment environment)
    {
        _env = environment;
        _config = configuration;
        dal = new DataAccess.DataAccess(Environment.GetEnvironmentVariable(AppConstants.RecipesAppEnv.ApiDomain).ToString()); 
        __t = CacheManagementService.TranslationsAsync().Result;
        translation = __t[Thread.CurrentThread.CurrentCulture.IetfLanguageTag];
    }
    protected void HandleViewData()
    {
        ViewData["Success"] = TempData["Success"];
        ViewData["Error"] = TempData["Error"];
        TempData["Success"] = TempData["Error"] = null;
    }

    public async Task ApiAuthAsync()
    {
        await dal.ApiAuthenticateAsync();
    }
}