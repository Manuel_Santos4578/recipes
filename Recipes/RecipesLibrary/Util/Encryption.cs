﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Util;

/// <summary>
/// Password Utility Class
/// </summary>
public static class Encryption
{
    /// <summary>
    /// Encrypts password
    /// </summary>
    /// <param name="valor">plain text password</param>
    /// <returns>hashed password</returns>
    public static string EncryptPass(string valor)
    {
        const string SharedSecret = "LockAndKey";
        if (string.IsNullOrEmpty(valor))
            return string.Empty;
        valor += SharedSecret;
        byte[] data = Encoding.ASCII.GetBytes(valor);
        data = SHA512.Create().ComputeHash(data);
        return Convert.ToHexString(data);
    }

    /// <summary>
    /// Generates random string for password usage
    /// </summary>
    /// <param name="length">password length</param>
    /// <returns>randomly generated string</returns>
    public static string RandomStringGen(int length = 0)
    {
        Random random = new Random();
        length = length != 0 ? random.Next(10, 20) : length;
        const string AllowedChars = "0123456789QAZXSWEDCVFRTGBNHYUJMKIOLPqazxswedcvfrtgbnhyujmkiolp#$%&_+-";
        string rnd = "";
        for (int i = 0; i < length; i++)
        {
            System.Threading.Thread.Sleep(50);
            byte index = (byte)random.Next(0, AllowedChars.Length);
            rnd += AllowedChars[index];
        }
        return rnd;
    }
}
