﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using RecipesApp.Services;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Threading;
using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Configuration;
using System.Linq;
using RecipesApp.Models;
using System.Globalization;
using Microsoft.AspNetCore.Authorization;
using RecipesApp.Models.FormModels;
using DataContracts.Localization;
using ModelsLibrary.Localization;
using ModelsLibrary.Constants;
using I18N = ModelsLibrary.Constants.AppConstants.I18N;

namespace RecipesApp.Controllers;

[Authorize]
[Route("cultures")]
public class CultureController : MyBaseController
{
    private readonly IOptions<RequestLocalizationOptions> _requestLocalizationOptions;
    private readonly IOptionsMonitor<RequestLocalizationOptions> _requestLocalizationOptionsMonitor;

    public CultureController(
        IConfiguration config,
        IWebHostEnvironment env,
        IOptions<RequestLocalizationOptions> requestLocalizationOptions,
        IOptionsMonitor<RequestLocalizationOptions> requestLocalizationOptionsMonitor) : base(config, env)
    {
        _requestLocalizationOptions = requestLocalizationOptions;
        _requestLocalizationOptionsMonitor = requestLocalizationOptionsMonitor;
    }
    #region Localization Management
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> SetCultureAsync(string culture)
    {
        var cultures = await CacheManagementService.CulturesAsync();
        if (cultures.FirstOrDefault(cul => cul.Name == culture) is not null)
        {
            await dal.ApiAuthenticateAsync();
            string username = User.FindFirst(ClaimTypes.Name).Value;
            await dal.ChangePreferedCultureAsync(username, culture);
            Response.Cookies.Delete(AppConstants.CookieNames.CULTURE);
            Response.Cookies.Append(
             CookieRequestCultureProvider.DefaultCookieName,
             CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
             new CookieOptions { Expires = DateTime.Now.AddYears(1) });
        }
        else
        {
            TempData["Error"] = string.Format(translation["Unsupported culture"], culture);
        }
        return Redirect(Request.Headers["Referer"].ToString());
    }

    [HttpPost("refresh")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> RefreshCultureDataAsync()
    {
        await Startup.RefreshCulturesAsync(_requestLocalizationOptions, _requestLocalizationOptionsMonitor);
        TempData["Success"] = translation[I18N.Data_refreshed_successfully];
        return Redirect(Request.Headers["Referer"]);
    }

    [HttpGet("check")]
    public IActionResult CheckCulture()
    {
        return Ok(Thread.CurrentThread.CurrentCulture.IetfLanguageTag);
    }
    #endregion

    [HttpGet]
    public async Task<IActionResult> IndexAsync(string name)
    {
        LocalizationViewModel model = new LocalizationViewModel
        {
            Cultures = (await dal.SelectCulturesAsync(name)).Select(cul => new CultureInfo(cul.Name)),
        };
        HandleViewData();
        return View(model);
    }

    [HttpGet("create")]
    public IActionResult Create()
    {
        return View(new CultureFormModel());
    }

    [HttpPost("store")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> StoreAsync(CultureFormModel model)
    {
        await dal.ApiAuthenticateAsync();
        CultureInfo info = new CultureInfo(model.Name);
        ICulture culture = new Culture() { Name = info.Name, DisplayName = info.EnglishName };
        culture = await dal.InsertCultureAsync(culture);
        if (culture == null)
        {
            TempData["Error"] = string.Format(translation[I18N.Failed_creation_of], model.Name);
        }
        else
        {
            await Startup.RefreshCulturesAsync(_requestLocalizationOptions, _requestLocalizationOptionsMonitor);
            TempData["Success"] = string.Format(translation[I18N.Successfully_created], model.Name);
        }
        return RedirectToAction(actionName: "Index", "Culture");
    }

    [HttpGet("delete/{culture}")]
    public IActionResult Delete(string culture)
    {
        CultureFormModel model = new CultureFormModel()
        {
            Name = culture
        };
        return View(model);
    }

    [HttpPost("destroy")]
    public async Task<IActionResult> DestroyAsync(CultureFormModel model)
    {
        if (_requestLocalizationOptions.Value.DefaultRequestCulture.Culture.Name == model.Name)
        {
            TempData["Error"] = string.Format(translation[I18N.Cannot_delete_default_language]);
            return Redirect(Request.Headers["Referer"].ToString());
        }
        var culture = await dal.DeleteCultureAsync(new Culture { Name = model.Name, DisplayName = "" });
        await CacheManagementService.RefreshCulturesAsync();
        if (culture is null)
        {
            TempData["Error"] = string.Format(translation[I18N.Failed_to_delete], model.Name);
        }
        else
        {
            TempData["Success"] = string.Format(translation[I18N.Successfully_deleted], model.Name);
        }
        return Redirect(Request.Headers["Referer"].ToString());
    }
}
