﻿using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace RecipesLibrary.Util;

/// <summary>
/// Class for Generating Slug strings
/// </summary>
public static class Slug
{
    /// <summary>
    ///  Generates Slug string
    /// </summary>
    /// <param name="phrase">string to process</param>
    /// <returns>slug string</returns>
    public static string GenerateSlug(this string phrase)
    {
        string str = phrase.RemoveDiacritics().ToLower();
        // invalid chars           
        str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
        // convert multiple spaces into one space   
        str = Regex.Replace(str, @"\s+", " ").Trim();
        // cut and trim 
        str = Regex.Replace(str, @"\s", "-"); // hyphens   
        return str;
    }
    /// <summary>
    /// Remove special characters
    /// </summary>
    /// <param name="text">string to process</param>
    /// <returns>string without special characters</returns>
    private static string RemoveDiacritics(this string text)
    {
        var s = new string(text.Normalize(NormalizationForm.FormD)
            .Where(c => CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
            .ToArray());

        return s.Normalize(NormalizationForm.FormC);
    }
}
