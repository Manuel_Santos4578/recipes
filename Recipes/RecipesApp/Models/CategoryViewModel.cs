﻿using System.Collections.Generic;
using System.Linq;
using DataContracts.Entities;
using ModelsLibrary.Models;

namespace RecipesApp.Models;

public sealed class CategoryViewModel : ViewModel
{

    public IEnumerable<ICategory> Categories { get; set; }
    public ICategory Category { get; set; }
    public List<Recipe> CategoryRecipes { get; set; }
    public int CategoryRecipesCount { get; set; }
    public int RecipesCurrentPage { get; set; }
    public CategoryViewModel()
    {
        Categories = new List<Category>();
        CategoryRecipes = new List<Recipe>();
        Category = new Category();
    }
}
