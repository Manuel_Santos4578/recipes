﻿using DataContracts.Entities;
using DataContracts.Localization;
using ModelsLibrary.Localization;
using ModelsLibrary.Models;
using Newtonsoft.Json;

namespace RecipesLibrary.Tests.UtilityLibrary;

public class SerializationDeserializationTest
{
    static JsonSerializerSettings _settings = new JsonSerializerSettings { DateFormatString = "dd/MM/yyyy HH:mm:ss" };

    [Fact]
    public void CategoriesDeserialization()
    {
        string api_response = @"
        [
          {
            ""category_id"": 1,
            ""category_name"": ""doces"",
            ""category_friendly_url"": ""doces"",
            ""created_at"": ""0001-01-01T00:00:00"",
            ""updated_at"": ""0001-01-01T00:00:00"",
            ""deleted_at"": null
          },
          {
            ""category_id"": 2,
            ""category_name"": ""salgados"",
            ""category_friendly_url"": ""salgados"",
            ""created_at"": ""0001-01-01T00:00:00"",
            ""updated_at"": ""0001-01-01T00:00:00"",
            ""deleted_at"": null
          },
          {
            ""category_id"": 3,
            ""category_name"": ""bolos"",
            ""category_friendly_url"": ""bolos"",
            ""created_at"": ""0001-01-01T00:00:00"",
            ""updated_at"": ""0001-01-01T00:00:00"",
            ""deleted_at"": null
          },
          {
            ""category_id"": 5,
            ""category_name"": ""testmerge"",
            ""category_friendly_url"": ""testmerge"",
            ""created_at"": ""0001-01-01T00:00:00"",
            ""updated_at"": ""0001-01-01T00:00:00"",
            ""deleted_at"": null
          },
          {
            ""category_id"": 6,
            ""category_name"": ""testmerge2"",
            ""category_friendly_url"": ""testmerge2"",
            ""created_at"": ""0001-01-01T00:00:00"",
            ""updated_at"": ""0001-01-01T00:00:00"",
            ""deleted_at"": null
          }
        ]";
        IEnumerable<ICategory>? categories = JsonConvert.DeserializeObject<IEnumerable<Category>>(api_response, _settings);
        Assert.NotNull(categories);
        Assert.NotEmpty(categories);
    }

    [Fact]
    public void CategoryDeserialization()
    {
        string api_response = @"
        {
            ""category_id"": 3,
            ""category_name"": ""bolos"",
            ""category_friendly_url"": ""bolos"",
            ""base_category"": {
                ""category_id"": 1,
                ""category_name"": ""doces"",
                ""category_friendly_url"": ""doces"",
                ""created_at"": ""01/01/0001 00:00:00"",
                ""updated_at"": ""01/01/0001 00:00:00"",
                ""deleted_at"": null
            },
            ""created_at"": ""26/01/2023 00:57:16"",
            ""updated_at"": ""28/03/2023 08:58:39"",
            ""deleted_at"": null
        }";

        string api_response_2 = @"
        {
            ""category_id"": 3,
            ""category_name"": ""bolos"",
            ""category_friendly_url"": ""bolos"",
            ""base_category"": null,
            ""created_at"": ""26/01/2023 00:57:16"",
            ""updated_at"": ""28/03/2023 08:58:39"",
            ""deleted_at"": null
        }";
        ICategory? categoryWithBase = JsonConvert.DeserializeObject<Category>(api_response, _settings);
        Assert.NotNull(categoryWithBase);
        Assert.NotNull(categoryWithBase?.BaseCategory);

        ICategory? categoryWithoutBase = JsonConvert.DeserializeObject<Category>(api_response_2, _settings);
        Assert.NotNull(categoryWithoutBase);
        Assert.Null(categoryWithoutBase?.BaseCategory);

    }

    [Fact]
    public void CategorySerialization()
    {
        ICategory category = new Category
        {
            ID = 1,
            Name = "Test",
            FriendlyUrl = "test",
            BaseCategory = null,
            CreatedAt = DateTime.Now,
            UpdatedAt = DateTime.Now
        };

        ICategory category1 = new Category
        {
            ID = 2,
            Name = "Test1",
            FriendlyUrl = "test1",
            BaseCategory = category,
            CreatedAt = DateTime.Now,
            UpdatedAt = DateTime.Now
        };

        string serializedCategoryWithoutBase = JsonConvert.SerializeObject(category, _settings);
        Assert.False(string.IsNullOrEmpty(serializedCategoryWithoutBase));
        string serializedCategoryWithBase = JsonConvert.SerializeObject(category1, _settings);
        Assert.False(string.IsNullOrEmpty(serializedCategoryWithBase));
    }

    [Fact]
    public void RecipesDeserializationTest()
    {
        IRecipe recipe = new Recipe
        {
            ID = 2,
            Name = "recipe1",
            FriendlyUrl = "recipe1",
            Text = "<b>a</b>",
            Author = new Author { ID = 1, Name = "Test", FriendlyUrl = "test", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now },
            Country = new Country { ID = 1, Name = "Test", FriendlyUrl = "test", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now },
            Source = new Source { ID = 1, Name = "Test", FriendlyUrl = "test", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now },
            Ingredients = new List<IRecipeIngredient>(),
            Courses = new List<ICourse>(),
            MainImage = null,
            MiscImages = null,
            IsFavorite = false,
            CreatedAt = DateTime.Now,
            UpdatedAt = DateTime.Now,
            Category = new Category { ID = 1, Name = "Test", FriendlyUrl = "test", BaseCategory = null, CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now },

        };
        string serializedRecipe = JsonConvert.SerializeObject(recipe, _settings);
        Assert.False(string.IsNullOrEmpty(serializedRecipe));

        IRecipe recipe_2 = new Recipe
        {
            ID = 2,
            Name = "recipe1",
            FriendlyUrl = "recipe1",
            Text = "<b>a</b>",
            Author = new Author { ID = 1, Name = "Test", FriendlyUrl = "test", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now },
            Country = new Country { ID = 1, Name = "Test", FriendlyUrl = "test", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now },
            Source = new Source { ID = 1, Name = "Test", FriendlyUrl = "test", CreatedAt = DateTime.Now, UpdatedAt = DateTime.Now },
            Ingredients = new List<IRecipeIngredient>(),
            Courses = new List<ICourse>(),
            MainImage = null,
            MiscImages = null,
            IsFavorite = false,
            CreatedAt = DateTime.Now,
            UpdatedAt = DateTime.Now,
            Category = new Category
            {
                ID = 1,
                Name = "Test",
                FriendlyUrl = "test",
                BaseCategory = new Category
                {
                    ID = 2,
                    Name = "Base",
                    FriendlyUrl = "base",
                    BaseCategory = null
                },
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            },
        };

        string serializedRecipe2 = JsonConvert.SerializeObject(recipe_2, _settings);
        Assert.False(string.IsNullOrEmpty(serializedRecipe2));
    }

    [Fact]
    public void RecipeDeserializationTest()
    {
        string recipeString = @"
        {
            ""recipe_id"": 1,
            ""recipe_name"": ""recipe 1"",
            ""recipe_friendly_url"": ""recipe-1"",
            ""recipe_text"": ""<div>\r\n<div><strong>Prep Time:</strong></div>\r\n<div>10 mins</div>\r\n</div>\r\n<div>\r\n<div><strong>Cook Time:</strong></div>\r\n<div>15 mins</div>\r\n</div>\r\n<div>\r\n<div><strong>Additional Time:</strong></div>\r\n<div>5 mins</div>\r\n</div>\r\n<div>\r\n<div><strong>Total Time:</strong></div>\r\n<div>30 mins</div>\r\n</div>\r\n<div>\r\n<div><strong>Servings:</strong></div>\r\n<div>4</div>\r\n<div>&nbsp;</div>\r\n</div>\r\n<h3 id=\""mntl-sc-block_1-0-10\"" class=\""comp mntl-sc-block allrecipes-sc-block-subheading mntl-sc-block-subheading\"" style=\""box-sizing: border-box; margin: 3rem 0px 1rem; padding: 0px; font-family: CopperPot, Helvetica, sans-serif; font-size: 1.5rem; line-height: 2rem; text-underline-offset: 0.4375rem; color: rgba(0, 0, 0, 0.95); background-color: #ffffff;\""><span class=\""mntl-sc-block-subheading__text\"" style=\""box-sizing: border-box;\"">How to Make Chocolate Lava Cake</span></h3>\r\n<p id=\""mntl-sc-block_1-0-11\"" class=\""comp mntl-sc-block mntl-sc-block-html\"" style=\""box-sizing: border-box; margin: 0px 0px 1rem; padding: 0px; font-family: SourceSansPro, Helvetica, sans-serif; font-size: 18px; line-height: 1.75rem; text-underline-offset: 0.25rem; counter-reset: section 0; color: rgba(0, 0, 0, 0.95); background-color: #ffffff;\"">Here's a brief overview of what you can expect when you make the best lava cake of your life:</p>\r\n<div id=\""mntl-sc-block_1-0-12\"" class=\""comp mntl-sc-block mntl-sc-block-adslot mntl-block\"" style=\""box-sizing: border-box; margin: 0px; padding: 0px; color: rgba(0, 0, 0, 0.95); font-family: SourceSansPro, Helvetica, sans-serif; font-size: 18px; background-color: #ffffff;\""></div>\r\n<p id=\""mntl-sc-block_1-0-13\"" class=\""comp mntl-sc-block mntl-sc-block-html\"" style=\""box-sizing: border-box; margin: 0px 0px 1rem; padding: 0px; font-family: SourceSansPro, Helvetica, sans-serif; font-size: 18px; line-height: 1.75rem; text-underline-offset: 0.25rem; counter-reset: section 0; color: rgba(0, 0, 0, 0.95); background-color: #ffffff;\"">1. Melt the chocolate and butter together using a double boiler.<br style=\""box-sizing: border-box;\"" />2. Beat the eggs and sugar together, then add the chocolate and remaining ingredients.<br style=\""box-sizing: border-box;\"" />3. Bake until the sides are set but the center is jiggly.</p>\r\n<div id=\""mntl-sc-block_1-0-14\"" class=\""comp mntl-sc-block mntl-sc-block-adslot mntl-block\"" style=\""box-sizing: border-box; margin: 0px; padding: 0px; color: rgba(0, 0, 0, 0.95); font-family: SourceSansPro, Helvetica, sans-serif; font-size: 18px; background-color: #ffffff;\""></div>\r\n<h2 id=\""mntl-sc-block_1-0-15\"" class=\""comp mntl-sc-block allrecipes-sc-block-heading mntl-sc-block-heading\"" style=\""box-sizing: border-box; margin: 3rem 0px 1rem; padding: 0px; font-family: CopperPot, Helvetica, sans-serif; font-size: 2.25rem; line-height: 2.75rem; text-underline-offset: 0.6875rem; color: rgba(0, 0, 0, 0.95); background-color: #ffffff;\""><span class=\""mntl-sc-block-heading__text\"" style=\""box-sizing: border-box;\"">How to Serve Lava Cake</span></h2>\r\n<p id=\""mntl-sc-block_1-0-16\"" class=\""comp mntl-sc-block mntl-sc-block-html\"" style=\""box-sizing: border-box; margin: 0px 0px 1rem; padding: 0px; font-family: SourceSansPro, Helvetica, sans-serif; font-size: 18px; line-height: 1.75rem; text-underline-offset: 0.25rem; counter-reset: section 0; color: rgba(0, 0, 0, 0.95); background-color: #ffffff;\"">For a gorgeous presentation, dust these lava cakes with powdered sugar and serve them alongside bright red fruits (such as strawberries or raspberries) for a pop of color. A scoop of&nbsp;<a style=\""box-sizing: border-box; outline: 0px; color: #114388; transition: color 0.15s ease-out 0s; text-decoration-color: #114388; text-decoration-thickness: 1px; text-decoration-skip-ink: none;\"" href=\""https://www.allrecipes.com/recipe/233928/how-to-make-vanilla-ice-cream/\"" data-component=\""link\"" data-source=\""inlineLink\"" data-type=\""internalLink\"" data-ordinal=\""1\"">vanilla ice cream</a>&nbsp;and a drizzle of&nbsp;<a style=\""box-sizing: border-box; outline: 0px; color: #114388; transition: color 0.15s ease-out 0s; text-decoration-color: #114388; text-decoration-thickness: 1px; text-decoration-skip-ink: none;\"" href=\""https://www.allrecipes.com/recipe/232396/easy-homemade-chocolate-sauce/\"" data-component=\""link\"" data-source=\""inlineLink\"" data-type=\""internalLink\"" data-ordinal=\""2\"">chocolate sauce</a>&nbsp;will take things over the top.</p>\r\n<div id=\""mntl-sc-block_1-0-17\"" class=\""comp mntl-sc-block mntl-sc-block-adslot mntl-block\"" style=\""box-sizing: border-box; margin: 0px; padding: 0px; color: rgba(0, 0, 0, 0.95); font-family: SourceSansPro, Helvetica, sans-serif; font-size: 18px; background-color: #ffffff;\""></div>\r\n<h2 id=\""mntl-sc-block_1-0-18\"" class=\""comp mntl-sc-block allrecipes-sc-block-heading mntl-sc-block-heading\"" style=\""box-sizing: border-box; margin: 3rem 0px 1rem; padding: 0px; font-family: CopperPot, Helvetica, sans-serif; font-size: 2.25rem; line-height: 2.75rem; text-underline-offset: 0.6875rem; color: rgba(0, 0, 0, 0.95); background-color: #ffffff;\""><span class=\""mntl-sc-block-heading__text\"" style=\""box-sizing: border-box;\"">How to Store Lava Cake</span></h2>\r\n<p><span id=\""toc-how-to-serve-lava-cake\"" class=\""heading-toc\"" style=\""box-sizing: border-box; color: rgba(0, 0, 0, 0.95); font-family: SourceSansPro, Helvetica, sans-serif; font-size: 18px; background-color: #ffffff;\""></span><span id=\""toc-how-to-store-lava-cake\"" class=\""heading-toc\"" style=\""box-sizing: border-box; color: rgba(0, 0, 0, 0.95); font-family: SourceSansPro, Helvetica, sans-serif; font-size: 18px; background-color: #ffffff;\""></span></p>\r\n<p id=\""mntl-sc-block_1-0-19\"" class=\""comp mntl-sc-block mntl-sc-block-html\"" style=\""box-sizing: border-box; margin: 0px 0px 1rem; padding: 0px; font-family: SourceSansPro, Helvetica, sans-serif; font-size: 18px; line-height: 1.75rem; text-underline-offset: 0.25rem; counter-reset: section 0; color: rgba(0, 0, 0, 0.95); background-color: #ffffff;\"">Store the cooled lava cakes in an airtight container in the refrigerator for up to three days. We don't recommend freezing lava cakes, as it will change the texture.</p>"",
            ""is_favorite"": false,
            ""ingredients"": [
                {
                    ""ingredient"": {
                        ""ingredient_id"": 3,
                        ""ingredient_name"": null,
                        ""ingredient_friendly_url"": null,
                        ""country"": null,
                        ""created_at"": ""0001-01-01T00:00:00"",
                        ""updated_at"": ""0001-01-01T00:00:00"",
                        ""deleted_at"": null
                    },
                    ""quantity"": 1.0,
                    ""unit"": ""g"",
                    ""created_at"": ""0001-01-01T00:00:00"",
                    ""updated_at"": ""0001-01-01T00:00:00"",
                    ""deleted_at"": null
                },
                {
                    ""ingredient"": {
                        ""ingredient_id"": 7,
                        ""ingredient_name"": null,
                        ""ingredient_friendly_url"": null,
                        ""country"": null,
                        ""created_at"": ""0001-01-01T00:00:00"",
                        ""updated_at"": ""0001-01-01T00:00:00"",
                        ""deleted_at"": null
                    },
                    ""quantity"": 1.0,
                    ""unit"": ""unit"",
                    ""created_at"": ""0001-01-01T00:00:00"",
                    ""updated_at"": ""0001-01-01T00:00:00"",
                    ""deleted_at"": null
                }
            ],
            ""category"": {
                ""category_id"": 2,
                ""category_name"": null,
                ""category_friendly_url"": null,
                ""created_at"": ""0001-01-01T00:00:00"",
                ""updated_at"": ""0001-01-01T00:00:00"",
                ""deleted_at"": null
            },
            ""author"": {
                ""author_id"": 1,
                ""author_name"": null,
                ""author_friendly_url"": null,
                ""created_at"": ""0001-01-01T00:00:00"",
                ""updated_at"": ""0001-01-01T00:00:00"",
                ""deleted_at"": null
            },
            ""source"": {
                ""recipesource_id"": 2,
                ""recipesource_name"": null,
                ""recipesource_friendly_url"": null,
                ""created_at"": ""0001-01-01T00:00:00"",
                ""updated_at"": ""0001-01-01T00:00:00"",
                ""deleted_at"": null
            },
            ""country"": {
                ""country_id"": 7,
                ""country_name"": null,
                ""country_friendly_url"": null,
                ""region"": null,
                ""created_at"": ""0001-01-01T00:00:00"",
                ""updated_at"": ""0001-01-01T00:00:00"",
                ""deleted_at"": null
            },
            ""main_image"": {
                ""recipeimages_id"": 0,
                ""recipeimages_recipe_id"": 0,
                ""recipeimages_image"": null,
                ""recipeimages_image_base_64"": null,
                ""recipeimages_is_display"": false,
                ""recipeimages_content_type"": null,
                ""created_at"": ""0001-01-01T00:00:00"",
                ""updated_at"": ""0001-01-01T00:00:00"",
                ""deleted_at"": null
            },
            ""misc_images"": [],
            ""courses"": [
                {
                    ""course_id"": 3,
                    ""course_name"": null,
                    ""course_friendly_url"": null,
                    ""created_at"": ""0001-01-01T00:00:00"",
                    ""updated_at"": ""0001-01-01T00:00:00"",
                    ""deleted_at"": null
                }
            ],
            ""created_at"": ""0001-01-01T00:00:00"",
            ""updated_at"": ""0001-01-01T00:00:00"",
            ""deleted_at"": null
        }";

        IRecipe? recipe = JsonConvert.DeserializeObject<Recipe>(recipeString);
        Assert.NotNull(recipe);
    }

    [Fact]
    public void CultureDeserializationTest()
    {
        string culture_json = @"{""i18n_culture_name"":""en-GB"",""i18n_culture_display_name"":""English (United Kingdom)""}";
        ICulture? culture = JsonConvert.DeserializeObject<Culture>(culture_json);
        Assert.NotNull(culture);
    }
}