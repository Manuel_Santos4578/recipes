﻿using System.Collections.Generic;

namespace ModelsLibrary.Localization;

public sealed class LocalizationCache : Dictionary<string, Dictionary<string, string>>
{
    public LocalizationCache() : base() { }
}
