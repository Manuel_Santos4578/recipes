﻿using RecipesLibrary.Util;

namespace RecipesLibrary.Tests.UtilityLibrary;

public class SlugTest
{
    [Theory]
    [InlineData("","")]
    [InlineData("ISCH MU EINEN CRÈME BRÛLÉE HABEN", "isch-mu-einen-creme-brulee-haben")]
    public void GenerateSlugTest(string text, string expected)
    {
        string actual = Slug.GenerateSlug(text);
        Assert.Equal(expected, actual);
    }
}
