﻿document.addEventListener('DOMContentLoaded', () => {
    tinymce.init({
        selector: "textarea.tinymce",
        // skin: (localStorage.getItem('themeMode') === 'dark' ? 'oxide-dark' : 'oxide'),
        height: 500,
    })
})
