async function GetTranslations(keys) {
    let translation_result = await fetch(`${location.origin}/localization/translations`, {
        method: 'POST',
        mode: 'cors',
        body: JSON.stringify(keys),
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        },
    })
    let translations = await translation_result.json();
    return translations;
}
