﻿using DataContracts.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System;

namespace ModelsLibrary.Models;

public class Region : Simple, IRegion
{
    [JsonProperty("region_id")]
    public int ID { get; set; }
    [JsonProperty("region_name")]
    public string Name { get; set; }
    [JsonProperty("region_friendly_url")]
    public string FriendlyUrl { get; set; }

    public static DataTable ExportToDataTable(IEnumerable<IRegion> regions)
    {
        DataTable table = new DataTable();
        table.Columns.AddRange(new DataColumn[]
        {
            new DataColumn(nameof(IRegion.Name), typeof(string)),
            new DataColumn(nameof(IRegion.CreatedAt), typeof(DateTime)),
            new DataColumn(nameof(IRegion.UpdatedAt), typeof(DateTime)),
        });

        foreach (IRegion region in regions)
        {
            table.Rows.Add(
                region.Name,
                region.CreatedAt,
                region.UpdatedAt);
        }
        return table;
    }
}
