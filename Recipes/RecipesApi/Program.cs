using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.IO;

namespace RecipesApi;

public class Program
{
    const string log_template = "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{@Variables}{NewLine} {Exception}";
    public static void Main(string[] args)
    {
        string log_directory_path = Path.Combine(AppContext.BaseDirectory, "Logs");
        if (!Directory.Exists(log_directory_path))
        {
            Directory.CreateDirectory(log_directory_path);
        }

        Log.Logger = new LoggerConfiguration()
            .ReadFrom.Configuration(new ConfigurationBuilder().AddJsonFile("appsettings.json").Build())
            .WriteTo.Console()
            .WriteTo.File(log_directory_path, outputTemplate: log_template, rollingInterval: RollingInterval.Day)
            .CreateLogger();
        try
        {
            Log.Information("Application starting");
            CreateHostBuilder(args).Build().Run();
        }
        catch (Exception ex)
        {
            Log.Fatal(ex, "Application failed to start");
        }
        finally
        {
            Log.CloseAndFlush();
        }
    }

    public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .UseSerilog()
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
            });
}
