﻿using DataContracts.Entities;

namespace DataContracts.Access;

public partial interface IUserDataAccess
{
    Task<IUser> LoginAsync(string username, string password);
    Task ChangePreferedCultureAsync(string username, string culture);
}