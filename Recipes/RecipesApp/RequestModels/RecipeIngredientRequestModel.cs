﻿namespace RecipesApp.RequestModels;

public sealed class RecipeIngredientRequestModel
{
    public int ingredient_id;
    public string ingredient_name;
    public decimal amount;
    public string unit;
}
