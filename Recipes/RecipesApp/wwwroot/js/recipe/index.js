async function LoadData() {
    let form = document.getElementById('search');
    let page = form['page'].value;
    let name = form['n'].value;
    let author = form['author'].value;
    let category = form['category'].value;
    let country = form['country'].value;
    let source = form['source'].value;
    let ingredient = form['ingredient'].value;
    let size = form['size'].options[form['size'].selectedIndex].value;
    let http_response = await fetch(`${location.origin}/recipes`, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        },
        body: JSON.stringify({
            name: name,
            author: author,
            category: category,
            country: country,
            source: source,
            ingredient: ingredient,
            page: page,
            size: size
        }),
    });
    let response = await http_response.text();
    document.getElementById('table_div').innerHTML = response;
    AddDeleteEvent();
    AddPageEvent();
    FavoriteEvent();
}

function AddTextBoxEvent(elementId) {
    document.getElementById(elementId).addEventListener('input', debounce(() => {
        document.getElementById('page').value = 1;
        LoadData();
    }))
}

document.addEventListener('DOMContentLoaded', async function () {
    await LoadData();
    AddTextBoxEvent('name');
    AddTextBoxEvent('author');
    AddTextBoxEvent('category');
    AddTextBoxEvent('country');
    AddTextBoxEvent('source');
    AddTextBoxEvent('ingredient');
    AddPageSizeEvent();
})