﻿using System.ComponentModel.DataAnnotations;

namespace RecipesApp.Models.FormModels;

public class CultureFormModel
{
    [Required]
    public string Name { get; set; }
}
