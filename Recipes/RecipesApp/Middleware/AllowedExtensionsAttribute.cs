﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;

namespace RecipesApp.Middleware;

public sealed class AllowedExtensionsAttribute : ValidationAttribute
{
    private readonly string[] _extensions;
    public AllowedExtensionsAttribute(string[] extensions)
    {
        _extensions = extensions;
    }

    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    {
        if (value is IFormFile file && value is not null)
        {
            var extension = Path.GetExtension(file.FileName);
            if (!_extensions.Contains(extension.ToLower()))
            {
                return new ValidationResult(GetErrorMessage());
            }
            return ValidationResult.Success;
        }
        if (value is List<IFormFile> files)
        {
            foreach (IFormFile file_from_list in files)
            {
                if (!_extensions.Contains(Path.GetExtension(file_from_list.FileName)))
                    return new ValidationResult(GetErrorMessage());
            }
            return ValidationResult.Success;
        }
        return ValidationResult.Success;
    }

    public string GetErrorMessage()
    {
        return $"Tipo de ficheiro não permitido!";
    }
}

