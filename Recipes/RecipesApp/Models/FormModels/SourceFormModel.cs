﻿using DataContracts.Entities;
using ModelsLibrary.Models;

namespace RecipesApp.Models.FormModels;

public sealed class SourceFormModel : Source
{
    public SourceFormModel()
    {

    }
    public SourceFormModel(ISource source)
    {
        ID = source.ID;
        Name = source.Name;
        FriendlyUrl = source.FriendlyUrl;
    }
}
