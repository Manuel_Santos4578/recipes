function FavoriteEvent() {
    let favorite_buttons = document.getElementsByClassName('favorite-btn');
    for (const fav of favorite_buttons) {
        fav.addEventListener('click', OnFavoriteClick)
    }
}

async function OnFavoriteClick(e) {
    e.preventDefault();
    let id = $(this).attr('data-recipe')
    let fav = $(this).attr('data-favorite');
    let response = await fetch(`${location.origin}/recipes/favorite`, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        },
        body: JSON.stringify({
            recipe_id: id,
            favorite: fav
        })
    })
    let result = await response.json();
    this.setAttribute('data-favorite', result !== 0);
    this.innerHTML = `<i class="fa fa-heart ${result !== 0 ? "text-pink" : ""}"></i>`;
}