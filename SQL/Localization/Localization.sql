
use Recipes
GO
create table i18NCulture (
	name varchar(5) not null primary key,
	display_name varchar(50) not null unique
)
GO

create table i18N (
	[key] varchar(255) not null,
	culture varchar(5) not null foreign key references i18NCulture(name) on delete cascade,
	[value] varchar(255) not null,
	constraint PK_i18N primary key([key], culture)
)
GO

ALTER TABLE Users
ADD prefered_culture varchar(5) not null default 'en-US' FOREIGN KEY REFERENCES i18NCulture(name)
GO

create or alter proc CulturesSearchProc @culture varchar(5) = '' as
BEGIN
	Select name as i18n_culture_name, display_name as i18n_culture_display_name 
	from i18NCulture
	Where name like '%' + @culture + '%'
END
GO

create or alter proc TranslationsSearchProc @culture varchar(5) = '' as
BEGIN
	Select [key] as i18n_key, [value] as i18n_value, culture 
	From i18n
	Where culture = @culture
END
GO

create or alter proc TranslationsKeysSearchProc as
BEGIN
	Select DISTINCT [key] as i18n_key
	From i18n
END
GO

create or alter proc MissingTranslations as 
BEGIN
	INSERT INTO i18N(culture, [key], value)
	SELECT DISTINCT c.culture, k.[key], ''
	FROM (
	  SELECT DISTINCT name as culture
	  FROM i18NCulture
	) c
	CROSS JOIN (
	  SELECT DISTINCT [key]
	  FROM i18N
	) k
	WHERE NOT EXISTS (
	  SELECT 1
	  FROM i18N t
	  WHERE t.culture = c.culture
		AND t.[key] = k.[key]
	)
	END
GO

CREATE or ALTER proc CreateCulture @name varchar(5), @display_name varchar(50) as
BEGIN
	DECLARE @cultures table(name varchar(50), display_name varchar(50))
	BEGIN Transaction;
	BEGIN Try
		Insert into i18NCulture (name, display_name) 
		output inserted.name, inserted.display_name into @cultures
		VALUES (@name, @display_name);
		exec MissingTranslations;
		commit;
	END Try
	BEGIN Catch
		Rollback;
	End Catch
	Select * from @cultures
END
GO

create or alter proc CreateCultureEntry @key varchar(255) as
BEGIN
	DECLARE @keys table([key] varchar(255))

	IF NOT EXISTS (Select distinct [key] from i18N Where [key] = @key)
		BEGIN
			INSERT INTO i18N ([key], [value], culture)
			output inserted.[key] into @keys
			SELECT @key, '', name from i18NCulture
		END
	ELSE
		BEGIN
			exec MissingTranslations
			insert into @keys
			Select distinct [key] from i18N Where [key] = @key
		END
	Select distinct * from @keys
END
GO

create or alter proc UpdateCultureEntry 
@key varchar(255), 
@culture varchar(5), 
@value varchar(255) as
BEGIN
	UPDATE i18N SET
	value = @value
	output inserted.[key]
	Where [key] = @key and culture = @culture
END

-- CREATE INDEX idx_culture ON i18N (culture)
GO

-- CREATE INDEX idx_key ON i18N ([key])
GO
