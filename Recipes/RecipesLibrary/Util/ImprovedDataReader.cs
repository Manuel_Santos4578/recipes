﻿using System;

namespace Util;

/// <summary>
/// Class for reading data, if it finds a null value it assigns the default struct value
/// </summary>
public static class ImprovedDataReader
{
    /// <summary>
    /// reads int32
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static int GetInt32(int? value)
    {
        return value ?? 0;
    }
    /// <summary>
    /// reads int64
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static long GetInt64(long? value)
    {
        return value ?? 0;
    }
    /// <summary>
    /// reads double
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static double GetDouble(double? value)
    {
        return value ?? 0;
    }

    /// <summary>
    /// reads single
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static float GetFloat(float? value)
    {
        return value ?? 0;
    }
    /// <summary>
    /// reads string
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static string GetString(string value)
    {
        return value ?? "";
    }
    /// <summary>
    /// reads string to DateTime
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public static DateTime GetDate(string value)
    {
        if (DateTime.TryParse(value, out DateTime date)) return date;
        return DateTime.MinValue;
    }
    /// <summary>
    /// reads DateTime
    /// </summary>
    /// <param name="dateTime"></param>
    /// <returns></returns>
    public static DateTime GetDate(DateTime? dateTime)
    {
        return dateTime ?? DateTime.MinValue;
    }
}
