﻿using DataContracts.Entities;
using ModelsLibrary.Models;
using System.Collections.Generic;
using System.Linq;

namespace RecipesApp.Models;

public sealed class SourceViewModel: ViewModel
{
    public ISource Source { get; set; }
    public IEnumerable<ISource> Sources { get; set; }
    public List<Recipe> SourceRecipes { get; set; }
    public SourceViewModel()
    {
        Source = new Source();
        Sources = new List<Source>();
        SourceRecipes = new List<Recipe>();
    }
}
