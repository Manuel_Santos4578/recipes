﻿using DataContracts.Entities;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;

namespace ModelsLibrary.Models;

public class Category : Simple, ICategory
{
    public enum CategorySearch
    {
        Base = 0
    }
    [JsonProperty("category_id")]
    public int ID { get; set; }
    [JsonProperty("category_name")]
    public string Name { get; set; }
    [JsonProperty("category_friendly_url")]
    public string FriendlyUrl { get; set; }
    //[JsonConverter(typeof(JsonConverters.JsonConverter<Category>))]
    [JsonConverter(typeof(JsonConverters.CategoryJsonConverter))]
    [JsonProperty("base_category", NullValueHandling = NullValueHandling.Ignore)]
    public ICategory BaseCategory { get; set; }

    public static DataTable ExportToDataTable(IEnumerable<ICategory> categories)
    {
        DataTable table = new DataTable();
        table.Columns.AddRange(new DataColumn[]
        {
            new DataColumn(nameof(ICategory.Name), typeof(string)),
            new DataColumn($"{nameof(ICategory.BaseCategory)}.{nameof(ICategory.BaseCategory.Name)}" , typeof(string)),
            new DataColumn(nameof(ICategory.CreatedAt), typeof(DateTime)),
            new DataColumn(nameof(ICategory.UpdatedAt), typeof(DateTime)),
        });

        foreach (ICategory category in categories)
        {
            table.Rows.Add(
                category.Name, 
                category.BaseCategory?.Name ?? "---", 
                category.CreatedAt, 
                category.UpdatedAt);
        }
        return table;
    }
}
