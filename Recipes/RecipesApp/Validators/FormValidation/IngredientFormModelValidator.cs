﻿using FluentValidation;
using RecipesApp.Models.FormModels;
using static ModelsLibrary.Constants.AppConstants;

namespace RecipesApp.Validators.FormValidation;

public class IngredientFormModelValidator : BaseFluentValidator<IngredientFormModel>
{
    public IngredientFormModelValidator() : base()
    {
        RuleFor(ingredient => ingredient.Name)
            .NotEmpty()
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.The_field_0_is_required], _translations[I18N.Name]));
        RuleFor(ingredient => ingredient.Name)
            .MaximumLength(255)
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.Maximum_length_for_0_is_1], _translations[I18N.Name], 255));
        RuleFor(ingredient => ingredient.CountryId)
            .GreaterThanOrEqualTo(1)
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.The_field_0_is_required], _translations[I18N.Country]));
        RuleFor(ingredient => ingredient.CountryId)
            .LessThanOrEqualTo(int.MaxValue)
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.The_field_0_is_required], _translations[I18N.Country]));
    }
}
