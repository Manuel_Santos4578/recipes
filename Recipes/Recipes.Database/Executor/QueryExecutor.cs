﻿using System.Data;
using System.Globalization;
using Microsoft.Data.SqlClient;
using Newtonsoft.Json;
using Recipes.Database.Config;

namespace Util;

public class QueryExecutor : IQueryExecutor
{
    static SqlConnectionStringBuilder connectionStringBuilder;
    readonly SqlConnection connection;

    public QueryExecutor(DB database)
    {
        CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
        connectionStringBuilder ??= new SqlConnectionStringBuilder
        {
            DataSource = database.Host,
            InitialCatalog = database.Database,
            UserID = database.User,
            Password = database.Password,
            TrustServerCertificate = database.TrustCertificate,
        };
        connection = new SqlConnection
        {
            ConnectionString = connectionStringBuilder.ConnectionString
        };
    }
    public List<Dictionary<string, string>> LoadData(string query, List<SqlParameter> parameters = null)
    {
        DataTable table = new DataTable { TableName = "result" };
        try
        {
            SqlCommand command = new SqlCommand(query, connection);
            if (parameters != null)
            {
                command.Parameters.AddRange(parameters.ToArray());
            }

            SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
            dataAdapter.Fill(table);
            dataAdapter.Dispose();
            command.Dispose();
            connection.Close();
            command.Dispose();
            return ProcessDataTable<List<Dictionary<string, string>>>(table);
        }
        catch (Exception)
        {
            connection.Close();
            throw;
        }
    }

    public T LoadData<T>(string query, List<SqlParameter> parameters = null)
    {
        DataTable table = new DataTable { TableName = "result" };
        try
        {
            SqlCommand command = new SqlCommand(query, connection);
            if (parameters != null)
            {
                command.Parameters.AddRange(parameters.ToArray());
            }

            SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
            dataAdapter.Fill(table);
            dataAdapter.Dispose();
            command.Dispose();
            connection.Close();
            command.Dispose();
            return ProcessDataTable<T>(table);
        }
        catch (Exception)
        {
            connection.Close();
            throw;
        }
    }
    public async Task<bool> Execute(string query, List<SqlParameter> parameters)
    {
        try
        {
            if (connection.State != ConnectionState.Open) connection.Open();
            SqlCommand command = new SqlCommand(query, connection);
            if (parameters != null)
            {
                command.Parameters.AddRange(parameters.ToArray());
            }
            int rows = await command.ExecuteNonQueryAsync();
            command.Dispose();
            await connection.CloseAsync();
            return rows != 0;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public async Task<List<Dictionary<string, string>>> TransactionAsync(Dictionary<string, List<SqlParameter>> actions)
    {
        List<Dictionary<string, string>> return_value = new List<Dictionary<string, string>>();
        if (connection.State != ConnectionState.Open) await connection.OpenAsync();
        SqlCommand command = connection.CreateCommand();
        command.Transaction = connection.BeginTransaction();
        try
        {
            foreach (var action in actions)
            {
                command.Parameters.Clear();
                command.CommandText = action.Key;
                command.Parameters.AddRange(action.Value.ToArray());
                if (return_value.Count is 0)
                {
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    DataTable table = new DataTable("result");
                    adapter.Fill(table);
                    return_value = ProcessDataTable<List<Dictionary<string, string>>>(table);
                    if (return_value.Count != 0 && return_value[0].ContainsKey("sql_transaction_id"))
                    {
                        foreach (var sql_action in actions)
                        {
                            SqlParameter parameter = sql_action.Value.FirstOrDefault(x => x.ParameterName == "@sql_transaction_id");
                            if (parameter != null) parameter.Value = SqlTypeParse(return_value[0]["sql_transaction_id"], parameter.SqlDbType);
                        }
                    }
                }
                else
                {
                    command.ExecuteNonQuery();
                }
            }
            command.Transaction.Commit();
            connection.Close();
            return return_value;
        }
        catch (Exception)
        {
            command.Transaction.Rollback();
            await connection.CloseAsync();
            throw;
        }
    }
    private static T ProcessDataTable<T>(DataTable table)
    {
        return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(table));
    }
    public SqlParameter GenerateParameter(string name, SqlDbType type, dynamic value)
    {
        return new SqlParameter(name, type) { Value = value };
    }
    private static dynamic SqlTypeParse(string sql_transaction_id, SqlDbType type)
    {
        return type switch
        {
            SqlDbType.BigInt => long.Parse(sql_transaction_id),
            SqlDbType.Int => int.Parse(sql_transaction_id),
            SqlDbType.UniqueIdentifier => Guid.Parse(sql_transaction_id),
            SqlDbType.SmallInt => short.Parse(sql_transaction_id),
            SqlDbType.TinyInt => byte.Parse(sql_transaction_id),
            SqlDbType.VarChar => sql_transaction_id,
            SqlDbType.NVarChar => sql_transaction_id,
            _ => throw new Exception("Unsupported SqlDbType"),
        };
    }
    public string GenerateStringValue(string value)
    {
        return $"%{value}%";
    }
}
public interface IQueryExecutor
{
    Task<bool> Execute(string query, List<SqlParameter> parameters);
    List<Dictionary<string, string>> LoadData(string query, List<SqlParameter> parameters = null);
    T LoadData<T>(string query, List<SqlParameter> parameters = null);
    Task<List<Dictionary<string, string>>> TransactionAsync(Dictionary<string, List<SqlParameter>> actions);
    SqlParameter GenerateParameter(string name, SqlDbType type, dynamic value);
    string GenerateStringValue(string value);
}
