﻿using Microsoft.Data.SqlClient;
using ModelsLibrary.Models;
using Recipes.Database.Config;
using RecipesLibrary.Util;
using System.Data;
using System.Text;
using Util;

namespace Recipes.Database.Repositories;

public static class IngredientRepository
{

    public static IEnumerable<Ingredient> ReadAll(string name = "", int region = 0, int country = 0, int page = 1, int size = 0)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = "exec IngredientSearchProc @name, @region, @country, @page, @size";
        List<SqlParameter> parameters = new List<SqlParameter> {
            executor.GenerateParameter("@name", SqlDbType.VarChar,name ?? ""),
            executor.GenerateParameter("@region", SqlDbType.Int,region),
            executor.GenerateParameter("@country", SqlDbType.Int,country),
            executor.GenerateParameter("@page", SqlDbType.Int,page),
            executor.GenerateParameter("@size", SqlDbType.Int,size),
        };
        List<Ingredient> ingredients = new List<Ingredient>();

        List<Dictionary<string, string>> results = executor.LoadData<List<Dictionary<string, string>>>(query, parameters);
        foreach (var result in results)
        {
            ingredients.Add(new Ingredient
            {
                ID = int.Parse(result["id"]),
                Name = result["name"],
                FriendlyUrl = result["friendly_url"],
                Country = new Country
                {
                    ID = int.Parse(result["country_id"]),
                    Name = result["country_name"],
                    FriendlyUrl = result["country_friendly_url"],
                    Region = new Region
                    {
                        ID = int.Parse(result["region_id"]),
                        Name = result["region_name"],
                        FriendlyUrl = result["region_friendly_url"]
                    }
                }
            });
        }
        return ingredients;
    }

    public static int ReadCount(string name = "", int region = 0, int country = 0)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = "exec IngredientSearchProc @name, @region, @country, @count = 1";
        List<SqlParameter> parameters = new List<SqlParameter> {
            executor.GenerateParameter("@name", SqlDbType.VarChar,name ?? ""),
            executor.GenerateParameter("@region", SqlDbType.Int,region),
            executor.GenerateParameter("@country", SqlDbType.Int,country),
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).First();
        return int.Parse(result["count"]);
    }

    public static Ingredient ReadBySlug(string slug, bool with_trashed = false)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = @"exec IngredientFindProc @slug, @with_trashed ";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@slug",SqlDbType.VarChar, slug),
            executor.GenerateParameter("@with_trashed",SqlDbType.Bit, with_trashed),
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        return result is not null ? new Ingredient
        {
            ID = int.Parse(result["id"]),
            Name = result["name"],
            FriendlyUrl = result["friendly_url"],
            Country = new Country
            {
                ID = int.Parse(result["country_id"]),
                Name = result["country_name"],
                FriendlyUrl = result["country_friendly_url"],
                Region = new Region
                {
                    ID = int.Parse(result["region_id"]),
                    Name = result["region_name"],
                    FriendlyUrl = result["region_friendly_url"]
                }
            }
        } : null;
    }

    public static Ingredient Store(Ingredient ingredient)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        ingredient.CreatedAt = ingredient.UpdatedAt = DateTime.Now;
        string query =
            @"INSERT INTO Ingredient (name,friendly_url,country_id,created_at,updated_at)
                    output inserted.id
                    VALUES (@name, @friendly_url, @country, @created_at, @updated_at)";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@name",SqlDbType.VarChar, ingredient.Name),
            executor.GenerateParameter("@friendly_url",SqlDbType.VarChar, ingredient.FriendlyUrl),
            executor.GenerateParameter("@country",SqlDbType.Int, ingredient.Country.ID),
            executor.GenerateParameter("@created_at",SqlDbType.DateTime, ingredient.CreatedAt),
            executor.GenerateParameter("@updated_at",SqlDbType.DateTime, ingredient.UpdatedAt)
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        if (result is null)
        {
            return null;
        }
        ingredient.ID = int.Parse(result["id"]);
        return ingredient;
    }
    public static IEnumerable<Ingredient> BulkStore(IEnumerable<Ingredient> ingredients)
    {
        StringBuilder query = new StringBuilder("DECLARE @ingredients IngredientType;INSERT INTO @ingredients (name, friendly_url, country_id, created_at, updated_at)\r\nVALUES ");
        List<SqlParameter> parameters = new List<SqlParameter>();
        QueryExecutor executor = new QueryExecutor(DB.Instance);
        int count = ingredients.Count();
        for (int i = 0; i < count; i++)
        {
            Ingredient ingredient = ingredients.ElementAt(i);
            query.Append($"(@name{i}, @slug{i}, (Select top 1 id from Country where Country.name like @country_name{i}), @created_at{i}, @updated_at{i})");
            if (i != count - 1)
            {
                query.Append(',');
            }
            ingredient.FriendlyUrl = ingredient.Name.GenerateSlug();
            parameters.AddRange(new[] {
                executor.GenerateParameter($"@name{i}", SqlDbType.VarChar, ingredient.Name),
                executor.GenerateParameter($"@slug{i}", SqlDbType.VarChar, ingredient.FriendlyUrl),
                executor.GenerateParameter($"@country_name{i}", SqlDbType.VarChar, ingredient.Country.Name),
                executor.GenerateParameter($"@created_at{i}", SqlDbType.DateTime, DateTime.Now),
                executor.GenerateParameter($"@updated_at{i}", SqlDbType.DateTime, DateTime.Now),
            });
        }
        query.AppendLine("exec ImportIngredients @ingredients");
        try
        {
            executor.Execute(query.ToString(), parameters).Wait();
            return ingredients;
        }
        catch (Exception)
        {
            return null;
        }
    }
    public static Ingredient Update(Ingredient ingredient)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        ingredient.UpdatedAt = DateTime.Now;
        string query =
            @"UPDATE Ingredient SET
                    name = @name,
                    friendly_url = @friendly_url,
                    country_id = @country,
                    updated_at = @updated_at
                    output inserted.id
                    Where id = @id";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@name",SqlDbType.VarChar, ingredient.Name),
            executor.GenerateParameter("@friendly_url",SqlDbType.VarChar, ingredient.FriendlyUrl),
            executor.GenerateParameter("@id",SqlDbType.Int, ingredient.ID),
            executor.GenerateParameter("@country",SqlDbType.Int, ingredient.Country.ID),
            executor.GenerateParameter("@updated_at",SqlDbType.DateTime, ingredient.UpdatedAt)
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        return result is not null ? ingredient : null;
    }

    public static Ingredient SoftDelete(Ingredient ingredient)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        ingredient.DeletedAt = DateTime.Now;
        string query =
            @"UPDATE Ingredient SET
                    deleted_at = @deleted_at
                    output inserted.id
                    Where id = @id";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@deleted_at",SqlDbType.DateTime, ingredient.DeletedAt),
            executor.GenerateParameter("@id",SqlDbType.Int, ingredient.ID),
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        return result is not null ? ingredient : null;
    }
}
