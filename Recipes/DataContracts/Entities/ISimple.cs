﻿namespace DataContracts.Entities;

public interface ISimple
{
    DateTime CreatedAt { get; set; }
    DateTime UpdatedAt { get; set; }
    DateTime? DeletedAt { get; set; }
}
