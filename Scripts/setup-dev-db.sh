#!/usr/bin/env bash

docker build -t mssql_dev:1.0 ../SQL --file ../SQL/Dockerfile

docker run --name mssql_dev --mount source=mssql_data,target=/var/opt/mssql/data -p 14433:1433 -d mssql_dev:1.0