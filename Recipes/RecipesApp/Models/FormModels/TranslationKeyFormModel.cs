﻿using System.ComponentModel.DataAnnotations;

namespace RecipesApp.Models.FormModels;

public class TranslationKeyFormModel
{
    [Required]
    public string Key { get; set; }
}
