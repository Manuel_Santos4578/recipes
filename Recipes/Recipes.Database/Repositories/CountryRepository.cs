﻿using Microsoft.Data.SqlClient;
using ModelsLibrary.Models;
using Recipes.Database.Config;
using RecipesLibrary.Util;
using System.Data;
using System.Text;
using Util;

namespace Recipes.Database.Repositories;

public static class CountryRepository
{
    public static List<Country> ReadAll(int region, string name, int page = 1, int size = 0)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = "exec CountriesSearchProc @name, @region, @page, @size";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@name",SqlDbType.VarChar, name ?? ""),
            executor.GenerateParameter("@region",SqlDbType.Int, region),
            executor.GenerateParameter("@page",SqlDbType.Int, page),
            executor.GenerateParameter("@size",SqlDbType.Int, size),
        };
        List<Country> countries = new List<Country>();
        List<Dictionary<string, string>> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters);

        foreach (Dictionary<string, string> country in result)
        {
            countries.Add(new Country
            {
                ID = int.Parse(country["country_id"]),
                Name = country["country_name"],
                FriendlyUrl = country["country_friendly_url"],
                Region = new Region
                {
                    ID = int.Parse(country["region_id"]),
                    Name = country["region_name"],
                    FriendlyUrl = country["region_friendly_url"],
                }
            });
        }
        return countries;
    }

    public static int ReadCount(int region, string name)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = "exec CountriesSearchProc @name, @region, @count = 1";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@name",SqlDbType.VarChar, name ?? ""),
            executor.GenerateParameter("@region",SqlDbType.Int, region),
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).First();
        return int.Parse(result["count"]);
    }

    public static Country ReadyBySlug(string slug, bool with_trashed = false)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = @"exec CountryFindProc @slug, @with_trashed";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@slug",SqlDbType.VarChar, slug),
            executor.GenerateParameter("@with_trashed", SqlDbType.Bit, with_trashed)
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        return result is not null ? new Country
        {
            ID = int.Parse(result["country_id"]),
            Name = result["country_name"],
            FriendlyUrl = result["country_friendly_url"],
            Region = new Region
            {
                ID = int.Parse(result["region_id"]),
                Name = result["region_name"],
                FriendlyUrl = result["region_friendly_url"],
            }
        } : null;
    }

    public static Country Store(Country country)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        country.CreatedAt = country.UpdatedAt = DateTime.Now;
        string query =
          @"INSERT INTO Country (name,friendly_url, region_id,created_at,updated_at)
                    output inserted.id
                    VALUES (@name, @friendly_url, @region,@created_at, @updated_at)";
        List<SqlParameter> parameters = new List<SqlParameter> {
            executor.GenerateParameter("@name",SqlDbType.VarChar, country.Name),
            executor.GenerateParameter("@friendly_url",SqlDbType.VarChar, country.FriendlyUrl),
            executor.GenerateParameter("@region",SqlDbType.Int, country.Region.ID),
            executor.GenerateParameter("@created_at",SqlDbType.DateTime, country.CreatedAt),
            executor.GenerateParameter("@updated_at",SqlDbType.DateTime, country.UpdatedAt),
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        if (result is null)
        {
            return null;
        }
        return country;
    }

    public static IEnumerable<Country> BulkStore(IEnumerable<Country> countries)
    {
        StringBuilder query = new StringBuilder("DECLARE @countries CountryType;INSERT INTO @countries (name, friendly_url, region_id, created_at, updated_at)\r\nVALUES ");
        List<SqlParameter> parameters = new List<SqlParameter>();
        QueryExecutor executor = new QueryExecutor(DB.Instance);
        int count = countries.Count();
        for (int i = 0; i < count; i++)
        {
            Country country = countries.ElementAt(i);
            query.Append($"(@name{i}, @slug{i}, (Select top 1 id from Region where Region.name like @region_name{i}), @created_at{i}, @updated_at{i})");
            if (i != count - 1)
            {
                query.Append(',');
            }
            country.FriendlyUrl = country.Name.GenerateSlug();
            parameters.AddRange(new[] {
                executor.GenerateParameter($"@name{i}", SqlDbType.VarChar, country.Name),
                executor.GenerateParameter($"@slug{i}", SqlDbType.VarChar, country.FriendlyUrl),
                executor.GenerateParameter($"@region_name{i}", SqlDbType.VarChar, country.Region.Name),
                executor.GenerateParameter($"@created_at{i}", SqlDbType.DateTime, DateTime.Now),
                executor.GenerateParameter($"@updated_at{i}", SqlDbType.DateTime, DateTime.Now),
            });
        }
        query.AppendLine("exec ImportCountries @countries");
        try
        {
            executor.Execute(query.ToString(), parameters).Wait();
            return countries;
        }
        catch (Exception)
        {
            return null;
        }
    }

    public static Country Update(Country country)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        country.UpdatedAt = DateTime.Now;
        string query =
            @"UPDATE Country SET
                    name = @name,
                    friendly_url = @friendly_url,
                    region_id = @region,
                    updated_at = @updated_at
                    output inserted.id,inserted.friendly_url
                    Where id = @id";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@id",SqlDbType.Int, country.ID),
            executor.GenerateParameter("@region",SqlDbType.Int, country.Region.ID),
            executor.GenerateParameter("@name",SqlDbType.VarChar, country.Name),
            executor.GenerateParameter("@friendly_url",SqlDbType.VarChar, country.FriendlyUrl),
            executor.GenerateParameter("@updated_at",SqlDbType.DateTime, DateTime.Now)
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        return result is not null ? country : null;
    }

    public static Country SoftDelete(Country country)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        country.DeletedAt = DateTime.Now;
        string query =
            @"UPDATE Country SET
                    deleted_at = @deleted_at
                    output inserted.id, inserted.friendly_url
                    Where id = @id";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@id",SqlDbType.VarChar, country.ID),
            executor.GenerateParameter("@deleted_at",SqlDbType.DateTime, country.DeletedAt)
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        return result is not null ? country : null;
    }
}
