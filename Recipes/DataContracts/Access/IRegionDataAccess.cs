﻿using DataContracts.Entities;

namespace DataContracts.Access;

public partial interface IRegionDataAccess
{
    Task<IEnumerable<IRegion>> SelectAllRegionsAsync(string name = "", int page = 0, int page_size = 0);
    Task<int> SelectRegionCountAsync(string name);
    Task<IRegion> SelectRegionBySlugAsync(string slug);
    Task<IRegion> InsertRegionAsync(IRegion region);
    Task<IRegion> UpdateRegionAsync(IRegion region);
    Task<bool> DeleteRegionAsync(IRegion region);
}
