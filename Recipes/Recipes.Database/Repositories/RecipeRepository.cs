﻿using DataContracts.Entities;
using Microsoft.Data.SqlClient;
using ModelsLibrary.Models;
using Newtonsoft.Json.Linq;
using Recipes.Database.Config;
using System.Data;
using Util;

namespace Recipes.Database.Repositories;

public static class RecipeRepository
{
    public static List<Recipe> ReadAll(string name = "", string author = "", string category = "", string country = "", string region = "", string source = "", string ingredient = "", int user = 0, int page = 1, int size = 0, bool favorites_only = false)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = @$"exec RecipeSearchProc @name, @author, @category, @country, @region, @source, @ingredient, @user, @page, @size, @favorites_only = @favorites";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@name",SqlDbType.VarChar, executor.GenerateStringValue(name)),
            executor.GenerateParameter("@author", SqlDbType.VarChar, executor.GenerateStringValue(author)),
            executor.GenerateParameter("@category", SqlDbType.VarChar, executor.GenerateStringValue(category)),
            executor.GenerateParameter("@country", SqlDbType.VarChar, executor.GenerateStringValue(country)),
            executor.GenerateParameter("@region", SqlDbType.VarChar, executor.GenerateStringValue(region)),
            executor.GenerateParameter("@source", SqlDbType.VarChar, executor.GenerateStringValue(source)),
            executor.GenerateParameter("@ingredient", SqlDbType.VarChar, executor.GenerateStringValue(ingredient)),
            executor.GenerateParameter("@user", SqlDbType.Int, user),
            executor.GenerateParameter("@page", SqlDbType.Int, page),
            executor.GenerateParameter("@size", SqlDbType.Int, size),
            executor.GenerateParameter("@favorites", SqlDbType.Bit, favorites_only),
        };
        List<Dictionary<string, string>> results = executor.LoadData<List<Dictionary<string, string>>>(query, parameters);
        List<Recipe> recipes = new List<Recipe>(results.Count);
        foreach (var result in results)
        {
            recipes.Add(new Recipe
            {
                ID = int.Parse(result["recipe_id"]),
                Name = result["recipe_name"],
                FriendlyUrl = result["recipe_friendly_url"],
                Text = result["recipe_text"],
                Category = new Category
                {
                    ID = int.Parse(result["category_id"]),
                    Name = result["category_name"],
                    FriendlyUrl = result["category_url"],
                },
                Author = new Author
                {
                    ID = int.Parse(result["author_id"]),
                    Name = result["author_name"],
                    FriendlyUrl = result["author_url"]
                },
                Country = new Country
                {
                    ID = int.Parse(result["country_id"]),
                    Name = result["country_name"],
                    FriendlyUrl = result["country_url"],
                    Region = new Region
                    {
                        ID = int.Parse(result["region_id"]),
                        Name = result["region_name"],
                        FriendlyUrl = result["region_friendly_url"]
                    }
                },
                Source = new Source
                {
                    ID = int.Parse(result["recipe_source_id"]),
                    Name = result["recipe_source_name"],
                    FriendlyUrl = result["recipe_source_friendly_url"]
                },
                MainImage = new RecipeImage
                {
                    ID = int.Parse(result["image_id"]),
                    Image = result["image"],
                    //ImageBase64 = result.Value<string>("image_base_64"),
                    ContentType = result["content_type"]
                },
                RecipeUser = result.ContainsKey("user_id") ? new RecipeUser
                {
                    IsCreator = bool.Parse(result["is_creator"]),
                    RecipeId = int.Parse(result["recipe_id"]),
                    User = new User
                    {
                        ID = int.Parse(result["user_id"]),
                        Username = result["username"],
                    }
                } : new RecipeUser(),
                IsFavorite = result.ContainsKey("recipe_favorite_id") && result["recipe_favorite_id"] != "0"
            });
        }
        return recipes;
    }

    public static int ReadCount(string name = "", string author = "", string category = "", string country = "", string region = "", string source = "", string ingredient = "", int user = 0)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = @$"exec RecipeSearchProc @name, @author, @category, @country, @region, @source, @ingredient, @user, @count = 1";
        List<SqlParameter> parameters = new List<SqlParameter> {
            executor.GenerateParameter("@name",SqlDbType.VarChar, executor.GenerateStringValue(name)),
            executor.GenerateParameter("@author", SqlDbType.VarChar, executor.GenerateStringValue(author)),
            executor.GenerateParameter("@category", SqlDbType.VarChar, executor.GenerateStringValue(category)),
            executor.GenerateParameter("@country", SqlDbType.VarChar, executor.GenerateStringValue(country)),
            executor.GenerateParameter("@region", SqlDbType.VarChar, executor.GenerateStringValue(region)),
            executor.GenerateParameter("@source", SqlDbType.VarChar, executor.GenerateStringValue(source)),
            executor.GenerateParameter("@ingredient", SqlDbType.VarChar, executor.GenerateStringValue(ingredient)),
            executor.GenerateParameter("@user", SqlDbType.Int, user),
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).First();
        return int.Parse(result["count"]);
    }

    public static List<RecipeIngredient> ReadIngredients(string recipe_slug)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = @"Select Ingredient.*, RecipeIngredient.amount, RecipeIngredient.unit, 
                Country.name as country_name,
                Country.friendly_url as country_friendly_url,
                Country.region_id,
                Region.name as region_name,
                Region.friendly_url as region_friendly_url
                From Ingredient
                inner join RecipeIngredient on RecipeIngredient.ingredient_id = Ingredient.id
                inner join Country on Country.id = Ingredient.country_id
                inner join Region on Region.id = Country.region_id
                inner join Recipe on RecipeIngredient.recipe_id = Recipe.id
                Where Recipe.friendly_url = @slug AND RecipeIngredient.deleted_at is null";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@slug",SqlDbType.VarChar, recipe_slug),
        };
        var result = executor.LoadData<List<JToken>>(query, parameters);
        List<RecipeIngredient> ingredients = new List<RecipeIngredient>();
        foreach (var db_ingredient in result)
        {
            ingredients.Add(new RecipeIngredient
            {
                Ingredient = new Ingredient
                {
                    ID = db_ingredient.Value<int>("id"),
                    Name = db_ingredient.Value<string>("name"),
                    FriendlyUrl = db_ingredient.Value<string>("friendly_url"),
                    Country = new Country
                    {
                        ID = db_ingredient.Value<int>("country_id"),
                        Name = db_ingredient.Value<string>("country_name"),
                        FriendlyUrl = db_ingredient.Value<string>("country_friendly_url"),
                        Region = new Region
                        {
                            ID = db_ingredient.Value<int>("region_id"),
                            Name = db_ingredient.Value<string>("region_name"),
                            FriendlyUrl = db_ingredient.Value<string>("region_friendly_url")
                        }
                    }
                },
                Unit = db_ingredient.Value<string>("unit"),
                Quantity = db_ingredient.Value<decimal>("amount")
            });
        }
        return ingredients;
    }

    public static List<Course> ReadCourses(string recipe_slug)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = @"exec RecipeCourseProc @recipe";
        List<SqlParameter> parameters = new List<SqlParameter>() { executor.GenerateParameter("@recipe", SqlDbType.VarChar, recipe_slug) };
        return executor.LoadData<List<Course>>(query, parameters);
    }
    public static Recipe ReadBySlug(string slug, bool with_trashed)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = @"exec RecipeFindProc @slug, @with_trashed";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@slug",SqlDbType.VarChar, slug),
            executor.GenerateParameter("@with_trashed",SqlDbType.VarChar, with_trashed),
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        return result is not null ? new Recipe
        {
            ID = int.Parse(result["recipe_id"]),
            Name = result["recipe_name"],
            FriendlyUrl = result["recipe_friendly_url"],
            Text = result["recipe_text"],
            Category = new Category
            {
                ID = int.Parse(result["category_id"]),
                Name = result["category_name"],
                FriendlyUrl = result["category_url"],
            },
            Author = new Author
            {
                ID = int.Parse(result["author_id"]),
                Name = result["author_name"],
                FriendlyUrl = result["author_url"]
            },
            Country = new Country
            {
                ID = int.Parse(result["country_id"]),
                Name = result["country_name"],
                FriendlyUrl = result["country_url"],
                Region = new Region
                {
                    ID = int.Parse(result["region_id"]),
                    Name = result["region_name"],
                    FriendlyUrl = result["region_friendly_url"]
                }
            },
            Source = new Source
            {
                ID = int.Parse(result["recipe_source_id"]),
                Name = result["recipe_source_name"],
                FriendlyUrl = result["recipe_source_friendly_url"]
            },
            MainImage = new RecipeImage
            {
                ID = int.Parse(result["image_id"]),
                Image = result["image"],
                //ImageBase64 = result.Value<string>("image_base_64"),
                ContentType = result["content_type"]
            },
            IsFavorite = result["recipe_favorite_id"] != "0"
        } : null;
    }

    public async static Task<Recipe> StoreAsync(Recipe recipe)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        Dictionary<string, List<SqlParameter>> actions = new Dictionary<string, List<SqlParameter>>();
        string query =
            @"INSERT INTO Recipe (name,friendly_url,category_id,author_id, recipe_source_id,country_id,recipe_text,created_at,updated_at)
                    output inserted.id as sql_transaction_id
                    VALUES (@name,@friendly_url,@category,@author,@source,@country,@recipe_text,@created_at,@updated_at)";
        recipe.CreatedAt = recipe.UpdatedAt = DateTime.Now;
        List<SqlParameter> parameters = GenerateRecipeParameters(recipe, executor);
        actions.Add(query, parameters);
        for (int i = 0; i < recipe.Ingredients.Count; i++)
        {
            IRecipeIngredient ingredient = recipe.Ingredients[i];
            string ingredient_query =
            @$"
                    INSERT INTO RecipeIngredient (ingredient_id, recipe_id, amount, unit, created_at, updated_at)
                    output inserted.ingredient_id, inserted.recipe_id
                    VALUES (@ingredient_id{i}, @sql_transaction_id, @amount, @unit, @created_at, @updated_at)
                ";
            ingredient.CreatedAt = ingredient.UpdatedAt = DateTime.Now;
            List<SqlParameter> ingredient_parameters = new List<SqlParameter>
            {
                executor.GenerateParameter($"@ingredient_id{i}",SqlDbType.Int, ingredient.Ingredient.ID),
                executor.GenerateParameter("@sql_transaction_id",SqlDbType.Int, 0),
                executor.GenerateParameter("@amount",SqlDbType.Decimal, ingredient.Quantity),
                executor.GenerateParameter("@unit",SqlDbType.VarChar, ingredient.Unit),
                executor.GenerateParameter("@created_at",SqlDbType.DateTime, ingredient.CreatedAt),
                executor.GenerateParameter("@updated_at",SqlDbType.DateTime, ingredient.UpdatedAt),
            };
            actions.Add(ingredient_query, ingredient_parameters);
        }
        for (int i = 0; i < recipe.Courses.Count; i++)
        {
            ICourse course = recipe.Courses[i];
            string course_query =
            @$"
                INSERT INTO RecipeCourse (recipe_id, course_id, created_at, updated_at)
                VALUES (@sql_transaction_id, @course_id{i},  @created_at, @updated_at)
            ";
            course.CreatedAt = course.UpdatedAt = DateTime.Now;
            List<SqlParameter> course_parameters = new List<SqlParameter>
            {
                executor.GenerateParameter($"@course_id{i}",SqlDbType.Int, course.ID),
                executor.GenerateParameter("@sql_transaction_id",SqlDbType.Int, 0),
                executor.GenerateParameter("@created_at",SqlDbType.DateTime, course.CreatedAt),
                executor.GenerateParameter("@updated_at",SqlDbType.DateTime, course.UpdatedAt),
            };
            actions.Add(course_query, course_parameters);
        }

        if (recipe.RecipeUser is not null)
        {
            actions.Add(
                "Insert into UserRecipes (recipe_id, user_id, is_creator) VALUES (@sql_transaction_id, @user_recipe_user, @user_recipe_is_creator)",
                new List<SqlParameter> {
                    executor.GenerateParameter($"@sql_transaction_id", SqlDbType.Int, 0),
                    executor.GenerateParameter($"@user_recipe_user", SqlDbType.Int, recipe.RecipeUser.User.ID),
                    executor.GenerateParameter($"@user_recipe_is_creator", SqlDbType.Bit, recipe.RecipeUser.IsCreator),
                });
        }

        if (recipe.MainImage != null)
        {
            string main_image_query =
            @"
                INSERT INTO RecipeImages (image, image_base_64, content_type, recipe_id, is_display, created_at, updated_at)
                VALUES (@image, @image_base_64, @content_type, @sql_transaction_id, @is_display, @created_at, @updated_at)
            ";
            recipe.MainImage.CreatedAt = recipe.MainImage.UpdatedAt = DateTime.Now;
            List<SqlParameter> main_image_parameters = new List<SqlParameter>
            {
                executor.GenerateParameter("@image",SqlDbType.VarChar, recipe.MainImage.Image),
                executor.GenerateParameter("@image_base_64",SqlDbType.VarChar, recipe.MainImage.ImageBase64),
                executor.GenerateParameter("@content_type",SqlDbType.VarChar, recipe.MainImage.ContentType),
                executor.GenerateParameter("@sql_transaction_id",SqlDbType.Int, 0),
                executor.GenerateParameter("@is_display",SqlDbType.Bit, recipe.MainImage.IsDisplay),
                executor.GenerateParameter("@created_at",SqlDbType.DateTime, recipe.MainImage.CreatedAt),
                executor.GenerateParameter("@updated_at",SqlDbType.DateTime, recipe.MainImage.UpdatedAt),
            };
            actions.Add(main_image_query, main_image_parameters);
        }
        Dictionary<string, string> result = (await executor.TransactionAsync(actions)).FirstOrDefault();
        if (result is null)
        {
            return null;
        }
        recipe.ID = int.Parse(result["sql_transaction_id"]);
        return recipe;
    }

    public async static Task<Recipe> UpdateAsync(Recipe recipe)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = @"
                    UPDATE Recipe SET
                    name = @name,
                    friendly_url = @friendly_url,
                    category_id = @category,
                    author_id = @author,
                    country_id = @country,
                    recipe_source_id = @source,
                    recipe_text = @recipe_text,
                    updated_at = @updated_at
                    output inserted.id as sql_transaction_id
                    Where id = @id";
        recipe.UpdatedAt = DateTime.Now;
        List<SqlParameter> parameters = GenerateRecipeParameters(recipe, executor);
        Dictionary<string, List<SqlParameter>> actions = new Dictionary<string, List<SqlParameter>>
        {
            {query, parameters}
        };
        string delete_query = @"
                UPDATE RecipeIngredient SET
                deleted_at = @deleted_at
                output inserted.ingredient_id, inserted.recipe_id
                Where recipe_id = @sql_transaction_id
            ";
        List<SqlParameter> delete_parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@deleted_at", SqlDbType.DateTime, recipe.UpdatedAt),
            executor.GenerateParameter("@sql_transaction_id", SqlDbType.Int, recipe.ID),
        };
        actions.Add(delete_query, delete_parameters);
        for (int i = 0; i < recipe.Ingredients.Count; i++)
        {
            IRecipeIngredient ingredient = recipe.Ingredients[i];
            string ingredient_query = @$"
                    IF NOT EXISTS (SELECT * FROM RecipeIngredient Where recipe_id = @sql_transaction_id AND ingredient_id = @ingredient_id{i})
                        BEGIN
                            INSERT INTO RecipeIngredient (ingredient_id, recipe_id, amount, unit, created_at, updated_at)
                            output inserted.ingredient_id, inserted.recipe_id
                            VALUES (@ingredient_id{i}, @sql_transaction_id, @amount, @unit, @created_at, @updated_at)
                        END
                    ELSE
                        BEGIN
                            UPDATE RecipeIngredient SET
                            amount = @amount,
                            unit = @unit,
                            updated_at = @updated_at,
                            deleted_at = null
                            output inserted.ingredient_id, inserted.recipe_id
                            WHERE recipe_id = @sql_transaction_id AND ingredient_id = @ingredient_id{i}
                        END";
            ingredient.CreatedAt = ingredient.UpdatedAt = DateTime.Now;
            List<SqlParameter> ingredient_parameters = new List<SqlParameter>
            {
                executor.GenerateParameter($"@ingredient_id{i}",SqlDbType.Int, ingredient.Ingredient.ID),
                executor.GenerateParameter("@sql_transaction_id",SqlDbType.Int, recipe.ID),
                executor.GenerateParameter("@amount",SqlDbType.Decimal, ingredient.Quantity),
                executor.GenerateParameter("@unit",SqlDbType.VarChar, ingredient.Unit),
                executor.GenerateParameter("@created_at",SqlDbType.DateTime, ingredient.CreatedAt),
                executor.GenerateParameter("@updated_at",SqlDbType.DateTime, ingredient.UpdatedAt),
            };
            actions.Add(ingredient_query, ingredient_parameters);
        }

        string delete_course_query = @"
                UPDATE RecipeCourse SET
                deleted_at = @deleted_at
                output inserted.course_id, inserted.recipe_id
                Where recipe_id = @sql_transaction_id
        ";
        actions.Add(delete_course_query, delete_parameters);

        for (int i = 0; i < recipe.Courses.Count; i++)
        {
            ICourse course = recipe.Courses[i];
            string course_query =
            @$"
                IF NOT EXISTS (SELECT * FROM RecipeCourse Where recipe_id = @sql_transaction_id AND course_id = @course_id{i})
                    BEGIN
                        INSERT INTO RecipeCourse (recipe_id, course_id, created_at, updated_at)
                        VALUES (@sql_transaction_id, @course_id{i},  @created_at, @updated_at)
                    END
                ELSE
                    BEGIN
                        UPDATE RecipeCourse SET
                        updated_at = @updated_at,
                        deleted_at = null
                        output inserted.course_id, inserted.recipe_id
                        WHERE recipe_id = @sql_transaction_id AND course_id = @course_id{i}
                    END
            ";
            course.CreatedAt = course.UpdatedAt = DateTime.Now;
            List<SqlParameter> course_parameters = new List<SqlParameter>
            {
                executor.GenerateParameter($"@course_id{i}",SqlDbType.Int, course.ID),
                executor.GenerateParameter("@sql_transaction_id",SqlDbType.Int, 0),
                executor.GenerateParameter("@created_at",SqlDbType.DateTime, course.CreatedAt),
                executor.GenerateParameter("@updated_at",SqlDbType.DateTime, course.UpdatedAt),
            };
            actions.Add(course_query, course_parameters);
        }

        Dictionary<string, string> result = (await executor.TransactionAsync(actions)).FirstOrDefault();
        return result is not null ? recipe : null;
    }

    public static Recipe SoftDelete(Recipe recipe)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query =
            @"UPDATE Recipe SET
                    deleted_at = @deleted_at
                    output inserted.id
                    Where id = @id";
        recipe.DeletedAt = DateTime.Now;
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@id",SqlDbType.Int, recipe.ID),
            executor.GenerateParameter("@deleted_at",SqlDbType.DateTime, recipe.DeletedAt),
        };
        var result = executor.LoadData<List<JToken>>(query, parameters).FirstOrDefault();
        return result is not null ? recipe : null;
    }

    #region Helpers

    public static List<SqlParameter> GenerateRecipeParameters(Recipe recipe, IQueryExecutor executor)
    {
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@name", SqlDbType.VarChar, recipe.Name),
            executor.GenerateParameter("@friendly_url", SqlDbType.VarChar, recipe.FriendlyUrl),
            executor.GenerateParameter("@recipe_text", SqlDbType.VarChar, recipe.Text),
        };
        if (recipe.CreatedAt != DateTime.MinValue)
        {
            parameters.Add(executor.GenerateParameter("@created_at", SqlDbType.DateTime, recipe.CreatedAt));
        }
        if (recipe.UpdatedAt != DateTime.MinValue)
        {
            parameters.Add(executor.GenerateParameter("@updated_at", SqlDbType.DateTime, recipe.UpdatedAt));
        }
        if (recipe.ID != 0)
        {
            parameters.Add(executor.GenerateParameter("@id", SqlDbType.Int, recipe.ID));
        }
        if (recipe.Category is not null && recipe.Category.ID != 0)
        {
            parameters.Add(executor.GenerateParameter("@category", SqlDbType.Int, recipe.Category.ID));
        }
        if (recipe.Author is not null && recipe.Author.ID != 0)
        {
            parameters.Add(executor.GenerateParameter("@author", SqlDbType.Int, recipe.Author.ID));
        }
        if (recipe.Source is not null && recipe.Source.ID != 0)
        {
            parameters.Add(executor.GenerateParameter("@source", SqlDbType.Int, recipe.Source.ID));
        }
        if (recipe.Country is not null && recipe.Country.ID != 0)
        {
            parameters.Add(executor.GenerateParameter("@country", SqlDbType.Int, recipe.Country.ID));
        }
        return parameters;
    }
    #endregion
}
