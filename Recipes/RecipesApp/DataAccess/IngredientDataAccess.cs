﻿using Newtonsoft.Json;
using ModelsLibrary.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Util;
using System;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json.Linq;
using DataContracts.Entities;
using DataContracts.Access;

namespace RecipesApp.DataAccess;

public sealed partial class DataAccess: IIngredientDataAccess
{
    #region Ingredient

    public async Task<IEnumerable<IIngredient>> SelectAllIngredientsAsync(string name = "", int region = 0, int country = 0, int page = 0, int size = 0)
    {
        Dictionary<string, string> query = new Dictionary<string, string>
        {
            {"page", page.ToString() },
            {"size", size.ToString() },
            {"region", region.ToString() },
            {"country", country.ToString() }
        };
        if (!string.IsNullOrWhiteSpace(name)) query.Add("name", name);
        Uri uri = new Uri(QueryHelpers.AddQueryString(ingredientsEndpoint, query), UriKind.Relative);
        return await new ApiClientV3(ApiDomain, ApiToken).GetAsync<IEnumerable<Ingredient>>(uri.ToString());
    }

    public async Task<IIngredient> SelectIngredientBySlugAsync(string slug)
    {
        return await new ApiClientV3(ApiDomain, ApiToken).GetAsync<Ingredient>($"{ingredientsEndpoint}/" + slug);
    }

    public async Task<int> SelectIngredientCountAsync(string name = "", int region = 0, int country = 0)
    {
        Dictionary<string, string> query = new Dictionary<string, string>
        {
            {"region", region.ToString() },
            {"country", country.ToString() },
            {"count", true.ToString() }
        };
        if (!string.IsNullOrWhiteSpace(name)) query.Add("name", name);
        Uri uri = new Uri(QueryHelpers.AddQueryString(ingredientsEndpoint, query), UriKind.Relative);
        JToken data = await new ApiClientV3(ApiDomain, ApiToken).GetAsync<JToken>(uri.ToString());
        return data.Value<int>("count");
    }

    public async Task<IIngredient> InsertIngredientAsync(IIngredient ingredient)
    {
        using StringContent content = new StringContent(JsonConvert.SerializeObject(ingredient));
        ingredient = await new ApiClientV3(ApiDomain, ApiToken).PostAsync<Ingredient>(ingredientsEndpoint, content);
        return ingredient;
    }
    public async Task InsertIngredientsAsync(IEnumerable<IIngredient> ingredients)
    {
        StringContent content = new StringContent(JsonConvert.SerializeObject(ingredients));
        await new ApiClientV3(ApiDomain, ApiToken).PostAsync<IEnumerable<Ingredient>>($"{ingredientsEndpoint}/bulk", content);
    }
    public async Task<IIngredient> UpdateIngredientAsync(IIngredient ingredient)
    {
        using StringContent content = new StringContent(JsonConvert.SerializeObject(ingredient));
        ingredient = await new ApiClientV3(ApiDomain, ApiToken).PutAsync<Ingredient>($"{ingredientsEndpoint}/" + ingredient.ID, content);
        return ingredient;
    }

    public async Task<bool> DeleteIngredientAsync(IIngredient ingredient)
    {
        ingredient = await new ApiClientV3(ApiDomain, ApiToken).DeleteAsync<Ingredient>($"{ingredientsEndpoint}/" + ingredient.ID);
        return ingredient is not null;
    }
    #endregion
}
