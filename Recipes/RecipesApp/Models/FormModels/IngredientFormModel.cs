﻿using DataContracts.Entities;
using ModelsLibrary.Models;
using System.Collections.Generic;

namespace RecipesApp.Models.FormModels;

public sealed class IngredientFormModel
{
    public int ID { get; set; }
    public string Name { get; set; }
    public string FriendlyUrl { get; set; }
    public int CountryId { get; set; }
    public IEnumerable<ICountry> Countries { get; set; }
    public IngredientFormModel()
    {
        Countries = new List<Country>();
    }
    public IngredientFormModel(IIngredient ingredient) : this()
    {
        ID = ingredient.ID;
        Name = ingredient.Name;
        FriendlyUrl = ingredient.FriendlyUrl;
        CountryId = ingredient.Country.ID;
    }
}
