﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ModelsLibrary.Models;
using Recipes.Database.Repositories;
using RecipesApi.Configuration;
using RecipesLibrary.Util;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Util;

namespace RecipesApi.Controllers;

/// <summary>
/// Region
/// </summary>
[Route("api/regions")]
[Produces("application/json")]
[Authorize(Roles = $"{ConfigurationState.ApiRoles.Write}, {ConfigurationState.ApiRoles.Root}")]
public class RegionController : BaseApiController
{
    public RegionController(IQueryExecutor _executor) : base(_executor)
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="name"></param>
    /// <param name="count"></param>
    /// <param name="page"></param>
    /// <param name="size"></param>
    /// <returns></returns>
    [HttpGet]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public IActionResult Index(string name, bool count = false, int page = 1, int size = 0)
    {
        try
        {
            if (count)
            {
                return Ok(new { count = RegionRespository.ReadCount(name) });
            }
            return Ok(RegionRespository.ReadAll(name, page, size));
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Select By slug
    /// </summary>
    /// <param name="friendly_url">friendly url</param>
    /// <returns>Region collection</returns>
    [HttpGet("{friendly_url}")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public ActionResult<Region> Show(string friendly_url, bool with_trashed = false)
    {
        try
        {
            return Ok(RegionRespository.ReadBySlug(friendly_url, with_trashed));
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Insert method
    /// </summary>
    /// <remarks>
    ///
    ///     {
    ///        "name": "string",
    ///        "friendly_url": "string"
    ///     }
    ///
    /// </remarks>
    /// <returns>inserted name</returns>
    [HttpPost]
    [ProducesResponseType(500)]
    [ProducesResponseType(201)]
    [ProducesResponseType(401)]
    public async Task<IActionResult> StoreAsync()
    {
        try
        {
            Region region = await Request.Body.ReadObject<Region>();
            region.FriendlyUrl = Slug.GenerateSlug(region.Name);
            region.FriendlyUrl = Validator.ValidateSlug(region.FriendlyUrl, RegionRespository.ReadBySlug);
            region = RegionRespository.Store(region);
            int status = region is not null ? 201 : 200;
            return StatusCode(status, region);
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Bulk Store method
    /// </summary>
    /// <returns></returns>
    [HttpPost("bulk")]
    [ProducesResponseType(500)]
    [ProducesResponseType(201)]
    [ProducesResponseType(401)]
    public async Task<IActionResult> BulkStoreAsync()
    {
        IEnumerable<Region> regions = await StreamParser.ReadObject<IEnumerable<Region>>(Request.Body);
        regions = RegionRespository.BulkStore(regions);
        return Ok(regions);
    }


    /// <summary>
    /// Update method
    /// </summary>
    /// <remarks>
    /// Sample request:
    ///
    ///     PUT /region/{id}
    ///     {
    ///        "name": "string",
    ///        "friendly_url": "string"
    ///     }
    ///
    /// </remarks>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpPut("{id:int}")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public async Task<ActionResult> UpdateAsync(int id)
    {
        try
        {
            Region region = await Request.Body.ReadObject<Region>();
            string slug = Slug.GenerateSlug(region.Name);
            if (slug != region.FriendlyUrl)
            {
                region.FriendlyUrl = Validator.ValidateSlug(slug, RegionRespository.ReadBySlug);
            }
            region.ID = id;
            region = RegionRespository.Update(region);
            return Ok(region);
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Delete method
    /// </summary>
    /// <param name="id">PK</param>
    /// <returns>name</returns>
    [HttpDelete("{id:int}")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public IActionResult Delete(int id)
    {
        try
        {
            Region region = new Region { ID = id };
            return Ok(RegionRespository.SoftDelete(region));
        }
        catch (Exception)
        {
            throw;
        }
    }
}
