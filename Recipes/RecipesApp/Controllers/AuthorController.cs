﻿using System.Linq;
using System.Threading.Tasks;
using DataContracts.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using RecipesApp.Models;
using RecipesApp.Models.FormModels;
using RecipesApp.Validators.FormValidation;
using RecipesLibrary.Util;
using Util;
using I18N = ModelsLibrary.Constants.AppConstants.I18N;


namespace RecipesApp.Controllers;

[Route("authors")]
[Authorize]
public sealed class AuthorController : MyBaseController
{
    public AuthorController(IConfiguration config, IWebHostEnvironment env) : base(config, env)
    {

    }
    [HttpGet]
    public async Task<IActionResult> IndexAsync()
    {
        await ApiAuthAsync();
        AuthorViewModel model = new AuthorViewModel
        {
            Count = await dal.SelectAuthorCountAsync()
        };
        HandleViewData();
        return View(model);
    }
    [HttpPost]
    public async Task<IActionResult> IndexAjaxAsync()
    {
        JToken body = await Request.Body.ReadObject<JToken>();
        int page = body.Value<int>("page");
        int page_size = body.Value<int>("size");
        string n = body.Value<string>("name");
        await ApiAuthAsync();
        AuthorViewModel model = new AuthorViewModel
        {
            CurrentPage = page,
            PageSize = page_size,
            Count = await dal.SelectAuthorCountAsync(n),
            Authors = await dal.SelectAllAuthorsAsync(page, page_size, n)
        };
        return Ok(model);
    }

    [HttpGet("show/{slug}")]
    public async Task<IActionResult> ShowAsync(string slug)
    {
        if (string.IsNullOrWhiteSpace(slug)) return BadRequest();
        await ApiAuthAsync();
        AuthorViewModel model = new AuthorViewModel
        {
            Author = await dal.SelectAuthorBySlugAsync(slug)
        };

        HandleViewData();

        if (model.Author is null) return View("NotFound");

        int user = User.Claims.Any(c => c.Type == System.Security.Claims.ClaimTypes.Role && c.Value == ModelsLibrary.Constants.AppConstants.Roles.Admin) ? 0 : int.Parse(User.FindFirst("ID").Value);

        model.Count = await dal.SelectRecipesCountAsync(author: model.Author.FriendlyUrl, user: user);
        model.AuthorRecipes = await dal.SelectAllRecipesAsync(author: model.Author.FriendlyUrl, user: user);

        return View(model);
    }

    [HttpGet("create")]
    public IActionResult CreateAsync()
    {
        AuthorFormModel author = new AuthorFormModel();
        return View(author);
    }

    [HttpPost("store")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> StoreAsync(AuthorFormModel model)
    {
        FluentValidation.Results.ValidationResult validation_result = await new AuthorFormModelValidator().ValidateAsync(model);
        if (!validation_result.IsValid)
        {
            TempData["Error"] = string.Join("; ", validation_result.Errors.Select(err => err.ErrorMessage).ToArray());
            return RedirectToAction("Index", "Author");
        }
        await ApiAuthAsync();
        model.Name = model.Name.Sanitize();
        IAuthor author = await dal.InsertAuthorAsync(model);
        if (author is not null)
        {
            TempData["Success"] = string.Format(translation[I18N.Successfully_created], model.Name);
            return RedirectToAction("Show", "Author", new { slug = author.FriendlyUrl });
        }
        TempData["Error"] = string.Format(translation[I18N.Failed_creation_of], model.Name);
        return RedirectToAction("Index", "Author");
    }

    [HttpGet("edit/{slug}")]
    public async Task<IActionResult> EditAsync(string slug)
    {
        if (string.IsNullOrWhiteSpace(slug)) return BadRequest();
        await ApiAuthAsync();
        IAuthor author = await dal.SelectAuthorBySlugAsync(slug);
        if (author is null) return NotFound();
        AuthorFormModel form_model = new AuthorFormModel(author);
        return View(form_model);
    }

    [HttpPost("update/{slug}")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> UpdateAsync(AuthorFormModel model, string slug)
    {
        FluentValidation.Results.ValidationResult validation_result = await new AuthorFormModelValidator().ValidateAsync(model);
        if (!validation_result.IsValid)
        {
            TempData["Error"] = string.Join("; ", validation_result.Errors.Select(err => err.ErrorMessage).ToArray());
            return Redirect(Request.Headers["Referer"]);
        }
        model.Name = model.Name.Sanitize();
        model.FriendlyUrl = slug;
        await ApiAuthAsync();
        IAuthor author = await dal.UpdateAuthorAsync(model);
        if (author is not null)
        {
            TempData["Success"] = string.Format(translation[I18N.Successfully_updated], model.Name);
            return RedirectToAction("Show", "Author", new { slug = author.FriendlyUrl });
        }
        else
        {
            TempData["Error"] = string.Format(translation[I18N.Failed_to_update], model.Name);
            return RedirectToAction("Show", "Author", new { slug });
        }
    }

    [HttpGet("delete/{slug}")]
    public async Task<IActionResult> DeleteAsync(string slug)
    {
        if (string.IsNullOrWhiteSpace(slug)) return BadRequest();
        await ApiAuthAsync();
        IAuthor author = await dal.SelectAuthorBySlugAsync(slug);
        if (author is null) return NotFound();
        AuthorFormModel form_model = new AuthorFormModel(author);
        return View(form_model);
    }

    [HttpPost("destroy")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DestroyAsync(AuthorFormModel model)
    {
        await ApiAuthAsync();
        if (await dal.DeleteAuthorAsync(model))
        {
            TempData["Success"] = string.Format(translation[I18N.Successfully_deleted], model.Name);
            return RedirectToAction("Index", "Author");
        }

        TempData["Error"] = string.Format(translation[I18N.Failed_to_delete], model.Name);
        return RedirectToAction("Show", "Author", new { friendly_url = model.FriendlyUrl });
    }
}