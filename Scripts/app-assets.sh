#!/usr/bin/env bash

bun run ../Recipes/RecipesApp/node_modules/sass/sass.js ../Recipes/RecipesApp/Scss/style.scss ../Recipes/RecipesApp/wwwroot/css/style.css --style compressed --no-source-map 
bun run ../Recipes/RecipesApp/node_modules/sass/sass.js ../Recipes/RecipesApp/Scss/login.scss ../Recipes/RecipesApp/wwwroot/css/login.css --style compressed --no-source-map
bun run ../Recipes/RecipesApp/node_modules/sass/sass.js ../Recipes/RecipesApp/Scss/unauthorized.scss ../Recipes/RecipesApp/wwwroot/css/unauthorized.css --style compressed --no-source-map
bun run ../Recipes/RecipesApp/node_modules/sass/sass.js ../Recipes/RecipesApp/Scss/themes/darkly/darkly.scss ../Recipes/RecipesApp/wwwroot/css/darkly.css --style compressed --no-source-map
bun run ../Recipes/RecipesApp/node_modules/sass/sass.js ../Recipes/RecipesApp/Scss/themes/sandstone/sandstone.scss ../Recipes/RecipesApp/wwwroot/css/sandstone.css --style compressed --no-source-map
bun run ../Recipes/RecipesApp/node_modules/sass/sass.js ../Recipes/RecipesApp/Scss/themes/minty/minty.scss ../Recipes/RecipesApp/wwwroot/css/minty.css --style compressed --no-source-map
bun run ../Recipes/RecipesApp/node_modules/sass/sass.js ../Recipes/RecipesApp/Scss/themes/default/default.scss ../Recipes/RecipesApp/wwwroot/css/default.css --style compressed --no-source-map
bun build ../Recipes/RecipesApp/TS/themechange.ts  --outdir=../Recipes/RecipesApp/wwwroot/colorthemes/js --minify
bun build ../Recipes/RecipesApp/TS//Login/login-events.ts  --outdir=../Recipes/RecipesApp/wwwroot/js/login --minify