﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace RecipesApp.Views.Shared.Components.Pagination;

public class PaginationViewComponent : ViewComponent
{
    public async Task<IViewComponentResult> InvokeAsync(int current_page, int total_pages)
    {
        int start = current_page >= 3 ? current_page - 2 : 1;
        int end = total_pages > current_page + 2 ? current_page + 2 : total_pages;
        return View((total_pages, current_page, start, end));
    }
}
