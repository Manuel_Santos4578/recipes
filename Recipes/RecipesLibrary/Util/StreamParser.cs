﻿using Newtonsoft.Json;
using System.IO;
using System.Threading.Tasks;

namespace RecipesLibrary.Util;

/// <summary>
/// Class for stream processing
/// </summary>
public static class StreamParser
{
    /// <summary>
    /// Reads stream to dynamic object
    /// </summary>
    /// <param name="stream"></param>
    /// <returns></returns>
    public async static Task<dynamic> ReadDynamic(this Stream stream)
    {
        return JsonConvert.DeserializeObject(await new StreamReader(stream).ReadToEndAsync());
    }

    /// <summary>
    /// Reads stream to string
    /// </summary>
    /// <param name="stream"></param>
    /// <returns></returns>
    public async static Task<string> ReadString(this Stream stream)
    {
        return await new StreamReader(stream).ReadToEndAsync();
    }

    /// <summary>
    /// Reads Stream to object
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="stream"></param>
    /// <returns></returns>
    public async static Task<T> ReadObject<T>(this Stream stream)
    {
        return JsonConvert.DeserializeObject<T>(await new StreamReader(stream).ReadToEndAsync());
    }
}
