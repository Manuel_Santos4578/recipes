﻿using Microsoft.Data.SqlClient;
using ModelsLibrary.Models;
using Newtonsoft.Json.Linq;
using Recipes.Database.Config;
using System.Data;
using Util;

namespace Recipes.Database.Repositories;

public class RecipeFavoriteRepository
{
    public static JToken Store(Recipe recipe, User user)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query =
        @"
                IF Exists (Select * from Recipefavorite Where recipe_id = @recipe_id AND user_id = @user_id)
                    Select * from Recipefavorite Where recipe_id = @recipe_id AND user_id = @user_id
                ELSE
                    INSERT INTO Recipefavorite (recipe_id, user_id)
                    output inserted.recipe_id
                    VALUES (@recipe_id, @user_id)
            ";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@recipe_id", SqlDbType.Int, recipe.ID),
            executor.GenerateParameter("@user_id", SqlDbType.Int, user.ID),
        };
        return executor.LoadData<List<JToken>>(query, parameters).FirstOrDefault();
    }

    public static JToken Delete(Recipe recipe, User user)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query =
        @"
                Delete from RecipeFavorite 
                output deleted.recipe_id
                where recipe_id = @recipe_id and user_id = @user_id
            ";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@recipe_id", SqlDbType.Int, recipe.ID),
            executor.GenerateParameter("@user_id", SqlDbType.Int, user.ID),
        };
        return executor.LoadData<List<JToken>>(query, parameters).FirstOrDefault();
    }
}
