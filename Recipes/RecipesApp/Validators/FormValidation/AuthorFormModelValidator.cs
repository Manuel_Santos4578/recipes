﻿using FluentValidation;
using RecipesApp.Models.FormModels;
using static ModelsLibrary.Constants.AppConstants;

namespace RecipesApp.Validators.FormValidation;

public class AuthorFormModelValidator : BaseFluentValidator<AuthorFormModel>
{
    public AuthorFormModelValidator() : base()
    {
        RuleFor(author => author.Name)
            .NotEmpty()
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.The_field_0_is_required], _translations[I18N.Name]));
        RuleFor(author => author.Name)
            .MaximumLength(255)
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.Maximum_length_for_0_is_1], _translations[I18N.Name], 255));
    }
}
