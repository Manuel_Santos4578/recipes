﻿using DataContracts.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ModelsLibrary.Constants;
using Newtonsoft.Json;
using QuestPDF.Fluent;
using RecipesApp.Models.FormModels;
using RecipesApp.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using I18N = ModelsLibrary.Constants.AppConstants.I18N;

namespace RecipesApp.Controllers;

[Route("export")]
[Authorize]
public class ExportController : MyBaseController
{
    public ExportController(IConfiguration configuration, IWebHostEnvironment environment) : base(configuration, environment) { }

    [HttpGet]
    public IActionResult ExportForm()
    {
        HandleViewData();
        return View(new ExportFormModel());
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> ExportAsync(ExportFormModel model)
    {
        #region paths
        string export_path = model.ModelType switch
        {
            AppConstants.Model.Author => EnvironmentSetup.AuthorExportPath,
            AppConstants.Model.Category => EnvironmentSetup.CategoryExportPath,
            AppConstants.Model.Country => EnvironmentSetup.CountryExportPath,
            AppConstants.Model.Course => EnvironmentSetup.CourseExportPath,
            AppConstants.Model.Ingredient => EnvironmentSetup.IngredientExportPath,
            AppConstants.Model.Recipe => EnvironmentSetup.RecipeExportPath,
            AppConstants.Model.Region => EnvironmentSetup.RegionExportPath,
            AppConstants.Model.Source => EnvironmentSetup.SourceExportPath,
            _ => throw new Exception("Invalid model"),
        };
        string file_extension = model.ExportType switch
        {
            AppConstants.ExportTypes.Pdf => "pdf",
            AppConstants.ExportTypes.Json => "json",
            AppConstants.ExportTypes.Excel => "xlsx",
            _ => throw new Exception("Invalid export type")
        };
        string file_type = model.ExportType switch
        {
            AppConstants.ExportTypes.Pdf => "application/pdf",
            AppConstants.ExportTypes.Json => "application/json",
            AppConstants.ExportTypes.Excel => "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            _ => throw new Exception("Invalid export type")
        };
        string filename = $"{DateTime.Now.Ticks}.{file_extension}";
        string file_path_name = Path.Combine(export_path, filename);

        #endregion
        await ApiAuthAsync();

        try
        {
            switch (model.ExportType)
            {
                case AppConstants.ExportTypes.Json:
                    #region Json
                    Formatting formatting = Formatting.None;
                    switch (model.ModelType)
                    {
                        case AppConstants.Model.Author:
                            System.IO.File.WriteAllText(file_path_name, JsonConvert.SerializeObject(await dal.SelectAllAuthorsAsync(), formatting));
                            break;
                        case AppConstants.Model.Category:
                            System.IO.File.WriteAllText(file_path_name, JsonConvert.SerializeObject(await dal.SelectAllCategoriesAsync(), formatting));
                            break;
                        case AppConstants.Model.Country:
                            System.IO.File.WriteAllText(file_path_name, JsonConvert.SerializeObject(await dal.SelectAllCountriesAsync(), formatting));
                            break;
                        case AppConstants.Model.Course:
                            System.IO.File.WriteAllText(file_path_name, JsonConvert.SerializeObject(await dal.SelectAllCoursesAsync(), formatting));
                            break;
                        case AppConstants.Model.Ingredient:
                            System.IO.File.WriteAllText(file_path_name, JsonConvert.SerializeObject(await dal.SelectAllIngredientsAsync(), formatting));
                            break;
                        case AppConstants.Model.Recipe:
                            IEnumerable<IRecipe> recipes = await dal.SelectAllRecipesAsync();
                            for (int i = 0; i < recipes.Count(); i++)
                            {
                                IRecipe recipe = recipes.ElementAt(i);
                                recipe.Ingredients = await dal.SelectAllRecipeIngredientsAsync(recipe);
                            }
                            System.IO.File.WriteAllText(file_path_name, JsonConvert.SerializeObject(await dal.SelectAllRecipesAsync(), formatting));
                            break;
                        case AppConstants.Model.Region:
                            System.IO.File.WriteAllText(file_path_name, JsonConvert.SerializeObject(await dal.SelectAllRegionsAsync(), formatting));
                            break;
                        case AppConstants.Model.Source:
                            System.IO.File.WriteAllText(file_path_name, JsonConvert.SerializeObject(await dal.SelectAllRecipeSourceAsync(), formatting));
                            break;
                        default:
                            break;
                    }
                    #endregion
                    break;
                case AppConstants.ExportTypes.Excel:
                    #region Excel
                    DataTable table = null;
                    switch (model.ModelType)
                    {
                        case AppConstants.Model.Author:
                            IEnumerable<IAuthor> authors = await dal.SelectAllAuthorsAsync();
                            table = ModelsLibrary.Models.Author.ExportToDataTable(authors);
                            break;
                        case AppConstants.Model.Category:
                            IEnumerable<ICategory> categories = await dal.SelectAllCategoriesAsync();
                            table = ModelsLibrary.Models.Category.ExportToDataTable(categories);
                            break;
                        case AppConstants.Model.Country:
                            IEnumerable<ICountry> countries = await dal.SelectAllCountriesAsync();
                            table = ModelsLibrary.Models.Country.ExportToDataTable(countries);
                            break;
                        case AppConstants.Model.Course:
                            IEnumerable<ICourse> courses = await dal.SelectAllCoursesAsync();
                            table = ModelsLibrary.Models.Course.ExportToDataTable(courses);
                            break;
                        case AppConstants.Model.Ingredient:
                            IEnumerable<IIngredient> ingredients = await dal.SelectAllIngredientsAsync();
                            table = ModelsLibrary.Models.Ingredient.ExportToDataTable(ingredients);
                            break;
                        case AppConstants.Model.Recipe:
                            IEnumerable<IRecipe> recipes = await dal.SelectAllRecipesAsync();
                            table = ModelsLibrary.Models.Recipe.ExportToDataTable(recipes);
                            break;
                        case AppConstants.Model.Region:
                            IEnumerable<IRegion> regions = await dal.SelectAllRegionsAsync();
                            table = ModelsLibrary.Models.Region.ExportToDataTable(regions);
                            break;
                        case AppConstants.Model.Source:
                            IEnumerable<ISource> sources = await dal.SelectAllRecipeSourceAsync();
                            table = ModelsLibrary.Models.Source.ExportToDataTable(sources);
                            break;
                        default:
                            throw new NotImplementedException();
                    }
                    if (table is not null)
                    {
                        ExcelExportService.Export(file_path_name, table);
                    }
                    #endregion
                    break;
                case AppConstants.ExportTypes.Pdf:
                    #region Pdf
                    Document document = model.ModelType switch
                    {
                        AppConstants.Model.Author => PdfExport.ExportList(await dal.SelectAllAuthorsAsync()),
                        AppConstants.Model.Category => PdfExport.ExportList(await dal.SelectAllCategoriesAsync()),
                        AppConstants.Model.Country => PdfExport.ExportList(await dal.SelectAllCountriesAsync()),
                        AppConstants.Model.Course => PdfExport.ExportList(await dal.SelectAllCoursesAsync()),
                        AppConstants.Model.Ingredient => PdfExport.ExportList(await dal.SelectAllIngredientsAsync()),
                        AppConstants.Model.Recipe => PdfExport.ExportList(await dal.SelectAllRecipesAsync()),
                        AppConstants.Model.Region => PdfExport.ExportList(await dal.SelectAllRegionsAsync()),
                        AppConstants.Model.Source => PdfExport.ExportList(await dal.SelectAllRecipeSourceAsync()),
                        _ => throw new NotImplementedException()
                    };
                    document.GeneratePdf(file_path_name);
                    break;
                #endregion
                default:
                    break;
            }
            FileStream str = new FileStream(file_path_name, FileMode.Open);
            byte[] content = new byte[str.Length];
            str.Read(content, 0, (int)str.Length);
            str.Close();
            System.IO.File.Delete(file_path_name);
            TempData["Success"] = string.Format(translation[I18N.Export_of_0_to_1_started_successfully], ExportFormModel.GetEnumValueName(model.ModelType), ExportFormModel.GetEnumValueName(model.ExportType));
            return File(content, file_type, filename);
        }
        catch (Exception)
        {
            TempData["Error"] = string.Format(translation[I18N.Export_of_0_to_1_failed], ExportFormModel.GetEnumValueName(model.ModelType), ExportFormModel.GetEnumValueName(model.ExportType));
        }
        return RedirectToAction(nameof(ExportForm), "Export");
    }

    [HttpGet("{slug}")]
    public async Task<IActionResult> ExportRecipeAsync(string slug)
    {
        if (string.IsNullOrWhiteSpace(slug))
        {
            return NotFound();
        }
        await ApiAuthAsync();
        IRecipe recipe = await dal.SelectRecipeBySlugAsync(slug);
        recipe.Ingredients = await dal.SelectAllRecipeIngredientsAsync(recipe);
        recipe.Courses = await dal.SelectRecipeCoursesAsync(recipe);

        Document doc = PdfExport.ExportRecipe(recipe);

        string export_path = EnvironmentSetup.RecipeExportPath;
        string file_extension = "pdf";
        string file_type = "application/pdf";
        string filename = $"{DateTime.Now.Ticks}.{file_extension}";
        string file_path_name = Path.Combine(export_path, filename);

        doc.GeneratePdf(file_path_name);
        FileStream str = new FileStream(file_path_name, FileMode.Open);
        byte[] content = new byte[str.Length];
        str.Read(content, 0, (int)str.Length);
        str.Close();
        System.IO.File.Delete(file_path_name);
        return File(content, file_type, filename);
    }
}