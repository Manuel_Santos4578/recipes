﻿using DataContracts.Entities;
using ModelsLibrary.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace RecipesApp.Models.FormModels;

public sealed class CategoryFormModel : Simple
{
    public int ID { get; set; }
    public string Name { get; set; }
    public string FriendlyUrl { get; set; }
    public int BaseCategoryId { get; set; }
    public IList<ICategory> Categories { get; set; }
    public CategoryFormModel()
    {
        Categories = new List<ICategory>();
    }
    public CategoryFormModel(ICategory category)
    {
        ID = category.ID;
        Name = category.Name;
        FriendlyUrl = category.FriendlyUrl;
        CreatedAt = category.CreatedAt;
        UpdatedAt = category.UpdatedAt;
        DeletedAt = category.DeletedAt;
        BaseCategoryId = category.BaseCategory?.ID ?? 0;
    }
}
