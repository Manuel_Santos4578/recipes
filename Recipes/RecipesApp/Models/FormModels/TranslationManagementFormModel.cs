﻿using System.Collections.Generic;
using System.Globalization;

namespace RecipesApp.Models.FormModels;

public class TranslationManagementFormModel
{
    public CultureInfo CultureInfo { get; set; }
    public Dictionary<string, string> Translations { get; set; }
}
