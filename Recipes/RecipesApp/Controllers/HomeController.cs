﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using RecipesApp.Models;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace RecipesApp.Controllers;

[Authorize]
public sealed class HomeController : MyBaseController
{

    public HomeController(IConfiguration config, IWebHostEnvironment env) : base(config, env)
    {
    }

    public async Task<IActionResult> IndexAsync()
    {
        await ApiAuthAsync();
        HomeViewModel model = new HomeViewModel
        {
            FavoriteRecipes = await dal.SelectFavoritesAsync(int.Parse(User.FindFirst("ID").Value)),
        };
        HandleViewData();
        return View(model);
    }

    //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    [HttpGet("error")]
    public IActionResult Error()
    {
        return View("NotFound");
    }
}
