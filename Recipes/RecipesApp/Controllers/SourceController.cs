﻿using DataContracts.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using RecipesApp.Models;
using RecipesApp.Models.FormModels;
using RecipesApp.Validators.FormValidation;
using RecipesLibrary.Util;
using System.Linq;
using System.Threading.Tasks;
using Util;
using I18N = ModelsLibrary.Constants.AppConstants.I18N;

namespace RecipesApp.Controllers;

[Route("sources")]
[Authorize]
public sealed class SourceController : MyBaseController
{
    private readonly SourceViewModel model;
    public SourceController(IConfiguration config, IWebHostEnvironment env) : base(config, env)
    {
        model = new SourceViewModel();
    }

    [HttpGet]
    public async Task<IActionResult> IndexAsync()
    {
        await ApiAuthAsync();
        HandleViewData();
        return View(model);
    }

    [HttpPost]
    public async Task<IActionResult> IndexAjaxAsync()
    {
        JToken body = await Request.Body.ReadObject<JToken>();
        int page = body.Value<int>("page");
        int page_size = body.Value<int>("size");
        string name = body.Value<string>("name");
        await ApiAuthAsync();
        SourceViewModel model = new SourceViewModel
        {
            PageSize = page_size,
            CurrentPage = page,
            Count = await dal.SelectRecipeSourceCountAsync(name),
            Sources = await dal.SelectAllRecipeSourceAsync(name, page, page_size)
        };
        return Ok(model);
    }
    [HttpGet("show/{friendly_url}")]
    public async Task<IActionResult> ShowAsync(string friendly_url)
    {
        await ApiAuthAsync();
        model.Source = await dal.SelectSourceBySlugAsync(friendly_url);
        if (model.Source is null)
        {
            return View("NotFound");
        }
        HandleViewData();

        return View(model);
    }

    [HttpGet("create")]
    public IActionResult Create()
    {
        return View(new SourceFormModel());
    }

    [HttpPost("store")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> StoreAsync(SourceFormModel model)
    {
        FluentValidation.Results.ValidationResult validation_result = await new SourceFormModelValidator().ValidateAsync(model);
        if (!validation_result.IsValid)
        {
            TempData["Error"] = string.Join("; ", validation_result.Errors.Select(err => err.ErrorMessage));
            return RedirectToAction("Index", "Source");
        }
        await ApiAuthAsync();
        model.Name = model.Name.Sanitize();

        ISource source = await dal.InsertSourceAsync(model);
        if (source is not null)
        {
            TempData["Success"] = string.Format(translation[I18N.Successfully_created], source.Name);
            return RedirectToAction("Show", "Source", new { friendly_url = source.FriendlyUrl });
        }
        TempData["Error"] = string.Format(translation[I18N.Failed_creation_of], source.Name);
        return RedirectToAction("Index", "Source");
    }

    [HttpGet("edit/{friendly_url}")]
    public async Task<IActionResult> EditAsync(string friendly_url)
    {
        await ApiAuthAsync();
        ISource source = await dal.SelectSourceBySlugAsync(friendly_url);
        if (source is null)
        {
            return NotFound();
        }
        SourceFormModel formModel = new SourceFormModel(source);
        return View(formModel);
    }

    [HttpPost("update/{friendly_url}")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> UpdateAsync(SourceFormModel model, string friendly_url)
    {
        FluentValidation.Results.ValidationResult validation_result = await new SourceFormModelValidator().ValidateAsync(model);
        if (!validation_result.IsValid)
        {
            TempData["Error"] = string.Join("; ", validation_result.Errors.Select(err => err.ErrorMessage));
            return RedirectToAction("Index", "Source");
        }
        await ApiAuthAsync();
        model.Name = model.Name.Sanitize();
        model.FriendlyUrl = friendly_url;

        ISource source = await dal.UpdateSourceAsync(model);
        if (source is not null)
        {
            TempData["Success"] = string.Format(translation[I18N.Successfully_updated], source.Name);
            return RedirectToAction("Show", "Source", new { friendly_url = source.FriendlyUrl });
        }
        TempData["Error"] = string.Format(translation[I18N.Failed_to_update], source.Name);
        return RedirectToAction("Show", "Source", new { friendly_url });
    }

    [HttpGet("delete/{friendly_url}")]
    public async Task<IActionResult> DeleteAsync(string friendly_url)
    {
        await ApiAuthAsync();
        ISource source = await dal.SelectSourceBySlugAsync(friendly_url);
        return View(new SourceFormModel(source));
    }

    [HttpPost("destroy")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DestroyAsync(SourceFormModel model)
    {
        await ApiAuthAsync();
        bool deleted = await dal.DeleteSourceAsync(model);
        if (deleted)
        {
            TempData["Success"] = string.Format(translation[I18N.Successfully_deleted], model.Name);
            return RedirectToAction("Index", "Source");
        }
        TempData["Error"] = string.Format(translation[I18N.Failed_to_delete], model.Name);
        return RedirectToAction("Show", "Source", new { friendly_url = model.FriendlyUrl });
    }
}