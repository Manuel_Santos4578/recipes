﻿using AngleSharp.Common;
using Microsoft.Extensions.Caching.Memory;
using ModelsLibrary.Constants;
using ModelsLibrary.Localization;
using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace RecipesApp.Services;

public static class CacheManagementService
{
    private static IMemoryCache _cache;
    private static MemoryCacheEntryOptions _entryOptions;
    private static IMemoryCache Cache
    {
        get { return _cache; }
        set { _cache = value; }
    }
    public static MemoryCacheEntryOptions EntryOptions
    {
        get
        {
            _entryOptions ??= new MemoryCacheEntryOptions().SetPriority(CacheItemPriority.NeverRemove);
            return _entryOptions;
        }
    }

    public static void UseCache(IMemoryCache cache)
    {
        if (_cache is null)
        {
            Cache = cache;
        }
    }

    public async static Task<CultureInfo[]> CulturesAsync()
    {
        CultureInfo[] cultures = Cache.Get<CultureInfo[]>(AppConstants.Caching.CULTURES);
        if (cultures is null)
        {
            DataAccess.DataAccess dal = new DataAccess.DataAccess(Environment.GetEnvironmentVariable(AppConstants.RecipesAppEnv.ApiDomain).ToString());
            await dal.ApiAuthenticateAsync();
            cultures = (await dal.SelectCulturesAsync()).Select(c => new CultureInfo(c.Name)).ToArray();
            Cache.Set(AppConstants.Caching.CULTURES, cultures, EntryOptions);
        }
        return cultures;
    }

    public async static Task<LocalizationCache> TranslationsAsync()
    {
        LocalizationCache translations = Cache.Get<LocalizationCache>(AppConstants.Caching.TRANSLATIONS);
        if (translations is null)
        {
            DataAccess.DataAccess dal = new DataAccess.DataAccess(Environment.GetEnvironmentVariable(AppConstants.RecipesAppEnv.ApiDomain).ToString());
            await dal.ApiAuthenticateAsync();
            translations = await dal.SelectAllTranslations();
            Cache.Set(AppConstants.Caching.TRANSLATIONS, translations, EntryOptions);
        }
        return translations;
    }

    public async static Task<CultureInfo[]> CulturesAvailableForInstallationAsync()
    {
        CultureInfo[] cultures = Cache.Get<CultureInfo[]>(AppConstants.Caching.AVAILABLE);
        if (cultures is null)
        {
            var installed_cultures = await CulturesAsync();
            cultures = CultureInfo.GetCultures(CultureTypes.AllCultures).Except(installed_cultures).Where(cul => cul.Name != "").ToArray();
            Cache.Set(AppConstants.Caching.AVAILABLE, cultures, EntryOptions);
        }
        return cultures;
    }

    public async static Task RefreshCulturesAsync()
    {
        DataAccess.DataAccess dal = new DataAccess.DataAccess(Environment.GetEnvironmentVariable(AppConstants.RecipesAppEnv.ApiDomain).ToString());
        await dal.ApiAuthenticateAsync();
        Cache.Remove(AppConstants.Caching.CULTURES);
        Cache.Remove(AppConstants.Caching.AVAILABLE);
        _ = await CulturesAsync();
        //_ = await CulturesAvailableForInstallationAsync();
    }

    public async static Task RefreshTranslationsAsync()
    {
        DataAccess.DataAccess dal = new DataAccess.DataAccess(Environment.GetEnvironmentVariable(AppConstants.RecipesAppEnv.ApiDomain).ToString());
        await dal.ApiAuthenticateAsync();
        Cache.Remove(AppConstants.Caching.TRANSLATIONS);
        _ = await TranslationsAsync();
    }
}
