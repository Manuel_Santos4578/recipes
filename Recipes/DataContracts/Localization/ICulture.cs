﻿namespace DataContracts.Localization;

public interface ICulture
{
    string DisplayName { get; set; }
    string Name { get; set; }
}