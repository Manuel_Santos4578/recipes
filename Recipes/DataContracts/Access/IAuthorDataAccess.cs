﻿using DataContracts.Entities;

namespace DataContracts.Access;

public partial interface IAuthorDataAccess
{
    Task<bool> DeleteAuthorAsync(IAuthor author);
    Task<IAuthor> InsertAuthorAsync(IAuthor author);
    Task<IEnumerable<IAuthor>> SelectAllAuthorsAsync(int page = 1, int page_size = 0, string name = "");
    Task<IAuthor> SelectAuthorBySlugAsync(string slug);
    Task<int> SelectAuthorCountAsync(string name = "");
    Task<IAuthor> UpdateAuthorAsync(IAuthor author);
}