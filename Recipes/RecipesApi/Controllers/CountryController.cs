﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ModelsLibrary.Models;
using Recipes.Database.Repositories;
using RecipesApi.Configuration;
using RecipesLibrary.Util;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Util;

namespace RecipesApi.Controllers;

/// <summary>
/// Country
/// </summary>
[Route("api/countries")]
[Produces("application/json")]
[Authorize(Roles = $"{ConfigurationState.ApiRoles.Write}, {ConfigurationState.ApiRoles.Root}")]
public class CountryController : BaseApiController
{
    public CountryController() : base()
    {

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="region"></param>
    /// <param name="name"></param>
    /// <param name="count"></param>
    /// <param name="page"></param>
    /// <param name="size"></param>
    /// <returns></returns>
    [HttpGet]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public IActionResult Index(int region, string name, bool count = false, int page = 1, int size = 0)
    {
        try
        {
            if (count)
            {
                return Ok(new { count = CountryRepository.ReadCount(region, name) });
            }
            return Ok(CountryRepository.ReadAll(region, name, page, size));
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Select By slug
    /// </summary>
    /// <param name="friendly_url">friendly url</param>
    /// <returns>Country collection</returns>
    [HttpGet("{friendly_url}")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public ActionResult<Country> Show(string friendly_url, bool with_trashed = false)
    {
        try
        {
            Country country = CountryRepository.ReadyBySlug(friendly_url, with_trashed);
            return Ok(country);
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Insert method
    /// </summary>
    /// <returns>inserted name</returns>
    [HttpPost]
    [ProducesResponseType(500)]
    [ProducesResponseType(201)]
    [ProducesResponseType(401)]
    public async Task<IActionResult> StoreAsync()
    {
        try
        {
            Country country = await Request.Body.ReadObject<Country>();
            country.FriendlyUrl = Slug.GenerateSlug(country.Name);
            country.FriendlyUrl = Validator.ValidateSlug(country.FriendlyUrl, CountryRepository.ReadyBySlug);
            country = CountryRepository.Store(country);
            int status = country is not null ? 201 : 200;
            return StatusCode(status, country);
        }
        catch (Exception)
        {

            throw;
        }
    }

    /// <summary>
    /// Bulk Store method
    /// </summary>
    /// <returns></returns>
    [HttpPost("bulk")]
    [ProducesResponseType(500)]
    [ProducesResponseType(201)]
    [ProducesResponseType(401)]
    public async Task<IActionResult> BulkStoreAsync()
    {
        try
        {
            IEnumerable<Country> countries = await Request.Body.ReadObject<IEnumerable<Country>>();
            countries = CountryRepository.BulkStore(countries);
            int status = countries is not null ? 201 : 200;
            return StatusCode(status, countries);
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Update method
    /// </summary>
    /// <param name="id">PK</param>
    /// <returns></returns>
    [HttpPut("{id:int}")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public async Task<IActionResult> UpdateAsync(int id)
    {
        try
        {
            Country country = await Request.Body.ReadObject<Country>();
            string slug = Slug.GenerateSlug(country.Name);
            if (slug != country.FriendlyUrl)
            {
                country.FriendlyUrl = Validator.ValidateSlug(slug, CountryRepository.ReadyBySlug);
            }
            country.ID = id;
            return Ok(CountryRepository.Update(country));
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="id">PK</param>
    /// <returns></returns>
    [HttpDelete("{id:int}")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public IActionResult Delete(int id)
    {
        try
        {
            Country country = new Country { ID = id };
            return Ok(CountryRepository.SoftDelete(country));
        }
        catch (Exception)
        {
            throw;
        }
    }
}
