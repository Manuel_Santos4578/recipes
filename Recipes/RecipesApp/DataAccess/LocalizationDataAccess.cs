﻿using DataContracts.Localization;
using Microsoft.AspNetCore.WebUtilities;
using ModelsLibrary.Localization;
using Newtonsoft.Json;
using RecipesLibrary.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Util;

namespace RecipesApp.DataAccess;

public sealed partial class DataAccess
{

    public async Task<IEnumerable<ICulture>> SelectCulturesAsync(string culture = "")
    {
        Dictionary<string, string> query = new Dictionary<string, string> { };
        if (!string.IsNullOrWhiteSpace(culture)) query.Add("culture", culture);
        Uri uri = new Uri(QueryHelpers.AddQueryString($"{culturesEndpoint}/cultures", query), UriKind.Relative);
        return (await new ResilientApiCaller(ApiDomain, ApiToken, 5).GetAsync<List<Culture>>(uri.ToString())).Cast<ICulture>();
    }
    public async Task<ICulture> InsertCultureAsync(ICulture culture)
    {
        StringContent content = new StringContent(JsonConvert.SerializeObject(culture));
        culture = await new ApiClientV3(ApiDomain, ApiToken).PostAsync<Culture>($"{culturesEndpoint}/cultures", content);
        return culture;
    }
    public async Task<ICulture> DeleteCultureAsync(ICulture culture)
    {
        culture = await new ApiClientV3(ApiDomain, ApiToken).DeleteAsync<Culture>($"{culturesEndpoint}/cultures/{culture.Name}");
        return culture;
    }
    public async Task<IEnumerable<ITranslation>> SelectTranslationsAsync(string culture)
    {
        if (string.IsNullOrWhiteSpace(culture))
        {
            throw new Exception($"Culture must have a value");
        }
        string uri = $"{culturesEndpoint}/translations/{culture}";
        return (await new ResilientApiCaller(ApiDomain, ApiToken, 5).GetAsync<List<Translation>>(uri)).Cast<ITranslation>();
    }
    public async Task<LocalizationCache> SelectAllTranslations()
    {
        await this.ApiAuthenticateAsync();
        IEnumerable<ICulture> cultures = await SelectCulturesAsync();
        LocalizationCache cache = new LocalizationCache();
        foreach (ICulture culture in cultures)
        {
            Dictionary<string, string> translations = (await SelectTranslationsAsync(culture.Name)).ToDictionary(t => t.Key, t => t.Value);
            cache[culture.Name] = translations;
        }
        return cache;
    }
    public async Task<ITranslation> InsertTranslationKeyAsync(ITranslation translation)
    {
        StringContent content = new StringContent(JsonConvert.SerializeObject(translation));
        translation = await new ApiClientV3(ApiDomain, ApiToken).PostAsync<Translation>($"{culturesEndpoint}/translations", content);
        return translation;
    }
    public async Task<IEnumerable<ITranslation>> UpdateTranslationsAsync(IEnumerable<ITranslation> translations)
    {
        StringContent content = new StringContent(JsonConvert.SerializeObject(translations));
        translations = await new ApiClientV3(ApiDomain, ApiToken).PutAsync<List<Translation>>($"{culturesEndpoint}/translations/bulk", content);
        return translations;
    }
}
