﻿using DataContracts.Localization;
using Newtonsoft.Json;

namespace ModelsLibrary.Localization;

public class Translation : ITranslation
{
    [JsonProperty("i18n_key")]
    public string Key { get; set; }
    [JsonProperty("i18n_value")]
    public string Value { get; set; }
    [JsonProperty("i18n_culture", NullValueHandling = NullValueHandling.Ignore)]
    [JsonConverter(typeof(JsonConverters.JsonConverter<Culture>))]
    public ICulture Culture { get; set; }
}