﻿using DataContracts.Localization;
using Newtonsoft.Json;

namespace ModelsLibrary.Localization;

public class Culture : ICulture
{
    [JsonProperty("i18n_culture_name")]
    public string Name { get; set; }
    [JsonProperty("i18n_culture_display_name")]
    public string DisplayName { get; set; }
}