﻿using FluentValidation;
using RecipesApp.Models.FormModels;
using static ModelsLibrary.Constants.AppConstants;

namespace RecipesApp.Validators.FormValidation;

public class CountryFormModelValidator : BaseFluentValidator<CountryFormModel>
{
    public CountryFormModelValidator() : base()
    {
        RuleFor(country => country.Name)
            .NotEmpty()
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.The_field_0_is_required], _translations[I18N.Name]));
        RuleFor(country => country.Name)
            .MaximumLength(255)
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.Maximum_length_for_0_is_1], _translations[I18N.Name], 255));
        RuleFor(country => country.RegionID)
            .GreaterThanOrEqualTo(1)
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.The_field_0_is_required], _translations[I18N.Region]));
        RuleFor(country => country.RegionID)
            .LessThanOrEqualTo(int.MaxValue)
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.The_field_0_is_required], _translations[I18N.Region]));
    }
}
