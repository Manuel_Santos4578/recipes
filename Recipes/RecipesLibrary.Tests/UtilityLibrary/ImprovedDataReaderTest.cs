using Util;

namespace RecipesLibrary.Tests.UtilityLibrary;

public class ImprovedDataReaderTest
{
    [Theory]
    [InlineData(null)]
    public void DateTimeTest(DateTime? test_date)
    {
        test_date = ImprovedDataReader.GetDate(test_date);
        Assert.NotNull(test_date);
    }

    [Theory]
    [InlineData(null)]
    public void DateTimeFromStringTest(string test_date_string)
    {
        DateTime? test_date = ImprovedDataReader.GetDate(test_date_string);
        Assert.NotNull(test_date);
    }

    [Theory]
    #region Data
    [InlineData("")]
    [InlineData(null)]
    [InlineData("Banana")]
    #endregion
    public void StringTest(string test_string)
    {
        test_string = ImprovedDataReader.GetString(test_string);
        Assert.NotNull(test_string);
    }

    [Theory]
    #region Data
    [InlineData(null)]
    [InlineData(-1)]
    [InlineData(0)]
    [InlineData(1)]
    #endregion
    public void Int32Test(int? test_int32)
    {
        test_int32 = ImprovedDataReader.GetInt32(test_int32);
        Assert.NotNull(test_int32);
    }

    [Theory]
    #region Data
    [InlineData(null)]
    [InlineData(-1)]
    [InlineData(0)]
    [InlineData(1)]
    #endregion
    public void Int64Test(long? test_int64)
    {
        test_int64 = ImprovedDataReader.GetInt64(test_int64);
        Assert.NotNull(test_int64);
    }

    [Theory]
    #region Data
    [InlineData(null)]
    [InlineData(-0.25)]
    [InlineData(-1)]
    [InlineData(-1.25)]
    [InlineData(0)]
    [InlineData(0.25)]
    [InlineData(1)]
    [InlineData(1.25)]
    #endregion
    public void DoubleTest(double? test_double)
    {
        test_double = ImprovedDataReader.GetDouble(test_double);
        Assert.NotNull(test_double);
    }

    [Theory]
    #region Data
    [InlineData(null)]
    [InlineData(-1.25f)]
    [InlineData(-1f)]
    [InlineData(-0.25f)]
    [InlineData(0f)]
    [InlineData(0.25f)]
    [InlineData(1f)]
    [InlineData(1.25f)]
    #endregion
    public void FloatTest(float? test_double)
    {
        test_double = ImprovedDataReader.GetFloat(value: test_double);
        Assert.NotNull(test_double);
    }
}