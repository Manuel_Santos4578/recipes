﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ModelsLibrary.Models;
using Recipes.Database.Repositories;
using RecipesApi.Configuration;
using RecipesLibrary.Util;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Util;

namespace RecipesApi.Controllers;

[Route("api/courses")]
[ApiController]
[Produces("application/json")]
[Authorize(Roles = $"{ConfigurationState.ApiRoles.Write}, {ConfigurationState.ApiRoles.Root}")]
/// <summary>
/// Course
/// </summary>
public class CourseController : BaseApiController
{
    public CourseController(IQueryExecutor _executor) : base(_executor)
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="name"></param>
    /// <param name="count"></param>
    /// <param name="page"></param>
    /// <param name="size"></param>
    /// <returns></returns>
    [HttpGet]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public IActionResult Index(string name, bool count = false, int page = 1, int size = 0)
    {
        try
        {
            if (count)
            {
                return Ok(new { count = CourseRepository.ReadCount(name) });
            }
            return Ok(CourseRepository.ReadAll(name, page, size));
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Select by friendly_url method
    /// </summary>
    /// <param name="friendly_url"></param>
    /// <returns></returns>
    [HttpGet("{friendly_url}")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public ActionResult<Course> Show(string friendly_url, bool with_trashed = false)
    {
        try
        {
            return Ok(CourseRepository.ReadBydSlug(friendly_url, with_trashed));
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Store method
    /// </summary>
    /// <remarks>
    /// 
    ///     {
    ///         "course_name": string,
    ///         "course_friendly_url": string
    ///     }
    /// 
    /// </remarks>
    /// <returns></returns>
    [HttpPost]
    [ProducesResponseType(500)]
    [ProducesResponseType(201)]
    [ProducesResponseType(401)]
    public async Task<IActionResult> Store()
    {
        try
        {
            Course course = await Request.Body.ReadObject<Course>();
            course.FriendlyUrl = Slug.GenerateSlug(course.FriendlyUrl);
            course.FriendlyUrl = Validator.ValidateSlug(course.FriendlyUrl, CourseRepository.ReadBydSlug);
            CourseRepository.Store(course);
            int status = course is not null ? 201 : 200;
            return StatusCode(status, course);
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Bulk Store method
    /// </summary>
    /// <returns></returns>
    [HttpPost("bulk")]
    [ProducesResponseType(500)]
    [ProducesResponseType(201)]
    [ProducesResponseType(401)]
    public async Task<IActionResult> BulkStoreAsync()
    {
        IEnumerable<Course> courses = await StreamParser.ReadObject<IEnumerable<Course>>(Request.Body);
        courses = CourseRepository.BulkStore(courses);
        return Ok(courses);
    }

    /// <summary>
    /// Update method
    /// </summary>
    /// <param name="id"></param>
    /// <remarks>
    ///     
    ///     {
    ///         "course_name": string,
    ///         "course_friendly_url": string
    ///     }
    ///     
    /// </remarks>
    /// <returns></returns>
    [HttpPut("{id:int}")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public async Task<IActionResult> Update(int id)
    {
        try
        {
            Course course = await Request.Body.ReadObject<Course>();
            string slug = Slug.GenerateSlug(course.FriendlyUrl);
            if (slug != course.FriendlyUrl)
            {
                course.FriendlyUrl = Validator.ValidateSlug(slug, CourseRepository.ReadBydSlug);
            }
            course.ID = id;
            return Ok(CourseRepository.Update(course));
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Delete method
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpDelete("{id:int}")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public IActionResult Delete(int id)
    {
        try
        {
            Course course = new Course { ID = id };
            return Ok(CourseRepository.SoftDelete(course));
        }
        catch (Exception)
        {
            throw;
        }
    }
}
