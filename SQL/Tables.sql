
use Recipes
GO
create table ApiRoles(
	id int identity(1,1) primary key,
	name varchar(255)
)
GO
create table ApiUser(
	username varchar(255) primary key,
	password varchar(max),
	role_id int foreign key references ApiRoles(id)
)
GO
create table Users(
	id int identity(1,1) primary key,
	name varchar(255) not null,
	username varchar(255) not null unique,
	password varchar(max) not null, 
	created_at datetime not null default getdate(),
	updated_at datetime not null default getdate(),
	deleted_at datetime
)
GO
create table Logs(
	id int identity(1,1) primary key,
	name varchar(255),
	JsonObject varchar(max),
	created_at datetime not null default getdate()
)
GO
create table Region(
	id int identity(1,1) primary key,
	name varchar(255),
	friendly_url varchar(255) unique,
	created_at datetime not null default getdate(),
	updated_at datetime not null default getdate(),
	deleted_at datetime
)
GO
create table Country(
	id int identity(1,1) primary key,
	name varchar(255),
	friendly_url varchar(255) unique,
	region_id int foreign key references Region(id),
	created_at datetime not null default getdate(),
	updated_at datetime not null default getdate(),
	deleted_at datetime
)
GO
create table RecipeSource(
	id int identity(1,1) primary key,
	name varchar(255),
	friendly_url varchar(255) unique,
	created_at datetime not null default getdate(),
	updated_at datetime not null default getdate(),
	deleted_at datetime
)
GO
create table Author(
	id int identity(1,1) primary key,
	name varchar(255),
	friendly_url varchar(255) unique,
	created_at datetime not null default getdate(),
	updated_at datetime not null default getdate(),
	deleted_at datetime
)
GO
create table Category(
	id int identity(1,1) primary key,
	name varchar(255),
	friendly_url varchar(255) unique,
	base_category_id int,
	created_at datetime not null default getdate(),
	updated_at datetime not null default getdate(),
	deleted_at datetime
)
GO
alter table Category
add constraint FK_CateGoy_base_category_id foreign key(base_category_id) references Category(id)
GO
create table Course(
	id int identity(1,1) primary key,
	name varchar(255),
	friendly_url varchar(255) unique,
	created_at datetime not null default getdate(),
	updated_at datetime not null default getdate(),
	deleted_at datetime
)
GO
create table Ingredient(
	id int identity(1,1) primary key,
	name varchar(255),
	friendly_url varchar(255) unique,
	country_id int foreign key references Country(id),
	created_at datetime not null default getdate(),
	updated_at datetime not null default getdate(),
	deleted_at datetime
)
GO
create table Recipe(
	id int identity(1,1) primary key,
	name varchar(255),
	friendly_url varchar(255) unique,
	category_id int foreign key references Category(id),
	author_id int foreign key references Author(id),
	recipe_source_id int foreign key references RecipeSource(id),
	country_id int foreign key references Country(id),
	recipe_text varchar(max),
	created_at datetime not null default getdate(),
	updated_at datetime not null default getdate(),
	deleted_at datetime
)
GO
create table RecipeImages(
	id int identity(1,1),
	recipe_id int foreign key references Recipe(id),
	is_display bit not null default 0,
	image varchar(255),
	image_base_64 varchar(max),
	content_type varchar(255),
	created_at datetime not null default getdate(),
	updated_at datetime not null default getdate(),
	deleted_at datetime
)
GO
create table RecipeIngredient(
	ingredient_id int not null foreign key references Ingredient(id),
	recipe_id int not null foreign key references Recipe(id),
	amount float,
	unit varchar(10),
	created_at datetime not null default getdate(),
	updated_at datetime not null default getdate(),
	deleted_at datetime,
	constraint PK_RecipeIngredient primary key(ingredient_id,recipe_id)
)
GO
create table RecipeCourse(
	recipe_id int not null foreign key references Recipe(id),
	course_id int not null foreign key references Course(id),
	created_at datetime not null default getdate(),
	updated_at datetime not null default getdate(),
	deleted_at datetime,
	constraint PK_RecipeCourse primary key(recipe_id,course_id)
)
GO

create table Recipefavorite(
	recipe_id int ,
	user_id int,
	constraint FK_RecipeFavorite_Recipe foreign key(recipe_id) references Recipe(id),
	constraint FK_RecipeFavorite_User foreign key(recipe_id) references Users(id),
	constraint PK_RecipeFavorite primary key(recipe_id, user_id)
)
GO