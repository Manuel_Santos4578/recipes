﻿async function FillIngredientTable(content) {
    let table = document.getElementById('ingredient_table_div');
    let ingredient_len = content.ingredients.length;
    let keys = [
        "No records were found", "Name", "Quantity"
    ]
    let translations = await GetTranslations(keys);
    if (ingredient_len > 0) {
        let table_html = `
            <table class="table">
                <thead>
                    <tr>
                        <th>${translations["Name"]}</th>
                        <th class="text-center">${translations["Quantity"]}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="ingredients_body">{body}</tbody>
                <tfoot id="ingredients_foot"></tfoot>
            </table>
        `;
        let table_body = [];
        for (let ingredient of content.ingredients) {
            table_body.push(`
                <tr>
                    <td>${ingredient.ingredient_name}</td>
                    <td class="text-center">${ingredient.amount} ${ingredient.unit}</td>
                    <td class="d-flex justify-content-end">
                        <button type="button" class="btn btn-danger btn-sm remove_ingredient" data-id="${ingredient.ingredient_id}"><i class="fa fa-trash"></i></button>
                    </td>
                </tr>
            `);
        }
        table_html = table_html.replace('{body}', table_body.join(''))
        table.innerHTML = table_html;
        await AddRemoveIngredientEvent();
        return;
    }
    table.innerHTML = `
        <div class="d-flex justify-content-center align-content-center h-100">
            <p class="text">${translations["No records were found"]}</p>
        </div>`;
}

async function AddRecipeIngredient() {
    let keys = [
        "Ingredient", "Quantity", "Unit", "Add/Edit recipe ingredient", "Save"
    ]
    let translations = await GetTranslations(keys);
    let html = `
        <div class="row">
            <div class="col-sm-12">
                <label class="swal2-input-label mb-2">${translations["Ingredient"]}</label>
                <select id="ingredient_name" name="ingredient_name" class="form-control" required></select>
            </div>
            <div class="col-sm-6">
                <label class="swal2-input-label mb-2">${translations["Quantity"]}</label>
                <input type="number" id="qtd" name="qtd" value="" class="form-control text-center" required />
            </div>
            <div class="col-sm-6">
                <label class="swal2-input-label mb-2">${translations["Unit"]}</label>
                <input type="text" id="unit" name="unit" value="" class="form-control text-center" placeholder="g,mg,L,mL" required />
            </div>
        </div>
    `;
    Swal.fire({
        title: translations["Add/Edit recipe ingredient"],
        html: html,
        focusConfirm: false,
        showCancelButton: true,
        confirmButtonText: translations["Save"],
        confirmButtonColor: 'var(--bs-primary)',
        allowOutsideClick: false,
        didOpen: function () {
            Select2Setup($('#ingredient_name'), `${location.origin}/ingredients`, 'ingredients', $('.swal2-container'))
            $('select[name=ingredient_name]').on('select2:select', (event) => {
                document.getElementById('selected_ingredient_name').setAttribute('data-name', event.params.data.text)
            })
        },
        preConfirm: () => {
            let ingredients = document.getElementById('ingredient_name');
            return [
                ingredients.options[ingredients.selectedIndex].value,
                document.getElementById('qtd').value,
                document.getElementById('unit').value,
                document.getElementById('selected_ingredient_name').getAttribute('data-name'),
            ]
        }
    }).then(async (form) => {
        if (form.isConfirmed) {
            try {
                let response = await fetch(`${location.origin}/recipes/addingredient`, {
                    method: 'POST',
                    mode: 'cors',
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8'
                    },
                    body: JSON.stringify({
                        ingredient_id: form.value[0],
                        quantity: form.value[1],
                        unit: form.value[2],
                        name: form.value[3]
                    }),
                });
                let content = await response.json();
                content.ingredients = JSON.parse(content.ingredients);
                await FillIngredientTable(content);
            } catch (error) {
                Swal.fire({
                    title: '',
                    text: translations["An error occurred"],
                    icon: 'error',
                    confirmButtonText: 'Ok',
                    confirmButtonColor: 'var(--bs-primary)',
                })
            }
        }
    });

}
async function AddRemoveIngredientEvent() {
    let keys = [
        "An error occurred",
    ]
    let translations = await GetTranslations(keys)
    let remove_buttons = Array.from(document.getElementsByClassName('remove_ingredient'));
    for (const remove_button of remove_buttons) {
        remove_button.addEventListener('click', async function (e) {
            let ingredient_id = this.getAttribute('data-id');
            try {
                let response = await fetch(`${location.origin}/recipes/removeingredient`, {
                    method: 'POST',
                    mode: 'cors',
                    headers: {
                        'Content-Type': 'application/json;charset=UTF-8'
                    },
                    body: JSON.stringify({
                        ingredient_id: ingredient_id,
                    }),
                });
                let content = await response.json();
                content.ingredients = JSON.parse(content.ingredients);
                document.getElementById('session_ingredients').setAttribute('data-ingredients', JSON.stringify(content));
                await FillIngredientTable(content);
            } catch (error) {
                Swal.fire({
                    title: '',
                    text: translations["An error occurred"],
                    icon: 'error',
                    confirmButtonText: 'Ok'
                })
            }
        })
    }
}

async function GetIngredients(){
    let url = `${location.origin}/recipes/getingredients`;
    let response = await fetch(url, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        },
        body: JSON.stringify({}),
    });
    let content = await response.json();
    content.ingredients = JSON.parse(content.ingredients);
    return content;
}

document.addEventListener('DOMContentLoaded', async () => {
    let recipe_ingredients_content = await GetIngredients();
    await FillIngredientTable(recipe_ingredients_content);
    document.getElementById('add_ingredient').addEventListener('click', async (e) => {
       await AddRecipeIngredient();
    });
});