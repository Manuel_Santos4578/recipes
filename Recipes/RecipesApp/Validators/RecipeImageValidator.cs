﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RecipesApp.Validators;

public sealed class RecipeImageValidator : ValidationAttribute
{
    public FormFile Main { get; set; }
    public RecipeImageValidator()
    {
    }
    public override bool IsValid(object value)
    {
        List<IFormFile> misc = value as List<IFormFile>;
        return misc.Count != 0;
    }
}
