﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ModelsLibrary.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Util;
using Microsoft.AspNetCore.WebUtilities;
using DataContracts.Entities;
using System.Linq;

namespace RecipesApp.DataAccess;

public sealed partial class DataAccess
{
    #region Recipes
    public async Task<IEnumerable<IRecipe>> SelectAllRecipesAsync(string name = "", string author = "", string category = "", string country = "", string region = "", string source = "", string ingredient = "", int user = 0, int page = 1, int size = 0)
    {
        Dictionary<string, string> filters = new Dictionary<string, string>
        {
            {"name",name },
            {"author",author },
            {"category",category },
            {"country",country },
            {"region",region },
            {"source",source },
            {"ingredient",ingredient },
            {"user",user.ToString() },
            {"page",page.ToString() },
            {"size",size.ToString() }
        };
        Uri request_uri = filters != null ? new Uri(QueryHelpers.AddQueryString(recipesEndpoint, filters), UriKind.Relative) : new Uri(recipesEndpoint, UriKind.Relative);
        return await new ApiClientV3(ApiDomain, ApiToken).GetAsync<IEnumerable<Recipe>>(request_uri.ToString());
    }


    public async Task<int> SelectRecipesCountAsync(string name = "", string author = "", string category = "", string country = "", string region = "", string source = "", string ingredient = "", int user = 0)
    {
        Dictionary<string, string> filters = new Dictionary<string, string>
        {
            {"name",name },
            {"author",author },
            {"category",category },
            {"country",country },
            {"region",region },
            {"source",source },
            {"ingredient",ingredient },
            {"user", user.ToString() },
            {"count",true.ToString() },
        };
        Uri request_uri = new Uri(QueryHelpers.AddQueryString(recipesEndpoint, filters), UriKind.Relative);
        JToken data = await new ApiClientV3(ApiDomain, ApiToken).GetAsync<JToken>(request_uri.ToString());
        return data.Value<int>("count");
    }
    public async Task<IRecipe> SelectRecipeBySlugAsync(string slug)
    {
        return await new ApiClientV3(ApiDomain, ApiToken).GetAsync<Recipe>($"{recipesEndpoint}/{slug}");
    }
    public async Task<IRecipe> InsertRecipeAsync(IRecipe recipe)
    {
        using StringContent content = new StringContent(JsonConvert.SerializeObject(recipe));
        recipe = await new ApiClientV3(ApiDomain, ApiToken).PostAsync<Recipe>(recipesEndpoint, content);
        return recipe;
    }
    public async Task<IRecipe> UpdateRecipeAsync(IRecipe recipe)
    {
        string recipeString = JsonConvert.SerializeObject(recipe);
        using StringContent content = new StringContent(recipeString);
        recipe = await new ApiClientV3(ApiDomain, ApiToken).PutAsync<Recipe>($"{recipesEndpoint}/{recipe.ID}", content);
        return recipe;
    }
    public async Task<bool> DeleteRecipeAsync(IRecipe recipe)
    {
        recipe = await new ApiClientV3(ApiDomain, ApiToken).DeleteAsync<Recipe>($"{recipesEndpoint}/{recipe.ID}");
        return recipe is not null;
    }
    #endregion
    #region Recipe Ingredient
    public async Task<IList<IRecipeIngredient>> SelectAllRecipeIngredientsAsync(IRecipe recipe)
    {
        return (await new ApiClientV3(ApiDomain, ApiToken).GetAsync<List<RecipeIngredient>>($"{recipesEndpoint}/{recipe.FriendlyUrl}/ingredients"))
            .Cast<IRecipeIngredient>().ToList();
    }
    #endregion
    #region Courses
    public async Task<IList<ICourse>> SelectRecipeCoursesAsync(IRecipe recipe)
    {
        return (await new ApiClientV3(ApiDomain, ApiToken).GetAsync<IEnumerable<Course>>($"{recipesEndpoint}/{recipe.FriendlyUrl}/courses")).Cast<ICourse>().ToList();
    }
    #endregion
    #region Recipe Image
    public async Task<IList<IRecipeImage>> SelectRecipeImagesAsync(IRecipe recipe, bool is_display)
    {
        Dictionary<string, string> query = new Dictionary<string, string>
        {
            {nameof(is_display),is_display.ToString() },
            {"recipe_id",recipe.ID.ToString() }
        };
        Uri uri = new Uri(QueryHelpers.AddQueryString(recipeImagesEndpoint, query), UriKind.Relative);
        return (await new ApiClientV3(ApiDomain, ApiToken).GetAsync<IEnumerable<RecipeImage>>(uri.ToString())).Cast<IRecipeImage>().ToList();
    }
    #endregion
    #region Recipe Favorites
    public async Task<IEnumerable<IRecipe>> SelectFavoritesAsync(int user_id)
    {
        Dictionary<string, string> query = new Dictionary<string, string>
        {
            {nameof(user_id), user_id.ToString() },
        };
        Uri uri = new Uri(QueryHelpers.AddQueryString(recipeFavoritesEndpoint, query), UriKind.Relative);
        return await new ApiClientV3(ApiDomain, ApiToken).GetAsync<IEnumerable<Recipe>>(uri.ToString());
    }
    #endregion
}
