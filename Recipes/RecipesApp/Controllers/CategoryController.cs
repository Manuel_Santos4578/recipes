﻿using DataContracts.Entities;
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ModelsLibrary.Models;
using Newtonsoft.Json.Linq;
using RecipesApp.Models;
using RecipesApp.Models.FormModels;
using RecipesApp.Validators.FormValidation;
using RecipesLibrary.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Util;
using I18N = ModelsLibrary.Constants.AppConstants.I18N;

namespace RecipesApp.Controllers;

[Route("categories")]
[Authorize]
public sealed class CategoryController : MyBaseController
{
    public CategoryController(IConfiguration config, IWebHostEnvironment env) : base(config, env) { }

    [HttpGet]
    public IActionResult Index()
    {
        CategoryViewModel model = new CategoryViewModel();
        HandleViewData();
        return View(model);
    }

    [HttpPost]
    public async Task<IActionResult> IndexAjaxAsync()
    {
        JToken body = await Request.Body.ReadObject<JToken>();
        int page = body.Value<int>("page");
        int page_size = body.Value<int>("size");
        string name = body.Value<string>("name");
        await ApiAuthAsync();
        CategoryViewModel model = new CategoryViewModel
        {
            PageSize = page_size,
            CurrentPage = page,
            Count = await dal.SelectCategoryCountAsync(name, (int)Category.CategorySearch.Base),
            Categories = await dal.SelectAllCategoriesAsync(page, page_size, name, (int)Category.CategorySearch.Base)
        };
        return Ok(model);
    }

    [HttpGet("show/{friendly_url}")]
    public async Task<IActionResult> ShowAsync(string friendly_url)
    {
        if (string.IsNullOrWhiteSpace(friendly_url)) return BadRequest();
        await ApiAuthAsync();
        CategoryViewModel model = new CategoryViewModel
        {
            Category = await dal.SelectCategoryBySlugAsync(friendly_url)
        };
        if (model.Category is null) return View("NotFound");

        model.Categories = await dal.SelectAllCategoriesAsync(base_category: model.Category.ID);
        model.Count = await dal.SelectCategoryCountAsync(base_category: model.Category.ID);

        HandleViewData();
        return View(model);
    }

    [HttpGet("create")]
    public async Task<IActionResult> CreateAsync(string base_slug)
    {
        await ApiAuthAsync();

        CategoryFormModel form_model = new CategoryFormModel();
        if (!string.IsNullOrWhiteSpace(base_slug))
        {
            ICategory category = await dal.SelectCategoryBySlugAsync(base_slug);
            if (category is not null)
            {
                form_model.Categories.Add(category);
            }
        }
        return View(form_model);
    }

    [HttpPost("store")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> StoreAsync(CategoryFormModel model)
    {
        await ApiAuthAsync();
        FluentValidation.Results.ValidationResult validation_result = await new CategoryFormModelValidator().ValidateAsync(model);
        model.Categories = (await dal.SelectAllCategoriesAsync()).ToList();
        if (!validation_result.IsValid)
        {
            TempData["Error"] = string.Join("; ", validation_result.Errors.Select(err => err.ErrorMessage).ToArray());
            return RedirectToAction("Index", "Category");
        }
        model.Name = model.Name.Sanitize();
        
        ICategory category = new Category
        {
            Name = model.Name,
            BaseCategory = model.BaseCategoryId != 0 ? new Category { ID = model.BaseCategoryId } : null
        };

        category = await dal.InsertCategoryAsync(category);
        if (category is not null)
        {
            TempData["Success"] = string.Format(translation[I18N.Successfully_created], model.Name);
            return RedirectToAction("Show", "Category", new { friendly_url = category.FriendlyUrl });
        }
        TempData["Error"] = string.Format(translation[I18N.Failed_creation_of], model.Name);
        return RedirectToAction("Index", "Category");
    }

    [HttpGet("edit/{friendly_url}")]
    public async Task<IActionResult> EditAsync(string friendly_url)
    {
        await ApiAuthAsync();
        ICategory category = await dal.SelectCategoryBySlugAsync(friendly_url);

        if (category is null) return NotFound();

        CategoryFormModel form_model = new CategoryFormModel(category)
        {
            Categories = new List<ICategory>()
        };
        if (category.BaseCategory is not null)
        {
            form_model.Categories.Add(category.BaseCategory);
        }
        else
        {
            form_model.Categories.Add(new Category() { Name = translation[I18N.__Select__] });
        }
        return View(form_model);
    }

    [HttpPost("update/{friendly_url}")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> UpdateAsync(CategoryFormModel model, string friendly_url)
    {
        await ApiAuthAsync();
        FluentValidation.Results.ValidationResult validation_result = await new CategoryFormModelValidator().ValidateAsync(model);
        if (!validation_result.IsValid)
        {
            TempData["Error"] = string.Join("; ", validation_result.Errors.Select(err => err.ErrorMessage).ToArray());
            return Redirect(Request.Headers["Referer"]);
        }

        model.Name = model.Name.Sanitize();
        model.FriendlyUrl = friendly_url;

        ICategory category = new Category
        {
            ID = model.ID,
            Name = model.Name,
            FriendlyUrl = model.FriendlyUrl,
            BaseCategory = model.BaseCategoryId != 0 ? new Category { ID = model.BaseCategoryId } : null
        };
        category = await dal.UpdateCategoryAsync(category);
        if (category is not null)
        {
            TempData["Success"] = string.Format(translation[I18N.Successfully_updated], category.Name);
            return RedirectToAction("Show", "Category", new { friendly_url = category.FriendlyUrl });
        }
        TempData["Error"] = string.Format(translation[I18N.Failed_to_update], model.Name);
        return RedirectToAction("Show", "Category", new { friendly_url });
    }

    [HttpGet("delete/{friendly_url}")]
    public async Task<IActionResult> DeleteAsync(string friendly_url)
    {
        await ApiAuthAsync();
        CategoryViewModel model = new CategoryViewModel { };
        model.Category = await dal.SelectCategoryBySlugAsync(friendly_url);
        if (model.Category is null) return NotFound();

        CategoryFormModel form_model = new CategoryFormModel(model.Category);
        return View(form_model);
    }

    [HttpPost("destroy")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DestroyAsync(CategoryFormModel model)
    {
        await ApiAuthAsync();
        Category category = new Category { ID = model.ID };
        if (await dal.DeleteCategoryAsync(category))
        {
            TempData["Success"] = string.Format(translation[I18N.Successfully_deleted], model.Name);
            return RedirectToAction("Index", "Category");
        }
        TempData["Error"] = string.Format(translation[I18N.Failed_to_delete], model.Name);
        return RedirectToAction("Show", "Category", new { friendly_url = model.FriendlyUrl });
    }
}
