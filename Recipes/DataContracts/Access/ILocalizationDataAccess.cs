﻿using DataContracts.Localization;

namespace DataContracts.Access;

public partial interface ILocalizationDataAccess
{
    Task<IEnumerable<ICulture>> SelectCulturesAsync(string culture = "");
    Task<IEnumerable<ITranslation>> SelectTranslationsAsync(string culture);
    Task<ICulture> InsertCultureAsync(ICulture culture);
    Task<ICulture> DeleteCultureAsync(ICulture culture);
    Task<ITranslation> InsertTranslationKeyAsync(ITranslation translation);
    Task<IEnumerable<ITranslation>> UpdateTranslationsAsync(IEnumerable<ITranslation> translations);
}
