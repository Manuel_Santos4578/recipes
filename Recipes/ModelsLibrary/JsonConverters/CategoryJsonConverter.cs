﻿using DataContracts.Entities;
using ModelsLibrary.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace ModelsLibrary.JsonConverters;

internal class CategoryJsonConverter : JsonConverter
{
    public override bool CanWrite => false;
    public override bool CanRead => true;
    public override bool CanConvert(Type objectType)
    {
        return objectType == typeof(Category);
    }

    public override object ReadJson(JsonReader reader,
        Type objectType, object existingValue,
        JsonSerializer serializer)
    {
        var jsonObject = JObject.Load(reader);

        int id = (int)jsonObject["category_id"];
        string name = (string)jsonObject["category_name"];
        string friendlyUrl = (string)jsonObject["category_friendly_url"];
        Category baseCategory = null;
        if (jsonObject.TryGetValue("base_category", out JToken _))
        {

            baseCategory = jsonObject["base_category"].ToObject<Category>(serializer);
        }

        var instance = new Category
        {
            ID = id,
            Name = name,
            FriendlyUrl = friendlyUrl,
            BaseCategory = baseCategory
        };
        //serializer.Populate(jsonObject.CreateReader(), instance);
        return instance;
    }

    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
        JObject jo = JObject.FromObject(value, serializer);
        jo.AddFirst(new JProperty("Type", value.GetType().FullName));
        jo.WriteTo(writer);
    }
}
