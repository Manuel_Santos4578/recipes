﻿using AngleSharp.Text;
using FluentValidation;
using RecipesApp.Models.FormModels;
using System.IO;
using static ModelsLibrary.Constants.AppConstants;

namespace RecipesApp.Validators.FormValidation;

public class RecipeFormModelValidator : BaseFluentValidator<RecipeFormModel>
{
    public RecipeFormModelValidator() : base()
    {
        RuleFor(recipe => recipe.Name)
            .NotEmpty()
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.The_field_0_is_required], _translations[I18N.Name]));
        RuleFor(recipe => recipe.Name)
            .MaximumLength(255)
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.Maximum_length_for_0_is_1], _translations[I18N.Name], 255));

        RuleFor(recipe => recipe.Text)
            .NotEmpty()
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.The_field_0_is_required], _translations[I18N.Instructions]));

        // category
        RuleFor(recipe => recipe.CategoryId)
            .GreaterThanOrEqualTo(1)
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.Invalid_value], _translations[I18N.Category]));
        RuleFor(recipe => recipe.CategoryId)
            .LessThanOrEqualTo(int.MaxValue)
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.Invalid_value], _translations[I18N.Category]));
        
        // country
        RuleFor(recipe => recipe.CountryId)
            .GreaterThanOrEqualTo(1)
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.Invalid_value], _translations[I18N.Country]));
        RuleFor(recipe => recipe.CountryId)
            .LessThanOrEqualTo(int.MaxValue)
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.Invalid_value], _translations[I18N.Country]));

        // source
        RuleFor(recipe => recipe.SourceId)
            .GreaterThanOrEqualTo(1)
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.Invalid_value], _translations[I18N.Source]));
        RuleFor(recipe => recipe.SourceId)
            .LessThanOrEqualTo(int.MaxValue)
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.Invalid_value], _translations[I18N.Source]));

        // author
        RuleFor(recipe => recipe.AuthorId)
            .GreaterThanOrEqualTo(1)
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.Invalid_value], _translations[I18N.Author]));
        RuleFor(recipe => recipe.AuthorId)
            .LessThanOrEqualTo(int.MaxValue)
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.Invalid_value], _translations[I18N.Author]));
        
        // courses
        RuleForEach(recipe => recipe.CourseIds)
            .NotEmpty()
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.The_field_0_is_required], _translations[I18N.Courses]));
        RuleForEach(recipe => recipe.CourseIds)
            .GreaterThanOrEqualTo(1)
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.Invalid_value], _translations[I18N.Courses]));
        RuleForEach(recipe => recipe.CourseIds)
            .LessThanOrEqualTo(int.MaxValue)
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.Invalid_value], _translations[I18N.Courses]));

        string[] extensions = new string[] { ".jpg", ".png" };
        int max_file_size = 5 * 1024 * 1024;
        
        // main image
        RuleFor(recipe => recipe.MainImage)
            .NotNull()
            .WithMessage(I18N.Main_Image_was_not_supplied);
        RuleFor(recipe => recipe.MainImage)
            .Must(img => extensions.Contains(Path.GetExtension(img.FileName)))
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.file_format_for_0_is_invalid_allowed_formats_are_1], _translations[I18N.Main_Image], string.Join(", ", extensions)));
        RuleFor(recipe => recipe.MainImage)
            .Must(img => img.Length <= max_file_size)
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.Maximum_file_size_exceeded_for_0_Maximum_file_size_is_1], _translations[I18N.Main_Image], "5 MB"));

        // extra images
        RuleForEach(recipe => recipe.MiscImages)
            .Must(img => extensions.Contains(Path.GetExtension(img.FileName)))
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.file_format_for_0_is_invalid_allowed_formats_are_1], _translations[I18N.Extra_Images], string.Join(", ", extensions)));

        RuleForEach(recipe => recipe.MiscImages)
            .Must(img => img.Length <= max_file_size)
            .WithMessage(string.Format(_translations[I18N.ValidationMessages.Maximum_file_size_exceeded_for_0_Maximum_file_size_is_1], _translations[I18N.Extra_Images], "5 MB"));
    }
}
