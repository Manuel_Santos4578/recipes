
use Recipes
GO
-- Authors
create type AuthorType as table
(
	name varchar(255),
	friendly_url varchar(255) unique,
	created_at datetime not null default getdate(),
	updated_at datetime not null default getdate()
)
GO
create or alter proc ImportAuthors (
	@AuthorImportSource AuthorType READONLY
) as 
BEGIN
    MERGE Author as target
    USING @AuthorImportSource as source
    ON (trim(lower(target.name)) = trim(lower(source.name)))
    WHEN MATCHED THEN
        UPDATE SET 
            target.name = source.name,
            target.friendly_url = source.friendly_url,
            target.updated_at = source.updated_at
    WHEN NOT MATCHED BY TARGET THEN
        INSERT (name, friendly_url, created_at, updated_at)
        VALUES (source.name, source.friendly_url, source.created_at, source.updated_at);
END
GO

-- Category
create type CategoryType as table(
    name varchar(255),
	friendly_url varchar(255) unique,
	base_category_id int,
    created_at datetime not null default getdate(),
	updated_at datetime not null default getdate()
)
GO
create or alter proc ImportCategories ( @CategoriesImportSource CategoryType READONLY ) as
BEGIN
    MERGE Category as target
    USING @CategoriesImportSource as source
    ON (trim(lower(target.name)) = trim(lower(source.name)))
    WHEN MATCHED THEN
        UPDATE SET 
            target.name = source.name,
            target.friendly_url = source.friendly_url,
            target.base_category_id = source.base_category_id,
            target.updated_at = source.updated_at
    WHEN NOT MATCHED BY TARGET THEN
        INSERT (name, friendly_url, base_category_id, created_at, updated_at)
        VALUES (source.name, source.friendly_url, source.base_category_id, source.created_at, source.updated_at);
END
GO
-- Course
create type CourseType as table
(
	name varchar(255),
	friendly_url varchar(255) unique,
	created_at datetime not null default getdate(),
	updated_at datetime not null default getdate()
)
GO
create or alter proc ImportCourses (
	@CourseImportSource CourseType READONLY
) as 
BEGIN
    MERGE Course as target
    USING @CourseImportSource as source
    ON (trim(lower(target.name)) = trim(lower(source.name)))
    WHEN MATCHED THEN
        UPDATE SET 
            target.name = source.name,
            target.friendly_url = source.friendly_url,
            target.updated_at = source.updated_at
    WHEN NOT MATCHED BY TARGET THEN
        INSERT (name, friendly_url, created_at, updated_at)
        VALUES (source.name, source.friendly_url, source.created_at, source.updated_at);
END
GO

-- Country
create type CountryType as table (
    name varchar(255),
	friendly_url varchar(255) unique,
    region_id int not null,
	created_at datetime not null default getdate(),
	updated_at datetime not null default getdate()
)
GO
create or alter proc ImportCountries ( @CountryImportSource CountryType READONLY) as
BEGIN
    MERGE Country as target
    USING @CountryImportSource as source
    ON (trim(lower(source.name)) = trim(lower(target.name)))
    WHEN MATCHED THEN
        UPDATE SET
            target.name = source.name,
            target.friendly_url = source.friendly_url,
            target.region_id = source.region_id,
            target.created_at = source.created_at,
            target.updated_at = source.updated_at
    WHEN NOT MATCHED BY TARGET THEN
        INSERT (name, friendly_url, region_id, created_at, updated_at)
        VALUES (source.name, source.friendly_url, source.region_id, source.created_at, source.updated_at);
END
GO

-- Region
create type RegionType as table
(
	name varchar(255),
	friendly_url varchar(255) unique,
	created_at datetime not null default getdate(),
	updated_at datetime not null default getdate()
)
GO
create or alter proc ImportRegions (
	@RegionImportSource RegionType READONLY
) as 
BEGIN
    MERGE Region as target
    USING @RegionImportSource as source
    ON (trim(lower(target.name)) = trim(lower(source.name)))
    WHEN MATCHED THEN
        UPDATE SET 
            target.name = source.name,
            target.friendly_url = source.friendly_url,
            target.updated_at = source.updated_at
    WHEN NOT MATCHED BY TARGET THEN
        INSERT (name, friendly_url, created_at, updated_at)
        VALUES (source.name, source.friendly_url, source.created_at, source.updated_at);
END
GO

-- Recipe

-- Ingredient
create type IngredientType as table (
    name varchar(255),
	friendly_url varchar(255) unique,
	country_id int not null,
    created_at datetime not null default getdate(),
	updated_at datetime not null default getdate()
)
GO
create or alter proc ImportIngredients (
    @IngredientImportSource IngredientType READONLY
) as 
BEGIN
    MERGE Ingredient as target
    USING @IngredientImportSource as source
    ON (trim(lower(target.name)) = trim(lower(source.name)))
    WHEN MATCHED THEN
        UPDATE SET
            target.name = source.name,
            target.friendly_url = source.friendly_url,
            target.country_id = source.country_id,
            target.updated_at = source.updated_at
    WHEN NOT MATCHED BY TARGET THEN
        INSERT (name, friendly_url, country_id, created_at, updated_at)
        VALUES (source.name, source.friendly_url, source.country_id, source.created_at, source.updated_at);
END
GO
-- Source
create type RecipeSourceType as table
(
	name varchar(255),
	friendly_url varchar(255) unique,
	created_at datetime not null default getdate(),
	updated_at datetime not null default getdate()
)
GO
create or alter proc ImportSources (
	@RecipeSourceImportSource RecipeSourceType READONLY
) as 
BEGIN
    MERGE RecipeSource as target
    USING @RecipeSourceImportSource as source
    ON (trim(lower(target.name)) = trim(lower(source.name)))
    WHEN MATCHED THEN
        UPDATE SET 
            target.name = source.name,
            target.friendly_url = source.friendly_url,
            target.updated_at = source.updated_at
    WHEN NOT MATCHED BY TARGET THEN
        INSERT (name, friendly_url, created_at, updated_at)
        VALUES (source.name, source.friendly_url, source.created_at, source.updated_at);
END
GO
