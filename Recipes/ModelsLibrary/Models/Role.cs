﻿using DataContracts.Entities;

namespace ModelsLibrary.Models;

public class Role : IRole
{
    public int ID { get; set; }
    public string Name { get; set; }
}
