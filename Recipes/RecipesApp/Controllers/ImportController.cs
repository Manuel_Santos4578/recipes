﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RecipesApp.Models.FormModels;
using RecipesApp.Services;
using System;
using System.IO;

namespace RecipesApp.Controllers;

[Route("import")]
[Authorize]
public class ImportController : MyBaseController
{
    public ImportController(IConfiguration configuration, IWebHostEnvironment env) : base(configuration, env) { }
    [HttpGet]
    public IActionResult Index()
    {
        return View(new ImportModel());
    }

    [HttpPost]
    public IActionResult Store(ImportModel model)
    {
        new ExcelParserService().ReadExcel(model.File);
        return RedirectToAction("Index", "Import");
    }

    [HttpGet("template")]
    public IActionResult Template()
    {
        string file = Path.Combine(AppContext.BaseDirectory, "ImportTemplate", "ImportTemplate.xlsx");
        Stream stream = System.IO.File.Open(file, FileMode.Open);
        return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", true);
    }
}
