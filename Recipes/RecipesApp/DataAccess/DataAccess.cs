﻿using System.Collections.Generic;
using Util;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using ModelsLibrary.Constants;
using System.Linq;
using ModelsLibrary.Models;
using DataContracts.Access;

namespace RecipesApp.DataAccess;

public sealed partial class DataAccess: IDataAccess
{
    public string ApiDomain { get; private set; }
    public string ApiToken { get; set; }

    #region endpoints

    private const string endpointPreffix = "api/";
    private const string authorsEndpoint = $"{endpointPreffix}authors";
    private const string categoriesEndpoint = $"{endpointPreffix}categories";
    private const string countryEndpoint = $"{endpointPreffix}countries";
    private const string coursesEndpoint = $"{endpointPreffix}courses";
    private const string ingredientsEndpoint = $"{endpointPreffix}ingredients";
    private const string regionsEndpoint = $"{endpointPreffix}regions";
    private const string sourcesEndpoint = $"{endpointPreffix}sources";
    private const string usersEndpoint = $"{endpointPreffix}users";
    private const string recipesEndpoint = $"{endpointPreffix}recipes";
    private const string recipeImagesEndpoint = $"{endpointPreffix}recipeimages";
    private const string recipeFavoritesEndpoint = $"{endpointPreffix}favorites";
    private const string culturesEndpoint = $"{endpointPreffix}localization";

    #endregion

    public DataAccess(string apiUrl)
    {
        ApiDomain = apiUrl;
    }
    #region ApiAuth
    public async Task ApiAuthenticateAsync()
    {
        Dictionary<string, string> values = new Dictionary<string, string>
        {
            {"username", Environment.GetEnvironmentVariable(AppConstants.RecipesAppEnv.ApiUsername).ToString() },
            {"password", Environment.GetEnvironmentVariable(AppConstants.RecipesAppEnv.ApiPassword).ToString() }
        };
        using StringContent content = new StringContent(JsonConvert.SerializeObject(values));
        JToken data = await new ApiClientV3(ApiDomain).PostAsync<JToken>($"{endpointPreffix}apiauth/login", content);
        ApiToken = data is null ? "" : data.Value<string>("token");
    }
    #endregion

    #region Bulk Insert

    public async Task<ArrayList> BulkSave(ArrayList entities)
    {
        if (entities.Count != 0)
        {
            switch (entities?[0].GetType().Name)
            {
                case nameof(Author):
                    await InsertAuthorsAsync(entities.Cast<Author>());
                    break;
                case nameof(Category):
                    await InsertCategoriesAsync(entities.Cast<Category>());
                    break;
                case nameof(Country):
                    await InsertCountriesAsync(entities.Cast<Country>());
                    break;
                case nameof(Course):
                    await InsertCoursesAsync(entities.Cast<Course>());
                    break;
                case nameof(Ingredient):
                    await InsertIngredientsAsync(entities.Cast<Ingredient>());
                    break;
                case nameof(Region):
                    await InsertRegionsAsync(entities.Cast<Region>());
                    break;
                case nameof(Source):
                    await InsertSourcesAsync(entities.Cast<Source>());
                    break;
                default:
                    break;
            }
        }
        return entities;
    }
    #endregion
}
