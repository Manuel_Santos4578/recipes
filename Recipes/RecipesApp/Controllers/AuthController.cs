﻿using DataContracts.Entities;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RecipesApp.Models.FormModels;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using static ModelsLibrary.Constants.AppConstants;

namespace RecipesApp.Controllers;

[Route("auth")]
public sealed class AuthController : MyBaseController
{
    public AuthController(IConfiguration config, IWebHostEnvironment env) : base(config, env) { }

    [HttpGet("login")]
    public IActionResult Login(string r)
    {
        HandleViewData();
        ViewData["url"] = r;
        return View(new AuthFormModel());
    }

    [HttpPost("signin")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> SignIn(AuthFormModel model)
    {
        if (!ModelState.IsValid) return View("Login", model);
        string issuer = Request.Host.Value;
        await ApiAuthAsync();
        IUser user = await dal.LoginAsync(model.Username, model.Password);
        
        if (user is null)
        {
            ViewData["url"] = Request.Form["url"];
            ViewData["Error"] = translation[I18N.Authentication_failed];
            return View("Login");
        }

        List<Claim> claims = new List<Claim>
        {
            new Claim("ID", user.ID.ToString(), ClaimValueTypes.String, issuer),
            new Claim(ClaimTypes.Name, user.Username, ClaimValueTypes.String, issuer),
            new Claim("Name", user.Name, ClaimValueTypes.String, issuer),
        };
        foreach (var role in user.Roles)
        {
            claims.Add(new Claim(ClaimTypes.Role, role.Name, ClaimValueTypes.String, issuer));
        }
        var userIdentity = new ClaimsIdentity("SuperSecureLogin");
        userIdentity.AddClaims(claims);
        var user_principal = new ClaimsPrincipal(userIdentity);
        await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, user_principal, new AuthenticationProperties
        {
            ExpiresUtc = DateTime.UtcNow.AddHours(10),
            IsPersistent = true,
            AllowRefresh = true,
        });
        Response.Cookies.Append(
           CookieRequestCultureProvider.DefaultCookieName,
           CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(user.PreferedCulture)),
           new CookieOptions { Expires = DateTime.Now.AddYears(1) });
        string return_url = string.IsNullOrWhiteSpace(HttpContext.Request.Form["url"].ToString()) ? "/" : HttpContext.Request.Form["url"].ToString();
        return Redirect(return_url);

    }

    [HttpPost("logout")]
    [ValidateAntiForgeryToken]
    [Authorize]
    public async Task<IActionResult> Logout()
    {
        await HttpContext.SignOutAsync();
        HttpContext.Session.Clear();
        return Redirect("/");
    }

    [HttpGet("forbidden")]
    public IActionResult Forbidden()
    {
        return View();
    }

}
