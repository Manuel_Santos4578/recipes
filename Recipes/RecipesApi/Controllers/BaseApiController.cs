﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using Util;

namespace RecipesApi.Controllers;

public abstract class BaseApiController : Controller
{
    protected IQueryExecutor executor;
    public BaseApiController(IQueryExecutor executor = null)
    {
        this.executor = executor;
    }
    protected IActionResult ErrorCustom(string message)
    {
        return StatusCode(500, new { message });
    }
    protected void SqlPaginate(int page, int size, ref string query, List<SqlParameter> parameters)
    {
        if (size > 0 && page > 0)
        {
            int offset = (page - 1) * size;
            query += " OFFSET @offset ROWS FETCH NEXT @take ROWS ONLY ";
            parameters.Add(executor.GenerateParameter("@offset", SqlDbType.Int, offset));
            parameters.Add(executor.GenerateParameter("@take", SqlDbType.Int, size));
        }
    }
}
