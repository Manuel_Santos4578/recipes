﻿namespace ModelsLibrary.Constants;

public static partial class AppConstants
{
    public static class RecipesApiEnv
    {
        public const string DatabaseIpAddress = nameof(DatabaseIpAddress);
        public const string DatabaseName = nameof(DatabaseName);
        public const string DatabaseUser = nameof(DatabaseUser);
        public const string DatabasePassword = nameof(DatabasePassword);

    }
    public static class RecipesAppEnv
    {
        public const string ApiUsername = nameof(ApiUsername);
        public const string ApiPassword = nameof(ApiPassword);
        public const string ApiDomain = nameof(ApiDomain);
    }
    public static class FormMessages
    {
        public const string REQUIRED = "Este campo é obrigatório";
        public const string INVALID = "Valor inválido";
    }
    public static class Policies
    {
        public const string PERMISSIVE = "Permissive";
    }

    public static class Caching
    {
        public const string CULTURES = "Cultures";
        public const string TRANSLATIONS = "Translations";
        public const string AVAILABLE = "Available";
    }

    public static class CookieNames
    {
        public const string CULTURE = "Culture";
    }

    public static class Roles
    {
        public const string Admin = nameof(Admin);
        public const string User = nameof(User);
    }

    public enum ExportTypes
    {
        Json = 1,
        Excel = 2,
        Pdf = 3
    }

    public enum Model
    {
        Author = 1,
        Category = 2,
        Country = 3,
        Course = 4,
        Ingredient = 5,
        Recipe = 6,
        Region = 7,
        Source = 8,
    }
}
