﻿using Quartz;
using Recipes.Database.Repositories;

namespace CronService.Jobs;

[DisallowConcurrentExecution]
public class UnblockUserJob : IJob
{
    public Task Execute(IJobExecutionContext context)
    {
        Serilog.Log.Information($"Starting {nameof(UnblockUserJob)} run");

		try
		{
			UserRepository.UnblockUsersAsync().RunSynchronously();
		}
		catch (Exception ex)
		{

            Serilog.Log.Information($"{nameof(UnblockUserJob)} failed");
            Serilog.Log.Error(ex.Message);
            Serilog.Log.Error(ex.StackTrace);
            throw;
        }
        Serilog.Log.Information($"{nameof(UnblockUserJob)} ran successfully");
        return Task.CompletedTask;
    }
}