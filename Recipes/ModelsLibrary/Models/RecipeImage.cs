﻿using DataContracts.Entities;
using Newtonsoft.Json;

namespace ModelsLibrary.Models;

public class RecipeImage : Simple, IRecipeImage
{
    [JsonProperty("recipeimages_id")]
    public int ID { get; set; }
    [JsonProperty("recipeimages_recipe_id")]
    public int RecipeId { get; set; }
    [JsonProperty("recipeimages_image")]
    public string Image { get; set; }
    [JsonProperty("recipeimages_image_base_64")]
    public string ImageBase64 { get; set; }
    [JsonProperty("recipeimages_is_display")]
    public bool IsDisplay { get; set; }
    [JsonProperty("recipeimages_content_type")]
    public string ContentType { get; set; }
}
