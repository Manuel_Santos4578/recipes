﻿using DataContracts.Entities;

namespace DataContracts.Access;

public partial interface IFavoriteDataAccess
{
    Task<bool> AddFavoriteAsync(IRecipe recipe, string user_id);
    Task<bool> RemoveFavoriteAsync(IRecipe recipe, string user_id);
}