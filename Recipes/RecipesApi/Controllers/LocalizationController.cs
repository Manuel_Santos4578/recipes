﻿using DataContracts.Localization;
using Microsoft.AspNetCore.Mvc;
using ModelsLibrary.Localization;
using Recipes.Database.Repositories;
using RecipesLibrary.Util;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;

namespace RecipesApi.Controllers;

/// <summary>
/// 
/// </summary>
[Route("api/localization")]
public class LocalizationController : BaseApiController
{
    public LocalizationController() { }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    [HttpGet("cultures")]
    public IActionResult Cultures()
    {
        return Ok(LocalizationRepository.SelectCultures());
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="culture"></param>
    /// <returns></returns>
    [HttpPost("cultures")]
    public async Task<IActionResult> StoreCulture()
    {
        ICulture culture = await Request.Body.ReadObject<Culture>();
        CultureInfo info = new CultureInfo(culture.Name);
        info = LocalizationRepository.CreateCultureAsync(info);
        culture.DisplayName = info.EnglishName;
        return Ok(culture);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="culture"></param>
    /// <returns></returns>
    [HttpDelete("cultures/{culture}")]
    public IActionResult DeleteCulture(string culture)
    {
        Culture cul = new Culture { Name = culture };
        cul = LocalizationRepository.DeleteCulture(cul);
        return Ok(cul);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="culture"></param>
    /// <returns></returns>
    [HttpGet("translations/{culture}")]
    public IActionResult Translations(string culture)
    {
        return Ok(LocalizationRepository.SelectTranslations(culture));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    [HttpPost("translations")]
    public async Task<IActionResult> CreateTranslationAsync()
    {
        Translation translation = await Request.Body.ReadObject<Translation>();
        translation = LocalizationRepository.CreateTranslation(translation);
        return Ok(translation);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    [HttpPut("translations")]
    public async Task<IActionResult> UpdateTranslationAsync()
    {
        Translation translation = await Request.Body.ReadObject<Translation>();
        if (translation is null || translation.Culture is null)
        {
            return BadRequest();
        }
        translation = LocalizationRepository.UpdateTranslation(translation);
        return Ok(translation);
    }

    [HttpPut("translations/bulk")]
    public async Task<IActionResult> UpdateTranslationsAsync()
    {
        List<Translation> translations = await Request.Body.ReadObject<List<Translation>>();
        await LocalizationRepository.UpdateTranslationsAsync(translations);
        return Ok(translations);
    }
}
