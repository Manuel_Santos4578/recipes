﻿using Microsoft.Data.SqlClient;
using ModelsLibrary.Models;
using Newtonsoft.Json.Linq;
using Recipes.Database.Config;
using System.Data;
using Util;

namespace Recipes.Database.Repositories;

public static class RecipeImageRepository
{
    public static List<RecipeImage> ReadAll(int recipe_id, bool? is_display)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = @$"exec RecipeImageSearchProc @recipe, @is_display";

        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@recipe", SqlDbType.Int, recipe_id),
            executor.GenerateParameter("@is_display", SqlDbType.Bit, is_display)
        };
        return executor.LoadData<List<RecipeImage>>(query, parameters);
    }

    public static int ReadCount(int recipe_id, bool? is_display)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = @$"exec RecipeImageSearchProc @recipe, @is_display, @count = 1";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@recipe", SqlDbType.Int, recipe_id ),
            executor.GenerateParameter("@is_display", SqlDbType.Bit, is_display)
        };
        return executor.LoadData<List<JToken>>(query, parameters).First().Value<int>("count");
    }

    public static RecipeImage Store(RecipeImage image)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query =
            @"INSERT INTO RecipeImages (image,image_base_64,content_type,is_display,recipe_id,created_at,updated_at)
                output inserted.id
                VALUES (@image,@image_base_64,@content_type,@is_display,@recipe,@created_at,@updated_at)";
        image.CreatedAt = image.UpdatedAt = DateTime.Now;
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@image", SqlDbType.VarChar, image.Image),
            executor.GenerateParameter("@image_base64", SqlDbType.VarChar, image.ImageBase64),
            executor.GenerateParameter("@content_type", SqlDbType.VarChar, image.ContentType),
            executor.GenerateParameter("@is_display", SqlDbType.Bit, image.IsDisplay),
            executor.GenerateParameter("@recipe", SqlDbType.Int, image.RecipeId),
            executor.GenerateParameter("@created_at", SqlDbType.DateTime, image.CreatedAt),
            executor.GenerateParameter("@updated_at", SqlDbType.DateTime, image.UpdatedAt),
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        return result is not null ? image : null;
    }
    public static RecipeImage Update(RecipeImage image)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query =
         @"UPDATE RecipeImages SET
                    is_display = @is_display,
                    updated_at = @updated_at
                    output inserted.id
                    Where id = @id";
        image.UpdatedAt = DateTime.Now;
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@id",SqlDbType.Int, image.ID),
            executor.GenerateParameter("@is_display",SqlDbType.Bit, image.IsDisplay ),
            executor.GenerateParameter("@id",SqlDbType.DateTime, image.UpdatedAt),
        };
        return executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault() is not null ? image : null;
    }
    public static RecipeImage SoftDelete(RecipeImage image)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query =
            @"UPDATE RecipeImages SET
                    deleted_at = @deleted_at
                    output inserted.id
                    Where id = @id";
        image.DeletedAt = DateTime.Now;
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@id",SqlDbType.Int, image.ID),
            executor.GenerateParameter("@deleted_at",SqlDbType.DateTime, image.DeletedAt),
        };
        return executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault() is not null ? image : null;
    }
}
