﻿using Microsoft.Data.SqlClient;
using System.Data;
using Util;
using ModelsLibrary.Models;
using System.Text;
using RecipesLibrary.Util;
using Recipes.Database.Config;

namespace Recipes.Database.Repositories;

public static class CategoryRepository
{
    public static List<Category> ReadAll(string name, int? base_category, int page = 1, int size = 0)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = @$"exec CategoriesSearchProc @name, @base, @page, @size, 0";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@name", SqlDbType.VarChar, name ?? ""),
            executor.GenerateParameter("@base", SqlDbType.Int, base_category is null ? DBNull.Value: base_category),
            executor.GenerateParameter("@page", SqlDbType.Int, page),
            executor.GenerateParameter("@size", SqlDbType.Int, size),
        };
        return executor.LoadData<List<Category>>(query, parameters);
    }

    public static int ReadCount(string name, int? base_category)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = @$"exec CategoriesSearchProc @name, @base, @count = 1";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@name", SqlDbType.VarChar, name ?? ""),
            executor.GenerateParameter("@base", SqlDbType.Int, base_category is null ? DBNull.Value: base_category),
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).First();
        return int.Parse(result["count"]);
    }

    public static Category ReadBySlug(string slug, bool with_trashed = false)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query =
        @"
                select 
                    cat1.id as category_id, 
                    cat1.name as category_name, 
                    cat1.friendly_url as category_friendly_url,
                    cat1.base_category_id as category_base_category_id,
                    cat2.name category_base_category_name, 
                    cat2.friendly_url category_base_category_friendly_url,
                    cat1.created_at as category_created_at,
                    cat1.updated_at as category_updated_at,
                    cat2.created_at category_base_category_created_at,
                    cat2.updated_at category_base_category_updated_at
                from Category cat1
                left join Category cat2 on cat1.base_category_id = cat2.id
                Where cat1.friendly_url = @slug
            ";
        query += with_trashed ? "" : " AND cat1.deleted_at is null ";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@slug",SqlDbType.VarChar, slug)
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        return result is not null ? new Category
        {
            ID = int.Parse(result["category_id"]),
            Name = ImprovedDataReader.GetString(result["category_name"]),
            FriendlyUrl = ImprovedDataReader.GetString(result["category_friendly_url"]),
            CreatedAt = ImprovedDataReader.GetDate(result["category_created_at"]),
            UpdatedAt = ImprovedDataReader.GetDate(result["category_updated_at"]),
            BaseCategory = !string.IsNullOrWhiteSpace(result["category_base_category_id"]) ? new Category
            {
                ID = int.Parse(result["category_base_category_id"]),
                Name = ImprovedDataReader.GetString(result["category_base_category_name"]),
                FriendlyUrl = ImprovedDataReader.GetString(result["category_base_category_friendly_url"]),
            } : null,
        } : null;
    }

    public static Category Store(Category category)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query =
        @"INSERT INTO Category (name,friendly_url,base_category_id,created_at,updated_at)
                output inserted.id
            VALUES (@name,@friendly_url,@base_category_id,@created_at,@updated_at)";
        category.CreatedAt = category.UpdatedAt = DateTime.Now;
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@name",SqlDbType.VarChar, !string.IsNullOrWhiteSpace(category.Name) ? category.Name: DBNull.Value),
            executor.GenerateParameter("@friendly_url",SqlDbType.VarChar, category.FriendlyUrl),
            executor.GenerateParameter("@created_at",SqlDbType.DateTime, category.CreatedAt),
            executor.GenerateParameter("@updated_at",SqlDbType.DateTime, category.UpdatedAt),
        };
        if (category.BaseCategory is not null)
        {
            parameters.Add(executor.GenerateParameter("@base_category_id", SqlDbType.Int, category.BaseCategory.ID));
        }
        else
        {
            parameters.Add(executor.GenerateParameter("@base_category_id", SqlDbType.Int, DBNull.Value));
        }
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        if (result is null)
        {
            return null;
        }
        category.ID = int.Parse(result["id"]);
        return category;
    }


    public static IEnumerable<Category> BulkStore(IEnumerable<Category> categories)
    {
        StringBuilder query = new StringBuilder("DECLARE @categories CategoryType; INSERT INTO @categories (name, friendly_url, base_category_id, created_at, updated_at) VALUES ");
        List<SqlParameter> parameters = new List<SqlParameter>();
        QueryExecutor executor = new QueryExecutor(DB.Instance);
        int count = categories.Count();
        for (int i = 0; i < count; i++)
        {
            Category category = categories.ElementAt(i);
            category.FriendlyUrl = category.Name.GenerateSlug();
            query.Append($"(@name{i}, @slug{i}, (Select top 1 ISNULL(id, null) as id from Category where name = @base_category_name{i}), @created_at{i}, @updated_at{i})");
            if (i != count - 1)
            {
                query.Append(',');
            }
            parameters.AddRange(new[] {
                executor.GenerateParameter($"@name{i}", SqlDbType.VarChar, category.Name),
                executor.GenerateParameter($"@slug{i}", SqlDbType.VarChar, category.FriendlyUrl),
                executor.GenerateParameter($"@base_category_name{i}", SqlDbType.VarChar, category.BaseCategory?.Name ?? ""),
                executor.GenerateParameter($"@created_at{i}", SqlDbType.DateTime, DateTime.Now),
                executor.GenerateParameter($"@updated_at{i}", SqlDbType.DateTime, DateTime.Now),
            });
        }
        query.AppendLine("exec ImportCategories @categories");
        try
        {
            executor.Execute(query.ToString(), parameters).Wait();
            return categories;
        }
        catch (Exception)
        {
            return null;
        }

    }

    public static Category Update(Category category)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        category.UpdatedAt = DateTime.Now;
        string query =
            @"UPDATE Category SET
                    name = @name,
                    friendly_url = @friendly_url,
                    base_category_id = @base_category_id,
                    updated_at = @updated_at
                    output inserted.id
                Where id = @id";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@name",SqlDbType.VarChar, !string.IsNullOrWhiteSpace(category.Name) ? category.Name: DBNull.Value),
            executor.GenerateParameter("@friendly_url",SqlDbType.VarChar, category.FriendlyUrl),
            executor.GenerateParameter("@updated_at",SqlDbType.DateTime, category.UpdatedAt),
            executor.GenerateParameter("@id",SqlDbType.Int, category.ID)
        };
        if (category.BaseCategory is not null)
        {
            parameters.Add(executor.GenerateParameter("@base_category_id", SqlDbType.Int, category.BaseCategory.ID));
        }
        else
        {
            parameters.Add(executor.GenerateParameter("@base_category_id", SqlDbType.Int, DBNull.Value));
        }
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        return result is not null ? category : null;
    }

    public static Category SoftDelete(Category category)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        category.DeletedAt = DateTime.Now;
        string query =
            @"UPDATE Category SET
                    deleted_at = @deleted_at
                    output inserted.id
                Where id = @id";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@id",SqlDbType.Int, category.ID),
            executor.GenerateParameter("@deleted_at",SqlDbType.DateTime, category.DeletedAt)
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        return result is not null ? category : null;
    }
}
