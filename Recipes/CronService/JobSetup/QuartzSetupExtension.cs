﻿using Quartz;

public static class QuartzSetupExtension
{
    public static IServiceCollectionQuartzConfigurator SetupCron<T>(this IServiceCollectionQuartzConfigurator configurator, string cronString) where T : IJob, new()
    {
        string jobName = typeof(T).Name;
        JobKey jobKey = new(jobName);

        configurator.AddTrigger(conf =>
        {
            conf
            .ForJob(jobKey)
            .WithIdentity($"{jobName}-t")
            .WithCronSchedule(cronString);
        });
        configurator.AddJob<T>(conf =>
        {
            conf.WithIdentity(jobName);
        });
        return configurator;
    }
}
