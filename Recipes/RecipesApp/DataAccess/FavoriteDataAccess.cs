﻿using DataContracts.Access;
using DataContracts.Entities;
using ModelsLibrary.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Util;

namespace RecipesApp.DataAccess;

public sealed partial class DataAccess: IFavoriteDataAccess
{
    public async Task<bool> AddFavoriteAsync(IRecipe recipe, string user_id)
    {
        Dictionary<string, string> values = new Dictionary<string, string>
        {
            {"recipe_id",recipe.ID.ToString() },
            {nameof(user_id), user_id.ToString() }
        };
        StringContent content = new StringContent(JsonConvert.SerializeObject(values));
        JToken data = await new ApiClientV3(ApiDomain, ApiToken).PostAsync<JToken>(recipeFavoritesEndpoint, content);
        return data is not null;
    }

    public async Task<bool> RemoveFavoriteAsync(IRecipe recipe, string user_id)
    {
        JToken data = await new ApiClientV3(ApiDomain, ApiToken).DeleteAsync<JToken>($"{recipeFavoritesEndpoint}/{recipe.ID}/user/{user_id}");
        return data is not null;
    }
}
