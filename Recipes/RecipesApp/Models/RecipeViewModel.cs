﻿using DataContracts.Entities;
using ModelsLibrary.Models;
using System.Collections.Generic;
using System.Linq;

namespace RecipesApp.Models;

public sealed class RecipeViewModel : ViewModel
{
    public IRecipe Recipe { get; set; }
    public IEnumerable<IRecipe> Recipes { get; set; }

    public RecipeViewModel()
    {
        Recipe = new Recipe();
        Recipes = new List<Recipe>();
    }
}
