﻿using Microsoft.Data.SqlClient;
using ModelsLibrary.Models;
using Newtonsoft.Json;
using Recipes.Database.Config;
using RecipesLibrary.Util;
using System.Data;
using System.Text;
using Util;

namespace Recipes.Database.Repositories;

public static class CourseRepository
{
    public static List<Course> ReadAll(string name, int page = 1, int size = 0)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = @$"exec CoursesSearchProc @name, @page, @size";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@name", SqlDbType.VarChar,name ?? ""),
            executor.GenerateParameter("@page", SqlDbType.Int,page),
            executor.GenerateParameter("@size", SqlDbType.Int,size),
        };
        return executor.LoadData<List<Course>>(query, parameters);
    }

    public static int ReadCount(string name)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = @$"exec CoursesSearchProc @name, @count = 1";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@name", SqlDbType.VarChar,name ?? "")
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).First();
        return int.Parse(result["count"]);
    }

    public static Course ReadBydSlug(string slug, bool with_trashed = false)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = @"exec CourseFindProc @slug, @with_trashed";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@slug",SqlDbType.VarChar, slug),
            executor.GenerateParameter("@with_trashed",SqlDbType.Bit, with_trashed)
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        return JsonConvert.DeserializeObject<Course>(JsonConvert.SerializeObject(result));
    }

    public static Course Store(Course course)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        course.CreatedAt = course.UpdatedAt = DateTime.Now;
        string query =
        @"
                INSERT INTO Course
                (name,friendly_url,created_at,updated_at)
                output inserted.id
                VALUES (@name, @friendly_url,@created_at,@updated_at)
            ";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@name",SqlDbType.VarChar, course.Name),
            executor.GenerateParameter("@friendly_url",SqlDbType.VarChar, course.FriendlyUrl),
            executor.GenerateParameter("@created_at",SqlDbType.DateTime, course.CreatedAt),
            executor.GenerateParameter("@updated_at",SqlDbType.Date, course.UpdatedAt),
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        if (result is null)
        {
            return null;
        }
        course.ID = int.Parse(result["id"]);
        return course;
    }

    public static IEnumerable<Course> BulkStore(IEnumerable<Course> courses)
    {
        StringBuilder query = new StringBuilder("DECLARE @courses CourseType;INSERT INTO @courses (name, friendly_url, created_at, updated_at)\r\nVALUES ");
        List<SqlParameter> parameters = new List<SqlParameter>();
        QueryExecutor executor = new QueryExecutor(DB.Instance);
        int count = courses.Count();
        for (int i = 0; i < count; i++)
        {
            Course course = courses.ElementAt(i);
            query.Append($"(@name{i}, @slug{i}, @created_at{i}, @updated_at{i})");
            if (i != count - 1)
            {
                query.Append(',');
            }
            course.FriendlyUrl = course.Name.GenerateSlug();
            parameters.AddRange(new[] {
                executor.GenerateParameter($"@name{i}", SqlDbType.VarChar, course.Name),
                executor.GenerateParameter($"@slug{i}", SqlDbType.VarChar, course.FriendlyUrl),
                executor.GenerateParameter($"@created_at{i}", SqlDbType.DateTime, DateTime.Now),
                executor.GenerateParameter($"@updated_at{i}", SqlDbType.DateTime, DateTime.Now),
            });
        }
        query.AppendLine("exec ImportCourses @courses");
        try
        {
            executor.Execute(query.ToString(), parameters).Wait();
            return courses;
        }
        catch (Exception)
        {
            return null;
        }
    }
    public static Course Update(Course course)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        course.UpdatedAt = DateTime.Now;
        string query =
        @"
                UPDATE Course SET
                name = @name,
                friendly_url = @friendly_url,
                updated_at = @updated_at
                output inserted.id
                Where id = @id
            ";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@id",SqlDbType.Int, course.ID),
            executor.GenerateParameter("@name",SqlDbType.VarChar, course.Name),
            executor.GenerateParameter("@friendly_url",SqlDbType.VarChar, course.FriendlyUrl),
            executor.GenerateParameter("@updated_at",SqlDbType.Date, course.UpdatedAt),
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        return result is not null ? course : null;
    }

    public static Course SoftDelete(Course course)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        course.DeletedAt = DateTime.Now;
        string query =
        @"
                UPDATE Course SET
                deleted_at = @deleted_at
                output inserted.id
                Where id = @id
            ";
        List<SqlParameter> parameters = new List<SqlParameter>()
        {
            executor.GenerateParameter("@id",SqlDbType.Int, course.ID),
            executor.GenerateParameter("@deleted_at",SqlDbType.Date, course.DeletedAt),
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        return result is not null ? course : null;
    }
}
