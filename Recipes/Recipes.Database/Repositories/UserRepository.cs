﻿using Microsoft.Data.SqlClient;
using ModelsLibrary.Models;
using Newtonsoft.Json.Linq;
using System.Data;
using Util;
using DataContracts.Entities;
using Recipes.Database.Config;

namespace Recipes.Database.Repositories;

public class UserRepository
{
    public static User ReadByUsernameAndPassword(string username, string password)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = @"exec UserLoginProc @username, @password";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@username",SqlDbType.VarChar, username ?? ""),
            executor.GenerateParameter("@password", SqlDbType.VarChar, password ?? ""),
        };
        JToken db_user = executor.LoadData<List<JToken>>(query, parameters).FirstOrDefault();
        return db_user switch
        {
            null => null,
            _ => new User
            {
                ID = db_user.Value<int>("id"),
                Name = db_user.Value<string>("name"),
                Username = db_user.Value<string>("username"),
                PreferedCulture = db_user.Value<string>("prefered_culture"),
                Roles = ReadRoles(db_user.Value<string>("username"))
            }
        };
    }

    public static IList<IRole> ReadRoles(string username)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = "exec UserRoleProc @username";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@username", SqlDbType.VarChar, username)
        };
        return executor.LoadData<List<Role>>(query, parameters).Cast<IRole>().ToList();
    }

    public static async Task<bool> ChangePreferedCultureAsync(string username, string culture)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = "UPDATE Users SET prefered_culture = @culture Where username = @username";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@username",SqlDbType.VarChar, username),
            executor.GenerateParameter("@culture", SqlDbType.VarChar, culture),
        };
        return await executor.Execute(query, parameters);
    }

    public async static Task UnblockUsersAsync()
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = "exec UnblockUsersProc";
        await executor.Execute(query, new());
    }
}
