﻿using Newtonsoft.Json;
using ModelsLibrary.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Util;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.WebUtilities;
using System;
using DataContracts.Entities;

namespace RecipesApp.DataAccess;

public sealed partial class DataAccess
{
    #region Country
    public async Task<IEnumerable<ICountry>> SelectAllCountriesAsync(int page = 0, int page_size = 0, IRegion region = null, string name = "")
    {
        Dictionary<string, string> query = new Dictionary<string, string>
        {
            {"page", page.ToString() },
            {"size", page_size.ToString() },
        };
        if (!string.IsNullOrWhiteSpace(name)) query.Add("name", name);
        if (region != null) query.Add("region", region.ID.ToString());
        Uri uri = new Uri(QueryHelpers.AddQueryString(countryEndpoint, query), UriKind.Relative);
        return await new ApiClientV3(ApiDomain, ApiToken).GetAsync<IEnumerable<Country>>(uri.ToString());
    }

    public async Task<int> SelectCountryCountAsync(IRegion region = null, string name = "")
    {
        Dictionary<string, string> query = new Dictionary<string, string>
        {
            {"count",true.ToString() }
        };
        if (!string.IsNullOrWhiteSpace(name)) query.Add("name", name);
        if (region != null) query.Add("region", region.ID.ToString());
        Uri uri = new Uri(QueryHelpers.AddQueryString(countryEndpoint, query), UriKind.Relative);
        JToken data = await new ApiClientV3(ApiDomain, ApiToken).GetAsync<JToken>(uri.ToString());
        return data.Value<int>("count");
    }
    public async Task<ICountry> SelectCountryBySlugAsync(string slug)
    {
        return await new ApiClientV3(ApiDomain, ApiToken).GetAsync<Country>($"{countryEndpoint}/{slug}");
    }
    public async Task<ICountry> InsertCountryAsync(ICountry country)
    {
        using StringContent content = new StringContent(JsonConvert.SerializeObject(country));
        country = await new ApiClientV3(ApiDomain, ApiToken).PostAsync<Country>(countryEndpoint, content);
        return country;
    }
    public async Task InsertCountriesAsync(IEnumerable<ICountry> countries)
    {
        StringContent content = new StringContent(JsonConvert.SerializeObject(countries));
        await new ApiClientV3(ApiDomain, ApiToken).PostAsync<IEnumerable<Country>>($"{countryEndpoint}/bulk", content);
    }
    public async Task<ICountry> UpdateCountryAsync(ICountry country)
    {
        using StringContent content = new StringContent(JsonConvert.SerializeObject(country));
        country = await new ApiClientV3(ApiDomain, ApiToken).PutAsync<Country>($"{countryEndpoint}/{country.ID}", content);
        return country;
    }
    public async Task<bool> DeleteCountryAsync(ICountry country)
    {
        country = await new ApiClientV3(ApiDomain, ApiToken).DeleteAsync<Country>($"{countryEndpoint}/{country.ID}");
        return country is not null;
    }
    #endregion
}
