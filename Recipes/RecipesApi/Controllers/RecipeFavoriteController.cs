﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ModelsLibrary.Models;
using Newtonsoft.Json.Linq;
using Recipes.Database.Repositories;
using RecipesApi.Configuration;
using RecipesLibrary.Util;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Util;

namespace RecipesApi.Controllers;

/// <summary>
/// favorites
/// </summary>
[Route("api/favorites")]
[Produces("application/json")]
[Authorize(Roles = $"{ConfigurationState.ApiRoles.Write}, {ConfigurationState.ApiRoles.Root}")]
public class RecipeFavoriteController : BaseApiController
{
    public RecipeFavoriteController(IQueryExecutor executor) : base(executor) { }
    /// <summary>
    /// Select all favorites
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public IActionResult Index(int user_id)
    {
        try
        {
            List<Recipe> favorites = RecipeRepository.ReadAll(user: user_id, favorites_only: true);
            return Ok(favorites);
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Create new 
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    [ProducesResponseType(500)]
    [ProducesResponseType(201)]
    [ProducesResponseType(401)]
    public async Task<IActionResult> StoreAsync()
    {
        JToken data = await Request.Body.ReadObject<JToken>();

        try
        {
            Recipe recipe = new Recipe { ID = data.Value<int>("recipe_id") };
            User user = new User { ID = data.Value<int>("user_id") };
            JToken result = RecipeFavoriteRepository.Store(recipe, user);
            int status = result is not null ? 201 : 200;
            return StatusCode(status, result);
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Delete Favorite
    /// </summary>
    /// <param name="id">primary key</param>
    /// <returns>deleted id</returns>
    [HttpDelete("{id}/user/{user_id}")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public IActionResult Delete(int id, int user_id)
    {
        try
        {
            Recipe recipe = new Recipe { ID = id };
            User user = new User { ID = user_id };
            return Ok(RecipeFavoriteRepository.Delete(recipe, user));
        }
        catch (Exception)
        {
            throw;
        }
    }
}
