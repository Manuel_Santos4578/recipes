﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ModelsLibrary.Models;
using Recipes.Database.Repositories;
using RecipesApi.Configuration;
using RecipesLibrary.Util;
using System;
using System.Threading.Tasks;
using Util;

namespace RecipesApi.Controllers;

/// <summary>
/// Recipes
/// </summary>
[Route("api/recipes")]
[Produces("application/json")]
[Authorize(Roles = $"{ConfigurationState.ApiRoles.Write}, {ConfigurationState.ApiRoles.Root}")]
public class RecipeController : BaseApiController
{
    public RecipeController(IQueryExecutor _executor) : base(_executor)
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="name"></param>
    /// <param name="author"></param>
    /// <param name="category"></param>
    /// <param name="country"></param>
    /// <param name="region"></param>
    /// <param name="source"></param>
    /// <param name="ingredient"></param>
    /// <param name="user"></param>
    /// <param name="count"></param>
    /// <param name="page"></param>
    /// <param name="size"></param>
    /// <returns></returns>
    [HttpGet]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public IActionResult Index(string name, string author, string category, string country, string region, string source, string ingredient, int user = 0, bool count = false, int page = 1, int size = 0)
    {
        try
        {
            if (count)
            {
                return Ok(new { count = RecipeRepository.ReadCount(name: name, author: author, category: category, country: country, region: region, source: source, ingredient: ingredient, user: user) });
            }
            return Ok(RecipeRepository.ReadAll(name: name, author: author, category: category, country: country, region: region, source: source, ingredient: ingredient, user: user, page: page, size: size));
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Select By slug
    /// </summary>
    /// <param name="friendly_url">friendly url</param>
    /// <param name="with_trashed"></param>
    /// <returns>Recipe collection</returns>
    [HttpGet("{friendly_url}")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public IActionResult Show(string friendly_url, bool with_trashed)
    {
        try
        {
            return Ok(RecipeRepository.ReadBySlug(friendly_url, with_trashed));
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Select Ingredients by recipe url
    /// </summary>
    /// <param name="friendly_url">recipe url</param>
    /// <returns>Ingredients collection</returns>
    [HttpGet("{friendly_url}/ingredients")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public IActionResult SelectRecipeIngredients(string friendly_url)
    {
        try
        {
            return Ok(RecipeRepository.ReadIngredients(friendly_url));
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Select Ingredients by recipe url
    /// </summary>
    /// <param name="friendly_url">recipe url</param>
    /// <returns>Ingredients collection</returns>
    [HttpGet("{friendly_url}/courses")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public IActionResult SelectRecipeCourses(string friendly_url)
    {
        return Ok(RecipeRepository.ReadCourses(friendly_url));
    }

    /// <summary>
    /// Insert method
    /// </summary>
    /// <returns>inserted id</returns>
    [HttpPost]
    [ProducesResponseType(500)]
    [ProducesResponseType(201)]
    [ProducesResponseType(401)]
    public async Task<IActionResult> StoreAsync()
    {
        try
        {
            Recipe recipe = await Request.Body.ReadObject<Recipe>();
            recipe.FriendlyUrl = Slug.GenerateSlug(recipe.Name);
            recipe.FriendlyUrl = Validator.ValidateSlug(recipe.FriendlyUrl, RecipeRepository.ReadBySlug);
            recipe = await RecipeRepository.StoreAsync(recipe);
            int status = recipe is null ? 201 : 200;
            return StatusCode(status, recipe);
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Update method
    /// </summary>
    /// <param name="id">PK</param>
    /// <returns>updated id</returns>
    [HttpPut("{id:int}")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public async Task<IActionResult> UpdateAsync(int id)
    {
        try
        {
            Recipe recipe = await Request.Body.ReadObject<Recipe>();
            string slug = Slug.GenerateSlug(recipe.Name);
            if (slug != recipe.FriendlyUrl)
            {
                recipe.FriendlyUrl = Validator.ValidateSlug(slug, RecipeRepository.ReadBySlug);
            }
            recipe.ID = id;
            recipe = await RecipeRepository.UpdateAsync(recipe);
            return Ok(recipe);
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Delete method
    /// </summary>
    /// <param name="id">PK</param>
    /// <returns>deleted id</returns>
    [HttpDelete("{id:int}")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public IActionResult Delete(int id)
    {
        try
        {
            Recipe recipe = new Recipe { ID = id };
            return Ok(RecipeRepository.SoftDelete(recipe));
        }
        catch (Exception)
        {
            throw;
        }
    }
}
