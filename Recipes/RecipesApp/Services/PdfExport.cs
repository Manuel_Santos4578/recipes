﻿using DataContracts.Entities;
using ModelsLibrary.Models;
using QuestPDF.Fluent;
using QuestPDF.Helpers;
using QuestPDF.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using Util;

namespace RecipesApp.Services;

public static class PdfExport
{

    #region setup constants

    private const int PDF_MARGIN = 30;

    #region header
    private const string HEADER_HEADING_FONT_COLOR = Colors.Blue.Medium;
    private const int HEADER_HEADING_FONT_SIZE = 20;
    private const int HEADER_HEIGHT = 80;

    #endregion

    #region body
    private const int BODY_TEXT_FONT_SIZE = 10;
    private const int BODY_TOP_MARGIN = 40;
    private const int BODY_BOTTOM_MARGIN = 20;
    private const int BODY_PADDING = 40;
    private const int BODY_HEIGHT = 600;

    #endregion

    #region footer
    private const int FOOTER_HEIGHT = 40;

    #endregion

    #endregion

    #region generic methods
    private static IDocumentContainer SetupPage<TEntity>(this IDocumentContainer document_container, IEnumerable<TEntity> entities)
    {
        return document_container.Page(page =>
        {
            page.Margin(PDF_MARGIN);
            string title = typeof(TEntity).Name switch
            {
                nameof(IAuthor) => nameof(Author),
                nameof(ICategory) => nameof(Category),
                nameof(ICountry) => nameof(Country),
                nameof(ICourse) => nameof(Course),
                nameof(IIngredient) => nameof(Ingredient),
                nameof(IRecipe) => nameof(Recipe),
                nameof(IRegion) => nameof(Region),
                nameof(ISource) => nameof(Source),
                _ => ""
            };
            page.SetupHeader<TEntity>(title);
            page.SetupContent(entities);
            page.SetupFooter();
        });
    }
    private static void SetupHeader<TEntity>(this PageDescriptor page, string headerText)
    {
        TextStyle heading_style = TextStyle.Default
            .FontSize(HEADER_HEADING_FONT_SIZE)
            .SemiBold()
            .FontColor(HEADER_HEADING_FONT_COLOR);

        page.Header().Element(header_container =>
        {
            header_container.Row(row =>
            {
                row.RelativeItem().Column(column =>
                {
                    column.Item().Text($"{headerText}").Style(heading_style);
                });
            });
        });
    }
    private static void SetupFooter(this PageDescriptor page)
    {
        page.Footer()
            .AlignRight()
            .Height(FOOTER_HEIGHT)
            .Text(text =>
            {
                text.CurrentPageNumber();
                text.Span(" / ");
                text.TotalPages();
            });
    }
    #endregion

    #region stylings
    private static IContainer TableCellStyle(IContainer container)
    {
        return container.BorderBottom(1).BorderColor(Colors.Grey.Lighten2).PaddingVertical(3);
    }
    private static IContainer TableHeaderStyle(IContainer container)
    {
        return container.DefaultTextStyle(x => x.SemiBold()).PaddingVertical(3).BorderBottom(1).BorderColor(Colors.Black);
    }
    #endregion

    #region tables
    private static void AuthorTable(this TableDescriptor table, List<Author> authors)
    {
        table.ColumnsDefinition(columns =>
        {
            columns.RelativeColumn();
        });
        table.Header(header =>
        {
            header.Cell().Element(TableHeaderStyle).Text("Nome");
        });

        foreach (Author author in authors)
        {
            table.Cell().Element(TableCellStyle).Text(author.Name);
        }
    }
    private static void CategoryTable(this TableDescriptor table, List<Category> categories)
    {
        table.ColumnsDefinition(columns =>
        {
            columns.RelativeColumn();
            columns.RelativeColumn();
        });
        table.Header(header =>
        {
            header.Cell().Element(TableHeaderStyle).Text("Nome");
            header.Cell().Element(TableHeaderStyle).Text("Supra Categoria");
        });

        foreach (var category in categories)
        {
            table.Cell().Element(TableCellStyle).Text(category.Name);
            table.Cell().Element(TableCellStyle).Text(category.BaseCategory?.Name ?? "---");
        }
    }
    private static void CountryTable(this TableDescriptor table, List<Country> countries)
    {
        table.ColumnsDefinition(columns =>
        {
            columns.RelativeColumn();
            columns.RelativeColumn();
        });
        table.Header(header =>
        {
            header.Cell().Element(TableHeaderStyle).Text("Nome");
            header.Cell().Element(TableHeaderStyle).Text("Região");
        });

        foreach (var country in countries)
        {
            table.Cell().Element(TableCellStyle).Text(country.Name);
            table.Cell().Element(TableCellStyle).Text(country.Region.Name);
        }
    }
    private static void CourseTable(this TableDescriptor table, List<Course> courses)
    {
        table.ColumnsDefinition(columns =>
        {
            columns.RelativeColumn();
        });
        table.Header(header =>
        {
            header.Cell().Element(TableHeaderStyle).Text("Nome");
        });

        foreach (var cource in courses)
        {
            table.Cell().Element(TableCellStyle).Text(cource.Name);
        }
    }
    private static void IngredientTable(this TableDescriptor table, List<Ingredient> ingredients)
    {
        table.ColumnsDefinition(columns =>
        {
            columns.RelativeColumn();
            columns.RelativeColumn();
            columns.RelativeColumn();
        });
        table.Header(header =>
        {
            header.Cell().Element(TableHeaderStyle).Text("Nome");
            header.Cell().Element(TableHeaderStyle).Text("País");
            header.Cell().Element(TableHeaderStyle).Text("Região");
        });

        foreach (var ingredient in ingredients)
        {
            table.Cell().Element(TableCellStyle).Text(ingredient.Name);
            table.Cell().Element(TableCellStyle).Text(ingredient.Country.Name);
            table.Cell().Element(TableCellStyle).Text(ingredient.Country.Region.Name);
        }
    }
    private static void RecipeTable(this TableDescriptor table, List<Recipe> recipes)
    {
        table.ColumnsDefinition(columns =>
        {
            columns.RelativeColumn();
            columns.RelativeColumn();
            columns.RelativeColumn();
            columns.RelativeColumn();
            columns.RelativeColumn();
            columns.RelativeColumn();
        });
        table.Header(header =>
        {
            header.Cell().Element(TableHeaderStyle).Text("Nome");
            header.Cell().Element(TableHeaderStyle).Text("Autor");
            header.Cell().Element(TableHeaderStyle).Text("Categoria");
            header.Cell().Element(TableHeaderStyle).Text("Pratos");
            header.Cell().Element(TableHeaderStyle).Text("País");
            header.Cell().Element(TableHeaderStyle).Text("Fonte");
            //header.Cell().Text("Imagem");
        });

        foreach (var recipe in recipes)
        {
            table.Cell().Element(TableCellStyle).Text(recipe.Name);
            table.Cell().Element(TableCellStyle).Text(recipe.Author.Name);
            table.Cell().Element(TableCellStyle).Text(recipe.Category.Name);
            table.Cell().Element(TableCellStyle).Text(string.Join(", ", recipe.Courses.Select(course => course.Name)));
            table.Cell().Element(TableCellStyle).Text(recipe.Country.Name);
            table.Cell().Element(TableCellStyle).Text(recipe.Source.Name);
            //table.Cell().Text(recipe.MainImage);
        }
    }
    private static void RegionTable(this TableDescriptor table, List<Region> regions)
    {
        table.ColumnsDefinition(columns =>
        {
            columns.RelativeColumn();
        });
        table.Header(header =>
        {
            header.Cell().Element(TableHeaderStyle).Text("Nome");
        });

        foreach (var region in regions)
        {
            table.Cell().Element(TableCellStyle).Text(region.Name);
        }
    }
    private static void SourceTable(this TableDescriptor table, List<Source> sources)
    {
        table.ColumnsDefinition(columns =>
        {
            columns.RelativeColumn();
        });
        table.Header(header =>
        {
            header.Cell().Element(TableHeaderStyle).Text("Nome");
        });

        foreach (var source in sources)
        {
            table.Cell().Element(TableCellStyle).Text(source.Name);
        }
    }
    #endregion

    #region list exports
    private static Document SetupDocument<TEntity>(IEnumerable<TEntity> entities)
    {
        return Document.Create(handler =>
        {
            handler.SetupPage(entities);
        });
    }
    private static void SetupContent<TEntity>(this PageDescriptor page, IEnumerable<TEntity> entities)
    {
        page.Content().Element(container =>
        {
            container
            .PaddingVertical(BODY_PADDING)
            .Height(BODY_HEIGHT)
            .AlignCenter()
            .Table(table =>
            {
                if (typeof(TEntity) == typeof(IAuthor))
                {
                    table.AuthorTable(entities as List<Author>);
                    return;
                }

                if (typeof(TEntity) == typeof(ICategory))
                {
                    table.CategoryTable(entities as List<Category>);
                    return;
                }

                if (typeof(TEntity) == typeof(ICountry))
                {
                    table.CountryTable(entities as List<Country>);
                    return;
                }
                if (typeof(TEntity) == typeof(ICourse))
                {
                    table.CourseTable(entities as List<Course>);
                    return;
                }
                if (typeof(TEntity) == typeof(IIngredient))
                {
                    table.IngredientTable(entities as List<Ingredient>);
                    return;
                }

                if (typeof(TEntity) == typeof(IRecipe))
                {
                    table.RecipeTable(entities as List<Recipe>);
                    return;
                }

                if (typeof(TEntity) == typeof(IRegion))
                {
                    table.RegionTable(entities as List<Region>);
                    return;
                }
                if (typeof(TEntity) == typeof(ISource))
                {
                    table.SourceTable(entities as List<Source>);
                    return;
                }
                throw new Exception("Invalid Export");
            });
        });
    }

    #endregion

    #region Recipe Export
    private static Document SetupDocument(Recipe recipe)
    {
        return Document.Create(handler =>
        {
            handler.SetupPage(recipe);
        });
    }
    private static IDocumentContainer SetupPage(this IDocumentContainer document_container, Recipe recipe)
    {
        return document_container.Page(page =>
        {
            page.Margin(PDF_MARGIN);
            page.SetupHeader<Recipe>(recipe.Name);
            page.Content()
            .Column(col =>
            {
                col.Item().Element(con =>
                {
                    con.Column(column =>
                    {
                        column.Item().Text($"Autor: {recipe.Author.Name}").FontSize(14);
                        column.Item().Text($"Tipo: {recipe.Category.Name}").FontSize(14);
                        string courses = recipe.Courses.Count != 0 ? string.Join(", ", recipe.Courses.Select(c => c.Name)) : "n/a";
                        column.Item().Text($"Prato: {courses}").FontSize(14);
                    });
                });
                col.Item().Element(con =>
                {
                    con.AlignCenter()
                    .Table(table =>
                    {
                        table.ColumnsDefinition(columns =>
                        {
                            columns.RelativeColumn();
                            columns.ConstantColumn(100);
                        });
                        table.Header(header =>
                        {
                            header.Cell().Element(TableHeaderStyle).Text("Ingrediente");
                            header.Cell().Element(cont => TableHeaderStyle(cont).AlignRight()).Text("Qtd");
                        });
                        foreach (var ingredient in recipe.Ingredients)
                        {
                            table.Cell().Element(TableCellStyle).Text(ingredient.Ingredient.Name);
                            table.Cell().Element(cont => TableCellStyle(cont).AlignRight()).Text($"{ingredient.Quantity} {ingredient.Unit}");
                        }
                    });
                });
                col.Item().Element(con =>
                {
                    con.Column(column =>
                    {
                        column.Spacing(5);
                        column.Item().Text("Instructions").FontSize(14);
                        column.Item().Text(HtmlParser.ConvertToPlainText(recipe.Text));
                    });
                });
            });
            page.SetupFooter();
        });
    }
    #endregion

    #region export methods
    public static Document ExportList<TEntity>(IEnumerable<TEntity> entities)
    {
        return SetupDocument(entities);
    }
    public static Document ExportRecipe(IRecipe recipe)
    {
        return SetupDocument(recipe as Recipe);
    }
    #endregion
}