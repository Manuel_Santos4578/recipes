﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RecipesApp.Models;
using RecipesApp.Models.FormModels;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Util;
using RecipesLibrary.Util;
using RecipesApp.RequestModels;
using ModelsLibrary.Models;
using System;
using HeyRed.Mime;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json.Linq;
using DataContracts.Entities;
using ExCSS;
using I18N = ModelsLibrary.Constants.AppConstants.I18N;
using RecipesApp.Validators.FormValidation;
using ModelsLibrary.Constants;

namespace RecipesApp.Controllers;

[Route("recipes")]
[Authorize]
public sealed class RecipeController : MyBaseController
{
    public RecipeController(IConfiguration config, IWebHostEnvironment env) : base(config, env) { }

    [HttpGet]
    public IActionResult Index()
    {
        RecipeViewModel model = new RecipeViewModel();
        HandleViewData();
        return View(model);
    }

    [HttpPost]
    public async Task<IActionResult> IndexAjaxAsync()
    {
        JToken body = await Request.Body.ReadObject<JToken>();
        int page = body.Value<int>("page");
        int size = body.Value<int>("size");
        string name = body.Value<string>("name");
        string category = body.Value<string>("category");
        string author = body.Value<string>("author");
        string country = body.Value<string>("country");
        string source = body.Value<string>("source");
        string ingredient = body.Value<string>("ingredient");
        await ApiAuthAsync();
        int user = /*User.Claims.Any(c => c.Type == System.Security.Claims.ClaimTypes.Role && c.Value == AppConstants.Roles.Admin) ? 0 : */
                int.Parse(User.FindFirst("ID").Value);
        RecipeViewModel model = new RecipeViewModel
        {
            CurrentPage = page,
            PageSize = size,
            Count = await dal.SelectRecipesCountAsync(name, author, category, country, source: source, ingredient: ingredient, user: user),
            Recipes = await dal.SelectAllRecipesAsync(name, author, category, country, source: source, ingredient: ingredient, user: user, page: page, size: size)
        };
        return View("RecipeTable", model);
    }

    [HttpGet("show/{friendly_url}")]
    public async Task<IActionResult> ShowAsync(string friendly_url)
    {
        await ApiAuthAsync();
        RecipeViewModel model = new RecipeViewModel
        {
            Recipe = await dal.SelectRecipeBySlugAsync(friendly_url)
        };
        if (model.Recipe is null) return View("NotFound");
        model.Recipe.Ingredients = await dal.SelectAllRecipeIngredientsAsync(model.Recipe);
        model.Recipe.Courses = await dal.SelectRecipeCoursesAsync(model.Recipe);
        model.Recipe.MiscImages = await dal.SelectRecipeImagesAsync(model.Recipe, false);
        HandleViewData();
        return View(model);
    }

    [HttpGet("create")]
    public async Task<IActionResult> CreateAsync()
    {
        await ApiAuthAsync();
        RecipeFormModel model = new RecipeFormModel
        {
            Courses = await dal.SelectAllCoursesAsync()
        };
        HttpContext.Session.SetString("ingredients", JsonConvert.SerializeObject(new List<RecipeIngredientRequestModel>()));
        return View(model);
    }

    #region ingredients
    [HttpPost("addingredient")]
    public async Task<IActionResult> AddIngredientAsync()
    {
        JToken body = await Request.Body.ReadObject<JToken>();
        int ingredient_id = body.Value<int>("ingredient_id");
        int quantity = body.Value<int>("quantity");
        string unit = body.Value<string>("unit");
        string name = body.Value<string>("name");
        List<RecipeIngredientRequestModel> ingredients = JsonConvert.DeserializeObject<List<RecipeIngredientRequestModel>>(HttpContext.Session.GetString("ingredients"));
        RecipeIngredientRequestModel ingredient = ingredients.FirstOrDefault(x => x.ingredient_id == ingredient_id);
        if (ingredient is null)
        {
            ingredients.Add(new RecipeIngredientRequestModel { ingredient_id = ingredient_id, ingredient_name = name, amount = quantity, unit = unit });
        }
        else
        {
            ingredient.amount = quantity;
            ingredient.unit = unit;
        }
        string ingredients_json = JsonConvert.SerializeObject(ingredients);
        HttpContext.Session.SetString("ingredients", ingredients_json);
        return Ok(new { ingredients = ingredients_json });
    }

    [HttpPost("removeingredient")]
    public async Task<IActionResult> RemoveIngredientAsync()
    {
        JToken body = await Request.Body.ReadObject<JToken>();
        int ingredient_id = body.Value<int>("ingredient_id");
        List<RecipeIngredientRequestModel> ingredients = JsonConvert.DeserializeObject<List<RecipeIngredientRequestModel>>(HttpContext.Session.GetString("ingredients"));
        ingredients.RemoveAll(x => x.ingredient_id == ingredient_id);
        string ingredients_json = JsonConvert.SerializeObject(ingredients);
        HttpContext.Session.SetString("ingredients", ingredients_json);
        return Ok(new { ingredients = ingredients_json });
    }

    [HttpPost("getingredients")]
    public IActionResult GetIngredients()
    {
        string ingredients_json = HttpContext.Session.GetString("ingredients");
        return Ok(new { ingredients = ingredients_json });
    }
    #endregion

    [HttpPost("store")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> StoreAsync(RecipeFormModel model)
    {

        model.Name = model.Name.Sanitize();
        model.Text = model.Text.Sanitize();
        await ApiAuthAsync();

        FluentValidation.Results.ValidationResult validation_result = await new RecipeFormModelValidator().ValidateAsync(model);
        if (!validation_result.IsValid)
        {
            ViewData["Error"] = string.Join("; ", validation_result.Errors.Select(err => err.ErrorMessage));
            return await HandleInvalidForm("Create", model);
        }

        string path = Path.Combine(_env.WebRootPath, "img", "recipe");

        IRecipe recipe = new Recipe
        {
            Name = model.Name,
            FriendlyUrl = model.FriendlyUrl,
            Author = new Author { ID = model.AuthorId },
            Category = new Category { ID = model.CategoryId },
            Country = new Country { ID = model.CountryId },
            Source = new Source { ID = model.SourceId },
            Text = model.Text,
            Ingredients = new List<IRecipeIngredient>(),
            RecipeUser = new RecipeUser
            {
                User = new User
                {
                    ID = int.Parse(User.FindFirst("ID").Value)
                },
                IsCreator = true,
            }
        };
        foreach (var course_id in model.CourseIds)
        {
            recipe.Courses.Add(new Course() { ID = course_id });
        }
        foreach (RecipeIngredientRequestModel ingredient in JsonConvert.DeserializeObject<List<RecipeIngredientRequestModel>>(HttpContext.Session.GetString("ingredients")))
        {
            recipe.Ingredients.Add(new RecipeIngredient
            {
                Ingredient = new Ingredient
                {
                    ID = ingredient.ingredient_id
                },
                Quantity = ingredient.amount,
                Unit = ingredient.unit
            });
        }
        if (model.MainImage.Length > 0)
        {
            string filename = $"{Guid.NewGuid()}.{MimeTypesMap.GetExtension(model.MainImage.ContentType)}";
            using FileStream file = System.IO.File.Create(path + filename);
            model.MainImage.CopyTo(file);
            file.Close();
            //using MemoryStream stream = new MemoryStream();
            //model.MainImage.CopyTo(stream);
            recipe.MainImage = new RecipeImage
            {
                ContentType = model.MainImage.ContentType,
                IsDisplay = true,
                Image = filename,
                RecipeId = 0,
                ImageBase64 = "" // Convert.ToBase64String(stream.ToArray()),
            };
            //stream.Close();
        }
        if (model.MiscImages.Count != 0)
        {
            recipe.MiscImages = new List<IRecipeImage>();
            foreach (var misc_image in model.MiscImages)
            {
                string filename = $"{Guid.NewGuid()}.{MimeTypesMap.GetExtension(misc_image.ContentType)}";
                using FileStream file = System.IO.File.Create(path + filename);
                misc_image.CopyTo(file);
                file.Close();
                //using MemoryStream stream = new MemoryStream();
                //misc_image.CopyTo(stream);
                recipe.MiscImages.Add(new RecipeImage
                {
                    ContentType = misc_image.ContentType,
                    IsDisplay = false,
                    ImageBase64 = "", // Convert.ToBase64String(stream.ToArray()),
                    RecipeId = 0,
                    Image = misc_image.FileName
                });
                //stream.Close();
            }
        }
        recipe = await dal.InsertRecipeAsync(recipe);
        if (recipe is not null)
        {
            HttpContext.Session.Remove("ingredients");
            TempData["Success"] = string.Format(translation[I18N.Successfully_created], recipe.Name);
            return RedirectToAction("Show", "Recipe", new { friendly_url = recipe.FriendlyUrl });
        }
        TempData["Error"] = string.Format(translation[I18N.Failed_creation_of], recipe.Name);
        return RedirectToAction("Index", "Recipe");
    }

    [HttpGet("edit/{friendly_url}")]
    public async Task<IActionResult> EditAsync(string friendly_url)
    {
        await ApiAuthAsync();
        IRecipe recipe = await dal.SelectRecipeBySlugAsync(friendly_url);
        if (recipe is null) return View("NotFound");
        recipe.Ingredients = await dal.SelectAllRecipeIngredientsAsync(recipe);
        recipe.Courses = await dal.SelectRecipeCoursesAsync(recipe);
        RecipeFormModel model = new RecipeFormModel(recipe)
        {
            Authors = new List<IAuthor>() { recipe.Author },
            Categories = new List<ICategory> { recipe.Category },
            Countries = new List<ICountry>() { recipe.Country },
            Sources = new List<ISource>() { recipe.Source },
            Courses = await dal.SelectAllCoursesAsync()
        };

        List<RecipeIngredientRequestModel> ingredients = new List<RecipeIngredientRequestModel>();
        foreach (var ing in model.RecipeIngredients)
        {
            ingredients.Add(new RecipeIngredientRequestModel { ingredient_id = ing.Ingredient.ID, ingredient_name = ing.Ingredient.Name, amount = ing.Quantity, unit = ing.Unit });
        }
        HttpContext.Session.SetString("ingredients", JsonConvert.SerializeObject(ingredients));
        return View(model);
    }

    [HttpPost("update/{friendly_url}")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> UpdateAsync(RecipeFormModel model, string friendly_url)
    {
        await ApiAuthAsync();
        model.Name = model.Name.Sanitize();

        FluentValidation.Results.ValidationResult validation_result = await new RecipeFormModelValidator().ValidateAsync(model);
        if (!validation_result.IsValid)
        {
            ViewData["Error"] = string.Join("; ", validation_result.Errors.Select(err => err.ErrorMessage));
            return await HandleInvalidForm("Edit", model);
        }

        IRecipe recipe = new Recipe
        {
            ID = model.ID,
            Name = model.Name,
            FriendlyUrl = friendly_url,
            Author = new Author { ID = model.AuthorId },
            Category = new Category { ID = model.CategoryId },
            Country = new Country { ID = model.CountryId },
            Source = new Source { ID = model.SourceId },
            Text = model.Text,
            Ingredients = new List<IRecipeIngredient>(),
        };
        foreach (var course_id in model.CourseIds)
        {
            recipe.Courses.Add(new Course() { ID = course_id });
        }
        foreach (RecipeIngredientRequestModel ingredient in JsonConvert.DeserializeObject<List<RecipeIngredientRequestModel>>(HttpContext.Session.GetString("ingredients")))
        {
            recipe.Ingredients.Add(new RecipeIngredient
            {
                Ingredient = new Ingredient
                {
                    ID = ingredient.ingredient_id
                },
                Quantity = ingredient.amount,
                Unit = ingredient.unit
            });
        }
        recipe = await dal.UpdateRecipeAsync(recipe);
        if (recipe is not null)
        {
            HttpContext.Session.Remove("ingredients");
            TempData["Success"] = string.Format(translation[I18N.Successfully_updated], recipe.Name);
            return RedirectToAction("Show", "Recipe", new { friendly_url = recipe.FriendlyUrl });
        }
        TempData["Error"] = string.Format(translation[I18N.Failed_to_update], recipe.Name);
        return RedirectToAction("Show", "Recipe", new { friendly_url = friendly_url });
    }

    [HttpPost("favorite")]
    public async Task<IActionResult> FavoriteAsync()
    {
        JToken body = await Request.Body.ReadObject<JToken>();
        int recipe_id = body.Value<int>("recipe_id");
        bool favorite = body.Value<bool>("favorite");
        Recipe recipe = new Recipe
        {
            ID = recipe_id
        };
        await ApiAuthAsync();
        if (favorite)
        {
            await dal.RemoveFavoriteAsync(recipe, User.FindFirst("ID").Value);
            return Ok(0);
        }
        await dal.AddFavoriteAsync(recipe, User.FindFirst("ID").Value);
        return Ok(recipe_id);
    }

    [HttpGet("delete/{friendly_url}")]
    public async Task<IActionResult> DeleteAsync(string friendly_url)
    {
        await ApiAuthAsync();
        IRecipe recipe = await dal.SelectRecipeBySlugAsync(friendly_url);
        if (recipe is null) return View("NotFound");
        RecipeFormModel model = new RecipeFormModel(recipe);
        return View(model);
    }

    [HttpPost("destroy")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DestroyAsync(RecipeFormModel model)
    {
        Recipe recipe = new Recipe
        {
            ID = model.ID
        };
        await ApiAuthAsync();
        if (await dal.DeleteRecipeAsync(recipe))
        {
            TempData["Success"] = string.Format(translation[I18N.Successfully_deleted], recipe.Name);
            return RedirectToAction("Index", "Recipe");
        }
        TempData["Error"] = string.Format(translation[I18N.Failed_to_delete], recipe.Name);
        return RedirectToAction("Show", "Recipe", new { friendly_url = model.FriendlyUrl });
    }

    #region Helpers

    private async Task<IActionResult> HandleInvalidForm(string view, RecipeFormModel model)
    {
        model.Authors = await dal.SelectAllAuthorsAsync();
        model.Categories = await dal.SelectAllCategoriesAsync();
        model.Countries = await dal.SelectAllCountriesAsync();
        model.Ingredients = await dal.SelectAllIngredientsAsync();
        model.Sources = await dal.SelectAllRecipeSourceAsync();
        model.Courses = await dal.SelectAllCoursesAsync();
        model.RecipeCourses = model.CourseIds.Select(id => new Course { ID = id }).ToList();
        return View(view, model);
    }
    #endregion

}
