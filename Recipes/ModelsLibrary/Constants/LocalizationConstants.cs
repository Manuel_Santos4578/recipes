﻿namespace ModelsLibrary.Constants;

public static partial class AppConstants
{
    public static class I18N
    {
        public const string Add_Edit_recipe_ingredient = "Add/Edit recipe ingredient";
        public const string An_error_occurred = "An error occurred";
        public const string Authentication_failed = "Authentication failed";
        public const string Author = "Author";
        public const string Authors = "Authors";
        public const string Base_Category = "Base Category";
        public const string Cancel = "Cancel";
        public const string Cannot_delete_default_language = "Cannot delete default language";
        public const string Categories = "Categories";
        public const string Category = "Category";
        public const string Countries = "Countries";
        public const string Country = "Country";
        public const string Course = "Course";
        public const string Courses = "Courses";
        public const string Csv_file = "Csv file";
        public const string Cultures = "Cultures";
        public const string Delete = "Delete";
        public const string Data = "Data";
        public const string Data_refreshed_successfully = "Data refreshed successfully";
        public const string Data_types = "Data types";
        public const string Download_template = "Download template";
        public const string Edit = "Edit";
        public const string Export = "Export";
        public const string Export_types = "Export types";
        public const string Export_of_0_to_1_started_successfully = "Export of {0} to {1} started successfully";
        public const string Export_of_0_to_1_failed = "Export of {0} to {1} failed";
        public const string Extra_Images = "Extra Images";
        public const string Failed_to_update_translations_for = "Failed to update translations for {0}";
        public const string Failed_creation_of = "Failed creation of {0}";
        public const string Failed_to_delete = "Failed to delete {0}";
        public const string Failed_to_update = "Failed to update {0}";
        public const string Form_was_not_filled_properly = "Form was not filled properly";
        public const string Import = "Import";
        public const string Information = "Information";
        public const string Ingredient = "Ingredient";
        public const string Ingredients = "Ingredients";
        public const string Instructions = "Instructions";
        public const string Language = "Language";
        public const string Language_does_not_exist = "Language does not exist {0}";
        public const string Languages = "Languages";
        public const string Localization = "Localization";
        public const string Main_Image = "Main Image";
        public const string Main_Image_was_not_supplied = "Main Image was not supplied";
        public const string Manage = "Manage";
        public const string Management = "Management";
        public const string Manual_Refresh = "Manual Refresh";
        public const string Name = "Name";
        public const string No_records_were_found = "No records were found";
        public const string No_recipes_marked_as_favorite = "No recipes marked as favorite";
        public const string New = "New";
        public const string New_export = "New export";
        public const string Quantity = "Quantity";
        public const string Recipe = "Recipe";
        public const string Recipes = "Recipes";
        public const string Region = "Region";
        public const string Regions = "Regions";
        public const string Refresh = "Refresh";
        public const string Save = "Save";
        public const string Search = "Search";
        public const string Source = "Source";
        public const string Sources = "Sources";
        public const string Sub_Categories = "Sub-Categories";
        public const string Successfully_updated_translations_for = "Successfully updated translations for {0}";
        public const string Successfully_created = "Successfully created {0}";
        public const string Successfully_deleted = "Successfully deleted {0}";
        public const string Successfully_updated = "Successfully updated {0}";
        public const string Themes = "Themes";
        public const string Title = "Title";
        public const string Translation = "Translation";
        public const string Translation_management = "Translation management";
        public const string Translations = "Translations";
        public const string Translation_key = "Translation key";
        public const string Translation_keys = "Translation keys";
        public const string Unit = "Unit";
        public const string Unsupported_culture = "Unsupported culture {0}";
        public const string View_recipe = "View recipe";
        public const string __Select__ = "-- Select --";

        public static class ValidationMessages
        {
            public const string Invalid_value = "Invalid value";
            public const string Maximum_length_for_0_is_1 = "Maximum length for {0} is {1}";
            public const string The_field_0_is_required = "The field {0} is required";
            public const string This_field_is_required = "This field is required";
            public const string file_format_for_0_is_invalid_allowed_formats_are_1 = "file format for {0} is invalid, allowed formats are: {1}";
            public const string Maximum_file_size_exceeded_for_0_Maximum_file_size_is_1 = "Maximum file size exceeded for {0}. Maximum file size is {1}";
        }
    }
}
