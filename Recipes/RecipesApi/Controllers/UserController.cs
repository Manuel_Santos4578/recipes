﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Util;
using RecipesLibrary.Util;
using Newtonsoft.Json.Linq;
using ModelsLibrary.Models;
using Microsoft.AspNetCore.Authorization;
using RecipesApi.Configuration;
using Recipes.Database.Repositories;

namespace RecipesApi.Controllers;

[Route("api/users")]
[Produces("application/json")]
[Authorize(Roles = $"{ConfigurationState.ApiRoles.Write}, {ConfigurationState.ApiRoles.Root}")]
public class UserController : BaseApiController
{
    public UserController(IQueryExecutor executor) : base(executor) { }

    [AllowAnonymous]
    [HttpPost("login")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    public async Task<IActionResult> LoginAsync()
    {
        try
        {
            JToken body = await Request.Body.ReadObject<JToken>();
            string password = Encryption.EncryptPass(body.Value<string>("password"));
            User user = UserRepository.ReadByUsernameAndPassword(body.Value<string>("username"), password);
            return Ok(user);
        }
        catch (Exception)
        {
            throw;
        }
    }
    
    [HttpPost("setculture")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    public async Task<IActionResult> SetCultureAsync()
    {
        try
        {
            JToken body = await Request.Body.ReadObject<JToken>();
            string culture = body.Value<string>("culture");
            string username = body.Value<string>("username");
            bool updated = await UserRepository.ChangePreferedCultureAsync(username, culture);
            if (!updated)
            {
                return NoContent();
            }
            return Ok(body);
        }
        catch (Exception)
        {
            throw;
        }
    }
}
