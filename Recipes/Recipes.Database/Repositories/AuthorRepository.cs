﻿using Microsoft.Data.SqlClient;
using ModelsLibrary.Models;
using Newtonsoft.Json;
using Recipes.Database.Config;
using RecipesLibrary.Util;
using System.Data;
using System.Text;
using Util;

namespace Recipes.Database.Repositories;

public static class AuthorRepository
{
    public static IEnumerable<Author> ReadAll(string name = "", int page = 1, int size = 0)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = @"exec AuthorsSearchProc @name, @page, @size";
        List<SqlParameter> parameters = new()
        {
            executor.GenerateParameter("@name", SqlDbType.VarChar, name ?? ""),
            executor.GenerateParameter("@page", SqlDbType.Int, page),
            executor.GenerateParameter("@size", SqlDbType.Int, size)
        };
        return executor.LoadData<List<Author>>(query, parameters);
    }

    public static int ReadCount(string name = "")
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = @"exec AuthorsSearchProc @name = @search, @count = 1";
        List<SqlParameter> parameters = new()
        {
            executor.GenerateParameter("@search", SqlDbType.VarChar, name ?? ""),
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).First();
        return int.Parse(result["count"]);
    }

    public static Author ReadBySlug(string slug, bool with_trashed)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = @"exec AuthorsFindProc @slug, @with_trashed";
        List<SqlParameter> parameters = new()
        {
            executor.GenerateParameter("@slug",SqlDbType.VarChar, slug),
            executor.GenerateParameter("@with_trashed",SqlDbType.Bit, with_trashed),
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        return JsonConvert.DeserializeObject<Author>(JsonConvert.SerializeObject(result));
    }

    public static Author Store(Author author)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query =
           @"INSERT INTO Author (name,friendly_url,created_at,updated_at)
                    output inserted.id
                    VALUES (@name,@friendly_url,@created_at,@updated_at)";
        author.CreatedAt = author.UpdatedAt = DateTime.Now;
        List<SqlParameter> parameters = new()
        {
            executor.GenerateParameter("@name",SqlDbType.VarChar, author.Name),
            executor.GenerateParameter("@friendly_url",SqlDbType.VarChar, author.FriendlyUrl),
            executor.GenerateParameter("@created_at",SqlDbType.DateTime, author.CreatedAt),
            executor.GenerateParameter("@updated_at",SqlDbType.DateTime, author.UpdatedAt),
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        if (result is not null)
        {
            author.ID = int.Parse(result["id"]);
            return author;
        }
        return null;
    }

    public static IEnumerable<Author> BulkStore(IEnumerable<Author> authors)
    {
        StringBuilder query = new StringBuilder("DECLARE @authors AuthorType;INSERT INTO @authors (name, friendly_url, created_at, updated_at)\r\nVALUES ");
        List<SqlParameter> parameters = new List<SqlParameter>();
        QueryExecutor executor = new QueryExecutor(DB.Instance);
        int count = authors.Count();
        for (int i = 0; i < count; i++)
        {
            Author author = authors.ElementAt(i);
            query.Append($"(@name{i}, @slug{i}, @created_at{i}, @updated_at{i})");
            if (i != count - 1)
            {
                query.Append(',');
            }
            author.FriendlyUrl = author.Name.GenerateSlug();
            parameters.AddRange(new[] {
                executor.GenerateParameter($"@name{i}", SqlDbType.VarChar, author.Name),
                executor.GenerateParameter($"@slug{i}", SqlDbType.VarChar, author.FriendlyUrl),
                executor.GenerateParameter($"@created_at{i}", SqlDbType.DateTime, DateTime.Now),
                executor.GenerateParameter($"@updated_at{i}", SqlDbType.DateTime, DateTime.Now),
            });
        }
        query.AppendLine("exec ImportAuthors @authors");
        try
        {
            executor.Execute(query.ToString(), parameters).Wait();
            return authors;
        }
        catch (Exception)
        {
            return null;
        }
    }

    public static Author UpdateAuthor(Author author)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query =
            @"UPDATE Author SET
                    name = @name,
                    friendly_url = @friendly_url,
                    updated_at = @updated_at
                    output inserted.id
                    Where id = @id";
        author.UpdatedAt = DateTime.Now;
        List<SqlParameter> parameters = new()
        {
            executor.GenerateParameter("@id",SqlDbType.Int, author.ID),
            executor.GenerateParameter("@updated_at",SqlDbType.DateTime, author.UpdatedAt),
            executor.GenerateParameter("@name",SqlDbType.VarChar, author.Name),
            executor.GenerateParameter("@friendly_url",SqlDbType.VarChar, author.FriendlyUrl)
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        return result is not null ? author : null;
    }

    public static Author SoftDeleteAuthor(Author author)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query =
            @"UPDATE Author SET 
                    deleted_at = @deleted_at 
                    output inserted.id 
                    Where id = @id";
        author.DeletedAt = DateTime.Now;
        List<SqlParameter> parameters = new()
        {
            executor.GenerateParameter("@id",SqlDbType.Int, author.ID),
            executor.GenerateParameter("@deleted_at",SqlDbType.DateTime, author.DeletedAt)
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        return result is not null ? author : null;
    }
    public static Author DeleteAuthor(Author author)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = "DELETE from Author output deleted.id Where id = @id";
        author.DeletedAt = DateTime.Now;
        List<SqlParameter> parameters = new()
        {
            executor.GenerateParameter("@id",SqlDbType.Int, author.ID),
            executor.GenerateParameter("@deleted_at",SqlDbType.DateTime,  author.DeletedAt)
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        return result is not null ? author : null;
    }
}
