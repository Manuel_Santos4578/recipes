﻿using Ganss.Xss;
using HtmlAgilityPack;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;

namespace Util;

/// <summary>
/// HTML processing class
/// </summary>
public static class HtmlParser
{
    /// <summary>
    /// Sanitizes html string for XSS prevention
    /// </summary>
    /// <param name="rawHtml"></param>
    /// <returns></returns>
    public static string Sanitize(this string rawHtml)
    {
        HtmlSanitizer sanitizer = new HtmlSanitizer();
        return sanitizer.Sanitize(rawHtml);
    }
    /// <summary>
    /// Decodes html string
    /// </summary>
    /// <param name="rawHtml"></param>
    /// <returns></returns>
    public static string Decode(string rawHtml)
    {
        return HttpUtility.HtmlDecode(rawHtml);
    }

    /// <summary>
    /// Converts HTML to plain text / strips tags.
    /// </summary>
    /// <param name="html_input">The HTML.</param>
    /// <![CDATA[
    ///     Source : https://github.com/ceee/ReadSharp/blob/master/ReadSharp/HtmlUtilities.cs
    /// ]]>
    /// <returns></returns>
    public static string ConvertToPlainText(string html_input)
    {
        HtmlDocument doc = new HtmlDocument();
        doc.LoadHtml(html_input);
        StringWriter sw = new StringWriter();
        ConvertTo(doc.DocumentNode, sw);
        sw.Flush();
        return Regex.Replace(sw.ToString(), pattern: "(\r\n){2,}", replacement: "\r\n", RegexOptions.Compiled);
    }

    private static void ConvertContentTo(HtmlNode node, TextWriter out_text)
    {
        foreach (HtmlNode subnode in node.ChildNodes)
        {
            ConvertTo(subnode, out_text);
        }
    }

    private static void ConvertTo(HtmlNode node, TextWriter out_text)
    {
        string html;
        switch (node.NodeType)
        {
            case HtmlNodeType.Comment:
                // don't output comments
                break;
            case HtmlNodeType.Document:
                ConvertContentTo(node, out_text);
                break;
            case HtmlNodeType.Text:
                // script and style must not be output
                string parentName = node.ParentNode.Name;
                if ((parentName == "script") || (parentName == "style"))
                {
                    break;
                }
                // get text
                html = ((HtmlTextNode)node).Text;
                // is it in fact a special closing node output as text?
                if (HtmlNode.IsOverlappedClosingElement(html))
                {
                    break;
                }
                // check the text is meaningful and not a bunch of whitespaces
                if (html.Trim().Length > 0)
                {
                    out_text.Write(HtmlEntity.DeEntitize(html));
                }
                break;
            case HtmlNodeType.Element:
                switch (node.Name)
                {
                    case "p":
                    case "br":
                    case "div":
                        // treat paragraphs as crlf
                        out_text.Write("\r\n");
                        break;
                }
                if (node.HasChildNodes)
                {
                    ConvertContentTo(node, out_text);
                }
                break;
        }
    }
}
