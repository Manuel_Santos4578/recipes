﻿using System.Collections.Generic;
using System.Globalization;

namespace RecipesApp.Models;

public class LocalizationViewModel : ViewModel
{
    public IEnumerable<CultureInfo> Cultures { get; set; }
}
