﻿using static ModelsLibrary.Constants.AppConstants;
using System;

namespace RecipesApp.Models.FormModels;

public class ExportFormModel
{
    public ExportTypes ExportType { get; set; }
    public Model ModelType { get; set; }

    public ExportTypes[] GetExportType => Enum.GetValues<ExportTypes>();
    public Model[] GetExportModel => Enum.GetValues<Model>();

    public static string GetEnumValueName(Enum @enum)
    {
        switch (@enum)
        {
            case ExportTypes:
                return @enum switch
                {
                    ExportTypes.Pdf => nameof(ExportTypes.Pdf),
                    ExportTypes.Json => nameof(ExportTypes.Json),
                    ExportTypes.Excel => nameof(ExportTypes.Excel),
                    _ => throw new Exception("Invalid entity")
                };
            case Model:
                return @enum switch
                {
                    Model.Author => "Autor",
                    Model.Category => "Categoria",
                    Model.Country => "País",
                    Model.Course => "Prato",
                    Model.Ingredient => "Ingrediente",
                    Model.Recipe => "Receita",
                    Model.Region => "Região",
                    Model.Source => "Fonte",
                    _ => throw new Exception("Invalid entity")
                };
            default:
                throw new Exception("Invalid type");

        }
    }
}
