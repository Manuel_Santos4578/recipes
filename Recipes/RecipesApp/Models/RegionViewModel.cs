﻿using DataContracts.Entities;
using ModelsLibrary.Models;
using System.Collections.Generic;
using System.Linq;

namespace RecipesApp.Models;

public sealed class RegionViewModel : ViewModel
{
    public IEnumerable<IRegion> Regions { get; set; }
    public IRegion Region { get; set; }
    public IEnumerable<ICountry> Countries { get; set; }
    public RegionViewModel()
    {
        Region = new Region();
        Regions = new List<IRegion>();
        Countries = new List<ICountry>();
    }
}
