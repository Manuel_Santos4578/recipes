﻿using DataContracts.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;

namespace ModelsLibrary.Models;

public class Author : Simple, IAuthor
{
    [JsonProperty("author_id")]
    public int ID { get; set; }
    [JsonProperty("author_name")]
    public string Name { get; set; }
    [JsonProperty("author_friendly_url")]
    public string FriendlyUrl { get; set; }
    [JsonProperty("author_created_at")]
    private DateTime createdAt { set { CreatedAt = value; } }
    [JsonProperty("author_updated_at")]
    private DateTime updatedAt { set { UpdatedAt = value; } }
    [JsonProperty("author_deleted_at")]
    private DateTime? deletedAt { set { DeletedAt = value; } }

    public static DataTable ExportToDataTable(IEnumerable<IAuthor> authors)
    {
        DataTable table = new DataTable();
        table.Columns.AddRange(new DataColumn[] {
            new DataColumn(nameof(IAuthor.Name), typeof(string)),
            new DataColumn(nameof(IAuthor.CreatedAt), typeof(DateTime)),
            new DataColumn(nameof(IAuthor.UpdatedAt), typeof(DateTime)),
        });

        foreach (IAuthor author in authors)
        {
            table.Rows.Add(
                author.Name,
                author.CreatedAt,
                author.UpdatedAt);
        }
        return table;
    }
}