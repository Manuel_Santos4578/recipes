﻿using Microsoft.Data.SqlClient;
using ModelsLibrary.Models;
using Newtonsoft.Json;
using Recipes.Database.Config;
using RecipesLibrary.Util;
using System.Data;
using System.Text;
using Util;

namespace Recipes.Database.Repositories;

public static class SourceRepository
{

    public static List<Source> ReadlAll(string name, int page = 1, int size = 0)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = @$"exec RecipeSourcesSearchProc @name, @page, @size";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@name",SqlDbType.VarChar,name ?? ""),
            executor.GenerateParameter("@page", SqlDbType.Int, page),
            executor.GenerateParameter("@size", SqlDbType.Int, size),
        };
        return executor.LoadData<List<Source>>(query, parameters);
    }

    public static int ReadCount(string name)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = @$"exec RecipeSourcesSearchProc @name, @count = 1";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@name", SqlDbType.VarChar, name ?? ""),
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).First();
        return int.Parse(result["count"]);
    }

    public static Source ReadBySlug(string slug, bool with_trashed = false)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = @"exec RecipeSourcesFindProc @slug, @with_trashed";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@slug", SqlDbType.VarChar, slug),
            executor.GenerateParameter("@with_trashed", SqlDbType.Bit, with_trashed),
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        return JsonConvert.DeserializeObject<Source>(JsonConvert.SerializeObject(result));
    }

    public static Source Store(Source source)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        source.CreatedAt = source.UpdatedAt = DateTime.Now;
        string query =
           @"INSERT INTO RecipeSource (name,friendly_url,created_at,updated_at)
                    output inserted.id
                    VALUES (@name,@friendly_url,@created_at,@updated_at)";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@name", SqlDbType.VarChar, source.Name),
            executor.GenerateParameter("@friendly_url", SqlDbType.VarChar, source.FriendlyUrl),
            executor.GenerateParameter("@created_at", SqlDbType.DateTime, source.CreatedAt),
            executor.GenerateParameter("@updated_at", SqlDbType.DateTime, source.UpdatedAt),
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        if (result is null)
        {
            return null;
        }
        source.ID = int.Parse(result["id"]);
        return source;
    }

    public static IEnumerable<Source> BulkStore(IEnumerable<Source> sources)
    {
        StringBuilder query = new StringBuilder("DECLARE @sources RecipeSourceType;INSERT INTO @sources (name, friendly_url, created_at, updated_at)\r\nVALUES ");
        List<SqlParameter> parameters = new List<SqlParameter>();
        QueryExecutor executor = new QueryExecutor(DB.Instance);
        int count = sources.Count();
        for (int i = 0; i < count; i++)
        {
            Source source = sources.ElementAt(i);
            query.Append($"(@name{i}, @slug{i}, @created_at{i}, @updated_at{i})");
            if (i != count - 1)
            {
                query.Append(',');
            }
            source.FriendlyUrl = source.Name.GenerateSlug();
            parameters.AddRange(new[] {
                executor.GenerateParameter($"@name{i}", SqlDbType.VarChar, source.Name),
                executor.GenerateParameter($"@slug{i}", SqlDbType.VarChar, source.FriendlyUrl),
                executor.GenerateParameter($"@created_at{i}", SqlDbType.DateTime, DateTime.Now),
                executor.GenerateParameter($"@updated_at{i}", SqlDbType.DateTime, DateTime.Now),
            });
        }
        query.AppendLine("exec ImportSources @sources");
        try
        {
            executor.Execute(query.ToString(), parameters).Wait();
            return sources;
        }
        catch (Exception)
        {
            return null;
        }
    }

    public static Source Update(Source source)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        source.UpdatedAt = DateTime.Now;
        string query =
            @"UPDATE RecipeSource SET
                name = @name,
                friendly_url = @friendly_url,
                updated_at = @updated_at
                output inserted.id
                Where id = @id";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@id", SqlDbType.Int, source.ID),
            executor.GenerateParameter("@name", SqlDbType.VarChar, source.Name),
            executor.GenerateParameter("@friendly_url", SqlDbType.VarChar, source.FriendlyUrl),
            executor.GenerateParameter("@updated_at", SqlDbType.DateTime, source.UpdatedAt)
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        return result is not null ? source : null;
    }

    public static Source SoftDelete(Source source)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        source.DeletedAt = DateTime.Now;
        string query =
           @"UPDATE RecipeSource SET
                    deleted_at = @deleted_at
                    output inserted.id
                    Where id = @id";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@id",SqlDbType.Int, source.ID),
            executor.GenerateParameter("@deleted_at",SqlDbType.DateTime, source.DeletedAt)
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        return result is not null ? source : null;
    }
}
