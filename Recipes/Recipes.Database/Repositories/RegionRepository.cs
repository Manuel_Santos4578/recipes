﻿using Microsoft.Data.SqlClient;
using ModelsLibrary.Models;
using Recipes.Database.Config;
using RecipesLibrary.Util;
using System.Data;
using System.Text;
using Util;

namespace Recipes.Database.Repositories;

public static class RegionRespository
{

    public static List<Region> ReadAll(string name, int page = 1, int size = 0)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = $"exec RegionsSearchProc @name, @page, @size";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@name", SqlDbType.VarChar, name ?? ""),
            executor.GenerateParameter("@page", SqlDbType.Int, page),
            executor.GenerateParameter("@size", SqlDbType.Int, size),
        };
        return executor.LoadData<List<Region>>(query, parameters);
    }

    public static int ReadCount(string name)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = $"exec RegionsSearchProc @name, @count = 1";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@name", SqlDbType.VarChar, name ?? ""),
        };
        return int.Parse(executor.LoadData<List<Dictionary<string, string>>>(query, parameters).First()["count"]);
    }

    public static Region ReadBySlug(string friendly_url, bool with_trashed = false)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = @"exec RegionFindProc @slug, @with_trashed ";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@slug",SqlDbType.VarChar, friendly_url),
            executor.GenerateParameter("@with_trashed",SqlDbType.Bit, with_trashed)
        };
        return executor.LoadData<List<Region>>(query, parameters).FirstOrDefault();
    }

    public static Region Store(Region region)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        region.CreatedAt = region.UpdatedAt = DateTime.Now;
        string query =
            @"INSERT INTO Region (name,friendly_url,created_at,updated_at)
                    output inserted.id, inserted.name, inserted.friendly_url
                    VALUES (@name,@friendly_url,@created_at,@updated_at)";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@name",SqlDbType.VarChar, region.Name),
            executor.GenerateParameter("@friendly_url",SqlDbType.VarChar, region.FriendlyUrl),
            executor.GenerateParameter("@created_at",SqlDbType.DateTime, region.CreatedAt),
            executor.GenerateParameter("@updated_at",SqlDbType.DateTime, region.UpdatedAt)
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        if (result is null)
        {
            return null;
        }
        region.ID = int.Parse(result["id"]);
        return region;
    }

    public static IEnumerable<Region> BulkStore(IEnumerable<Region> regions)
    {
        StringBuilder query = new StringBuilder("DECLARE @regions RegionType;INSERT INTO @regions (name, friendly_url, created_at, updated_at)\r\nVALUES ");
        List<SqlParameter> parameters = new List<SqlParameter>();
        QueryExecutor executor = new QueryExecutor(DB.Instance);
        int count = regions.Count();
        for (int i = 0; i < count; i++)
        {
            Region region = regions.ElementAt(i);
            query.Append($"(@name{i}, @slug{i}, @created_at{i}, @updated_at{i})");
            if (i != count - 1)
            {
                query.Append(',');
            }
            region.FriendlyUrl = region.Name.GenerateSlug();
            parameters.AddRange(new[] {
                executor.GenerateParameter($"@name{i}", SqlDbType.VarChar, region.Name),
                executor.GenerateParameter($"@slug{i}", SqlDbType.VarChar, region.FriendlyUrl),
                executor.GenerateParameter($"@created_at{i}", SqlDbType.DateTime, DateTime.Now),
                executor.GenerateParameter($"@updated_at{i}", SqlDbType.DateTime, DateTime.Now),
            });
        }
        query.AppendLine("exec ImportRegions @regions");
        try
        {
            executor.Execute(query.ToString(), parameters).Wait();
            return regions;
        }
        catch (Exception)
        {
            return null;
        }
    }

    public static Region Update(Region region)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        region.UpdatedAt = DateTime.Now;
        string query =
        @"
                UPDATE Region SET
                name = @name,
                friendly_url = @friendly_url,
                updated_at = @updated_at
                output inserted.id, inserted.name, inserted.friendly_url
                Where id = @id
            ";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@id",SqlDbType.Int, region.ID),
            executor.GenerateParameter("@name",SqlDbType.VarChar, region.Name),
            executor.GenerateParameter("@friendly_url",SqlDbType.VarChar, region.FriendlyUrl),
            executor.GenerateParameter("@updated_at",SqlDbType.DateTime, region.UpdatedAt)
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        return result is not null ? region : null;
    }


    public static Region SoftDelete(Region region)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        region.DeletedAt = DateTime.Now;
        string query =
        @"UPDATE Region SET
                deleted_at = @deleted_at
                output inserted.id, inserted.friendly_url
                Where id = @id";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@id",SqlDbType.Int, region.ID),
            executor.GenerateParameter("@deleted_at",SqlDbType.DateTime, region.DeletedAt),
        };
        Dictionary<string, string> result = executor.LoadData<List<Dictionary<string, string>>>(query, parameters).FirstOrDefault();
        return result is not null ? region : null;
    }
}
