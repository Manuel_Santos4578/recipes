﻿using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Util;

public class Validator
{
    public string ApiDomain { get; set; }
    public string ApiToken { get; set; }
    private const string VALID_CHARS = "123456789zaqwsxcderfvbgtyhnmjuiklopZAQWSXCDERFVBGTYHNMJUIKLOP";
    public delegate T read_from_table<T>(string slug, bool with_trashed);

    [ThreadStatic]
    private static Random _random;

    private static readonly Random RANDOM = _random ??= new Random();

    public Validator(string apiDomain, string apiToken)
    {
        ApiDomain = apiDomain;
        ApiToken = apiToken;
    }

    /// <summary>
    /// Validate given email address
    /// </summary>
    /// <param name="emailAddress"></param>
    /// <returns></returns>
    public static bool ValidateEmail(string emailAddress)
    {
        Regex regex = new Regex("^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$");
        return regex.Match(emailAddress).Success;
    }
    /// <summary>
    /// Validate Name method, must me 2+ characters long
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public static bool ValidateName(string name)
    {
        if (!string.IsNullOrWhiteSpace(name))
        {
            Regex regex = new Regex("^([A - z])([a - z]){ 2,}$");
            Match match = regex.Match(name);
            return match.Success;
        }
        return false;
    }
    /// <summary>
    /// Checks if given slug already exists in provided data source
    /// </summary>
    /// <param name="slug">input slug</param>
    /// <param name="endpoint">api endpoint uri</param>
    /// <param name="originalPK">original slug</param>
    /// <param name="addition">randomly generated string</param>
    /// <returns></returns>
    public async Task<string> ValidateSlug(string slug, string endpoint, string originalPK = "", string addition = "")
    {
        try
        {
            Dictionary<string, string> query = new Dictionary<string, string> { { "with_trashed", true.ToString() } };
            Uri request_uri = new Uri(QueryHelpers.AddQueryString($"{endpoint}/{slug}", query), UriKind.Relative);
            JToken data = await new ApiClientV3(ApiDomain, ApiToken).GetAsync<JToken>(request_uri.ToString());
            if (data is not null)
            {
                originalPK = addition != "" ? originalPK : slug;
                addition = "";
                for (int i = 0; i < 5; i++)
                {
                    addition += VALID_CHARS[RANDOM.Next(0, VALID_CHARS.Length - 1)];
                }
                slug = originalPK != "" ? originalPK + addition : slug + addition;
                return await ValidateSlug(slug, endpoint, originalPK, addition);
            }
            return slug;
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Checks if given slug already exists in provided data source
    /// </summary>
    /// <param name="slug">input slug</param>
    /// <param name="validate_exists">existance validation</param>
    /// <param name="original_slug">original slug</param>
    /// <param name="addition">randomly generated string</param>
    /// <returns></returns>
    public static string ValidateSlug<TEntity>(string slug, read_from_table<TEntity> validate_exists, string original_slug = "", string addition = "")
    {
        try
        {
            TEntity entity = validate_exists(slug, true);
            if (entity is not null)
            {
                original_slug = addition != "" ? original_slug : slug;
                addition = "";
                for (int i = 0; i < 5; i++)
                {
                    addition += VALID_CHARS[RANDOM.Next(0, VALID_CHARS.Length - 1)];
                }
                slug = original_slug != "" ? original_slug + addition : slug + addition;
                return ValidateSlug(slug, validate_exists, original_slug, addition);
            }
            return slug;
        }
        catch (Exception)
        {
            throw;
        }
    }
}
