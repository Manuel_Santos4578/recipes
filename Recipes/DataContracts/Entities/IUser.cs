﻿namespace DataContracts.Entities;

public interface IUser
{
    int ID { get; set; }
    string Name { get; set; }
    string Username { get; set; }
    DateTime? BlockedAt { get; set; }
    string PreferedCulture { get; set; }
    IList<IRole> Roles { get; set; }
}