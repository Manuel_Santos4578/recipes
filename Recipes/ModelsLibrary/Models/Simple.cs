﻿using DataContracts.Entities;
using Newtonsoft.Json;
using System;

namespace ModelsLibrary.Models;

public abstract class Simple : ISimple
{
    [JsonProperty("created_at")]
    public DateTime CreatedAt { get; set; }
    [JsonProperty("updated_at")]
    public DateTime UpdatedAt { get; set; }
    [JsonProperty("deleted_at")]
    public DateTime? DeletedAt { get; set; }
}
