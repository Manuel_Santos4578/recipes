﻿using DataContracts.Entities;
using ModelsLibrary.JsonConverters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;

namespace ModelsLibrary.Models;

public class Recipe : Simple, IRecipe
{
    [JsonProperty("recipe_id")]
    public int ID { get; set; }
    [JsonProperty("recipe_name")]
    public string Name { get; set; }
    [JsonProperty("recipe_friendly_url")]
    public string FriendlyUrl { get; set; }
    [JsonProperty("recipe_text")]
    public string Text { get; set; }
    [JsonProperty("is_favorite")]
    public bool IsFavorite { get; set; }
    [JsonConverter(typeof(CollectionJsonConverter<IRecipeIngredient, List<RecipeIngredient>>))]
    [JsonProperty("ingredients")]
    public IList<IRecipeIngredient> Ingredients { get; set; }
    [JsonProperty("category", NullValueHandling = NullValueHandling.Ignore)]
    [JsonConverter(typeof(CategoryJsonConverter))]
    public ICategory Category { get; set; }
    [JsonProperty("author")]
    [JsonConverter(typeof(JsonConverters.JsonConverter<Author>))]
    public IAuthor Author { get; set; }
    [JsonProperty("source")]
    [JsonConverter(typeof(JsonConverters.JsonConverter<Source>))]
    public ISource Source { get; set; }
    [JsonProperty("country")]
    [JsonConverter(typeof(JsonConverters.JsonConverter<Country>))]
    public ICountry Country { get; set; }
    [JsonProperty("main_image")]
    [JsonConverter(typeof(JsonConverters.JsonConverter<RecipeImage>))]
    public IRecipeImage MainImage { get; set; }
    [JsonProperty("misc_images")]
    public IList<IRecipeImage> MiscImages { get; set; }
    [JsonProperty("courses")]
    [JsonConverter(typeof(CollectionJsonConverter<ICourse, List<Course>>))]
    public IList<ICourse> Courses { get; set; }
    [JsonProperty("recipe_user", NullValueHandling = NullValueHandling.Ignore)]
    [JsonConverter(typeof(JsonConverters.JsonConverter<RecipeUser>))]
    public IRecipeUser RecipeUser { get; set; }

    public Recipe()
    {
        Category = new Category();
        Author = new Author();
        Ingredients = new List<IRecipeIngredient>();
        Source = new Source();
        Country = new Country();
        Courses = new List<ICourse>();
        MiscImages = new List<IRecipeImage>();
        MainImage = new RecipeImage();
        RecipeUser = new RecipeUser();
    }

    public static DataTable ExportToDataTable(IEnumerable<IRecipe> recipes)
    {
        DataTable table = new DataTable();
        table.Columns.AddRange(new DataColumn[]
        {
            new DataColumn(nameof(IRecipe.Name), typeof(string)),
            new DataColumn($"{nameof(IRecipe.Category)}.{nameof(IRecipe.Category.Name)}", typeof(string)),
            new DataColumn($"{nameof(IRecipe.Country)}.{nameof(IRecipe.Country.Name)}", typeof(string)),
            new DataColumn($"{nameof(IRecipe.Source)}.{nameof(IRecipe.Source.Name)}", typeof(string)),
            new DataColumn(nameof(IRecipe.CreatedAt), typeof(DateTime)),
            new DataColumn(nameof(IRecipe.UpdatedAt), typeof(DateTime)),
        });

        foreach (IRecipe recipe in recipes)
        {
            table.Rows.Add(
                recipe.Name,
                recipe.Category?.Name ?? "---",
                recipe.Country?.Name ?? "---",
                recipe.Source?.Name ?? "---",
                recipe.CreatedAt,
                recipe.UpdatedAt);
        }
        return table;
    }
}
