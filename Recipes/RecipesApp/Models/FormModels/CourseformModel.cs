﻿using DataContracts.Entities;
using ModelsLibrary.Models;

namespace RecipesApp.Models.FormModels;

public sealed class CourseFormModel: Course
{
    public CourseFormModel()
    {

    }
    public CourseFormModel(ICourse course)
    {
        ID = course.ID;
        Name = course.Name;
        FriendlyUrl = course.FriendlyUrl;
    }
}
