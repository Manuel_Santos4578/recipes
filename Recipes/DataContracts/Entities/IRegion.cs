﻿namespace DataContracts.Entities;

public interface IRegion : ISimple
{
    int ID { get; set; }
    string Name { get; set; }
    string FriendlyUrl { get; set; }
}
