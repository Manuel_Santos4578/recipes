﻿namespace DataContracts.Entities;

public interface IRecipeIngredient : ISimple
{
    IIngredient Ingredient { get; set; }
    decimal Quantity { get; set; }
    string Unit { get; set; }
}