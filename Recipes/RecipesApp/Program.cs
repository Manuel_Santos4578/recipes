using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace RecipesApp;

public class Program
{
    public static void Main(string[] args)
    {
        EnvironmentSetup.Setup();
        CreateHostBuilder(args).Build().Run();
    }

    public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
            });
}
