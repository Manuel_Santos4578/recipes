﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ModelsLibrary.Models;
using Newtonsoft.Json.Linq;
using Recipes.Database.Repositories;
using RecipesApi.Configuration;
using RecipesLibrary.Util;
using System;
using System.Threading.Tasks;
using Util;

namespace RecipesApi.Controllers;

/// <summary>
/// RecipeCourse
/// </summary>
[Route("recipecourses")]
[ApiController]
[Produces("application/json")]
[Authorize(Roles = $"{ConfigurationState.ApiRoles.Write}, {ConfigurationState.ApiRoles.Root}")]
public class RecipeCourseController : BaseApiController
{
    public RecipeCourseController(IQueryExecutor _executor) : base(_executor)
    {
    }

    /// <summary>
    /// Store method
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    [ProducesResponseType(500)]
    [ProducesResponseType(201)]
    [ProducesResponseType(401)]
    public async Task<IActionResult> Store()
    {
        try
        {
            JToken data = await Request.Body.ReadObject<JToken>();
            Recipe recipe = new Recipe { ID = data.Value<int>("recipe_id") };
            Course course = new Course { ID = data.Value<int>("course_id") };
            var result = RecipeCourseRepository.Store(recipe, course);
            int status = result is not null ? 201 : 200;
            return StatusCode(status, result);
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Delete method
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpDelete]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public IActionResult Delete(int recipe_id, int course_id)
    {
        try
        {
            Recipe recipe = new Recipe { ID = recipe_id };
            Course course = new Course { ID = course_id };
            return Ok(RecipeCourseRepository.Delete(recipe, course));
        }
        catch (Exception)
        {
            throw;
        }
    }
}
