
use Recipes
GO
-- query columns from one table
create or alter function dbo.query_columns(@table varchar(100)) 
returns varchar(max) as
	BEGIN
		DECLARE @statements varchar(8000)
		SELECT @statements = ISNULL(@statements + ',', '') + QUOTENAME(table_name) + '.' + QUOTENAME(column_name) + ' AS ' + lower(table_name) + '_' + lower(column_name)
		FROM INFORMATION_SCHEMA.COLUMNS
		WHERE TABLE_NAME = '' + @table + '' 
		ORDER BY ORDINAL_POSITION
		return @statements
	END;
GO
create or alter function dbo.paginate_results(@page int, @size int) 
	returns varchar(255) as
BEGIN
	declare @query varchar(255)
	return ' OFFSET '+ CAST(((@page - 1) * @size) as varchar(10)) + ' ROWS FETCH NEXT ' + cast( @size as varchar(10)) +' ROWS ONLY '
END
GO
-- query columns from two tables 
create or alter function dbo.query_columns_two_tables(@table varchar(100), @table_2 varchar(100)) 
returns varchar(max) as
	BEGIN
		DECLARE @statements varchar(8000)
		SELECT @statements = ISNULL(@statements + ',', '') + QUOTENAME(table_name) + '.' + QUOTENAME(column_name) + ' AS ' + lower(table_name) + '_' + lower(column_name)
		FROM INFORMATION_SCHEMA.COLUMNS
		WHERE TABLE_NAME = '' + @table + '' OR TABLE_NAME = '' + @table_2 + ''
		ORDER BY ORDINAL_POSITION
		return @statements
	END;
GO
-- sql result pagination
create or alter function dbo.paginate_results(@page int, @size int) 
	returns varchar(255) as
BEGIN
	declare @query varchar(255)
	return ' OFFSET '+ CAST(((@page - 1) * @size) as varchar(10)) + ' ROWS FETCH NEXT ' + cast( @size as varchar(10)) +' ROWS ONLY '
END
GO

-- string pattern matching

create or alter function dbo.varchar_pattern_matching(@expression varchar(max)) returns varchar(max) as 
	BEGIN
		declare @result varchar(max) = ''
		set @result =(Select concat('%', @expression ,'%'))
		return @result
	END
GO
