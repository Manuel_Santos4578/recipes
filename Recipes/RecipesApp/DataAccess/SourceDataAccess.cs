﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ModelsLibrary.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Util;
using System;
using Microsoft.AspNetCore.WebUtilities;
using DataContracts.Access;
using DataContracts.Entities;

namespace RecipesApp.DataAccess;

public sealed partial class DataAccess: ISourceDataAccess
{
    #region Recipe Source
    public async Task<IEnumerable<ISource>> SelectAllRecipeSourceAsync(string name = "", int page = 1, int size = 0)
    {
        Dictionary<string, string> query = new Dictionary<string, string>
        {
            {"page",page.ToString() },
            {"size",size.ToString() }
        };
        if (!string.IsNullOrWhiteSpace(name)) query.Add("name", name);
        Uri uri = new Uri(QueryHelpers.AddQueryString(sourcesEndpoint, query), UriKind.Relative);
        return await new ApiClientV3(ApiDomain, ApiToken).GetAsync<IEnumerable<Source>>(uri.ToString());
    }
    public async Task<int> SelectRecipeSourceCountAsync(string name = "")
    {
        Dictionary<string, string> query = new Dictionary<string, string>
        {
            {"count",true.ToString() },
        };
        if (!string.IsNullOrWhiteSpace(name)) query.Add("name", name);
        Uri uri = new Uri(QueryHelpers.AddQueryString(sourcesEndpoint, query), UriKind.Relative);
        JToken apiData = await new ApiClientV3(ApiDomain, ApiToken).GetAsync<JToken>(uri.ToString());
        return apiData.Value<int>("count");
    }
    public async Task<ISource> SelectSourceBySlugAsync(string slug)
    {
        return await new ApiClientV3(ApiDomain, ApiToken).GetAsync<Source>($"{sourcesEndpoint}/" + slug);
    }
    public async Task<ISource> InsertSourceAsync(ISource source)
    {
        using StringContent content = new StringContent(JsonConvert.SerializeObject(source));
        source = await new ApiClientV3(ApiDomain, ApiToken).PostAsync<Source>(sourcesEndpoint, content);
        return source;
    }
    public async Task InsertSourcesAsync(IEnumerable<ISource> sources)
    {
        StringContent content = new StringContent(JsonConvert.SerializeObject(sources));
        await new ApiClientV3(ApiDomain, ApiToken).PostAsync<IEnumerable<Source>>($"{sourcesEndpoint}/bulk", content);
    }
    public async Task<ISource> UpdateSourceAsync(ISource source)
    {
        using StringContent content = new StringContent(JsonConvert.SerializeObject(source));
        source = await new ApiClientV3(ApiDomain, ApiToken).PutAsync<Source>($"{sourcesEndpoint}/{source.ID}", content);
        return source;
    }
    public async Task<bool> DeleteSourceAsync(ISource source)
    {
        source = await new ApiClientV3(ApiDomain, ApiToken).DeleteAsync<Source>($"{sourcesEndpoint}/" + source.ID);
        return source is not null;
    }
    #endregion
}
