﻿namespace Recipes.Database.Config;

public class DB
{
    /// <summary>
    /// 
    /// </summary>
    public string Host { get; private set; }
    /// <summary>
    /// 
    /// </summary>
    public string Database { get; private set; }
    /// <summary>
    /// 
    /// </summary>
    public string User { get; private set; }
    /// <summary>
    /// 
    /// </summary>
    public string Password { get; private set; }
    /// <summary>
    /// 
    /// </summary>
    public bool TrustCertificate { get; private set; }

    private static DB _instance;

    /// <summary>
    /// 
    /// </summary>
    public static DB Instance
    {
        get
        {
            if (_instance is null)
            {
                throw new Exception($"DB.CreateInstance method must be invoked before making use of {nameof(Instance)}");
            }
            return _instance;
        }
    }
    private DB() { }

    /// <summary>
    /// Creates the DB object instance
    /// </summary>
    /// <param name="host"></param>
    /// <param name="database"></param>
    /// <param name="username"></param>
    /// <param name="password"></param>
    /// <param name="trustCertificate"></param>
    public static void CreateInstance(string host, string database, string username, string password, bool trustCertificate = true)
    {
        _instance = new DB
        {
            Host = host,
            Database = database,
            User = username,
            Password = password,
            TrustCertificate = trustCertificate,
        };
    }
}
