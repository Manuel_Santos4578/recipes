﻿using DataContracts.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using RecipesApp.Models;
using RecipesApp.Models.FormModels;
using RecipesApp.Validators.FormValidation;
using RecipesLibrary.Util;
using System.Linq;
using System.Threading.Tasks;
using Util;
using I18N = ModelsLibrary.Constants.AppConstants.I18N;

namespace RecipesApp.Controllers;

[Route("courses")]
[Authorize]
public sealed class CourseController : MyBaseController
{
    public CourseController(IConfiguration config, IWebHostEnvironment env) : base(config, env) { }

    [HttpGet]
    public async Task<IActionResult> IndexAsync()
    {
        await ApiAuthAsync();
        CourseViewModel model = new CourseViewModel();
        HandleViewData();
        return View(model);
    }

    [HttpPost]
    public async Task<IActionResult> IndexAjaxAsync()
    {
        JToken body = await Request.Body.ReadObject<JToken>();
        int page = body.Value<int>("page");
        int page_size = body.Value<int>("size");
        string name = body.Value<string>("name");
        await ApiAuthAsync();
        CourseViewModel model = new CourseViewModel
        {
            PageSize = page_size,
            CurrentPage = page,
            Count = await dal.SelectCoursesCountAsync(name),
            Courses = await dal.SelectAllCoursesAsync(name, page, page_size)
        };
        return Ok(model);
    }
    [HttpGet("show/{friendly_url}")]
    public async Task<IActionResult> ShowAsync(string friendly_url)
    {
        await ApiAuthAsync();
        CourseViewModel model = new CourseViewModel
        {
            Course = await dal.SelectCourseBySlugAsync(friendly_url)
        };
        if (model.Course is null) return View("NotFound");
        return View(model);
    }

    [HttpGet("create")]
    public IActionResult CreateAsync()
    {
        return View(new CourseFormModel());
    }

    [HttpPost("store")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> StoreAsync(CourseFormModel model)
    {
        FluentValidation.Results.ValidationResult validation_result = new CourseFormModelValidator().Validate(model);
        if (!validation_result.IsValid)
        {
            TempData["Error"] = string.Join("; ", validation_result.Errors.Select(err => err.ErrorMessage));
            return RedirectToAction("Index", "Course");
        }

        await ApiAuthAsync();
        model.Name = model.Name.Sanitize();
        ICourse course = await dal.InsertCourseAsync(model);
        if (course is not null)
        {
            TempData["Success"] = string.Format(translation[I18N.Successfully_created], model.Name);
            return RedirectToAction("Show", "Course", new { friendly_url = course.FriendlyUrl });
        }
        TempData["Error"] = string.Format(translation[I18N.Failed_creation_of], model.Name);
        return RedirectToAction("Index", "Course");
    }

    [HttpGet("edit/{friendly_url}")]
    public async Task<IActionResult> EditAsync(string friendly_url)
    {
        await ApiAuthAsync();
        ICourse course = await dal.SelectCourseBySlugAsync(friendly_url);
        if (course is null) return NotFound();
        return View(new CourseFormModel(course));
    }

    [HttpPost("update/{friendly_url}")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> UpdateAsync(CourseFormModel model, string friendly_url)
    {
        FluentValidation.Results.ValidationResult validation_result = new CourseFormModelValidator().Validate(model);
        if (!validation_result.IsValid)
        {
            TempData["Error"] = string.Join("; ", validation_result.Errors.Select(err => err.ErrorMessage));
            return Redirect(Request.Headers["Referer"]);
        }
        await ApiAuthAsync();
        model.Name = model.Name.Sanitize();
        model.FriendlyUrl = friendly_url;
        ICourse course = await dal.UpdateCourseAsync(model);
        if (course is not null)
        {
            TempData["Success"] = string.Format(translation[I18N.Successfully_updated], model.Name);
            return RedirectToAction("Show", "Course", new { friendly_url = course.FriendlyUrl });
        }
        TempData["Error"] = string.Format(translation[I18N.Failed_to_update], model.Name);
        return RedirectToAction("Show", "Course", new { friendly_url });
    }

    [HttpGet("delete/{friendly_url}")]
    public async Task<IActionResult> DeleteAsync(string friendly_url)
    {
        await ApiAuthAsync();
        ICourse course = await dal.SelectCourseBySlugAsync(friendly_url);
        if (course is null) return NotFound();
        return View(new CourseFormModel(course));
    }

    [HttpPost("destroy/{friendly_url}")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DestroyAsync(CourseFormModel model, string friendly_url)
    {
        await ApiAuthAsync();
        if (await dal.DeleteCourseAsync(model))
        {
            TempData["Success"] = string.Format(translation[I18N.Successfully_deleted], model.Name);
            return RedirectToAction("Index", "Course");
        }
        TempData["Error"] = string.Format(translation[I18N.Failed_to_delete], model.Name);
        return RedirectToAction("Show", "Course", new { friendly_url });
    }
}
