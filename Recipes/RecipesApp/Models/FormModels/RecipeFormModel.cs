﻿using DataContracts.Entities;
using Microsoft.AspNetCore.Http;
using ModelsLibrary.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace RecipesApp.Models.FormModels;

public sealed class RecipeFormModel
{
    public int ID { get; set; }
    public string Name { get; set; }
    public string FriendlyUrl { get; set; }
    public string Text { get; set; }
    public int CountryId { get; set; }
    public int SourceId { get; set; }
    public int AuthorId { get; set; }
    public int CategoryId { get; set; }
    public List<int> CourseIds { get; set; }

    [DataType(DataType.Upload)]
    public IFormFile MainImage { get; set; }
    public List<IFormFile> MiscImages { get; set; }
    public IEnumerable<ICountry> Countries { get; set; }
    public IEnumerable<ICategory> Categories { get; set; }
    public IEnumerable<IAuthor> Authors { get; set; }
    public IEnumerable<ISource> Sources { get; set; }
    public IEnumerable<ICourse> Courses { get; set; }
    public IEnumerable<IIngredient> Ingredients { get; set; }
    public List<RecipeIngredient> RecipeIngredients { get; set; }
    public IEnumerable<ICourse> RecipeCourses { get; set; }
    public RecipeFormModel()
    {
        Countries = new List<Country>();
        Categories = new List<Category>();
        Authors = new List<Author>();
        Sources = new List<Source>();
        Ingredients = new List<Ingredient>();
        RecipeIngredients = new List<RecipeIngredient>();
        RecipeCourses = new List<Course>();
        MiscImages = new List<IFormFile>();
        CourseIds = new List<int>();
        Courses = new List<Course>();
    }
    public RecipeFormModel(IRecipe recipe) : this()
    {
        ID = recipe.ID;
        Name = recipe.Name;
        FriendlyUrl = recipe.FriendlyUrl;
        CountryId = recipe.Country.ID;
        CategoryId = recipe.Category.ID;
        SourceId = recipe.Source.ID;
        AuthorId = recipe.Author.ID;
        Text = recipe.Text;
        RecipeIngredients = recipe.Ingredients.Cast<RecipeIngredient>().ToList();
        RecipeCourses = recipe.Courses;
    }
}
