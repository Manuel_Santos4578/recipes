﻿namespace RecipesApp.RequestModels;

public sealed class RecipeImageRequestModel
{
    public int recipe_id;
    public bool is_display;
    public string image;
    public string image_base_64;
    public string content_type;
}
