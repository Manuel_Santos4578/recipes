﻿using DataContracts.Entities;
using ModelsLibrary.Models;
using System.Collections.Generic;
using System.Linq;

namespace RecipesApp.Models;

public sealed class IngredientViewModel : ViewModel
{
    public IIngredient Ingredient { get; set; }
    public IEnumerable<IIngredient> Ingredients { get; set; }
    public List<Recipe> IngredientRecipes { get; set; }
    public IEnumerable<ICountry> Countries { get; set; }
    public IngredientViewModel()
    {
        Ingredient = new Ingredient();
        Ingredients = new List<Ingredient>();
        Countries = new List<Country>();
        IngredientRecipes = new List<Recipe>();
    }
}
