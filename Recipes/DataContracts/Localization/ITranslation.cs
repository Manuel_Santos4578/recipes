﻿namespace DataContracts.Localization;

public interface ITranslation
{
    ICulture Culture { get; set; }
    string Key { get; set; }
    string Value { get; set; }
}