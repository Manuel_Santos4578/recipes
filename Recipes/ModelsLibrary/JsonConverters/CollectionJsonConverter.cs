﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Linq;

namespace ModelsLibrary.JsonConverters;

internal class CollectionJsonConverter<T, U>: JsonConverter 
    where U : IList, new()
{
    public override bool CanWrite => false;
    public override bool CanRead => true;
    public override bool CanConvert(Type objectType)
    {
        return objectType == typeof(T);
    }
    public override void WriteJson(JsonWriter writer,
        object value, JsonSerializer serializer)
    {
        throw new InvalidOperationException("Use default serialization.");
    }

    public override object ReadJson(JsonReader reader,
        Type objectType, object existingValue,
        JsonSerializer serializer)
    {
        var jsonObject = JArray.Load(reader);
        U instance = new();

        serializer.Populate(jsonObject.CreateReader(), instance);
        return instance.Cast<T>().ToList();
    }
}
