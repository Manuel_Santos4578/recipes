﻿using FluentValidation;
using RecipesApp.Services;
using System.Collections.Generic;
using System.Threading;

namespace RecipesApp.Validators;

public class BaseFluentValidator<T> : AbstractValidator<T>
{
    protected readonly Dictionary<string, string> _translations;
    public BaseFluentValidator()
    {
        _translations = CacheManagementService.TranslationsAsync().Result[Thread.CurrentThread.CurrentCulture.Name];
    }
}
