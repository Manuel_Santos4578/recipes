﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using RecipesApi.ApiAuth;
using RecipesApi.Configuration;
using RecipesApi.Repositories;
using RecipesLibrary.Util;
using System;
using System.Threading.Tasks;
using Util;

namespace RecipesApi.Controllers;

/// <summary>
/// 
/// </summary>
[Route("api/apiauth")]
[Produces("application/json")]
[Authorize(Roles = $"{ConfigurationState.ApiRoles.Write}, {ConfigurationState.ApiRoles.Root}")]
public class ApiAuthController : Controller
{
    public ApiAuthController()
    {
    }

    /// <summary>
    /// Api authentication
    /// </summary>
    /// <remarks>
    /// Request Body:
    /// 
    ///     {
    ///         "username": string,
    ///         "password": string
    ///     }
    ///     
    /// </remarks>
    /// <returns>bearer token</returns>
    [AllowAnonymous]
    [HttpPost("login")]
    [ProducesResponseType(200)]
    [ProducesResponseType(400)]
    [ProducesResponseType(500)]
    public async Task<IActionResult> LoginAsync()
    {
        try
        {
            ApiUser api_user = await Request.Body.ReadObject<ApiUser>();
            api_user.Password = Encryption.EncryptPass(api_user.Password);

            api_user = ApiUserRepository.ReadByUsernameAndPassword(api_user.Username, api_user.Password);
            if (api_user is not null)
            {
                string bearer_token = JwtGenerator.GenerateToken(api_user);
                api_user.Password = null;
                return Ok(new { user = api_user, token = bearer_token });
            }
            return Unauthorized(new { message = "Authentication failure" });
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Register 
    /// </summary>
    /// <remarks>
    /// Request Body:
    /// 
    ///     {
    ///         "username": string,
    ///         "password": string,
    ///         "role_id": int
    ///     }
    ///     
    /// </remarks>
    /// <returns>inserted username</returns>
    [HttpPost("register")]
    [ProducesResponseType(200)]
    [ProducesResponseType(500)]
    public async Task<IActionResult> RegisterAsync()
    {
        try
        {
            JToken data = await Request.Body.ReadObject<JToken>();
            string password = data.Value<string>("password");
            string encrypedPW = Encryption.EncryptPass(password);

            ApiUser api_user = new ApiUser
            {
                Username = data.Value<string>("username"),
                Password = encrypedPW,
            };
            api_user = ApiUserRepository.Store(api_user, data.Value<int>("role_id"));
            return Ok(api_user);
        }
        catch (Exception)
        {
            throw;
        }
    }
}
