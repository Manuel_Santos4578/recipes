﻿using Microsoft.AspNetCore.Mvc;

namespace RecipesApi.api;

[Route("error")]
[Produces("application/json")]
public class ErrorController : Controller
{
    [HttpGet]
    public IActionResult Error()
    {
        return StatusCode(500, new { message = "Internal Error Ocurred" });
    }
}
