﻿using DataContracts.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ModelsLibrary.Models;
using Newtonsoft.Json.Linq;
using RecipesApp.Models;
using RecipesApp.Models.FormModels;
using RecipesApp.Validators.FormValidation;
using RecipesLibrary.Util;
using System.Linq;
using System.Threading.Tasks;
using Util;
using I18N = ModelsLibrary.Constants.AppConstants.I18N;

namespace RecipesApp.Controllers;

[Route("countries")]
[Authorize]
public sealed class CountryController : MyBaseController
{

    public CountryController(IConfiguration config, IWebHostEnvironment env) : base(config, env)
    {
    }

    [HttpGet]
    public async Task<IActionResult> Index(string n, int r)
    {
        await ApiAuthAsync();
        CountryViewModel model = new CountryViewModel
        {
            Regions = await dal.SelectAllRegionsAsync()
        };
        IRegion region = model.Regions.FirstOrDefault(x => x.ID == r);
        ViewData["country_name"] = n;
        ViewData["region"] = r;
        HandleViewData();
        return View(model);
    }

    [HttpPost]
    public async Task<IActionResult> IndexAjaxAsync()
    {
        JToken body = await Request.Body.ReadObject<JToken>();
        int page = body.Value<int>("page");
        int page_size = body.Value<int>("size");
        string region_slug = body.Value<string>("region");
        string name = body.Value<string>("name");
        await ApiAuthAsync();
        IRegion region = !string.IsNullOrWhiteSpace(region_slug) ? await dal.SelectRegionBySlugAsync(region_slug) : null;
        CountryViewModel model = new CountryViewModel
        {
            PageSize = page_size,
            CurrentPage = page,
            Count = await dal.SelectCountryCountAsync(region, name),
            Countries = await dal.SelectAllCountriesAsync(page, page_size, region, name)
        };
        return Ok(model);
    }
    [HttpGet("show/{friendly_url}")]
    public async Task<IActionResult> ShowAsync(string friendly_url)
    {
        await ApiAuthAsync();
        CountryViewModel model = new CountryViewModel
        {
            Country = await dal.SelectCountryBySlugAsync(friendly_url)
        };

        if (model.Country is null) return View("NotFound");

        int user = User.Claims.Any(c => c.Type == System.Security.Claims.ClaimTypes.Role && c.Value == ModelsLibrary.Constants.AppConstants.Roles.Admin) ? 0 : int.Parse(User.FindFirst("ID").Value);

        model.CountryRecipes = await dal.SelectAllRecipesAsync(country: model.Country.FriendlyUrl, user: user);
        model.CountryRecipesCount = await dal.SelectRecipesCountAsync(country: model.Country.FriendlyUrl, user: user);

        HandleViewData();
        return View(model);
    }


    [HttpGet("create")]
    public async Task<IActionResult> CreateAsync()
    {
        await ApiAuthAsync();
        CountryFormModel form_model = new CountryFormModel
        {
            Regions = await dal.SelectAllRegionsAsync()
        };
        return View(form_model);
    }

    [HttpPost("store")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> StoreAsync(CountryFormModel model)
    {
        FluentValidation.Results.ValidationResult validation_result = await new CountryFormModelValidator().ValidateAsync(model);
        if (!validation_result.IsValid)
        {
            TempData["Error"] = string.Join("; ", validation_result.Errors.Select(err => err.ErrorMessage));
            return RedirectToAction("Index", "Country");
        }
        await ApiAuthAsync();
        model.Name = model.Name.Sanitize();

        ICountry country = await dal.InsertCountryAsync(new Country
        {
            Name = model.Name,
            FriendlyUrl = model.FriendlyUrl,
            Region = new Region
            {
                ID = model.RegionID
            }
        });
        if (country is not null)
        {
            TempData["Success"] = string.Format(translation[I18N.Successfully_created], country.Name);
            return RedirectToAction("Show", "Country", new { friendly_url = country.FriendlyUrl });
        }
        TempData["Error"] = string.Format(translation[I18N.Failed_creation_of], model.Name);
        return RedirectToAction("Index", "Country");
    }

    [HttpGet("edit/{friendly_url}")]
    public async Task<IActionResult> EditAsync(string friendly_url)
    {
        await ApiAuthAsync();
        ICountry country = await dal.SelectCountryBySlugAsync(friendly_url);
        CountryFormModel form_model = new CountryFormModel(country)
        {
            Regions = await dal.SelectAllRegionsAsync()
        };
        return View(form_model);
    }

    [HttpPost("update/{friendly_url}")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> UpdateAsync(CountryFormModel model, string friendly_url)
    {
        await ApiAuthAsync();
        FluentValidation.Results.ValidationResult validation_result = await new CountryFormModelValidator().ValidateAsync(model);
        if (!ModelState.IsValid)
        {
            TempData["Error"] = string.Join("; ", validation_result.Errors.Select(err => err.ErrorMessage));
            return Redirect(Request.Headers["Referer"]);
        }
        model.Name = model.Name.Sanitize();

        ICountry country = await dal.UpdateCountryAsync(new Country
        {
            ID = model.ID,
            Name = model.Name,
            FriendlyUrl = friendly_url,
            Region = new Region
            {
                ID = model.RegionID
            }
        });
        if (country is not null)
        {
            TempData["Success"] = string.Format(translation[I18N.Successfully_updated], model.Name);
            return RedirectToAction("Show", "Country", new { friendly_url = country.FriendlyUrl });
        }
        TempData["Error"] = string.Format(translation[I18N.Failed_to_update], model.Name);
        return RedirectToAction("Show", "Country", new { friendly_url });
    }

    [HttpGet("delete/{friendly_url}")]
    public async Task<IActionResult> DeleteAsync(string friendly_url)
    {
        await ApiAuthAsync();
        ICountry country = await dal.SelectCountryBySlugAsync(friendly_url);
        CountryFormModel form_model = new CountryFormModel(country);
        return View(form_model);
    }

    [HttpPost("destroy")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DestroyAsync(CountryFormModel model)
    {
        await ApiAuthAsync();
        ICountry country = new Country
        {
            ID = model.ID
        };
        bool deleted = await dal.DeleteCountryAsync(country);
        if (deleted)
        {
            TempData["Success"] = string.Format(translation[I18N.Successfully_deleted], model.Name);
            return RedirectToAction("Index", "Country");
        }
        TempData["Error"] = string.Format(translation[I18N.Failed_to_delete], model.Name);
        return RedirectToAction("Show", "Country", new { friendly_url = model.FriendlyUrl });
    }
}
