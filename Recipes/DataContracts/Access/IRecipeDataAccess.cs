﻿using DataContracts.Entities;

namespace DataContracts.Access;

public partial interface IRecipeDataAccess
{
    Task<bool> DeleteRecipeAsync(IRecipe recipe);
    Task<IRecipe> InsertRecipeAsync(IRecipe recipe);
    Task<IList<IRecipeIngredient>> SelectAllRecipeIngredientsAsync(IRecipe recipe);
    Task<IEnumerable<IRecipe>> SelectAllRecipesAsync(string name = "", string author = "", string category = "", string country = "", string region = "", string source = "", string ingredient = "", int user = 0, int page = 1, int size = 0);
    Task<IEnumerable<IRecipe>> SelectFavoritesAsync(int user_id);
    Task<IRecipe> SelectRecipeBySlugAsync(string slug);
    Task<IList<ICourse>> SelectRecipeCoursesAsync(IRecipe recipe);
    Task<IList<IRecipeImage>> SelectRecipeImagesAsync(IRecipe recipe, bool is_display);
    Task<int> SelectRecipesCountAsync(string name = "", string author = "", string category = "", string country = "", string region = "", string source = "", string ingredient = "", int user = 0);
    Task<IRecipe> UpdateRecipeAsync(IRecipe recipe);
}
