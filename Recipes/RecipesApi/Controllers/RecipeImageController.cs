﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ModelsLibrary.Models;
using Recipes.Database.Repositories;
using RecipesApi.Configuration;
using RecipesLibrary.Util;
using System;
using System.Threading.Tasks;
using Util;

namespace RecipesApi.Controllers;

/// <summary>
/// Recipe Image
/// </summary>
[Route("api/recipeimages")]
[Produces("application/json")]
[Authorize(Roles = $"{ConfigurationState.ApiRoles.Write}, {ConfigurationState.ApiRoles.Root}")]
public class RecipeImageController : BaseApiController
{
    public RecipeImageController(IQueryExecutor _executor) : base(_executor)
    {
    }

    /// <summary>
    /// Select all method
    /// </summary>
    /// <param name="recipe_id">recipe id</param>
    /// <param name="is_display"></param>
    /// <param name="count"></param>
    /// <returns></returns>
    [HttpGet]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public IActionResult Index(int recipe_id, bool is_display = false, bool count = false)
    {
        try
        {
            if (count)
            {
                return Ok(new { count = RecipeImageRepository.ReadCount(recipe_id, is_display) });
            }
            return Ok(RecipeImageRepository.ReadAll(recipe_id, is_display));
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Select by id method
    /// </summary>
    /// <param name="id">PK</param>
    /// <returns>Images collection</returns>
    //[HttpGet("{id}")]
    //[ProducesResponseType(500)]
    //[ProducesResponseType(200)]
    //[ProducesResponseType(401)]
    //public IActionResult Show(int id)
    //{
    //    try
    //    {
    //        string query = @"Select * from RecipeImages Where deleted_at is null and id = @id";
    //        List<SqlParameter> parameters = new List<SqlParameter>
    //        {
    //            executor.GenerateParameter("@id",SqlDbType.Int, id)
    //        };
    //        return Ok(executor.LoadData(query, parameters));
    //    }
    //    catch (Exception)
    //    {
    //        throw;
    //    }
    //}

    /// <summary>
    /// Insert method
    /// </summary>
    /// <returns>inserted id</returns>
    [HttpPost]
    [ProducesResponseType(500)]
    [ProducesResponseType(201)]
    [ProducesResponseType(401)]
    public async Task<IActionResult> StoreAsync()
    {
        try
        {
            RecipeImage image = await Request.Body.ReadObject<RecipeImage>();
            image = RecipeImageRepository.Store(image);
            int status = image is not null ? 201 : 200;
            return StatusCode(status, image);
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Update method
    /// </summary>
    /// <param name="id">PK</param>
    /// <returns>updated id</returns>
    [HttpPut("{id}")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public async Task<IActionResult> UpdateAsync(int id)
    {
        try
        {
            RecipeImage image = await Request.Body.ReadObject<RecipeImage>();
            image.ID = id;
            return Ok(RecipeImageRepository.Update(image));
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <summary>
    /// Delete method
    /// </summary>
    /// <param name="id">PK</param>
    /// <returns>deleted id</returns>
    [HttpDelete("{id}")]
    [ProducesResponseType(500)]
    [ProducesResponseType(200)]
    [ProducesResponseType(401)]
    public IActionResult Delete(int id)
    {
        try
        {
            RecipeImage image = new RecipeImage { ID = id };
            return Ok(RecipeImageRepository.SoftDelete(image));
        }
        catch (Exception)
        {
            throw;
        }
    }
}
