
use Recipes
GO

create table UserRecipes (
    recipe_id int not null Foreign key references Recipe(id),
    user_id int not null foreign key references Users(id),
    is_creator bit not null default 0,
    created_at datetime not null default getdate(),
    updated_at datetime not null default getdate(),
    constraint PK_UserRecipes primary key (recipe_id, user_id)
)
GO

create or alter TRIGGER CheckRecipeCreator 
On UserRecipes
INSTEAD OF INSERT, UPDATE
AS
BEGIN
    IF exists (select * from inserted) AND NOT EXISTS (SELECT * FROM deleted)
    BEGIN
        IF EXISTS (
            Select 1 from inserted
            where is_creator = 1
            AND EXISTS (SELECT 1 from UserRecipes as ur
                        where ur.recipe_id = inserted.recipe_id
                        AND ur.is_creator = 1 and ur.user_id != inserted.user_id) 
            )
        BEGIN
            RAISERROR('Only one record with is_creator = 1 is allowed per recipe_id.', 10, 1);
            ROLLBACK TRANSACTION;
        END
        ELSE
        BEGIN
            INSERT into UserRecipes (recipe_id, user_id, is_creator)
            SELECT recipe_id, user_id, is_creator
            from inserted
        END
    END
    ELSE
    BEGIN
        declare @recipe_id int = (select recipe_id from inserted)
        DECLARE @user_id int = (select user_id from inserted)

        UPDATE UserRecipes SET
        is_creator = 0
        Where recipe_id = @recipe_id;

        UPDATE UserRecipes SET
        is_creator = 1
        Where recipe_id = @recipe_id and user_id = @user_id;
    END
END
GO

create or alter proc RecipeSearchProc
	@name varchar(255)= null, @author varchar(255)= null, 
	@category varchar(255)= null, @country varchar(255)= null, 
	@region varchar(255)= null, @source varchar(255)= null, @ingredient varchar(255) = null,
	@user int= null, @page int=1, @size int=0,  @count bit=0 , @favorites_only bit=0 as 
BEGIN

IF @page < 1 or @page is null
BEGIN
	set @page = 1
END

IF @size is null OR @size = 0
BEGIN
	set @size = 2147483647
END

set @author = dbo.varchar_pattern_matching(@author)
set @category = dbo.varchar_pattern_matching(@category)
set @country = dbo.varchar_pattern_matching(@country)
set @region = dbo.varchar_pattern_matching(@region)
set @source = dbo.varchar_pattern_matching(@source)

declare @isAdmin bit = isnull(
(Select top 1 user_id 
from UserRoles 
inner join Roles on Roles.id = UserRoles.role_id
Where user_id = @user AND role_id = 1), 0)

IF @count != 0
BEGIN
	IF @isAdmin = 1
	BEGIN
		Select count(distinct recipe.id) as count
		From Recipe    
		inner join Author on Author.id = Recipe.author_id    
		inner join RecipeSource on RecipeSource.id = Recipe.recipe_source_id    
		inner join Category on Category.id = Recipe.category_id    
		inner join Country on Country.id = Recipe.country_id    
		inner join Region on Country.region_id = Region.id    
		inner join RecipeImages on Recipe.id = RecipeImages.recipe_id  
		left join RecipeIngredient on RecipeIngredient.recipe_id = Recipe.id 
		inner join Ingredient on Ingredient.id = RecipeIngredient.ingredient_id
		left  join Recipefavorite on recipe.id = Recipefavorite.recipe_id AND Recipefavorite.user_id = @user
		Where Recipe.deleted_at is null AND RecipeImages.is_display = 1  -- AND UserRecipes.is_creator = 1  
		AND ( (Ingredient.name like @ingredient OR Ingredient.friendly_url like @ingredient) OR @ingredient is null OR @ingredient = '')
		AND (Recipe.name like @name OR Recipe.friendly_url like @name OR @name is null)
		AND (Author.name like @author OR Author.friendly_url like @author OR @author is null)
		AND (Category.name like @category OR Category.friendly_url like @category OR @category is null) 
		AND (Country.name like @country OR Country.friendly_url like @country OR @country is null)
		AND (Region.name like @region OR Region.friendly_url like @region OR @region is null)
		AND (RecipeSource.name like @source OR RecipeSource.friendly_url like @source OR @source is null)
	END
	ELSE
	BEGIN
		Select count(distinct recipe.id) as count		
		From Recipe    
		inner join Author on Author.id = Recipe.author_id    
		inner join RecipeSource on RecipeSource.id = Recipe.recipe_source_id    
		inner join Category on Category.id = Recipe.category_id    
		inner join Country on Country.id = Recipe.country_id    
		inner join Region on Country.region_id = Region.id    
		inner join RecipeImages on Recipe.id = RecipeImages.recipe_id  
		left join RecipeIngredient on RecipeIngredient.recipe_id = Recipe.id 
		inner join Ingredient on Ingredient.id = RecipeIngredient.ingredient_id
		left join UserRecipes on UserRecipes.recipe_id = recipe.id AND (UserRecipes.user_id = @user OR UserRecipes.user_id is null)     
		inner join Users on UserRecipes.user_id = Users.id  
		Where Recipe.deleted_at is null AND RecipeImages.is_display = 1  -- AND UserRecipes.is_creator = 1  
		AND (UserRecipes.user_id = @user)
		AND ( (Ingredient.name like @ingredient OR Ingredient.friendly_url like @ingredient) OR @ingredient is null)
		AND (Recipe.name like @name OR Recipe.friendly_url like @name OR @name is null)
		AND (Author.name like  @author OR Author.friendly_url like @author OR @author is null)
		AND (Category.name like @category OR Category.friendly_url like @category OR @category is null) 
		AND (Country.name like @country OR Country.friendly_url like @country OR @country is null)
		AND (Region.name like @region OR Region.friendly_url like @region OR @region is null)
		AND (RecipeSource.name like @source OR RecipeSource.friendly_url like @source OR @source is null)
	END
END
ELSE
BEGIN
	IF @isAdmin = 1
		BEGIN
			Select 
			Distinct 
			Recipe.id recipe_id, Recipe.name recipe_name,     
			Recipe.friendly_url recipe_friendly_url, Recipe.recipe_text,
			Category.id as category_id,	Category.name as category_name,	Category.friendly_url as category_url,     
			RecipeSource.id as recipe_source_id, RecipeSource.name as recipe_source_name, RecipeSource.friendly_url as recipe_source_friendly_url,     
			Author.id as author_id,	Author.name as author_name, Author.friendly_url as author_url,     
			Recipe.country_id, Country.name as country_name, Country.friendly_url as country_url, 
			Country.region_id, Region.name as region_name, Region.friendly_url as region_friendly_url, 
			Recipe.created_at, Recipe.updated_at, Recipe.deleted_at, RecipeImages.id as image_id, 
			ISNULL(Recipefavorite.user_id,0) as recipe_favorite_id,  
			RecipeImages.image, RecipeImages.image_base_64, RecipeImages.content_type, RecipeImages.is_display
			--count(distinct recipe.id)
			From Recipe    
			inner join Author on Author.id = Recipe.author_id    
			inner join RecipeSource on RecipeSource.id = Recipe.recipe_source_id    
			inner join Category on Category.id = Recipe.category_id    
			inner join Country on Country.id = Recipe.country_id    
			inner join Region on Country.region_id = Region.id    
			inner join RecipeImages on Recipe.id = RecipeImages.recipe_id  
			left join RecipeIngredient on RecipeIngredient.recipe_id = Recipe.id 
			inner join Ingredient on Ingredient.id = RecipeIngredient.ingredient_id
			left  join Recipefavorite on recipe.id = Recipefavorite.recipe_id AND Recipefavorite.user_id = @user
			Where Recipe.deleted_at is null AND RecipeImages.is_display = 1  -- AND UserRecipes.is_creator = 1  
			AND ( (Ingredient.name like @ingredient OR Ingredient.friendly_url like @ingredient) OR @ingredient is null OR @ingredient = '')
			AND (Recipe.name like @name OR Recipe.friendly_url like @name OR @name is null)
			AND (Author.name like @author OR Author.friendly_url like @author OR @author is null)
			AND (Category.name like @category OR Category.friendly_url like @category OR @category is null) 
			AND (Country.name like @country OR Country.friendly_url like @country OR @country is null)
			AND (Region.name like @region OR Region.friendly_url like @region OR @region is null)
			AND (RecipeSource.name like @source OR RecipeSource.friendly_url like @source OR @source is null)
			AND (@favorites_only = 1 AND Recipefavorite.recipe_id = recipe.id OR @favorites_only = 0)
			order by Recipe.id 
			OFFSET (@page - 1) * @size ROWS FETCH NEXT @size ROWS ONLY
		END
	ELSE
		BEGIN
			Select 
			Distinct 
			Recipe.id recipe_id, Recipe.name recipe_name,     
			Recipe.friendly_url recipe_friendly_url, Recipe.recipe_text,
			Category.id as category_id,	Category.name as category_name,	Category.friendly_url as category_url,     
			RecipeSource.id as recipe_source_id, RecipeSource.name as recipe_source_name, RecipeSource.friendly_url as recipe_source_friendly_url,     
			Author.id as author_id,	Author.name as author_name, Author.friendly_url as author_url,     
			Recipe.country_id, Country.name as country_name, Country.friendly_url as country_url, 
			Country.region_id, Region.name as region_name, Region.friendly_url as region_friendly_url, 
			Recipe.created_at, Recipe.updated_at, Recipe.deleted_at, RecipeImages.id as image_id, 
			UserRecipes.is_creator,	UserRecipes.user_id, Users.username,  
			ISNULL(Recipefavorite.recipe_id,0) as recipe_favorite_id,  
			RecipeImages.image, RecipeImages.image_base_64, RecipeImages.content_type, RecipeImages.is_display 
			-- count(distinct recipe.id)		
			From Recipe    
			inner join Author on Author.id = Recipe.author_id    
			inner join RecipeSource on RecipeSource.id = Recipe.recipe_source_id    
			inner join Category on Category.id = Recipe.category_id    
			inner join Country on Country.id = Recipe.country_id    
			inner join Region on Country.region_id = Region.id    
			inner join RecipeImages on Recipe.id = RecipeImages.recipe_id  
			left join RecipeIngredient on RecipeIngredient.recipe_id = Recipe.id 
			inner join Ingredient on Ingredient.id = RecipeIngredient.ingredient_id
			left join UserRecipes on UserRecipes.recipe_id = recipe.id AND (UserRecipes.user_id = @user OR UserRecipes.user_id is null)     
			inner join Users on UserRecipes.user_id = Users.id  
			left  join Recipefavorite on recipe.id = Recipefavorite.recipe_id AND Recipefavorite.user_id = @user
			Where Recipe.deleted_at is null AND RecipeImages.is_display = 1  -- AND UserRecipes.is_creator = 1  
			AND (UserRecipes.user_id = @user)
			AND ( (Ingredient.name like @ingredient OR Ingredient.friendly_url like @ingredient) OR @ingredient is null)
			AND (Recipe.name like @name OR Recipe.friendly_url like @name OR @name is null)
			AND (Author.name like  @author OR Author.friendly_url like @author OR @author is null)
			AND (Category.name like @category OR Category.friendly_url like @category OR @category is null) 
			AND (Country.name like @country OR Country.friendly_url like @country OR @country is null)
			AND (Region.name like @region OR Region.friendly_url like @region OR @region is null)
			AND (RecipeSource.name like @source OR RecipeSource.friendly_url like @source OR @source is null)
			AND (@favorites_only = 1 AND Recipefavorite.recipe_id = recipe.id OR @favorites_only = 0)
			order by Recipe.id 
			OFFSET (@page - 1) ROWS FETCH NEXT @size ROWS ONLY
		END
END
END
