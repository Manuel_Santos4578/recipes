﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Util;

namespace RecipesLibrary.Util;

public class ResilientApiCaller : ApiClientV3
{
    private int _retry_count;
    public ResilientApiCaller(int retry_count) : base() { _retry_count = retry_count; }
    public ResilientApiCaller(string apiUrl, int retry_count) : base(apiUrl) { _retry_count = retry_count; }
    public ResilientApiCaller(string apiUrl, string token, int retry_count) : base(apiUrl, token) { _retry_count = retry_count; }
    public ResilientApiCaller(string apiUrl, Dictionary<string, string> headers, int retry_count) : base(apiUrl, headers) { _retry_count = retry_count; }

    public new async Task<T> GetAsync<T>(string uri)
    {
        _retry_count = _retry_count <= 0 ? 1 : _retry_count;
    GetCall:
        try
        {
            return await base.GetAsync<T>(uri);
        }
        catch (Exception)
        {
            if (_retry_count > 0)
            {
                _retry_count--;
                await Task.Delay(1000);
                goto GetCall;
            }
            throw;
        }
    }

    public new async Task<T> PostAsync<T>(string uri, StringContent content)
    {
        _retry_count = _retry_count <= 0 ? 1 : _retry_count;
    PostCall:
        try
        {
            return await base.PostAsync<T>(uri, content);
        }
        catch (Exception)
        {
            if (_retry_count > 0)
            {
                _retry_count--;
                await Task.Delay(1000);
                goto PostCall;
            }
            throw;
        }
    }

    public new async Task<T> PutAsync<T>(string uri, StringContent content)
    {
        _retry_count = _retry_count <= 0 ? 1 : _retry_count;
    PutCall:
        try
        {
            return await base.PutAsync<T>(uri, content);
        }
        catch (Exception)
        {
            if (_retry_count > 0)
            {
                _retry_count--;
                await Task.Delay(1000);
                goto PutCall;
            }
            throw;
        }
    }

    public new async Task<T> DeleteAsync<T>(string uri)
    {
        _retry_count = _retry_count <= 0 ? 1 : _retry_count;
    DeleteCall:
        try
        {
            return await base.DeleteAsync<T>(uri);
        }
        catch (Exception)
        {
            if (_retry_count > 0)
            {
                _retry_count--;
                await Task.Delay(1000);
                goto DeleteCall;
            }
            throw;
        }
    }
}
