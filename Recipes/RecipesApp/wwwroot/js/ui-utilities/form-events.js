function AddTextBoxEvent(elementId) {
    document.getElementById(elementId).addEventListener('input', debounce(() => {
        document.getElementById('page').value = 1;
        LoadData();
    }))
}

function debounce(func, timeout = 400) {
    let timer;
    return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => { func.apply(this, args); }, timeout);
    };
}
