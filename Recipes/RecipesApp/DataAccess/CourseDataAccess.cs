﻿using DataContracts.Entities;
using Microsoft.AspNetCore.WebUtilities;
using ModelsLibrary.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Util;

namespace RecipesApp.DataAccess;

public sealed partial class DataAccess
{
    #region Course
    public async Task<IEnumerable<ICourse>> SelectAllCoursesAsync(string name="", int page = 1, int size = 0)
    {
        Dictionary<string, string> filters = new Dictionary<string, string>
        {
            {"page",page.ToString() },
            {"size",size.ToString() }
        };
        if (!string.IsNullOrWhiteSpace(name)) filters.Add("name", name);
        Uri request_uri = new Uri(QueryHelpers.AddQueryString(coursesEndpoint, filters), UriKind.Relative);
        return await new ApiClientV3(ApiDomain, ApiToken).GetAsync<IEnumerable<Course>>(request_uri.ToString());
    }

    public async Task<int> SelectCoursesCountAsync(string name)
    {
        Dictionary<string, string> filters = new Dictionary<string, string>
        {
            {"count",true.ToString() }
        };
        if (!string.IsNullOrWhiteSpace(name)) filters.Add("name", name);
        Uri request_uri = new Uri(QueryHelpers.AddQueryString(coursesEndpoint, filters), UriKind.Relative);
        JToken apiData = await new ApiClientV3(ApiDomain, ApiToken).GetAsync<JToken>(request_uri.ToString());
        return apiData.Value<int>("count");
    }

    public async Task<ICourse> SelectCourseBySlugAsync(string slug)
    {
        return await new ApiClientV3(ApiDomain, ApiToken).GetAsync<Course>($"{coursesEndpoint}/{slug}");
    }

    public async Task<ICourse> InsertCourseAsync(ICourse course)
    {
        string uri = coursesEndpoint;
        using var content = new StringContent(JsonConvert.SerializeObject(course));
        course = await new ApiClientV3(ApiDomain, ApiToken).PostAsync<Course>(uri, content);
        return course;
    }
    public async Task InsertCoursesAsync(IEnumerable<Course> courses)
    {
        StringContent content = new StringContent(JsonConvert.SerializeObject(courses));
        await new ApiClientV3(ApiDomain, ApiToken).PostAsync<IList<Course>>($"{coursesEndpoint}/bulk", content);
    }
    public async Task<ICourse> UpdateCourseAsync(ICourse course)
    {
        using var content = new StringContent(JsonConvert.SerializeObject(course));
        course = await new ApiClientV3(ApiDomain, ApiToken).PutAsync<Course>($"{coursesEndpoint}/{course.ID}", content);
        return course;
    }

    public async Task<bool> DeleteCourseAsync(ICourse course)
    {
        course = await new ApiClientV3(ApiDomain, ApiToken).DeleteAsync<Course>($"{coursesEndpoint}/{course.ID}");
        return course is not null;
    }
    #endregion
}
