﻿using Newtonsoft.Json;
using ModelsLibrary.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Util;
using System;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json.Linq;
using DataContracts.Access;
using DataContracts.Entities;

namespace RecipesApp.DataAccess;

public sealed partial class DataAccess: IRegionDataAccess
{
    #region Regions
    public async Task<IEnumerable<IRegion>> SelectAllRegionsAsync(string name = "", int page = 0, int page_size = 0)
    {
        Dictionary<string, string> query = new Dictionary<string, string>
        {
            {"page",page.ToString() },
            {"size",page_size.ToString() },
        };
        if (!string.IsNullOrWhiteSpace(name)) query.Add("name", name);
        Uri uri = new Uri(QueryHelpers.AddQueryString(regionsEndpoint, query), UriKind.Relative);
        return await new ApiClientV3(ApiDomain, ApiToken).GetAsync<IEnumerable<Region>>(uri.ToString());
    }
    public async Task<int> SelectRegionCountAsync(string name)
    {
        Dictionary<string, string> query = new Dictionary<string, string> { { "count", true.ToString() } };
        if (!string.IsNullOrWhiteSpace(name)) query.Add("name", name);
        Uri uri = new Uri(QueryHelpers.AddQueryString(regionsEndpoint, query), UriKind.Relative);
        JToken apiData = await new ApiClientV3(ApiDomain, ApiToken).GetAsync<JToken>(uri.ToString());
        return apiData.Value<int>("count");
    }
    public async Task<IRegion> SelectRegionBySlugAsync(string slug)
    {
        return await new ApiClientV3(ApiDomain, ApiToken).GetAsync<Region>($"{regionsEndpoint}/" + slug);
    }
    public async Task<IRegion> InsertRegionAsync(IRegion region)
    {
        using StringContent content = new StringContent(JsonConvert.SerializeObject(region));
        region = await new ApiClientV3(ApiDomain, ApiToken).PostAsync<Region>(regionsEndpoint, content);
        return region;
    }
    public async Task<IEnumerable<IRegion>> InsertRegionsAsync(IEnumerable<Region> regions)
    {
        using StringContent content = new StringContent(JsonConvert.SerializeObject(regions));
        regions = await new ApiClientV3(ApiDomain, ApiToken).PostAsync<IEnumerable<Region>>($"{regionsEndpoint}/bulk", content);
        return regions;
    }
    public async Task<IRegion> UpdateRegionAsync(IRegion region)
    {
        using StringContent content = new StringContent(JsonConvert.SerializeObject(region));
        region = await new ApiClientV3(ApiDomain, ApiToken).PutAsync<Region>($"{regionsEndpoint}/{region.ID}", content);
        return region;
    }
    public async Task<bool> DeleteRegionAsync(IRegion region)
    {
        region = await new ApiClientV3(ApiDomain, ApiToken).DeleteAsync<Region>($"{regionsEndpoint}/" + region.ID);
        return region is not null;
    }
    #endregion
}
