﻿using Newtonsoft.Json;
using ModelsLibrary.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Util;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.WebUtilities;
using DataContracts.Access;
using DataContracts.Entities;

namespace RecipesApp.DataAccess;

public sealed partial class DataAccess
{
    #region Author
    public async Task<IEnumerable<IAuthor>> SelectAllAuthorsAsync(int page = 1, int page_size = 0, string name = "")
    {
        Dictionary<string, string> query = new Dictionary<string, string>
        {
            {"page",page.ToString() },
            {"size",page_size.ToString() },
        };
        if (!string.IsNullOrWhiteSpace(name)) query.Add("name", name);
        Uri uri = new Uri(QueryHelpers.AddQueryString(authorsEndpoint, query), UriKind.Relative);
        return await new ApiClientV3(ApiDomain, ApiToken).GetAsync<List<Author>>(uri.ToString());
    }

    public async Task<int> SelectAuthorCountAsync(string name = "")
    {
        Dictionary<string, string> query = new Dictionary<string, string>
        {
            { "count", true.ToString() }
        };
        if (!string.IsNullOrWhiteSpace(name)) query.Add("name", name);
        Uri uri = new Uri(QueryHelpers.AddQueryString(authorsEndpoint, query), UriKind.Relative);
        JToken data = await new ApiClientV3(ApiDomain, ApiToken).GetAsync<JToken>(uri.ToString());
        return data.Value<int>("count");
    }
    public async Task<IAuthor> SelectAuthorBySlugAsync(string slug)
    {
        return await new ApiClientV3(ApiDomain, ApiToken).GetAsync<Author>($"{authorsEndpoint}/" + slug);
    }
    public async Task<IAuthor> InsertAuthorAsync(IAuthor author)
    {
        try
        {
            using StringContent content = new StringContent(JsonConvert.SerializeObject(author));
            author = await new ApiClientV3(ApiDomain, ApiToken).PostAsync<Author>(authorsEndpoint, content);
            return author;
        }
        catch (Exception)
        {
        }
        return null;
    }

    public async Task InsertAuthorsAsync(IEnumerable<IAuthor> authors)
    {
        StringContent content = new StringContent(JsonConvert.SerializeObject(authors));
        await new ApiClientV3(ApiDomain, ApiToken).PostAsync<IList<Author>>($"{authorsEndpoint}/bulk", content);
    }

    public async Task<IAuthor> UpdateAuthorAsync(IAuthor author)
    {
        try
        {
            using StringContent content = new StringContent(JsonConvert.SerializeObject(author));
            author = await new ApiClientV3(ApiDomain, ApiToken).PutAsync<Author>($"{authorsEndpoint}/" + author.ID, content);
            return author;
        }
        catch (Exception)
        {
        }
        return null;
    }
    public async Task<bool> DeleteAuthorAsync(IAuthor author)
    {
        try
        {
            author = await new ApiClientV3(ApiDomain, ApiToken).DeleteAsync<Author>($"{authorsEndpoint}/" + author.ID);
            return author is not null;
        }
        catch (Exception)
        {
        }
        return false;
    }
    #endregion
}
