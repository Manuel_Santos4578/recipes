﻿namespace DataContracts.Entities;

public interface IAuthor : ISimple
{
    int ID { get; set; }
    string Name { get; set; }
    string FriendlyUrl { get; set; }
}
