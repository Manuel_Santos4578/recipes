﻿using DataContracts.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System;

namespace ModelsLibrary.Models;

public class Source : Simple, ISource
{
    [JsonProperty("recipesource_id")]
    public int ID { get; set; }
    [JsonProperty("recipesource_name")]
    public string Name { get; set; }
    [JsonProperty("recipesource_friendly_url")]
    public string FriendlyUrl { get; set; }

    public static DataTable ExportToDataTable(IEnumerable<ISource> sources)
    {
        DataTable table = new DataTable();
        table.Columns.AddRange(new DataColumn[]
        {
            new DataColumn(nameof(ISource.Name), typeof(string)),
            new DataColumn(nameof(ISource.CreatedAt), typeof(DateTime)),
            new DataColumn(nameof(ISource.UpdatedAt), typeof(DateTime)),
        });

        foreach (ISource source in sources)
        {
            table.Rows.Add(
                source.Name,
                source.CreatedAt,
                source.UpdatedAt);
        }
        return table;
    }
}
