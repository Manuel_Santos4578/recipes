﻿namespace DataContracts.Entities;

public interface ICourse : ISimple
{
    int ID { get; set; }
    string Name { get; set; }
    string FriendlyUrl { get; set; }
}
