﻿using DataContracts.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System;

namespace ModelsLibrary.Models;

public class Course : Simple, ICourse
{
    [JsonProperty("course_id")]
    public int ID { get; set; }
    [JsonProperty("course_name")]
    public string Name { get; set; }
    [JsonProperty("course_friendly_url")]
    public string FriendlyUrl { get; set; }

    public static DataTable ExportToDataTable(IEnumerable<ICourse> courses)
    {
        DataTable table = new DataTable();
        table.Columns.AddRange(new DataColumn[]
        {
            new DataColumn(nameof(ICourse.Name), typeof(string)),
            new DataColumn(nameof(ICourse.CreatedAt), typeof(DateTime)),
            new DataColumn(nameof(ICourse.UpdatedAt), typeof(DateTime)),
        });

        foreach (ICourse course in courses)
        {
            table.Rows.Add(
                course.Name,
                course.CreatedAt,
                course.UpdatedAt);
        }
        return table;
    }
}
