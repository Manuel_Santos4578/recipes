﻿using DataContracts.Entities;

namespace DataContracts.Access;

public partial interface ICategoryDataAccess
{
    Task<bool> DeleteCategoryAsync(ICategory category);
    Task<ICategory> InsertCategoryAsync(ICategory category);
    Task<IEnumerable<ICategory>> SelectAllCategoriesAsync(int page = 0, int page_size = 0, string name = "", int? base_category = null);
    Task<int> SelectCategoryCountAsync(string name = "", int? base_category = null);
    Task<ICategory> UpdateCategoryAsync(ICategory category);
    Task<ICategory> SelectCategoryBySlugAsync(string slug);
}
