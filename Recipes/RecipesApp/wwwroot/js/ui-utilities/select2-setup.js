async function Select2Setup(element, url, processResults, parentElement) {
    let culture = await (await fetch(`${location.origin}/cultures/check`)).text();
    let options = {
        width: '100%',
        minimumInputLength: 3,
        language: culture,
        placeholder: null,
        allowClear: true,
        ajax: {
            url: url,
            dataType: 'json',
            type: 'POST',
            delay: 300,
            contentType: "application/json; charset=utf-8",
            data: function (term, page) {
                return JSON.stringify({ name: term.term, });
            },
            processResults: function (data) {
                let opt = $.map(data[processResults], function (obj) {
                    return {
                        id: obj.id,
                        text: obj.name,
                        created: obj.created_at
                    }
                });
                return { results: opt }
            },
        },
    };
    if (parentElement) {
        options.dropdownParent = parentElement
    }
    element.select2(options);
}