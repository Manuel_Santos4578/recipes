﻿using Microsoft.Data.SqlClient;
using Recipes.Database.Config;
using RecipesApi.ApiAuth;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Util;

namespace RecipesApi.Repositories;

public static class ApiUserRepository
{
    public static ApiUser ReadByUsernameAndPassword(string username, string encryptPW)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query = "SELECT * FROM ApiUserView Where username = @username AND password = @password";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@username",SqlDbType.VarChar, username.ToLower()),
            executor.GenerateParameter("@password",SqlDbType.VarChar, encryptPW)
        };
        return executor.LoadData<List<ApiUser>>(query, parameters).FirstOrDefault();
    }


    public static ApiUser Store(ApiUser api_user, int role_id)
    {
        IQueryExecutor executor = new QueryExecutor(DB.Instance);
        string query =
        @"
            INSERT INTO ApiUser (username,password,role_id)
            output inserted.username
            VALUES (@username, @password, @role_id)
        ";
        List<SqlParameter> parameters = new List<SqlParameter>
        {
            executor.GenerateParameter("@username", SqlDbType.VarChar, api_user.Username),
            executor.GenerateParameter("@password", SqlDbType.VarChar, api_user.Password),
            executor.GenerateParameter("@role_id",SqlDbType.Int, role_id)
        };
        return executor.LoadData<List<ApiUser>>(query, parameters).FirstOrDefault();
    }

}
