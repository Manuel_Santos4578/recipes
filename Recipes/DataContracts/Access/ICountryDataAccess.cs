﻿using DataContracts.Entities;

namespace DataContracts.Access;

public partial interface ICountryDataAccess
{
    Task<bool> DeleteCountryAsync(ICountry country);
    Task<ICountry> InsertCountryAsync(ICountry country);
    Task<IEnumerable<ICountry>> SelectAllCountriesAsync(int page = 0, int page_size = 0, IRegion? region = null, string name = "");
    Task<ICountry> SelectCountryBySlugAsync(string slug);
    Task<int> SelectCountryCountAsync(IRegion? region = null, string name = "");
    Task<ICountry> UpdateCountryAsync(ICountry country);
}