using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ModelsLibrary.Constants;
using RecipesApi.Swagger;
using Serilog;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace RecipesApi;

public class Startup
{

    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddCors(options =>
        {
            options.AddPolicy(name: AppConstants.Policies.PERMISSIVE, builder =>
            {
                builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();
            });
        });
        services.AddControllers().AddNewtonsoftJson(action =>
        {
            action.SerializerSettings.DateFormatString = "dd/MM/yyyy HH:mm:ss";
        });
        services.AddResponseCompression();
        services.AddDatabase();
        services.AddSwagger();
        services.AddJwtAuthentication();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }
        else
        {
            app.UseExceptionHandler("/error");
        }

        app.UseHttpsRedirection();
        app.UseSerilogRequestLogging();

        app.UseRouting();
        app.UseCors(AppConstants.Policies.PERMISSIVE);
        app.UseAuthentication();
        app.UseAuthorization();
        app.UseResponseCompression();
        var swaggerOptions = new SwaggerOptions();
        Configuration.GetSection(nameof(SwaggerOptions)).Bind(swaggerOptions);
        app.UseSwagger(options =>
        {
            options.RouteTemplate = swaggerOptions.JsonRoute;
        });
        app.UseSwaggerUI(options =>
        {
            options.DocumentTitle = "Recipes API";
            options.DocExpansion(DocExpansion.None);
            options.SwaggerEndpoint(swaggerOptions.UIEndpoint, swaggerOptions.Description);
        });

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    }
}
