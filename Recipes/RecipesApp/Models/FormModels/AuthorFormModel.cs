﻿using DataContracts.Entities;
using ModelsLibrary.Models;

namespace RecipesApp.Models.FormModels;

public sealed class AuthorFormModel: Author
{
    public AuthorFormModel()
    {

    }
    public AuthorFormModel(IAuthor author)
    {
        ID = author.ID;
        Name = author.Name;
        FriendlyUrl = author.FriendlyUrl;
        CreatedAt = author.CreatedAt;
        UpdatedAt = author.UpdatedAt;
        DeletedAt = author.DeletedAt;
    }
}
