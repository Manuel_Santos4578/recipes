﻿using Microsoft.AspNetCore.Http;
using RecipesApp.Middleware;

namespace RecipesApp.Models.FormModels;

public class ImportModel
{
    [AllowedExtensions(new string[] { ".csv", ".xlsx" })]
    public IFormFile File { get; set; }
}
