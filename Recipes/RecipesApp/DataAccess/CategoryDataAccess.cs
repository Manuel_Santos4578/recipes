﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ModelsLibrary.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Util;
using Microsoft.AspNetCore.WebUtilities;
using DataContracts.Entities;
using DataContracts.Access;

namespace RecipesApp.DataAccess;

public sealed partial class DataAccess
{
    #region Category
    public async Task<IEnumerable<ICategory>> SelectAllCategoriesAsync(int page = 0, int page_size = 0, string name = "", int? base_category = null)
    {
        Dictionary<string, string> query = new Dictionary<string, string>
        {
            {"name", name },
            {"page",page.ToString() },
            {"size",page_size.ToString() },
            {"base_category",base_category.ToString() }
        };
        Uri request_uri = new Uri(QueryHelpers.AddQueryString(categoriesEndpoint, query), UriKind.Relative);
        return await new ApiClientV3(ApiDomain, ApiToken).GetAsync<IEnumerable<Category>>(request_uri.ToString());
    }

    public async Task<int> SelectCategoryCountAsync(string name = "", int? base_category = null)
    {
        Dictionary<string, string> query = new Dictionary<string, string>
        {
            {"name",name },
            {"base_category", base_category.ToString() },
            {"count", true.ToString() }
        };
        Uri uri = new Uri(QueryHelpers.AddQueryString(categoriesEndpoint, query), UriKind.Relative);
        JToken data = await new ApiClientV3(ApiDomain, ApiToken).GetAsync<JToken>(uri.ToString());
        return data.Value<int>("count");
    }
    public async Task<ICategory> SelectCategoryBySlugAsync(string slug)
    {
        ICategory category = await new ApiClientV3(ApiDomain, ApiToken).GetAsync<Category>($"{categoriesEndpoint}/{slug}");
        return category;
    }
    public async Task<ICategory> InsertCategoryAsync(ICategory category)
    {
        using StringContent content = new StringContent(JsonConvert.SerializeObject(category));
        category = await new ApiClientV3(ApiDomain, ApiToken).PostAsync<Category>(categoriesEndpoint, content);
        return category;
    }
    public async Task InsertCategoriesAsync(IEnumerable<ICategory> categories)
    {
        StringContent content = new StringContent(JsonConvert.SerializeObject(categories));
        await new ApiClientV3(ApiDomain, ApiToken).PostAsync<IEnumerable<Category>>($"{categoriesEndpoint}/bulk", content);
    }
    public async Task<ICategory> UpdateCategoryAsync(ICategory category)
    {
        using StringContent content = new StringContent(JsonConvert.SerializeObject(category));
        category = await new ApiClientV3(ApiDomain, ApiToken).PutAsync<Category>($"{categoriesEndpoint}/" + category.ID, content);
        return category;
    }
    public async Task<bool> DeleteCategoryAsync(ICategory category)
    {
        category = await new ApiClientV3(ApiDomain, ApiToken).DeleteAsync<Category>($"{categoriesEndpoint}/" + category.ID);
        return category is not null;
    }
    #endregion
}
