﻿using Svg;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Util;

/// <summary>
/// Class for converting images
/// </summary>
public static class ImageConverter
{
    /// <summary>
    /// Converts SVG -> PNG
    /// </summary>
    /// <param name="path">file path</param>
    /// <param name="height"></param>
    /// <param name="with"></param>
    public static void ConvertSVGToPng(string path, int height,int with)
    {
        var svg = SvgDocument.Open(path);
        using var smallBitmap = svg.Draw();
        {
            using var bitmap = svg.Draw(with,height);
            {
                if (!File.Exists(path.Replace(".svg", "").Replace(".SVG", "") + ".png"))
                {
                    bitmap.Save(path.Replace(".svg", "").Replace(".SVG", "") + ".png", ImageFormat.Png);
                }
                path = path.Replace(".svg", "").Replace(".SVG", "") + ".png";
            }
        }
    }
    /// <summary>
    /// Converts JPEG -> PNG
    /// </summary>
    /// <param name="path">file path</param>
    public static void ConvertJpegToPng(string path)
    {
        using Bitmap bitMap = (Bitmap)Bitmap.FromFile(path);
        {
            using var stream = new MemoryStream();
            {
                if (!File.Exists(path.Replace(".jpeg", "").Replace(".JPEG", "") + ".png"))
                {

                    bitMap.Save(path.Replace(".jpeg", "").Replace(".JPEG", "") + ".png", ImageFormat.Png);
                }
                path = path.Replace(".jpeg", "").Replace(".JPEG", "") + ".png";
            }
        }
    }
}
