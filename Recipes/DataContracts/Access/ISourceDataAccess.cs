﻿using DataContracts.Entities;

namespace DataContracts.Access;

public partial interface ISourceDataAccess
{
    Task<bool> DeleteSourceAsync(ISource source);
    Task<ISource> InsertSourceAsync(ISource source);
    Task<IEnumerable<ISource>> SelectAllRecipeSourceAsync(string name = "", int page = 1, int size = 0);
    Task<int> SelectRecipeSourceCountAsync(string name = "");
    Task<ISource> SelectSourceBySlugAsync(string slug);
    Task<ISource> UpdateSourceAsync(ISource source);
}