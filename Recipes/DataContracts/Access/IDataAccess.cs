﻿using System.Collections;

namespace DataContracts.Access;

public partial interface IDataAccess: 
    IAuthorDataAccess, 
    ICategoryDataAccess, 
    ICountryDataAccess, 
    ICourseDataAccess, 
    IFavoriteDataAccess, 
    ILocalizationDataAccess,
    IIngredientDataAccess,
    IRecipeDataAccess,
    IRegionDataAccess,
    ISourceDataAccess, 
    IUserDataAccess
{
    Task ApiAuthenticateAsync();
    Task<ArrayList> BulkSave(ArrayList entities);
}