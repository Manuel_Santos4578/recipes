﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;

namespace RecipesApp.Services;

public class ExcelExportService
{
    public delegate DataTable GenerateDataTableDelegate<TEntity>(IEnumerable<TEntity> objects) where TEntity : class;
    public static void Export(string file_path, DataTable data_table)
    {
        using SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Create(file_path, SpreadsheetDocumentType.Workbook);

        WorkbookPart workbookPart = spreadsheetDocument.AddWorkbookPart();
        workbookPart.Workbook = new Workbook();

        WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
        var worksheet = new Worksheet(new SheetData());
        worksheetPart.Worksheet = worksheet;

        var sheets = spreadsheetDocument.WorkbookPart.Workbook.AppendChild(new Sheets());
        var sheet = new Sheet()
        {
            Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(worksheetPart),
            SheetId = 1,
            Name = "Sheet1"
        };
        sheets.Append(sheet);

        SheetData sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();

        Row headerRow = new Row();
        foreach (DataColumn column in data_table.Columns)
        {
            var cell = new Cell
            {
                DataType = CellValues.String,
                CellValue = new CellValue(column.ColumnName)
            };
            headerRow.AppendChild(cell);
        }
        sheetData.AppendChild(headerRow);

        foreach (DataRow row in data_table.Rows)
        {
            Row dataRow = new Row();
            foreach (object cellValue in row.ItemArray)
            {
                Cell cell;
                if (cellValue is DateTime)
                {
                    cell = new Cell
                    {
                        DataType = CellValues.String,
                        CellValue = new CellValue(((DateTime)cellValue).ToString("dd/MM/yyyyy HH:mm"))
                    };
                }
                else
                {
                    cell = new Cell
                    {
                        DataType = CellValues.String,
                        CellValue = new CellValue(cellValue.ToString())
                    };
                }
                dataRow.AppendChild(cell);
            }
            sheetData.AppendChild(dataRow);
        }

        workbookPart.Workbook.Save();

        spreadsheetDocument.Close();
    }
}
