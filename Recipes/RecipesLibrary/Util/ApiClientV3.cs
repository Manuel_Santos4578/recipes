﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Util
{
    /// <summary>
    /// Class for making and processing http requests, primary use: http requests to web apis
    /// </summary>
    public class ApiClientV3 : HttpClient
    {
        /// <summary>
        /// domain for http requests
        /// </summary>
        readonly string _apiUrl;
        /// <summary>
        /// Initialize object
        /// </summary>
        public ApiClientV3() : base()
        {
            _apiUrl = "";
        }
        /// <summary>
        /// Initialize object with domain
        /// </summary>
        /// <param name="apiUrl">api domain or ip address</param>
        public ApiClientV3(string apiUrl) : this()
        {
            _apiUrl = apiUrl;
        }
        /// <summary>
        /// Initialize object with domain and autentication bearer token
        /// </summary>
        /// <param name="apiUrl">api domain or ip address</param>
        /// <param name="token">authentication token</param>
        public ApiClientV3(string apiUrl, string token) : this(apiUrl)
        {
            DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
        }
        /// <summary>
        /// Initialize object with doman and header collection
        /// </summary>
        /// <param name="apiUrl">api domain or ip address</param>
        /// <param name="headers">header collection</param>
        public ApiClientV3(string apiUrl, Dictionary<string, string> headers) : this(apiUrl)
        {
            foreach (var header in headers)
            {
                DefaultRequestHeaders.Add(header.Key, header.Value);
            }
        }
        /// <summary>
        /// Makes an HTTP GET request async
        /// </summary>
        /// <param name="uri">request uri</param>
        /// <returns>response content string</returns>
        public async Task<T> GetAsync<T>(string uri)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, _apiUrl + uri);
            return await ProcessRequest<T>(request);
        }
        /// <summary>
        /// Makes an HTTP POST request async
        /// </summary>
        /// <param name="uri">request uri</param>
        /// <param name="content">request body</param>
        /// <returns>response content string</returns>
        public async Task<T> PostAsync<T>(string uri, StringContent content)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, _apiUrl + uri)
            {
                Content = content
            };
            return await ProcessRequest<T>(request);
        }
        /// <summary>
        /// Makes an HTTP PUT request async
        /// </summary>
        /// <param name="uri">request uri</param>
        /// <param name="content">request body</param>
        /// <returns>response content string</returns>
        public async Task<T> PutAsync<T>(string uri, StringContent content)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Put, _apiUrl + uri)
            {
                Content = content
            };
            return await ProcessRequest<T>(request);
        }
        /// <summary>
        /// Makes an HTTP DELETE request async
        /// </summary>
        /// <param name="uri">request uri</param>
        /// <returns>response content string</returns>
        public async Task<T> DeleteAsync<T>(string uri)
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Delete, _apiUrl + uri);
            return await ProcessRequest<T>(request);
        }
        #region Helper Methods
        private async Task<T> ProcessRequest<T>(HttpRequestMessage request)
        {
            HttpResponseMessage response = await SendAsync(request);
            return await DeserializeResponse<T>(response);
        }
        private static async Task<T> DeserializeResponse<T>(HttpResponseMessage response)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings { DateFormatString = "dd/MM/yyyy HH:mm:ss", };
            string response_body = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(response_body, settings);
        }
        #endregion
    }
}
