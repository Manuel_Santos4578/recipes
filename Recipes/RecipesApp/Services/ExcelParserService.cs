﻿using DataContracts.Access;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.AspNetCore.Http;
using ModelsLibrary.Constants;
using ModelsLibrary.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace RecipesApp.Services;

public class ExcelParserService
{
    private delegate T excelToObjectMapping<T>(Row row);
    private SharedStringTablePart _stringTable;

    public ExcelParserService() { }

    public void ReadExcel(IFormFile file)
    {
        using SpreadsheetDocument document = SpreadsheetDocument.Open(file.OpenReadStream(), false);
        WorkbookPart workbookPart = document.WorkbookPart;

        foreach (Sheet sheet in workbookPart.Workbook.Descendants<Sheet>())
        {
            string name = sheet.Name.Value;
            WorksheetPart worksheetPart = (WorksheetPart)workbookPart.GetPartById(sheet.Id);
            SheetData sheetData = worksheetPart.Worksheet.Elements<SheetData>().FirstOrDefault();
            IEnumerable<Row> rows = sheetData.Elements<Row>();
            _stringTable = workbookPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();
            int count = rows.Count();

            ArrayList entities = new ArrayList();

            for (int i = 1; i < count; i++)
            {
                Row row = rows.ElementAt(i);
                object entity = name switch
                {
                    nameof(ModelsLibrary.Models.Author) => ReadRow(row, AuthorMapper),
                    nameof(Category) => ReadRow(row, CategoryMapper),
                    nameof(Course) => ReadRow(row, CourseMapper),
                    nameof(Source) => ReadRow(row, SourceMapper),
                    nameof(Region) => ReadRow(row, RegionMapper),
                    nameof(Country) => ReadRow(row, CountryMapper),
                    nameof(Ingredient) => ReadRow(row, IngredientMapper),
                    _ => throw new Exception("Invalid Type")
                };
                entities.Add(entity);
            }

            if (entities.Count != 0)
            {
                Save(entities);
            }
        }
    }
    private static TEntity ReadRow<TEntity>(Row row, excelToObjectMapping<TEntity> mapper)
    {
        return mapper(row);
    }

    #region Mappers
    private ModelsLibrary.Models.Author AuthorMapper(Row row)
    {
        IEnumerable<Cell> cells = row.Elements<Cell>();
        return new ModelsLibrary.Models.Author
        {
            Name = _stringTable.SharedStringTable.ElementAt(int.Parse(cells.ElementAt(0).InnerText)).InnerText.Trim(),
        };
    }
    private Category CategoryMapper(Row row)
    {
        IEnumerable<Cell> cells = row.Elements<Cell>();
        string name = _stringTable.SharedStringTable.ElementAt(int.Parse(cells.ElementAt(0).InnerText)).InnerText.Trim();
        string base_name = cells.Count() >= 2 ? _stringTable.SharedStringTable.ElementAt(int.Parse(cells.ElementAt(1).InnerText)).InnerText.Trim() : null;
        return new Category
        {
            Name = name,
            BaseCategory = !string.IsNullOrWhiteSpace(base_name) ? new Category { Name = base_name } : null,
        };
    }
    private Course CourseMapper(Row row)
    {
        IEnumerable<Cell> cells = row.Elements<Cell>();
        return new Course
        {
            Name = _stringTable.SharedStringTable.ElementAt(int.Parse(cells.ElementAt(0).InnerText)).InnerText,
        };
    }

    private Source SourceMapper(Row row)
    {
        IEnumerable<Cell> cells = row.Elements<Cell>();
        return new Source
        {
            Name = _stringTable.SharedStringTable.ElementAt(int.Parse(cells.ElementAt(0).InnerText)).InnerText.Trim(),
        };
    }
    private Region RegionMapper(Row row)
    {
        IEnumerable<Cell> cells = row.Elements<Cell>();
        return new Region
        {
            Name = _stringTable.SharedStringTable.ElementAt(int.Parse(cells.ElementAt(0).InnerText)).InnerText.Trim(),
        };
    }
    private Country CountryMapper(Row row)
    {
        IEnumerable<Cell> cells = row.Elements<Cell>();
        string name = _stringTable.SharedStringTable.ElementAt(int.Parse(cells.ElementAt(0).InnerText)).InnerText.Trim();
        string region_name = _stringTable.SharedStringTable.ElementAt(int.Parse(cells.ElementAt(1).InnerText)).InnerText.Trim();
        return new Country
        {
            Name = name,
            Region = new Region
            {
                Name = region_name
            }
        };
    }
    private Ingredient IngredientMapper(Row row)
    {
        IEnumerable<Cell> cells = row.Elements<Cell>();
        string name = _stringTable.SharedStringTable.ElementAt(int.Parse(cells.ElementAt(0).InnerText)).InnerText.Trim();
        string country_name = _stringTable.SharedStringTable.ElementAt(int.Parse(cells.ElementAt(1).InnerText)).InnerText.Trim();
        return new Ingredient
        {
            Name = name,
            Country = new Country
            {
                Name = country_name
            }
        };
    }
    #endregion

    #region Savers
    private static async void Save(ArrayList entities)
    {
        IDataAccess dal = new DataAccess.DataAccess(Environment.GetEnvironmentVariable(AppConstants.RecipesAppEnv.ApiDomain).ToString());
        await dal.ApiAuthenticateAsync();
        await dal.BulkSave(entities);
    }

    #endregion
}
