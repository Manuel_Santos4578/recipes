﻿namespace DataContracts.Entities;

public interface ISource : ISimple
{
    string FriendlyUrl { get; set; }
    int ID { get; set; }
    string Name { get; set; }
}