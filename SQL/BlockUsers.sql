
use Recipes
GO
ALTER table Users
add blocked_at datetime
GO

ALTER table Users
add number_of_tries int not null default 0
GO

create Trigger UserLoginTrigger 
on Users
after UPDATE as 
BEGIN
	UPDATE Users SET
	number_of_tries = 0,
	blocked_at = GETDATE()
	Where number_of_tries >= 5
END
GO
create or alter proc UserLoginProc @username varchar(255), @password varchar(max) as 
BEGIN
IF EXISTS (SELECT id
    FROM Users
    Where username = @username AND password = @password and blocked_at is null)
BEGIN            
	UPDATE Users SET
	number_of_tries = 0
	Where username = @username AND password = @password;
	SELECT id, name, username, created_at, updated_at, blocked_at, prefered_culture 
	FROM Users
	Where username = @username AND password = @password and blocked_at is null
END
ELSE IF EXISTS (SELECT id
    FROM Users
    Where username = @username)
	BEGIN
		update Users SET 
		number_of_tries = number_of_tries +1
		Where username = @username and blocked_at is null
	END
END
GO
create or alter proc UnblockUsersProc @minutesSinceBlocked int = -30 as
BEGIN
	Update Users SET
	blocked_at = null
	Where blocked_at >= Dateadd(minute, @minutesSinceBlocked, blocked_at) and blocked_at is not null
END
GO