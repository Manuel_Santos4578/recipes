
use Recipes
GO

create table Roles(
    id int identity(1,1) primary key,
    name varchar(50) not null unique,
    created_at datetime not null default getdate(), 
    updated_at datetime not null default getdate(),
    deleted_at datetime
)
GO
create table UserRoles (
    user_id int not null foreign key references Users(id),
    role_id int not null foreign key references Roles(id),
    constraint PK_UserRoles primary key(user_id, role_id)
)
GO

create or alter proc UserRoleProc @username varchar(255) as
BEGIN
    Select Roles.id, Roles.name 
    from Roles 
    inner join UserRoles on Roles.id = UserRoles.role_id
    inner join Users on Users.id = UserRoles.user_id
    Where username = @username
END
